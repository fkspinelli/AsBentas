<?php

namespace AsBentas\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /**if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);**/

        $user = Auth::user();

        if (Auth::guard($guard)->check()) {

            if ($guard == 'admin') {
                return redirect()->route("{$guard}.dashboard.index");
            }

            VarDumper::dump($user);
            exit;
            if ($user->data_primeiro_acesso) {
                return redirect()->route("{$guard}.home");
            } else {
                return redirect()->route("conta-usuario.primeiro-acesso");
            }

        }
        return $next($request);
    }
}
