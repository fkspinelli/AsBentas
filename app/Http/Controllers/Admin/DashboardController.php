<?php

namespace AsBentas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.dashboard.index');
    }

}
