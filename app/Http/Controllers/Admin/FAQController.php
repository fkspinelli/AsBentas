<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\FAQRequest;
use AsBentas\Model\FAQ;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class FAQController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $faqs = FAQ::sortable();

        if ($search) {
            $faqs = $faqs->where('id', '=', "$search")
                ->orWhere('pergunta', 'like', '%' . $search . '%')
                ->orWhere('resposta', 'like', '%' . $search . '%')
            ;
        }

        $faqs = $faqs->paginate(15);

        return view('admin.faq.index', ['faqs' => $faqs]);

    }


    public function create()
    {
        return view('admin.faq.formulario', [ 'faq' => new FAQ()]);
    }

    public function store(FAQRequest $request)
    {

        $empresa = FAQ::create([
            'pergunta'  => $request->pergunta,
            'resposta'  => $request->resposta,
            'ordem'     => $request->ordem,
            'status'    => $request->status
        ]);


        flash('FAQ salvo com sucesso.')->success();

        return redirect()->route('admin.faq.index');

    }

    public function edit($id)
    {
        $faq = FAQ::find($id);


        return view('admin.faq.formulario', ['faq' => $faq]);
    }

    public function update(FAQRequest $request, $id)
    {

        $local = FAQ::updateOrCreate(['id' => $request->id], [
            'pergunta'  => $request->pergunta,
            'resposta'  => $request->resposta,
            'ordem'     => $request->ordem,
            'status'     => $request->status
        ]);

        flash('FAQ atualizado com sucesso.')->success();

        return redirect()->route('admin.faq.index');

    }


    public function destroy($id)
    {

        $faq = FAQ::find($id);
        $faq->delete();

        flash('FAQ removido com sucesso.')->success();

        return redirect()->route('admin.faq.index');

    }
}
