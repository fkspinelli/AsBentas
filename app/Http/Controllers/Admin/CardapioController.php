<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\CardapioRequest;
use AsBentas\Model\Cardapio;
use AsBentas\Repositories\AcompanhamentoRepository;
use AsBentas\Repositories\CardapioRepository;
use AsBentas\Repositories\PratoRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class CardapioController extends Controller
{
    public function __construct(CardapioRepository $cardapioRepository,
                                PratoRepository $pratoRepository,
                                AcompanhamentoRepository $acompanhamentoRepository)
    {
        $this->cardapioRepository = $cardapioRepository;
        $this->pratoRepository = $pratoRepository;
        $this->acompanhamentoRepository = $acompanhamentoRepository;
    }

    public function index(Request $request)
    {

        $mes = $request->get('mes', date('m'));
        $ano = $request->get('ano', date('Y'));

        $cardapios = $this->cardapioRepository->buscarPorMes($mes, $ano);

        $meses = [
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro',
        ];

        
        return view('admin.cardapio.index', [
            'cardapios' => $cardapios,
            'mes'   => $mes,
            'ano'   => $ano,
            'meses' => $meses,
        ]);

    }

    public function create()
    {

        return view('admin.cardapio.formulario', [
            'cardapio'  => new Cardapio(),
            'pratos'    => $this->pratoRepository->findAll(),
            'bebidas'   => $this->acompanhamentoRepository->buscarPorCategoria('Bebidas')
        ]);

    }

    public function store(CardapioRequest $request)
    {

        $cardapio = Cardapio::create([
            'data_cardapio' => Carbon::createFromFormat('d/m/Y' , $request->data_cardapio),
            'dia_da_semana' => $request->dia_da_semana,
            'status' => $request->status
        ]);

        $pratos = [];

        foreach ($request->prato_id as $key => $id) {
            $pratos[$id] = ['ordem' => $request->prato_ordem[$key]];
        }

        $cardapio->pratos()->attach($pratos);
        $cardapio->bebidas()->attach($request->bebida_id);

        flash('Cardápio cadastrado com sucesso.')->success();

        return redirect()->route('admin.cardapio.index');

    }

    public function edit($id)
    {

        return view('admin.cardapio.formulario', [
            'cardapio' => $this->cardapioRepository->find($id),
            'pratos'   => $this->pratoRepository->findAll(),
            'bebidas'   => $this->acompanhamentoRepository->buscarPorCategoria('Bebidas')
        ]);

    }

    public function update(CardapioRequest $request)
    {

        $cardapio = Cardapio::updateOrCreate(['id' => $request->id], [
            'data_cardapio' => Carbon::createFromFormat('d/m/Y', $request->data_cardapio),
            'status' => $request->status
        ]);

        $pratos = [];

        foreach ($request->prato_id as $key => $id) {
            $pratos[$id] = ['ordem' => $request->prato_ordem[$key]];
        }

        $cardapio->pratos()->sync($pratos);
        $cardapio->bebidas()->sync($request->bebida_id);

        flash('Cardápio salvo com sucesso.')->success();

        return redirect()->route('admin.cardapio.index');

    }

    public function destroy($id)
    {

        $cardapio = $this->cardapioRepository->find($id);
        $cardapio->delete();

        flash('Cardápio removido com sucesso.')->success();

        return redirect()->route('admin.cardapio.index');

    }

}
