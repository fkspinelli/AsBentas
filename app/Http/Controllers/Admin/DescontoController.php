<?php

namespace AsBentas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use AsBentas\Model\DescontoProgressivo;
use AsBentas\Http\Requests\Admin\DescontoRequest;

class DescontoController extends Controller
{
    public function index()
    {

        $descontos = DescontoProgressivo::orderBy('dia', 'asc')->paginate(10);

        return view('admin.desconto.index', ['descontos' => $descontos]);

    }

    public function create()
    {
        return view('admin.desconto.formulario', ['desconto' => new DescontoProgressivo()]);
    }

    public function edit($id)
    {
        $desconto = DescontoProgressivo::findOrFail($id);


        return view('admin.desconto.formulario', ['desconto' => $desconto]);
    }

    public function store(DescontoRequest $request)
    {
        $desconto = DescontoProgressivo::create($request->except( [ "_token" ] ));

        if ($desconto) {
            flash('Desconto salvo com sucesso.')->success();
            return redirect()->route('admin.desconto.index');
        }
        else{
            flash('Erro ao salvar Desconto.')->danger();
            return redirect()->route('admin.desconto.index');
        }

    }

    public function update(DescontoRequest $request)
    {

        $desconto = DescontoProgressivo::updateOrCreate(['id' => $request->id], $request->except( [ "_token" ] ));

        flash('Desconto atualizado com sucesso.')->success();

        return redirect()->route('admin.desconto.index');

    }


    public function destroy($id)
    {


        $desconto = DescontoProgressivo::findOrFail($id);
        $desconto->delete();

        flash('Desconto removido com sucesso.')->success();

        return redirect()->route('admin.desconto.index');

    }
}
