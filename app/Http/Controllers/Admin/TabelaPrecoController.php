<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\TabelaPrecoAcompanhamentoRequest;
use AsBentas\Http\Requests\Admin\TabelaPrecoPratoRequest;
use AsBentas\Model\Prato;
use AsBentas\Model\TabelaPrecoPratoEmpresa;
use AsBentas\Model\TabelaPrecoAcompanhamentoEmpresa;
use AsBentas\Repositories\AcompanhamentoRepository;
use AsBentas\Repositories\ComboRepository;
use AsBentas\Repositories\EmpresaRepository;
use AsBentas\Repositories\PratoRepository;
use AsBentas\Repositories\TabelaDePrecoComboRepository;
use AsBentas\Repositories\TabelaDePrecoPratoRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use Symfony\Component\VarDumper\VarDumper;

class TabelaPrecoController extends Controller
{

    public function __construct(PratoRepository $pratoRepository,
                                EmpresaRepository $empresaRepository,
                                AcompanhamentoRepository $acompanhamentoRepository,
                                ComboRepository $comboRepository,
                                TabelaDePrecoComboRepository $tabelaDePrecoComboRepository,
                                TabelaDePrecoPratoRepository $tabelaDePrecoPratoRepository)
    {
        $this->pratoRepository = $pratoRepository;
        $this->empresaRepository = $empresaRepository;
        $this->acompanhamentoRepository = $acompanhamentoRepository;
        $this->comboRepository = $comboRepository;
        $this->tabelaDePrecoComboRepository = $tabelaDePrecoComboRepository;
        $this->tabelaDePrecoPratoRepository = $tabelaDePrecoPratoRepository;

    }

    public function index($id)
    {
        return view('admin.prato.tabela-preco', [
            'prato' => $this->pratoRepository->find($id),
            'empresas' => $this->empresaRepository->findAtivas()
        ]);
    }

    public function store(TabelaPrecoPratoRequest $request, $id)
    {

        $valor = str_replace(',','.',$request->valor);


        /** @var Prato $prato */
        $prato = $this->pratoRepository->find($id);
        $prato->atualizarPreco($request->empresa_id, ['valor' => $valor]);

        flash('Preço salvo com sucesso.')->success();

        return redirect()->route('admin.prato.tabela-preco.salvar', ['id' => $id]);

    }

    public function destroyPrato($idEmpresa, $idPrato)
    {
        $pratoPreco = TabelaPrecoPratoEmpresa::where('empresa_id',  '=', $idEmpresa)
            ->where('prato_id', '=',$idPrato)
            ->first();


        if($pratoPreco){
            $pratoPreco->delete($idPrato);
            flash('Preço deletado com sucesso.')->success();
            return redirect()->route('admin.prato.tabela-preco', ['id' => $idPrato]);
        }



        flash('Nenhum preço especial encontrado.')->error();
        return redirect()->route('admin.prato.tabela-preco', ['id' => $pratoPreco]);

    }

    public function indexAcompanhamento($id)
    {
        return view('admin.acompanhamento.tabela-preco', [
            'acompanhamento' => $this->acompanhamentoRepository->find($id),
            'empresas' => $this->empresaRepository->findAtivas()
        ]);
    }

    public function storeAcompanhamento(TabelaPrecoAcompanhamentoRequest $request, $id)
    {
        $valor = str_replace(',','.',$request->valor);

        $acompanhamento = $this->acompanhamentoRepository->find($id);
        $acompanhamento->atualizarPreco($request->empresa_id, ['valor' => $valor]);

        flash('Preço salvo com sucesso.')->success();

        return redirect()->route('admin.acompanhamento.tabela-preco.salvar', ['id' => $id]);

    }

    public function destroyAcompanhamento($idEmpresa, $idAcompanhamento)
    {
        $acompanhamentoPreco = TabelaPrecoAcompanhamentoEmpresa::where('empresa_id',  '=', $idEmpresa)
            ->where('acompanhamento_id', '=',$idAcompanhamento)
            ->first();


        if($acompanhamentoPreco){
            $acompanhamentoPreco->delete($idAcompanhamento);
            flash('Acompahamento deletado com sucesso.')->success();
            return redirect()->route('admin.acompanhamento.tabela-preco', ['id' => $idAcompanhamento]);
        }



        flash('Nenhum preço especial encontrado.')->error();
        return redirect()->route('admin.acompanhamento.tabela-preco', ['id' => $idAcompanhamento]);

    }



    public function indexCombo($id)
    {
        return view('admin.combos.tabela-preco', [
            'combo' => $this->comboRepository->find($id),
            'empresas' => $this->empresaRepository->findAtivas()
        ]);
    }

    public function storeCombo(Request $request, $id)
    {
        $valor = str_replace(',','.',$request->valor);

        $combo = $this->comboRepository->find($id);

        $combo->atualizarPreco($request->empresa_id, ['valor' => $valor]);

        flash('Preço salvo com sucesso.')->success();

        return redirect()->route('admin.combo.tabela-preco.salvar', ['id' => $id]);

    }

    public function destroyCombo($idEmpresa, $idCombo)
    {
        $comboPreco = $this->tabelaDePrecoComboRepository
            ->findByCombo($idCombo)
            ->where('empresa_id', '=', "$idEmpresa");

        $comboPreco->delete();

        flash('Preço deletado com sucesso.')->success();

        return redirect()->route('admin.combo.tabela-preco.salvar', ['id' => $idCombo]);

    }


}
