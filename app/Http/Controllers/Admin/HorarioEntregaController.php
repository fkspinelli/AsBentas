<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\HorarioEntregaRequest;
use AsBentas\Model\HorarioEntrega;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class HorarioEntregaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $horarios = HorarioEntrega::all();

        return view('admin.horarios.index', ['horarios' => $horarios]);

    }


    public function create()
    {
        return view('admin.horarios.formulario', [ 'horario' => new HorarioEntrega()]);
    }

    public function store(HorarioEntregaRequest $request)
    {

        $horario = HorarioEntrega::create([
            'horario_inicial'  => $request->horario_inicial,
            'horario_final'  => $request->horario_final
        ]);


        flash('Horário Entrega salvo com sucesso.')->success();

        return redirect()->route('admin.horarios-entrega.index');

    }

    public function edit($id)
    {
        $horario = HorarioEntrega::find($id);


        return view('admin.horarios.formulario', ['horario' => $horario]);
    }

    public function update(HorarioEntregaRequest $request, $id)
    {

        $local = HorarioEntrega::updateOrCreate(['id' => $request->id], [
            'horario_inicial'  => $request->horario_inicial,
            'horario_final'  => $request->horario_final
        ]);

        flash('Horário Entrega atualizado com sucesso.')->success();

        return redirect()->route('admin.horarios-entrega.index');

    }


    public function destroy($id)
    {

        $faq = HorarioEntrega::find($id);
        $faq->delete();

        flash('Horario removido com sucesso.')->success();

        return redirect()->route('admin.horarios-entrega.index');

    }
}
