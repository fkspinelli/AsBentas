<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\CategoriaAcompanhamentoRequest;
use AsBentas\Model\CategoriaAcompanhamento;
use AsBentas\Repositories\CategoriaAcompanhamentoRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class CategoriaAcompanhamentoController extends Controller
{

    public function __construct(CategoriaAcompanhamentoRepository $categoriaAcompanhamentoRepository)
    {
        $this->repository = $categoriaAcompanhamentoRepository;
    }

    public function index()
    {

        $categorias = $this->repository->findAll()->sortBy('id');
        //CategoriaAcompanhamento::orderBy('id', 'desc')->paginate(10);

        return view('admin.categoria-acompanhamento.index', ['categorias' => $categorias]);

    }

    public function create()
    {
        return view('admin.categoria-acompanhamento.formulario', [
            'categoria' => new CategoriaAcompanhamento(),
        ]);
    }

    public function store(CategoriaAcompanhamentoRequest $request)
    {

        $categoria = CategoriaAcompanhamento::create([
            'nome' => $request->nome,
            'status' => $request->status,
        ]);

        flash('Categoria cadastrada com sucesso.')->success();


        return redirect()->route('admin.categoria-acompanhamento.index');

    }

    public function edit($id)
    {
        $categoria = $this->repository->find($id);

        return view('admin.categoria-acompanhamento.formulario', ['categoria' => $categoria]);
    }

    public function update(CategoriaAcompanhamentoRequest $request)
    {

        $user = CategoriaAcompanhamento::updateOrCreate(['id' => $request->id], $request->except( [ "_token" ] ));

        flash('Categoria atualizado com sucesso.')->success();

        return redirect()->route('admin.categoria-acompanhamento.index');

    }


    public function destroy($id)
    {

        $this->repository->remove($id);

        flash('Categoria removida com sucesso.')->success();

        return redirect()->route('admin.categoria-acompanhamento.index');

    }
}
