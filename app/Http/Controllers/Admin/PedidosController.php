<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Model\Cardapio;
use AsBentas\Model\ItemPedidoCardapio;
use AsBentas\Model\Pedido;
use AsBentas\Model\PedidoCardapio;
use AsBentas\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;
use Barryvdh\DomPDF\Facade as PDF;

class PedidosController extends Controller
{
    public $pedidoCardapio;

    public function gerarLista($search = null, $dataInicio = null, $dataTermino = null)
    {
        $dataInicio = str_replace("-", "/", $dataInicio);
        $dataInicio = ($dataInicio ? Carbon::createFromFormat('d/m/Y H:i:s', $dataInicio . " 00:00:00") : null);

        $dataTermino = str_replace("-", "/", $dataTermino);
        $dataTermino = ($dataTermino ? Carbon::createFromFormat('d/m/Y H:i:s', $dataTermino . " 23:59:59") : null);

        $pedidos = Pedido::select('pedidos.id', 'pedidos.created_at', 'pedidos.valor_total', 'pedidos.user_id',
            'pedidos.complemento_endereco', 'pedidos.fulfillment', 'pedidos.pagamento', 'pedidos.pagamento_id', 'pedidos.horario_entrega','pagamentos.tipo', 'endereco_entrega', 'endereco_entrega_complemento' ,'users.name')
            ->join('pedido_cardapios', 'pedido_cardapios.pedido_id', '=', 'pedidos.id')
            ->join('pagamentos', 'pagamentos.id', '=', 'pedidos.pagamento_id')
            ->join('cardapios', 'pedido_cardapios.cardapio_id', '=', 'cardapios.id')
            ->join('users', 'users.id', '=', 'pedidos.user_id')
            ->groupBy('pedidos.id', 'pedidos.created_at', 'pedidos.valor_total', 'pedidos.user_id',
                'pedidos.complemento_endereco', 'pedidos.fulfillment', 'pedidos.pagamento', 'pedidos.pagamento_id',
                'pagamentos.tipo', 'pedidos.horario_entrega', 'endereco_entrega', 'endereco_entrega_complemento',
                'users.name'
            );

//        if ($search) {
//            $pedidos = $pedidos->where('pedidos.id', '=', "$search")
//                ->orWhere('name', '$search', '%' . $search . '%');
//        }

        if ($dataInicio) {
            $pedidos = $pedidos->whereDate('cardapios.data_cardapio', '>=', $dataInicio);
        }

        if ($dataTermino) {
            $pedidos = $pedidos->whereDate('cardapios.data_cardapio', '<=', $dataTermino);
        }

        return $pedidos;
    }

    public function index(Request $request)
    {
        $dataInicio = $request->get('data-inicio', null);
        $dataInicio = str_replace("/", "-", $dataInicio);
        $dataTermino = $request->get('data-termino', null);
        $dataTermino = str_replace("/", "-", $dataTermino);
        $search = $request->get('search', null);

        $pedidos = $this->gerarLista($search, $dataInicio, $dataTermino);

        return view('admin.pedidos.index', [
            'pedidos'       => $pedidos->sortable()->paginate(15),
            'search'        => $search,
            'dataInicio'    => $dataInicio,
            'dataTermino'   => $dataTermino
        ]);


        /*
        $search = $request->get('search', null);

        $pedidos = Pedido::select('pedidos.id', 'pedidos.created_at', 'pedidos.valor_total', 'pedidos.user_id', 'pedidos.complemento_endereco', 'pedidos.fulfillment', 'pedidos.pagamento')
            ->join('pedido_cardapios', 'pedido_cardapios.pedido_id', '=', 'pedidos.id')
            ->join('cardapios', 'pedido_cardapios.cardapio_id', '=', 'cardapios.id')
            ->join('users', 'users.id', '=', 'pedidos.user_id')
            ->groupBy('pedidos.id', 'pedidos.created_at', 'pedidos.valor_total', 'pedidos.user_id', 'pedidos.complemento_endereco', 'pedidos.fulfillment', 'pedidos.pagamento')
        ;

//        if ($search) {
//            $pedidos = $pedidos->where('pedidos.id', '=', "$search")
//                ->orWhere('name', '$search', '%' . $search . '%');
//        }

        if ($request->has('data-inicio')) {
            $pedidos = $pedidos->whereDate('cardapios.data_cardapio', '>=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-inicio') . " 00:00:00"));
        }


        if ($request->has('data-termino')) {
            $pedidos = $pedidos->whereDate('cardapios.data_cardapio', '<=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-termino') . " 23:59:59"));
        }

        return view('admin.pedidos.index', [
            'pedidos' => $pedidos->sortable()->paginate(15),
            'data' => $pedidos->toSql()
        ]);
        */

    }

    public function show($id)
    {

        $pedido = Pedido::find($id);

        return view('admin.pedidos.show', ['pedido' => $pedido]);

    }

    public function mudarStatusItemPedido(Request $request, $id){

        $pedidoCardapio = PedidoCardapio::find($id);

        $pedidoCardapio->fulfillment = $request->fulfillment;
        $pedidoCardapio->pagamento = $request->pagamento;
        $pedidoCardapio->updated_at = new Carbon();

        $pedidoCardapio->save();


        return redirect()->intended(route('admin.pedido.visualizar', ['id' =>  $pedidoCardapio->pedido->id]));
    }

    public function mudarStatusPedido(Request $request, $id){


        $pedido = Pedido::find($id);

        $pedido->pagamento = $request['pagamento'];
        $pedido->fulfillment = $request['fulfillment'];
        $pedido->updated_at = new Carbon();

        $pedido->save();

        return redirect()->intended(route('admin.pedido.visualizar', ['id' =>  $pedido->id]));
    }

    public function printInvoice($id)
    {

        $pedido = Pedido::find($id);

        return view('admin.pedidos.print', ['pedido' => $pedido]);

    }

    /**
     * Imprimir pedido de 1 dia
     *
     * @param $id
     * @return mixed
     */
    public function printInvoicePedido($id)
    {
        $pedidoCardapio = PedidoCardapio::find($id);
        $pedido = $pedidoCardapio->pedido;
        $usuario = $pedido->usuario;
        $empresa = $usuario->empresaAtiva();

        $tamanho = 100;
        $tamanhoDia = 45;
        $tamanhoItem = 16;

        $qtdDias = 1;
        $qtdItens = $pedidoCardapio->getQtdItensImpressao();

        $tamanho += ($tamanhoDia * $qtdDias) + ($tamanhoItem * $qtdItens);


        $tamanhoPoint = $tamanho * 2.83465;

        return PDF::loadView('admin.pedidos.printCozinha', [
            'pedidoCardapio' => $pedidoCardapio,
            'pedido' => $pedido,
            'usuario' => $usuario,
            'empresa' => $empresa
        ])
            ->setPaper(array(0, 0, 280, $tamanhoPoint), 'portrait')
            ->stream();
    }

    public function printInvoicePedidoCompleto($id)
    {
        $pedido = Pedido::find($id);
        $usuario = User::find($pedido->user_id);
        $empresa = $usuario->empresaAtiva();

        $tamanho = 120;
        $tamanhoDia = 45;
        $tamanhoItem = 16;

        $qtdDias = count($pedido->pedidoCardapios);
        $qtdItens = 0;

        foreach($pedido->pedidoCardapios as $pedidoCardapio)
        {
            $qtdItens += $pedidoCardapio->getQtdItensImpressao();
        }

        $tamanho += ($tamanhoDia * $qtdDias) + ($tamanhoItem * $qtdItens);


        $tamanhoPoint = $tamanho * 2.83465;

        return PDF::loadView('admin.pedidos.print-cozinha-pedido-completo', [
            'pedido' => $pedido,
            'usuario' => $usuario,
            'empresa' => $empresa
        ])
            ->setPaper(array(0, 0, 280, $tamanhoPoint), 'portrait')
            ->stream();
    }

    public function download(Request $request)
    {
        $dataInicio = $request->get('dataInicio', null);
        $dataInicio = str_replace("/", "-", $dataInicio);
        $dataTermino = $request->get('dataTermino', null);
        $dataTermino = str_replace("/", "-", $dataTermino);
        $search = $request->get('search', null);

        $pedidos = $this->gerarLista($search, $dataInicio, $dataTermino);

        $dataInicio = str_replace("-", "/", $dataInicio);
        $dataTermino = str_replace("-", "/", $dataTermino);

        $dataInicio = ($dataInicio ? Carbon::createFromFormat('d/m/Y H:i:s', $dataInicio . " 00:00:00") : null);
        $dataTermino = ($dataTermino ? Carbon::createFromFormat('d/m/Y H:i:s', $dataTermino . " 23:59:59") : null);

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=Pedidos.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $dados[] = [
            'Número do Pedido',
            'Data','Nome do Cliente ',
            'Endereço',
            'Complemento Endereço',
            'Item',
            'Quantidade do Item',
            'Valor do Item',
            'Valor total do Dia (Com frete e Descontos)',
            'Forma de Pagamento',
            'Faixa de Horário'
        ];



        /** @var Pedido $pedido */
        foreach($pedidos->get() as $pedido) {
            /** @var PedidoCardapio $pedidoCardapio */
            foreach($pedido->pedidoCardapios as $pedidoCardapio) {
                /** @var ItemPedidoCardapio $item */

                $data = $pedidoCardapio->cardapio->data_cardapio;

                if($dataInicio != null && $data < $dataInicio)
                {
                    continue;
                }

                if($dataTermino != null && $data > $dataTermino)
                {
                    continue;
                }

                foreach($pedidoCardapio->itens as $item) {
                    $dados[$item->id] = [];
                    array_push($dados[$item->id], $pedido->id);
                    array_push($dados[$item->id], $data->format('d/m/Y'));
                    array_push($dados[$item->id], $pedido->usuario->name);
                    array_push($dados[$item->id], $pedido->endereco_entrega);
                    array_push($dados[$item->id], $pedido->complemento_endereco);
                    array_push($dados[$item->id], $item->descricao);
                    array_push($dados[$item->id], $item->quantidade);
                    array_push($dados[$item->id], $item->valor);
                    array_push($dados[$item->id], $pedidoCardapio->subtotal);
                    array_push($dados[$item->id], $pedido->tipo);
                    array_push($dados[$item->id], $pedido->horario_entrega);
                }
            }
        }

        $callback = function() use ($dados)
        {
            $FH = fopen('php://output', 'w');

            foreach ($dados as $row) {
                fputcsv($FH, $row);
            }

            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);

        /*
        dd($query);

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=Pedidos.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $pedidos = Pedido::query();

        if ($request->has('data-inicio')) {
            $pedidos = $pedidos->whereDate('created_at', '>=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-inicio') . " 00:00:00"));
        }

        if ($request->has('data-termino')) {
            $pedidos = $pedidos->whereDate('created_at', '<=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-termino') . " 23:59:59"));
        }

        if ($request->has('busca')) {
            $pedidos = $pedidos->where('id', '=', $request->get('busca'));
        }


        $pedidos = $pedidos
            ->orderBy('created_at', 'desc')
            ->get()
        ;

        $dados[] = [
            'Número do Pedido','Data','Nome do Cliente ','Endereço', 'Complemento Endereço','Item', 'Quantidade do Item',
            'Valor do Item', 'Valor total do Dia (Com frete)', 'Forma de Pagamento', 'Faixa de Horário'];

        /** @var Pedido $pedido */

        /*
        foreach($pedidos as $pedido) {

            /** @var PedidoCardapio $pedidoCardapio */
        /*
            foreach($pedido->pedidoCardapios as $pedidoCardapio) {
                /** @var ItemPedidoCardapio $item */
        /*
                foreach($pedidoCardapio->itens as $item) {

                    $dados[$item->id] = [];
                    array_push($dados[$item->id], $pedido->id);
                    array_push($dados[$item->id], $pedidoCardapio->cardapio->data_cardapio->format('d/m/Y'));
                    array_push($dados[$item->id], $pedido->usuario->name);
                    array_push($dados[$item->id], $pedido->endereco->resumo());
                    array_push($dados[$item->id], $pedido->complemento_endereco);
                    array_push($dados[$item->id], $item->descricao);
                    array_push($dados[$item->id], $item->quantidade);
                    array_push($dados[$item->id], $item->valor);
                    array_push($dados[$item->id], $pedidoCardapio->subtotal);
                    array_push($dados[$item->id], $pedido->formaPagamento->tipo);
                    $faixaHorario = $pedido->horario->horario_inicial. " - " .$pedido->horario->horario_final;
                    array_push($dados[$item->id], $faixaHorario);
                }

            }

        }

        $callback = function() use ($dados)
        {
            $FH = fopen('php://output', 'w');
            foreach ($dados as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);
        */

    }

    public function roteiroEntrega(Request $request)
    {
        $dataInicio = $request->get('dataInicio', null);
        $dataInicio = str_replace("/", "-", $dataInicio);
        $dataTermino = $request->get('dataTermino', null);
        $dataTermino = str_replace("/", "-", $dataTermino);
        $search = $request->get('search', null);

        $pedidos = $this->gerarLista($search, $dataInicio, $dataTermino);

        $dataInicio = str_replace("-", "/", $dataInicio);
        $dataTermino = str_replace("-", "/", $dataTermino);

        $dataInicio = ($dataInicio ? Carbon::createFromFormat('d/m/Y H:i:s', $dataInicio . " 00:00:00") : null);
        $dataTermino = ($dataTermino ? Carbon::createFromFormat('d/m/Y H:i:s', $dataTermino . " 23:59:59") : null);

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=Pedidos.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $dados[] = [
            'Código do Pedido',
            'Dia Entrega do Pedido',
            'Código do Cliente',
            'Nome do Cliente ',
            'Endereço',
            'Complemento Endereço',
        ];



        /** @var Pedido $pedido */
        foreach($pedidos->get() as $pedido) {
            /** @var PedidoCardapio $pedidoCardapio */
            foreach($pedido->pedidoCardapios as $pedidoCardapio) {
                /** @var ItemPedidoCardapio $item */

                $data = $pedidoCardapio->cardapio->data_cardapio;

                if($dataInicio != null && $data < $dataInicio)
                {
                    continue;
                }

                if($dataTermino != null && $data > $dataTermino)
                {
                    continue;
                }

                $dados[$pedidoCardapio->id] = [];
                array_push($dados[$pedidoCardapio->id], $pedido->id);
                array_push($dados[$pedidoCardapio->id], $data->format('d/m/Y'));
                array_push($dados[$pedidoCardapio->id], $pedido->user_id);
                array_push($dados[$pedidoCardapio->id], $pedido->usuario->name);
                array_push($dados[$pedidoCardapio->id], $pedido->endereco_entrega);
                array_push($dados[$pedidoCardapio->id], $pedido->endereco_entrega_complemento);

            }
        }

        $callback = function() use ($dados)
        {
            $FH = fopen('php://output', 'w');

            foreach ($dados as $row) {
                fputcsv($FH, $row);
            }

            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);

        /*
        dd($query);

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=Pedidos.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $pedidos = Pedido::query();

        if ($request->has('data-inicio')) {
            $pedidos = $pedidos->whereDate('created_at', '>=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-inicio') . " 00:00:00"));
        }

        if ($request->has('data-termino')) {
            $pedidos = $pedidos->whereDate('created_at', '<=', Carbon::createFromFormat('d/m/Y H:i:s', $request->get('data-termino') . " 23:59:59"));
        }

        if ($request->has('busca')) {
            $pedidos = $pedidos->where('id', '=', $request->get('busca'));
        }


        $pedidos = $pedidos
            ->orderBy('created_at', 'desc')
            ->get()
        ;

        $dados[] = [
            'Número do Pedido','Data','Nome do Cliente ','Endereço', 'Complemento Endereço','Item', 'Quantidade do Item',
            'Valor do Item', 'Valor total do Dia (Com frete)', 'Forma de Pagamento', 'Faixa de Horário'];

        /** @var Pedido $pedido */

        /*
        foreach($pedidos as $pedido) {

            /** @var PedidoCardapio $pedidoCardapio */
        /*
            foreach($pedido->pedidoCardapios as $pedidoCardapio) {
                /** @var ItemPedidoCardapio $item */
        /*
                foreach($pedidoCardapio->itens as $item) {

                    $dados[$item->id] = [];
                    array_push($dados[$item->id], $pedido->id);
                    array_push($dados[$item->id], $pedidoCardapio->cardapio->data_cardapio->format('d/m/Y'));
                    array_push($dados[$item->id], $pedido->usuario->name);
                    array_push($dados[$item->id], $pedido->endereco->resumo());
                    array_push($dados[$item->id], $pedido->complemento_endereco);
                    array_push($dados[$item->id], $item->descricao);
                    array_push($dados[$item->id], $item->quantidade);
                    array_push($dados[$item->id], $item->valor);
                    array_push($dados[$item->id], $pedidoCardapio->subtotal);
                    array_push($dados[$item->id], $pedido->formaPagamento->tipo);
                    $faixaHorario = $pedido->horario->horario_inicial. " - " .$pedido->horario->horario_final;
                    array_push($dados[$item->id], $faixaHorario);
                }

            }

        }

        $callback = function() use ($dados)
        {
            $FH = fopen('php://output', 'w');
            foreach ($dados as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);
        */

    }


}
