<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\AcompanhamentoRequest;
use AsBentas\Model\Acompanhamento;
use AsBentas\Repositories\AcompanhamentoRepository;
use AsBentas\Repositories\CategoriaAcompanhamentoRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class AcompanhamentoController extends Controller
{

    public function __construct(AcompanhamentoRepository $repository, CategoriaAcompanhamentoRepository $categoriaAcompanhamentoRepository)
    {
        $this->repository = $repository;
        $this->categoriaRepository = $categoriaAcompanhamentoRepository;
    }

    public function index(Request $request)
    {

        $search = $request->get('search', null);

        $acompanhamentos = Acompanhamento::sortable();

        if ($search) {
            $acompanhamentos = $acompanhamentos->where('id', '=', "$search")
                ->orWhere('nome', 'like', '%' . $search . '%')
                ->orWhere('categoria_id', 'like', '%' . $search . '%')
            ;
        }

        $acompanhamentos = $acompanhamentos->paginate(15);

        return view('admin.acompanhamento.index', ['acompanhamentos' => $acompanhamentos]);

    }

    public function create()
    {

        return view('admin.acompanhamento.formulario', [
            'acompanhamento' => new Acompanhamento(),
            'categorias' => $this->categoriaRepository->findAtivos(),
        ]);
    }

    public function store(AcompanhamentoRequest $request)
    {

        $valor = str_replace(',', '.', $request->valor_sugerido);
        $request->merge(['valor_sugerido' => $valor]);

        $acompanhamento = Acompanhamento::create($request->except(["_token"]));

        flash('Acompanhamento salvo com sucesso.')->success();

        return redirect()->route('admin.acompanhamento.index');

    }

    public function edit($id)
    {
        return view('admin.acompanhamento.formulario', [
            'acompanhamento' => $this->repository->find($id),
            'categorias' => $this->categoriaRepository->findAtivos(),
        ]);
    }

    public function update(AcompanhamentoRequest $request)
    {
        $valor = str_replace(',', '.', $request->valor_sugerido);
        $request->merge(['valor_sugerido' => $valor]);

        $acompanhamento = Acompanhamento::updateOrCreate(['id' => $request->id], $request->except(["_token"]));
        flash('Acompanhamento atualizado com sucesso.')->success();
        return redirect()->route('admin.acompanhamento.index');

    }


    public function destroy($id)
    {

        $this->repository->remove($id);
        flash('Cliente removido com sucesso.')->success();

        return redirect()->route('admin.acompanhamento.index');

    }
}
