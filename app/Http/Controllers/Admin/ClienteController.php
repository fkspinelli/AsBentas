<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\EditarClienteRequest;
use AsBentas\Http\Requests\Admin\NovoClienteRequest;
use AsBentas\Model\Empresa;
use AsBentas\Model\User;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class ClienteController extends Controller
{

    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $clientes = User::sortable();

        if ($search) {
            $clientes = $clientes->where('id', '=', "$search")
                ->orWhere('name', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
            ;
        }

        $clientes = $clientes->paginate(15);

        return view('admin.cliente.index', ['usuarios' => $clientes]);

    }

    public function searchByName(Request $request){
        $search = $request['nome'];

    }

    public function edit($id)
    {
        $usuario = User::findOrFail($id);


        return view('admin.cliente.formulario', ['usuario' => $usuario]);
    }

    public function update(EditarClienteRequest $request)
    {

        $user = User::updateOrCreate(['id' => $request->id], $request->except( [ "_token" ] ));

        flash('Cliente atualizado com sucesso.')->success();

        return redirect()->route('admin.clientes.index');

    }

    public function destroy($id)
    {
        $cliente = User::findOrFail($id);
        $cliente->delete();

        flash('Cliente removido com sucesso.')->success();

        return redirect()->route('admin.clientes.index');

    }

    public function download()
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=clientes.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = User::all()->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);
    }

}
