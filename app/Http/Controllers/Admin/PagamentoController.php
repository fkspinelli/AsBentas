<?php

namespace AsBentas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use AsBentas\Model\Pagamento;
use AsBentas\Http\Controllers\Controller;

class PagamentoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagamentos = Pagamento::orderBy('id', 'asc')->paginate(15);
        return view('admin.pagamento.index', ['pagamentos' => $pagamentos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pagamento.formulario', ['pagamento' => new Pagamento()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pagamento = Pagamento::create([
            'tipo' => $request->tipo,
            'status' => $request->status,
        ]);

        if ($pagamento) {
            flash('Forma de pagamento salva com sucesso.')->success();
            return redirect()->route('admin.pagamento.index');
        }
        else{
            flash('Erro ao salvar forma de pagamento.')->danger();
            return redirect()->route('admin.pagamento.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pagamento = Pagamento::find($id);
        return view('admin.pagamento.formulario', ['pagamento' => $pagamento]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pagamento = Pagamento::updateOrCreate(['id' => $request->id],
        [
            'tipo' => $request->tipo,
            'status' => $request->status,
        ]);

        if ($pagamento) {
            flash('Forma de pagamento alterada com sucesso.')->success();
            return redirect()->route('admin.pagamento.index');
        }
        else{
            flash('Erro ao alterar forma de pagamento.')->danger();
            return redirect()->route('admin.pagamento.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pagamento = Pagamento::find($id);
        $pagamento->delete();

        flash('Pagamento removido com sucesso.')->success();

        return redirect()->route('admin.pagamento.index');
    }
}
