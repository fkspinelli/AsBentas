<?php

namespace AsBentas\Http\Controllers\Admin;

use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use AsBentas\Model\ConsultaCep;

class BuscaCEPController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $buscaCep = ConsultaCep::sortable();

        if ($search) {
            $pratos = $buscaCep->where('id', '=', "$search")
                ->orWhere('cep', '=', "$search")
                ->orWhere('email', 'like', '%' . $search . '%')
            ;
        }

        $buscaCep = $buscaCep->paginate(15);

        return view('admin.buscascep.index', ['buscas' => $buscaCep]);
    }

    public function download()
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=buscas.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = ConsultaCep::all()->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);
    }
}
