<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Model\Admin;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use Symfony\Component\VarDumper\VarDumper;

class UsuarioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $usuarios = Admin::sortable();

        if ($search) {
            $usuarios = $usuarios->where('name', 'like', '%'. "$search" . '%')
                ->orWhere('email', 'like', '%'. "$search" . '%')
                ->orWhere('id', '=', "$search")
            ;
        }

        $usuarios = $usuarios->paginate(15);

        return view('admin.usuario.index', ['usuarios' => $usuarios]);

    }

    public function create()
    {
        return view('admin.usuario.formulario', [ 'usuario' => new Admin()]);
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'nome' => 'required',
            'email' => 'required',
        ]);

        $admin = new Admin();
        $admin->name = $request->nome;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->senha);

        $admin->save();

        flash('Usuário salvo com sucesso.')->success();

        return redirect()->route('admin.usuarios.index');

    }

    public function edit($id)
    {
        $usuario = Admin::findOrFail($id);


        return view('admin.usuario.formulario', ['usuario' => $usuario]);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'nome' => 'required',
            'email' => 'required',
        ]);

        $admin = Admin::findOrFail($id);
        $admin->name = $request->nome;
        $admin->email = $request->email;

        if ($request->senha) {
            $admin->password = bcrypt($request->senha);
        }


        $admin->save();

        flash('Usuário atualizado com sucesso.')->success();

        return redirect()->route('admin.usuarios.index');

    }


    public function destroy($id)
    {


        $admin = Admin::findOrFail($id);
        $admin->delete();

        flash('Usuário removido com sucesso.')->success();

        return redirect()->route('admin.usuarios.index');

    }

}
