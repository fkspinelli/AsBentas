<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\CupomRequest;
use AsBentas\Model\Cupom;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use Carbon\Carbon;

class CupomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {

        $search = $request->get('search', null);

        $cupons = Cupom::sortable();

        if ($search) {
            $cupons = $cupons->where('id', '=', "$search")
                ->orWhere('codigo', '=', "$search" )
            ;
        }


        $cupons = $cupons->paginate(10);

        return view('admin.cupom.index', ['cupons' => $cupons]);

    }


    public function create()
    {
        return view('admin.cupom.formulario', ['cupom' => new Cupom()]);
    }

    public function store(CupomRequest $request)
    {
        $valor = str_replace(',','.',$request->valor);

        $cupom = Cupom::create([
            'codigo' => $request->codigo,
            'tipo' => $request->tipo,
            'valor' => $valor,
            'data_expiracao' => $request->data_expiracao ? Carbon::createFromFormat('d/m/Y', $request->data_expiracao) : null,
            'status' => $request->status,
            'tipo_cupom' => $request->tipo_cupom
        ]);

        if ($cupom) {
            flash('Cupom salvo com sucesso.')->success();
            return redirect()->route('admin.cupom.index');
        }
        else{
            flash('Erro ao salvar cupom.')->danger();
            return redirect()->route('admin.cupom.index');
        }

    }

    public function edit($id)
    {
        $cupom = Cupom::find($id);
        return view('admin.cupom.formulario', ['cupom' => $cupom]);
    }

    public function update(CupomRequest $request, $id)
    {
        $data = $request->data_expiracao;

        $valor = str_replace(',','.',$request->valor);

        $local = Cupom::updateOrCreate(['id' => $request->id], [
            'codigo' => $request->codigo,
            'tipo' => $request->tipo,
            'valor' => $valor,
            'data_expiracao' => $data ? Carbon::createFromFormat('d/m/Y', $data) : null,
            'status' => $request->status,
            'tipo_cupom' => $request->tipo_cupom
        ]);

        flash('Cupom atualizado com sucesso.')->success();

        return redirect()->route('admin.cupom.index');

    }


    public function destroy($id)
    {
        $cupom = Cupom::find($id);
        $cupom->delete();

        flash('Cupom removido com sucesso.')->success();

        return redirect()->route('admin.cupom.index');

    }

    public function utilizacao($id) 
    {
        $cupom = Cupom::findOrFail($id);

        return view('admin.cupom.utilizacao', ['cupom' => $cupom]);
        

    }

}
