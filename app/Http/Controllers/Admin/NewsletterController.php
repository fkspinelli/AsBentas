<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Model\Newsletter;

use AsBentas\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NewsletterController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $newsletter = Newsletter::sortable();

        if ($search) {
            $newsletter = $newsletter->where('id', '=', "$search")
                ->orWhere('nome', 'like', '%' . $search . '%')
                ->orWhere('email', 'like', '%' . $search . '%')
            ;
        }

        $newsletter = $newsletter->paginate(15);

        return view('admin.newsletter.index', ['newsletters' => $newsletter]);
    }

    public function download()
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=newsletter.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $list = Newsletter::all()->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Illuminate\Support\Facades\Response::stream($callback, 200, $headers);
    }
}
