<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\EmpresaRequest;
use AsBentas\Http\Requests\EnderecoEmpresaRequest;
use AsBentas\Model\Empresa;
use AsBentas\Model\Endereco;
use AsBentas\Model\LocalEntrega;
use AsBentas\Repositories\EmpresaRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;
use Symfony\Component\VarDumper\VarDumper;

class EmpresaController extends Controller
{

    private $empresaRepository;

    public function __construct(EmpresaRepository $empresaRepository)
    {
        $this->empresaRepository = $empresaRepository;

        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $empresas = Empresa::sortable();

        if ($search) {
            $empresas = $empresas->where('id', '=', "$search")
                ->orWhere('nome', 'like', '%' . $search . '%')
                ->orWhere('cnpj', '=', "$search" )
                ->orWhere('codigo_empresa', '=', "$search")
            ;
        }

        $empresas = $empresas->paginate(15);

        return view('admin.empresas.index', [
            'empresas' => $empresas
        ]);

    }

    public function preCadastro(Request $request)
    {
        $search = $request->get('search', null);

        $empresas = Empresa::sortable();

        $empresas = $empresas->where('status', '=', 'pre');

        if ($search) {
            $empresas = $empresas->where(function ($query) use ($search) {
                $query->where('id', '=', "$search")
                    ->orWhere('nome', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhere('contato', 'like', '%' . $search . '%')
                    ->orWhere('cep', '=', "$search" )
                    ->orWhere('codigo_empresa', '=', "$search")
                ;
            });
        }

        $empresas = $empresas->paginate(15);



        return view('admin.empresas.pre-cadastro', ['empresas' => $empresas]);//

    }

    public function create()
    {
        return view('admin.empresas.formulario', [ 'empresa' => new Empresa()]);
    }

    public function store(EmpresaRequest $request)
    {
        $empresa = Empresa::create($request->all());

        flash('Empresa salva com sucesso.')->success();

        return redirect()->route('admin.empresas.index');

    }

    public function edit($id)
    {

        $empresa = Empresa::findOrFail($id);

        $precosAcompanhamentos = $this->empresaRepository->buscarPrecosAcompanhamento($id);
        $precosPratos = $this->empresaRepository->buscarPrecosPrato($id);

        return view('admin.empresas.formulario', [
            'empresa' => $empresa,
            'precosAcompanhamentos' => $precosAcompanhamentos,
            'precosPratos' => $precosPratos
        ]);
    }

    public function update(EmpresaRequest $request, $id)
    {

        $request->offsetSet("frete_gratis", $request->get('frete_gratis', 0));
        $request->offsetSet("frete_gratis_funcionario", $request->get('frete_gratis_funcionario', 0));

        $empresa = Empresa::updateOrCreate(['id' => $request->id], $request->except( [ "_token" ] ));

        flash('Empresa atualizada com sucesso.')->success();

        return redirect()->route('admin.empresas.index');

    }

    public function ativar($id)
    {
        $empresa = $this->empresaRepository->find($id);
        $empresa->status = 'ativo';
        $empresa->save();


        flash('Empresa Ativada com sucesso.')->success();

        return redirect()->back();
    }

    public function destroy($id)
    {

        $usuarios = \AsBentas\Model\User::whereHas('empresas', function ($query) use ($id) {
            $query->where('empresas.id', $id);
        })->get();

        foreach($usuarios as $usuario) {
            foreach($usuario->empresas as $empresa) {
                if ($empresa->id == $id && ! $empresa->pivot->dt_termino) {
                    $empresa->pivot->dt_termino = new \Carbon\Carbon();
                    $empresa->pivot->save();
                }
            }
        }

        $empresa = Empresa::findOrFail($id);

        if ($usuarios->count() > 0) {
            $empresa->status = Empresa::STATUS_INATIVO;
            $empresa->save();
            
            flash('Existem usuários vinculadas a esta empresa. Por esse motivo a empresa foi desativada.')->success();
        } else {
            $empresa->delete();
            flash('Empresa removida com sucesso.')->success();
        }
        

        return redirect()->back();

    }

    # Endereco

    public function storeEndereco(EnderecoEmpresaRequest $request)
    {
        $request['nome'] = $request['nomeEndereco'];

        $cep = preg_replace("/[^0-9]/", "", $request->get('cep'));

        $local = LocalEntrega::whereRaw('? between cep_inicial and cep_final and status = ?', [$cep, 'ativo'])->first();

        if (! $local || $local->status == 'inativo') {
            flash('Cep recusado! De acordo com nossos locais de entrega disponíveis, esta empresa esta fora da nossa área de atauação.')->error();
            return redirect()->route('admin.empresas.editar', ['id' => $request->get('empresa_id')]);
        }

        $endereco = Endereco::create($request->except( [ "_token" ] ));

        flash('Endereço salvo com sucesso.')->success();
        return redirect()->route('admin.empresas.editar', ['id' => $request->get('empresa_id')]);

    }

    public function updateEndereco(EnderecoEmpresaRequest $request)
    {
        $cep = preg_replace("/[^0-9]/", "", $request->get('cep'));

        $local = LocalEntrega::whereRaw('? between cep_inicial and cep_final and status = ?', [$cep, 'ativo'])->first();

        if (! $local || $local->status == 'inativo') {
            flash('Cep recusado! De acordo com nossos locais de entrega disponíveis, esta empresa esta fora da nossa área de atauação.')->error();
            return redirect()->route('admin.empresas.editar', ['id' => $request->get('empresa_id')]);
        }

        $endereco = Endereco::updateOrCreate(['id' => $request->id], $request->except( [ "_token", "id" ] ));

        flash('Endereço atualizada com sucesso.')->success();

        return redirect()->route('admin.empresas.editar', ['id' => $request->get('empresa_id')]);

    }

    public function destroyEndereco($id)
    {
        $endereco = Endereco::findOrFail($id);
        $empresaId = $endereco->empresa_id;
        Endereco::destroy($id);
        flash('Endereço removido com sucesso.')->success();

        return redirect()->route('admin.empresas.editar', ['id' => $empresaId]);

    }

}
