<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\LocalEntregaRequest;
use AsBentas\Model\LocalEntrega;
use AsBentas\Repositories\LocalEntregasRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Controllers\Controller;

class LocalEntregaController extends Controller
{
    private $localEntregasRepository;

    public function __construct(LocalEntregasRepository $localEntregasRepository)
    {
        $this->localEntregasRepository = $localEntregasRepository;

        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {

        $search = $request->get('search', null);

        $locais = LocalEntrega::sortable();

        if ($search) {
            $locais = $locais->where('cep_inicial', '=', "$search")
                ->orWhere('id', '=', "$search")
                ->orWhere('cep_final', '=', "$search")
                ->orWhere('bairro', 'like', '%' . $search . '%')
                ->orWhere('valor_entrega', '=', "$search")
            ;
        }

        $locais = $locais->paginate(15);

        return view('admin.local-entrega.index', ['locais' => $locais]);

    }


    public function create()
    {
        return view('admin.local-entrega.formulario', [ 'local' => new LocalEntrega()]);
    }

    public function store(LocalEntregaRequest $request)
    {
        $valor = str_replace(',', '.', $request->valor_entrega);
        $request->merge(['valor_entrega' => $valor]);

        $empresa = LocalEntrega::create([
            'cep_inicial' => preg_replace("/[^0-9]/", "",$request->cep_inicial),
            'cep_final'   => preg_replace("/[^0-9]/", "",$request->cep_final),
            'bairro' => $request->bairro,
            'valor_entrega' => $request->valor_entrega,
            'status' => $request->status,
        ]);


        flash('Local de Entrega salvo com sucesso.')->success();

        return redirect()->route('admin.local-entrega.index');

    }

    public function edit($id)
    {
        $local = $this->localEntregasRepository->find($id);

        return view('admin.local-entrega.formulario', ['local' => $local]);
    }

    public function update(LocalEntregaRequest $request, $id)
    {
        $valor = str_replace(',', '.', $request->valor_entrega);
        $request->merge(['valor_entrega' => $valor]);

        $local = LocalEntrega::updateOrCreate(['id' => $request->id], [
            'cep_inicial' => preg_replace("/[^0-9]/", "",$request->cep_inicial),
            'cep_final'   => preg_replace("/[^0-9]/", "",$request->cep_final),
            'bairro' => $request->bairro,
            'valor_entrega' => $request->valor_entrega,
            'status' => $request->status,
        ]);

        flash('Local de Entrega atualizado com sucesso.')->success();

        return redirect()->route('admin.local-entrega.index');

    }


    public function destroy($id)
    {

        $local = $this->localEntregasRepository->find($id);
        $local->delete();

        flash('Local de Entrega removido com sucesso.')->success();

        return redirect()->route('admin.local-entrega.index');

    }


}
