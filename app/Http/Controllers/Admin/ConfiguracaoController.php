<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Controllers\Controller;
use AsBentas\Model\Configuracao;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\VarDumper;

class ConfiguracaoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.configuracao.index', [
            'configuracoes' => Configuracao::all()
        ]);
    }

    public function findAll()
    {
        return Configuracao::all();
    }

    public function store(Request $request)
    {


        $campos = $request->get('valor');

        foreach ($campos as $id => $valor) {

            $configuracao = Configuracao::find($id);
            $configuracao->valor = $valor;
            $configuracao->save();

        }
        flash('Configurações atualizadas com sucesso.')->success();

        return redirect()->intended(route('admin.configuracao.index'));
    }
}
