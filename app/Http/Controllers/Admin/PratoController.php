<?php

namespace AsBentas\Http\Controllers\Admin;

use AsBentas\Http\Requests\Admin\PratoRequest;
use AsBentas\Model\Prato;
use AsBentas\Repositories\CaracteristicaPratoRepository;
use AsBentas\Repositories\IconePratoRepository;
use AsBentas\Repositories\PratoRepository;
use AsBentas\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PratoController extends Controller
{
    public function __construct(PratoRepository $pratoRepository,
                                CaracteristicaPratoRepository $caracteristicaPratoRepository,
                                IconePratoRepository $iconePratoRepository)
    {
        $this->repository = $pratoRepository;
        $this->caracteristicaPratoRepository = $caracteristicaPratoRepository;
        $this->iconePratoRepository = $iconePratoRepository;
    }

    public function index(Request $request)
    {
        $search = $request->get('search', null);

        $pratos = Prato::sortable();

        if ($search) {
            $pratos = $pratos->where('codigo', '=', "$search")
                ->orWhere('id', '=', "$search")
                ->orWhere('nome', 'like', '%' . $search . '%')
                ->orWhere('descricao', 'like', '%' . $search . '%')
            ;
        }

        $pratos = $pratos->paginate(15);

        return view('admin.prato.index', ['pratos' => $pratos]);

    }

    public function create()
    {

        return view('admin.prato.formulario', [
            'prato' => new Prato(),
            'caracteristicas' => $this->caracteristicaPratoRepository->findAtivos(),
            'icones' =>  $this->iconePratoRepository->findAll(),
        ]);
    }

    public function store(PratoRequest $request)
    {

        $valor = str_replace(',','.', $request->valor_sugerido);

        $request->merge(['valor_sugerido' => $valor]);

        $prato = Prato::create($request->except( [ "_token" ] ))
            ->caracteristicas()
            ->attach($request->input('caracteristica_id'));;

        flash('Prato salvo com sucesso.')->success();

        return redirect()->route('admin.prato.index');

    }

    public function edit($id)
    {
        return view('admin.prato.formulario', [
            'prato' => $this->repository->find($id),
            'caracteristicas' => $this->caracteristicaPratoRepository->findAtivos(),
            'icones' =>  $this->iconePratoRepository->findAll(),
        ]);
    }

    public function update(PratoRequest $request)
    {
        $valor = str_replace(',','.', $request->valor_sugerido);

        $request->merge(['valor_sugerido' => $valor]);


        $prato= Prato::updateOrCreate(['id' => $request->id], $request->except( [ "_token" ] ))
            ->caracteristicas()
            ->sync($request->input('caracteristica_id'));

        flash('Prato atualizado com sucesso.')->success();

        return redirect()->route('admin.prato.index');

    }


    public function destroy($id)
    {

        $prato = Prato::find($id);

        if (count($prato->cardapios) > 0) {
            flash('Não é possível remover este prato, pois está sendo usado.')->warning();
        } else {
            $this->repository->remove($id);
            flash('Prato removido com sucesso.')->success();
        }

        return redirect()->route('admin.prato.index');

    }
}
