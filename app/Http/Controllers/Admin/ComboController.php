<?php

namespace AsBentas\Http\Controllers\Admin;


use AsBentas\Repositories\CategoriaAcompanhamentoRepository;
use AsBentas\Repositories\ComboCategoriaAcompanhamentoRepository;
use AsBentas\Repositories\ComboRepository;
use AsBentas\Repositories\TabelaDePrecoComboRepository;
use Illuminate\Http\Request;
use AsBentas\Http\Requests\Admin\CadastrarCombo;
use AsBentas\Model\Combo;
use AsBentas\Http\Controllers\Controller;
use Symfony\Component\VarDumper\VarDumper;

class ComboController extends Controller
{
    public function __construct(CategoriaAcompanhamentoRepository $categoriaAcompanhamentoRepository,
                                ComboRepository $comboRepository,
                                ComboCategoriaAcompanhamentoRepository $comboCategoriaAcompanhamentoRepository,
                                TabelaDePrecoComboRepository $tabelaDePrecoComboRepository

                                )
    {
        $this->categoriaAcompanhamentoRepository = $categoriaAcompanhamentoRepository;
        $this->comboRepository = $comboRepository;
        $this->comboCategoriaAcopanhamentoRepository = $comboCategoriaAcompanhamentoRepository;
        $this->tabelaDePrecoComboRepository = $tabelaDePrecoComboRepository;


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $combos = Combo::orderBy('nome', 'asc')->paginate(10);

        return view('admin.combos.index', ['combos' => $combos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.combos.formulario', [
            'combo' => new Combo(),
            'categorias' => $this->categoriaAcompanhamentoRepository->findAll(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CadastrarCombo $request)
    {
        $valor = str_replace(',','.',$request->valor);

        $combo = Combo::create([
            'nome' => $request->nome,
            'valor' => $valor
        ]);

        $combo->categorias()->attach($request->categoria_id);

        flash('Combo cadastrado com sucesso.')->success();

        return redirect()->route('admin.combo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.combos.formulario', [
            'combo' => $this->comboRepository->find($id),
            'categorias'   => $this->categoriaAcompanhamentoRepository->findAll(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valor = str_replace(',','.',$request->valor);

        $combo = Combo::updateOrCreate(['id' => $request->id], [
            'nome' => $request['nome'],
            'valor' =>$valor
        ]);

        $combo->categorias()->sync($request->categoria_id);

        flash('Combo salvo com sucesso.')->success();

        return redirect()->route('admin.combo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $combo = $this->comboRepository->find($id);

        $combo->delete();
        flash('Combo removido com sucesso.')->success();


        return redirect()->route('admin.combo.index');

    }
}
