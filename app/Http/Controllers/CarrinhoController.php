<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Http\Requests\CarrinhoEntregaRequest;
use AsBentas\Http\Requests\FinalizarPedidoRequest;
use AsBentas\Mail\PedidoFinalizado;
use AsBentas\Model\Acompanhamento;
use AsBentas\Model\Cardapio;
use AsBentas\Model\Carrinho;
use AsBentas\Model\CategoriaAcompanhamento;
use AsBentas\Model\Endereco;
use AsBentas\Model\HorarioEntrega;
use AsBentas\Model\ItemPedidoCardapio;
use AsBentas\Model\Pedido;
use AsBentas\Model\PedidoCardapio;
use AsBentas\Model\Prato;
use AsBentas\Model\Cupom;
use AsBentas\Model\CupomPedido;
use AsBentas\Model\DescontoProgressivo;
use AsBentas\Http\Controllers\ContaUsuarioController;
use AsBentas\Model\LocalEntrega;
use AsBentas\Repositories\CardapioRepository;
use AsBentas\Repositories\CategoriaAcompanhamentoRepository;
use AsBentas\Repositories\ComboRepository;
use AsBentas\Repositories\EmpresaRepository;
use AsBentas\Repositories\EnderecoRepository;
use AsBentas\Repositories\LocalEntregasRepository;
use AsBentas\Repositories\PagamentoRepository;
use AsBentas\Util\Combo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Symfony\Component\VarDumper\VarDumper;


class CarrinhoController extends Controller
{
    public function __construct(PagamentoRepository $pagamentoRepository){
        $this->pagrepository = $pagamentoRepository;
    }

    public function atualizarCarrinho(Request $request) 
    {
        $pratos = $request->get('prato', []);
        $acompanhamentos = $request->get('acompanhamento', []);
        $pratoValor = $request->get('prato_valor');
        $acompanhamentoValor = $request->get('acompanhamento_valor');
        $addItemCardapio = $request->get('add-item-id-cardapio', null);
        $addItemAcompanhamento = $request->get('add-item-id-acompanhamento', null);
        $cupom = $request->get('cupom', null);
        $formaPagamento = $request->get('forma-pagamento', null);
        $entrega = session('entrega', []);
        $carrinho = [];



        foreach($pratos as $idCardapio => $itens) {

            $cardapio = Cardapio::find($idCardapio);

            $carrinho[$idCardapio] = [
                'data'   => $cardapio->data_cardapio,
                'pratos' => [],
                'acompanhamentos' => []
            ];

            foreach ($itens as $id => $qtd) {
                
                if ((int) $qtd > 0) {

                    $prato = Prato::find($id);

                    $carrinho[$idCardapio]['pratos'][$id] = [
                        'nome' => $prato->nome,
                        'descricao' => $prato->descricao,
                        'valor' => $pratoValor[$idCardapio][$id],
                        'qtd' => $qtd,
                        'acompanhamentos' => []
                    ];
                }
            }


            if (isset($acompanhamentos[$idCardapio])) {
                foreach ($acompanhamentos[$idCardapio] as $idPrato => $itens) {

                    if (!is_array($itens)) continue;

                    foreach ($itens as $idAcompanhamento => $qtd) {

                        if ((int)$qtd > 0) {

                            $acompanhamento = Acompanhamento::find($idAcompanhamento);

                            $carrinho[$idCardapio]['pratos'][$idPrato]['acompanhamentos'][$idAcompanhamento] = [
                                'nome' => $acompanhamento->nome,
                                'tipo' => $acompanhamento->categoria->nome,
                                'valor' => $acompanhamentoValor[$idCardapio][$idPrato][$idAcompanhamento],
                                'qtd' => $qtd
                            ];


                            if (!isset($carrinho[$idCardapio]['acompanhamentos'][$idAcompanhamento])) {

                                $carrinho[$idCardapio]['acompanhamentos'][$idAcompanhamento] = [
                                    'nome' => $acompanhamento->nome,
                                    'tipo' => $acompanhamento->categoria->nome,
                                    'valor' => $acompanhamentoValor[$idCardapio][$idPrato][$idAcompanhamento],
                                    'qtd' => $qtd
                                ];
                            } else {
                                $carrinho[$idCardapio]['acompanhamentos'][$idAcompanhamento]['qtd'] += $qtd;
                            }
                        }
                    }
                }
            }
        }

        if ($addItemCardapio && $addItemAcompanhamento) {


            $acompanhamento = Acompanhamento::find($addItemAcompanhamento);
            $qtd = 1;

    //                //Isso ta fazendo dar o erro de adicionar mais de um item de uma vez usando os botões azuis.
            if (isset($carrinho[$addItemCardapio]['pratos'][$prato->id]['acompanhamentos'][$addItemAcompanhamento]['qtd'])) {
                $qtd += $carrinho[$addItemCardapio]['pratos'][$prato->id]['acompanhamentos'][$addItemAcompanhamento]['qtd'];
            }


            //$carrinho[$idCardapio]['pratos'];
            if (isset($carrinho[$addItemCardapio]) && count($carrinho[$addItemCardapio]['pratos']) > 0) {
                $idPrato = (int) key($carrinho[$addItemCardapio]['pratos']);

                if ($idPrato > 0) {


                    if (isset($carrinho[$addItemCardapio]['pratos'][$idPrato]['acompanhamentos'][$addItemAcompanhamento])) {
                        $carrinho[$addItemCardapio]['pratos'][$idPrato]['acompanhamentos'][$addItemAcompanhamento]['qtd'] = $carrinho[$addItemCardapio]['pratos'][$idPrato]['acompanhamentos'][$addItemAcompanhamento]['qtd'] + 1;
                    } else {
                        $carrinho[$addItemCardapio]['pratos'][$idPrato]['acompanhamentos'][$addItemAcompanhamento] = [
                            'nome' => $acompanhamento->nome,
                            'tipo' => $acompanhamento->categoria->nome,
                            'valor' => $acompanhamento->valor_sugerido,
                            'qtd' => $qtd
                        ];
                    }
                }
            }


        }

        unset($carrinho['cupom']);
        unset($entrega['formaPagamento']);
        $user = Auth::user();
        $userId = ($user) ? $user->id : null;

        if ($formaPagamento) {
            $entrega['formaPagamento'] = $formaPagamento;
            $request->session()->put('entrega', $entrega);
        }

        if ($cupom) {

            $dadosCupom = Cupom::where('codigo', '=', $cupom)
            ->whereDate('data_expiracao', '>=', date('Y-m-d'))
            ->where('status', '=', Cupom::STATUS_ATIVO)
            ->get()
            ->first();


            if ($dadosCupom) {
                
                $carrinho['cupom'] = $dadosCupom;
                
                if ($dadosCupom->tipo_cupom == Cupom::CUPOM_CAMPANHA) {
                    
                    //Verifica se cupom já foi usado pelo usuário alguma vez.
                    $pedidos = Pedido::whereHas('cupons', function ($query) use ($userId, $dadosCupom) {
                        $query->where('usuario_id', '=', $userId)
                            ->where('cupom_id', '=', $dadosCupom->id);
                    })->get()->count();
                    
                    if ($pedidos > 0) {
                        flash('Você já utilizou o cupom informado.')->warning();
                        unset($carrinho['cupom']);
                    }
                
                }
    
                if ($dadosCupom->tipo_cupom == Cupom::CUPOM_UNICO) {
                    //Verifica se cupom já foi usado alguma vez.
                    $pedidos = Pedido::whereHas('cupons', function ($query) use ($dadosCupom) {
                        $query->where('cupom_id', '=', $dadosCupom->id);
                    })->get()->count();
                    
                    if ($pedidos > 0) {
                        flash('O Cupom informado não é válido.')->warning();
                        unset($carrinho['cupom']);
                    }
                }
            } else {
                flash('O Cupom informado não é válido.')->warning();
                unset($carrinho['cupom']);
            }
        }

        if (isset($carrinho['cupom'])) {
            flash('Parabéns! O Cupom de Desconto ' . $carrinho['cupom']->codigo .' foi aplicado em suas compras.')->success();
        }

        return $carrinho;
    }

    public function atualizar(Request $request)
    {
        $redirect = $request->get('redirect', 'carrinho.exibir');

        $carrinho = $this->atualizarCarrinho($request);

        $request->session()->put('carrinho', $carrinho);

        return redirect()->intended(route($redirect));
    }

    public function limpar(Request $request)
    {

        $request->session()->put('carrinho', []);
        $request->session()->put('entrega', []);

        return redirect()->intended(route('site.cardapio'));
    }

    public function finalizar(FinalizarPedidoRequest $request, LocalEntregasRepository $localEntregasRepository)
    {
        $empresa = null;
        if (Auth::check()) {

            $user = Auth::user();
            $empresas = null;
            if ($user->empresaAtiva()) {
                $empresa = $user->empresaAtiva()->id;
            }

        }

        $carrinho = $this->atualizarCarrinho($request);

        //$carrinho = $request->session()->get('carrinho', null);

        $pin = $request->get('pin', null);
        $pratos = $request->get('prato', []);
        $pratosValor = $request->get('prato_valor', []);
        $acompanhamentos = $request->get('acompanhamento', []);
        $acompanhamentosValor = $request->get('acompanhamento_valor', []);
        $entrega = $request->session()->get('entrega', []);

        $descontos = [];
        
        foreach(DescontoProgressivo::orderBy('dia', 'asc')->get() as $desconto) {
            $descontos[$desconto->dia] = $desconto;
        }

        //Desconto por Combo
        $comboRepository = new ComboRepository();

        $combos = $comboRepository->all();

        $aCombos = [];

        foreach($combos as $combo) {
            $valorCombo = (float) $combo->valor;
            if ($empresa) {
                $valorEmpresa = $combo->precoEmpresa($empresa)->get()->first();
                if ($valorEmpresa) {
                    $valorCombo = (float) $valorEmpresa->pivot->valor;
                }
            }
            $aCombos[$combo->id] = [];
            $aCombos[$combo->id]['valor'] = $valorCombo;
            $aCombos[$combo->id]['itens']['principal'] = 1;
            foreach($combo->categorias as $categoria) {
                $aCombos[$combo->id]['itens'][strtolower($categoria->nome)] = 1;
            }
        }

        //Array para confrontar com os combos
        $aItensPedido = [];


        $user = Auth::user();
        DB::beginTransaction();

        try {

            $pedido = new Pedido();
            $pedido->fulfillment = 'colocado';
            $pedido->pagamento = 'nao_pago';
            $pedido->endereco_entrega_id = (isset($entrega['endereco'])) ? $entrega['endereco'] : null;//$request->endereco_entrega;
            $endereco = Endereco::find($entrega['endereco']);
            $horarioEntrega  = HorarioEntrega::find($entrega['horario']);

            $pedido->endereco_entrega = $endereco->label();
            $pedido->endereco_entrega_complemento  = $endereco->complemento;
            $pedido->horario_entrega  = $horarioEntrega->label();

            $pedido->user_id = Auth::id();
            $pedido->complemento_endereco = (isset($entrega['complemento'])) ? $entrega['complemento'] : null;
            $pedido->horario_id = $entrega['horario'];
            $pedido->pagamento_id = $entrega['formaPagamento'];

            $localEntrega = $localEntregasRepository->buscarIntervalo(preg_replace('/\D/', '', $endereco->cep))->first();
            $valorFrete = $localEntrega->valor_entrega;


            if ($user->empresaAtiva()) {
                $enderecoId = $user->empresaAtiva()->pivot->endereco_id;
                $enderecos = $user->empresaAtiva()->enderecos;
                $empresaId = $user->empresaAtiva()->id;

                //Se o id do endereço que foi setado pra entrega ($enderecoEntrega->id) for igual a algum id de qualquer endereço da
                //empresa, quer dizer que o usuário ta dentro da empresa
                foreach ($enderecos as $endereco){
                    if ( $endereco->id == $endereco->id)
                    {

                        //Se a empresa possuir um valor de frete especial o valor da entrega recebe esse valor
                        if($user->empresaAtiva()->valor_frete){
                            $valorFrete = $user->empresaAtiva()->valor_frete;
                        }

                        //Se a empresa possuir a opção frete grátias o valor do frete vai pra 0
                        if($user->empresaAtiva()->frete_gratis == 1)
                        {
                            $valorFrete = 0;
                        }
                    }
                }

                //Se a empresa ativa do funcionário em questão tiver a cláusula de frete gratis para funcionário, independente
                //de onde ele estiver o valor do frete vira 0

                if( ($user->empresaAtiva()->frete_gratis_funcionario == 1)){
                    $valorFrete = 0;
                }
            }

            $total = 0;

            $pedido->save();

            $pedidoCardapio = [];

            $dados = array();
            $valorDiaPedido = [];

            foreach($pratos as $idCardapio => $itens) {

                if (! isset($aItensPedido[$idCardapio])) {
                    $aItensPedido[$idCardapio] = [];
                }

                if (! isset($pedidoCardapio[$idCardapio])) {
                    $pedidoCardapio[$idCardapio] = new PedidoCardapio();
                    $pedidoCardapio[$idCardapio]->cardapio_id = $idCardapio;
                    $pedidoCardapio[$idCardapio]->pedido_id = $pedido->id;
                    $pedidoCardapio[$idCardapio]->valor_frete = $valorFrete;
                    $pedidoCardapio[$idCardapio]->save();
                    $valorDiaPedido[$idCardapio] = 0;
                }

                $aItensPedido[$idCardapio]['principal'] = 0;

                foreach($itens as $idPrato => $qtd) {
                    if($qtd > 0) {
                        $prato = Prato::find($idPrato);

                        $item = new ItemPedidoCardapio();
                        $item->pedido_cardapio_id = $pedidoCardapio[$idCardapio]->id;
                        $item->descricao = $prato->nome;
                        $item->quantidade = $qtd;
                        $item->valor = $qtd * (float) $pratosValor[$idCardapio][$idPrato];
                        $item->save();
                        $valorDiaPedido[$idCardapio] += $item->valor;

                        $aItensPedido[$idCardapio]['principal'] += $qtd;

                    }
                }

            }

            foreach($acompanhamentos as $idCardapio => $itens) {

                if (! isset($aItensPedido[$idCardapio])) {
                    $aItensPedido[$idCardapio] = [];
                }

                if (! isset($pedidoCardapio[$idCardapio])) {
                    $pedidoCardapio[$idCardapio] = new PedidoCardapio();
                    $pedidoCardapio[$idCardapio]->cardapio_id = $idCardapio;
                    $pedidoCardapio[$idCardapio]->pedido_id = $pedido->id;
                    $pedidoCardapio[$idCardapio]->valor_frete = $valorFrete;
                    $pedidoCardapio[$idCardapio]->save();

                    $valorDiaPedido[$idCardapio] = 0;
                    
                }

                foreach($itens as $idPrato => $itens) {

                    foreach($itens as $idAcompanhamento => $qtd) {
                        if($qtd > 0){
                            $oAcompanhamento = Acompanhamento::find($idAcompanhamento);

                            $item = new ItemPedidoCardapio();
                            $item->pedido_cardapio_id = $pedidoCardapio[$idCardapio]->id;
                            $item->descricao = $oAcompanhamento->nome;
                            $item->quantidade = (int) $qtd;
                            $item->valor =  $qtd * (float) $acompanhamentosValor[$idCardapio][$idPrato][$idAcompanhamento];

                            $item->save();
                            $valorDiaPedido[$idCardapio] += $item->valor;
                            $item->save();

                            $categoria = strtolower($oAcompanhamento->categoria->nome);

                            if (!isset($aItensPedido[$idCardapio][$categoria])) {
                                $aItensPedido[$idCardapio][$categoria] = 0;
                            }

                            $aItensPedido[$idCardapio][$categoria] += $qtd;

                        }
                    }
                }
                
            }

            //Confrontar os combos

            $descontoComboPorCardapio = Combo::calcularValor($aItensPedido, $aCombos);


            foreach ($descontoComboPorCardapio as $idCardapio => $valor) {

                //$total -= $valor;

                $pedidoCardapio[$idCardapio]->desconto_combo = $valor;
                $pedidoCardapio[$idCardapio]->save();
            }


            //Verifica desconto / aplica
            $dia = 1;
            foreach($valorDiaPedido as $idCardapio => $valor) {


                $combo = $pedidoCardapio[$idCardapio]->desconto_combo;

                $valor = ($valor - $combo);

                $desconto = $valor * ($descontos[$dia]->desconto / 100);

                $valor -= $desconto;

                $valor += $valorFrete;

                $total += $valor;

                $pedidoCardapio[$idCardapio]->desconto = $desconto;
                $pedidoCardapio[$idCardapio]->subtotal = $valor;
                $pedidoCardapio[$idCardapio]->save();
                $dia++;
            }


            $pedido->valor_total = $total;
            $pedido->save();
            $user->pin = $pin;
            $user->save();

            //Cupom
            if (isset($carrinho['cupom'])) {
                $cupom = $carrinho['cupom'];

                if($cupom->tipo == '%')
                {
                    $descontoCupom = $total * $cupom->valor/100;
                    $total -= $descontoCupom;
                    $pedido->valor_total = $total;
                    $pedido->valor_cupom_desconto = $descontoCupom;
                    $pedido->save();
                }
                else{
                    $descontoCupom = $cupom->valor;
                    $total -= $descontoCupom;
                    $pedido->valor_total = $total;
                    $pedido->valor_cupom_desconto = $descontoCupom;
                    $pedido->save();
                }

                $cupomPedido = new CupomPedido();
                $cupomPedido->pedido_id = $pedido->id;
                $cupomPedido->cupom_id = $cupom->id;
                $cupomPedido->usuario_id = $user->id;
                $cupomPedido->save();
            }

            DB::commit();
            $dados['pedido'] = $pedido;
            $dados['carrinho'] = $carrinho;
            $dados['frete'] = $valorFrete;

            $request->session()->forget('carrinho');
            $request->session()->forget('local');
            $request->session()->forget('entrega');


            // enviar email de confirmação
            Mail::to($user)->send(new PedidoFinalizado($user, $dados));

            return redirect()->intended(route('meus-pedidos.obrigado', ['id' => $pedido->id]));

        } catch(\Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    public function exibir(Request $request,
                           CardapioRepository $cardapioRepository,
                           CategoriaAcompanhamentoRepository $categoriaAcompanhamentoRepository,
                           LocalEntregasRepository $localEntregasRepository)
    {
        $empresa = null;
        if (Auth::check()) {

            $user = Auth::user();
            $empresas = null;
            if ($user->empresaAtiva()) {
                $empresa = $user->empresaAtiva()->id;
            }

        }

        //session('entrega', null);
        $localSession = session('local', null); 
        $carrinho = session('carrinho', null);
        $entrega = session('entrega', []);
        $pagamento = session('pagamento',null);

        if (! $carrinho) {
            return redirect()->route('site.cardapio');
        }

        $descontos = [];

        foreach(DescontoProgressivo::orderBy('dia', 'asc')->get() as $desconto) {
            $descontos[$desconto->dia] = $desconto;
        }

        
        if (!Auth::check()) {
            return redirect()->route('usuario.login');
        }

        //Valida Endereco/Horario
        if ((!isset($entrega['endereco']) || ((int) $entrega['endereco'] == 0)) 
        && (!isset($entrega['horario']) || (int) $entrega['horario'] == 0)) {
            
           return redirect()->intended(route('carrinho.entrega'));
       }

        $user = Auth::user();
        $pin = $user->pin;
        
        $enderecoId = null;
        $enderecos = [];
        $empresaId = null;
        

        
        $entrega = $request->session()->get('entrega', []);
        $enderecoEntrega = null;
        $localEntrega = null;
        $valorFrete = 0;


        if (count($entrega) > 0) {

            $enderecoEntrega = Endereco::find($entrega['endereco']);

            if ($enderecoEntrega) {
                $localEntrega = $localEntregasRepository->buscarIntervalo(preg_replace('/\D/', '', $enderecoEntrega->cep))->first();
                $valorFrete = $localEntrega->valor_entrega;
            }

            
        }

        if ($user->empresaAtiva()) {
            $enderecoId = $user->empresaAtiva()->pivot->endereco_id;
            $enderecos = $user->empresaAtiva()->enderecos;
            $empresaId = $user->empresaAtiva()->id;

            //Se o id do endereço que foi setado pra entrega ($enderecoEntrega->id) for igual a algum id de qualquer endereço da
            //empresa, quer dizer que o usuário ta dentro da empresa
            foreach ($enderecos as $endereco){
                if ( $enderecoEntrega->id == $endereco->id)
                {

                    //Se a empresa possuir um valor de frete especial o valor da entrega recebe esse valor
                    if($user->empresaAtiva()->valor_frete){
                        $valorFrete = $user->empresaAtiva()->valor_frete;
                    }

                    //Se a empresa possuir a opção frete grátias o valor do frete vai pra 0
                    if($user->empresaAtiva()->frete_gratis == 1)
                    {
                        $valorFrete = 0;
                    }
                }
            }

            //Se a empresa ativa do funcionário em questão tiver a cláusula de frete gratis para funcionário, independente
            //de onde ele estiver o valor do frete vira 0

            if( ($user->empresaAtiva()->frete_gratis_funcionario == 1)){
                $valorFrete = 0;
            }
        }

        if (count($entrega) == 0) {
            //Dados de Entrega nao definidos
            return redirect()->intended(route('carrinho.entrega'));
        }


        $acompanhamentos = $categoriaAcompanhamentoRepository->buscarPreco($empresaId);
        $bebidas = [];
        $carrinho = $request->session()->get('carrinho', []);

        if (count($carrinho) > 0) {
            foreach ($carrinho as $codigo => $item) {
                if (isset($item['data'])) $bebidas[$item['data']->format('d/m')] = $cardapioRepository->buscarBebidasPorCardapio($item['data']);
                // else dd($item);
            }
        }

        $comboRepository = new ComboRepository();

        $combos = $comboRepository->all();

        $jsonCombos = [];

        foreach($combos as $combo) {
            $valorCombo = (float) $combo->valor;
            if ($empresa) {
                $valorEmpresa = $combo->precoEmpresa($empresa)->get()->first();
                if ($valorEmpresa) {
                    $valorCombo = (float) $valorEmpresa->pivot->valor;
                }
            }
            $jsonCombos[$combo->id] = [];
            $jsonCombos[$combo->id]['valor'] = $valorCombo;
            $jsonCombos[$combo->id]['itens'][] = "principal";
            foreach($combo->categorias as $categoria) {
                $jsonCombos[$combo->id]['itens'][] = strtolower($categoria->nome);
            }
        }

        return response()->view('site.carrinho', [
            'carrinho' => $request->session()->get('carrinho', []),
            'pin' => $pin,
            'endereco_id' => $enderecoId,
            'enderecos' => $enderecos,
            'acompanhamentos' => $acompanhamentos,
            'bebidas'   => $bebidas,
            'enderecoEntrega' => $enderecoEntrega,
            'horarioEntrega' => HorarioEntrega::find($entrega['horario']),
            'horarioEntregaId' => $entrega['horario'],
            'enderecoEntregaId' => $entrega['endereco'],
            'complemento' => $entrega['complemento'],
            'descontos' => $descontos,
            'localEntrega' => $localEntrega,
            'valorFrete'  => $valorFrete,
            'entrega' => $entrega,
            'pagamentos' => $this->pagrepository->findAll(),
            'combos' => json_encode($jsonCombos)
        ])
            ->header('Cache-Control', 'no-cache, no-store, must-revalidate')
            ->header('Pragma', 'no-cache')
            ->header('Expires', '0')
        ;
     }

    public function entrega(Request $request)
    {
        $user = Auth::user();

        $carrinho = session('carrinho', null);
        

        $horarios = HorarioEntrega::orderBy('horario_inicial', 'desc')->get();
        
        $entregaSession = session('entrega', [
            'horario' => '', 'endereco' => '', 'complemento' => ''
        ]);

        $entrega = [
            'horario' => (isset($entregaSession['horario'])) ? $entregaSession['horario'] : null,
            'endereco' => (isset($entregaSession['endereco'])) ? $entregaSession['endereco'] : null,
            'complemento' => (isset($entregaSession['complemento'])) ? $entregaSession['complemento'] : null,
        ];

        $enderecosEmpresa = collect();
        
        if ($user->empresaAtiva()) {
            $empresa = $user->empresaAtiva();
            $enderecosEmpresa = $empresa->enderecos;
        }

        $listaEnderecosEcontrados = $enderecosEmpresa->merge($user->enderecos);

        $enderecosAtivos = collect();
        $novosEnderecosInativos = collect();


        foreach ( $listaEnderecosEcontrados as $enderecoLista){


            $endereco = Endereco::find($enderecoLista->id);

            $cep = preg_replace("/[^0-9]/", "", 	$endereco->cep);

            $localAtivo = LocalEntrega::whereRaw('? between cep_inicial and cep_final and status = ?', [$cep, 'ativo'])->first();



            if($localAtivo){
                $enderecosAtivos->push($endereco);
                $endereco->status = 'ativo';

                    $endereco->save();
            }
            else{
                if($endereco->status != 'inativo'){
                    $novosEnderecosInativos->push($endereco);
                }
                $endereco->status = 'inativo';

                    $endereco->save();
            }



        }


        return view('site.entrega', [
            'entrega' => $entrega,
            'horarios' => $horarios,
            'enderecos' => $enderecosAtivos,
            'enderecosInativos' => $novosEnderecosInativos,
            'carrinho' => $request->session()->get('carrinho', []),
            'valorFrete' => 0,
        ]);
    }

    public function salvarEntrega(CarrinhoEntregaRequest $request)
    {

        $entrega = [
            'horario' => $request->get('horario'),
            'endereco' => $request->get('endereco'),
            'complemento' => $request->get('complemento'),
        ];

        $request->session()->put('entrega', $entrega);

        return redirect()->intended(route('carrinho.exibir'));


    }

    public function adicionarComplemento($id, $idAcompanhamento, Request $request)
    {
        $carrinho = $request->session()->get('carrinho', []);

        $acompanhamento = Acompanhamento::find($idAcompanhamento);


        if (! isset($carrinho[$id]['pratos'][key($carrinho[$id]['pratos'])]['acompanhamentos'][$idAcompanhamento]))
        {
            $carrinho[$id]['pratos'][key($carrinho[$id]['pratos'])]['acompanhamentos'][$idAcompanhamento] = [
                'nome' => $acompanhamento->nome,
                'valor' => $acompanhamento->valor_sugerido,
                'tipo' => $acompanhamento->categoria->nome,
                'qtd' => 1
            ];
        } else {
            $carrinho[$id]['pratos'][key($carrinho[$id]['pratos'])]['acompanhamentos'][$idAcompanhamento]['qtd'] += 1;
        }



        if (! isset($carrinho[$id]['acompanhamentos'][$idAcompanhamento])) {

            $carrinho[$id]['acompanhamentos'][$idAcompanhamento] = [
                'nome' => $acompanhamento->nome,
                'tipo' => $acompanhamento->categoria->nome,
                'valor' => $acompanhamento->valor_sugerido,
                'qtd' => 1
            ];
        } else {
            $carrinho[$id]['acompanhamentos'][$idAcompanhamento]['qtd'] += 1;
        }



        $request->session()->put('carrinho', $carrinho);

        return redirect()->intended(route('carrinho.exibir'));

    }

    public function buscarItens(Request $request)
    {
     //   $request->session()->put('carrinho', new Carrinho());

        $carrinho = $request->session()->get('carrinho', new Carrinho());

        return response()->json($carrinho->getItens());
    }

    public function adicionarPrato(Request $request)
    {

        $cardapio = $request->request->get('cardapio');
        $id= $request->request->get('id');
        $qtd = $request->request->get('qtd');

        $cardapio = Cardapio::find($cardapio);

        /** @var Carrinho $carrinho */
        $carrinho = $request->session()->get('carrinho', new Carrinho());

        $item = Prato::find($id);

        $carrinho->addPrato($cardapio->id, $cardapio->data_cardapio, $id, $item->nome, $qtd);

    }

    public function adicionarAcompanhamento(Request $request)
    {
        $cardapio = $request->request->get('cardapio');
        $id = $request->request->get('id');
        $idPrato = $request->request->get('id-prato');
        $qtd = $request->request->get('qtd');
        /** @var Carrinho $carrinho */
        $carrinho = $request->session()->get('carrinho', new Carrinho());

        $item = Acompanhamento::find($id);

        $carrinho->addAcompanhamento($cardapio, $idPrato, $id, $item->nome, $qtd);

    }

    public function removerItem($id, Request $request) {

        $carrinho = $request->session()->get('carrinho', []);

        $carrinhoVazio = true;

        if (isset($carrinho[$id])) {

            $carrinho[$id] = [
                'data' => $carrinho[$id]['data'],
                'pratos' => [],
                'acompanhamentos' => [],
            ];

            $request->session()->put('carrinho', $carrinho);
        }

        foreach($carrinho as $itens)
        {
            if (count($itens['pratos']) > 0) $carrinhoVazio = false;
        }

        if ($carrinhoVazio) {
            return redirect()->intended(route('site.cardapio'));
        } else {
            return redirect()->intended(route('carrinho.exibir'));
        }

    }

}
