<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Model\Configuracao;
use AsBentas\Model\Pedido;
use AsBentas\Model\PedidoCardapio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;
use AsBentas\Mail\CancelarPedido;
use Illuminate\Support\Facades\Mail;


class MeusPedidosController extends Controller
{
    public function index(Request $request)
    {

        $dias = $request->input('dias', 7);
        $data = new Carbon();
        $data->subDays($dias);


        $pedidos = Pedido::where('user_id', '=', Auth::id())
            ->where('created_at',  '>', $data->toDateString())->orderBy('id', 'desc')
            ->get();


        $conf = Configuracao::where('chave', '=', 'qtd_hora_cancelamento')->first();

        return view('meus-pedidos.index', [
            'pedidos' => $pedidos,
            'qtd_hora_cancelamento' => $conf->valor,
            'dias' => $dias
        ]);
    }

    public function resumo($id)
    {
        $user = Auth::user();
        $conf = Configuracao::where('chave', '=', 'qtd_hora_cancelamento')->first();
        $pedido = Pedido::where('id', '=', $id)->where('user_id', '=', $user->id)->first();

        $itensPedido = [];

        foreach($pedido->pedidoCardapios as $pedidoCardapio)
        {
            $itensPedido[$pedidoCardapio->cardapio->data_cardapio->format('d/m/Y')] = $pedidoCardapio;
        }
        ksort($itensPedido);
        return view('meus-pedidos.resumo', [
            'pedido' => $pedido,
            'itensPedido' => $itensPedido,
            'qtd_hora_cancelamento' => $conf->valor
        ]);
    }

    public function cancelarItem(Request $request, $id)
    {
        $justificativa = $request->get('justificativa', null);

        if (! $justificativa) {
            return response()->json(['message' => 'Informe a justificativa'], 500);
        }

        $item = PedidoCardapio::find($id);
        $pedido = Pedido::find($item->pedido->id);

        $item->fulfillment = 'cancelado';
        $item->canceled_by = Auth::user()->getAuthIdentifierName();
        $item->canceled_at = new Carbon();
        $item->justificativa_cancelamento = $justificativa;
        $item->save();

        $itensCancelados = PedidoCardapio::where('fulfillment', '=', 'cancelado')
            ->where('pedido_id', '=', $item->pedido->id)
            ->count();

        $itens = PedidoCardapio::where('pedido_id', '=', $item->pedido->id)
            ->count();

        if ($itensCancelados == $itens)
        {
            $pedido->fulfillment = 'cancelado';
            $pedido->save();
        }

        $emailConfiguracao = Configuracao::where('chave', '=', 'email_recebimento_cancelamento_pedido')->first();
        
        Mail::to($emailConfiguracao->valor)
            ->send(new CancelarPedido($item));

        return response()->json(['message' => 'Item Pedido Cancelado.']);
    }

    public function obrigado(Request $request, $id)
    {

        $user = Auth::user();

//        $pin = $user->pin;
        $conf = Configuracao::where('chave', '=', 'qtd_hora_cancelamento')->first();


         return response()->view('meus-pedidos.obrigado', [
             'conf' => $conf->valor,
             'pedido' => $id
         ])
         ->header('Cache-Control', 'no-cache, no-store, must-revalidate')
         ->header('Pragma', 'no-cache')
         ->header('Expires', '0')
         ;
        /*return view('meus-pedidos.obrigado', [
//            'pin' => $pin,
            'conf' => $conf->valor,
            'pedido' => $id
        ]);*/

    }

}
