<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Http\Requests\LoginUsuarioRequest;
use AsBentas\Http\Requests\NovaSenhaRequest;
use AsBentas\Http\Requests\RegistroUsuarioRequest;
use AsBentas\Mail\EsqueceuSenhaMail;
use AsBentas\Mail\NovoUsuarioMail;
use AsBentas\Mail\UsuarioConfirmadoMail;
use AsBentas\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Psy\Exception\FatalErrorException;
use Symfony\Component\VarDumper\VarDumper;

class UsuarioController extends Controller
{

    public function sair(Request $request)
    {
        Auth::logout();
        $request->session()->forget('carrinho');
        $request->session()->forget('local');
        $request->session()->forget('entrega');

        return redirect(route('site.index'));


    }

    public function login()
    {

        if (Auth::check()) {
            return redirect()->intended(route('site.cardapio'));
        }

        return view('usuario.login');
    }

    public function lostPassword()
    {
        return view('usuario.esqueceu-senha');
    }

    public function lostPasswordSendEmail(Request $request)
    {
        $usuario = User::whereEmail($request->email)->first();

        if (! $usuario) {

            $mensagem = "Verifique o e-mail informado.";

        } else {

            $mensagem = "Verifique seu e-mail para criar a nova senha.";

            $usuario->new_password = 1;
            $usuario->save();

            Mail::to($usuario)
                ->send(new EsqueceuSenhaMail($usuario));

        }

        return redirect()->back()->with('message', $mensagem);

    }

    public function newPassword()
    {

        return view('usuario.nova-senha');
    }

    public function newPasswordSend(Request $request)
    {
        $usuario = User::whereEmail($request->email)->first();

        if (! $usuario) {

            $mensagem = "Verifique o e-mail informado.";

        } else {

            $mensagem = "Verifique seu e-mail para criar a nova senha.";

            $usuario->new_password = 1;
            $usuario->save();

            Mail::to($usuario)
                ->send(new EsqueceuSenhaMail($usuario));

        }

        return redirect()->back()->with('message', $mensagem);

    }

    public function changePassword($id) {

        $usuario = User::find($id);

        return view('usuario.nova-senha', [ 'usuario' => $usuario ]);
    }

    public function changePasswordSave(NovaSenhaRequest $request)
    {

        $usuario = User::find($request->id);

        $usuario->new_password = 0;
        $usuario->password = $request->password;
        $usuario->save();

        return redirect(route('usuario.sucesso-senha'))->with('message', 'Sua senha foi alterada.');

    }

    public function newPasswordSuccess()
    {
        return view('usuario.sucesso-nova-senha');
    }

    public function auth(LoginUsuarioRequest $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'confirmed' => 1])) {
    
            $user = Auth::user();

            $endereco = session('local', null);

            if (! $user->data_primeiro_acesso)
            {
                return redirect()->route('conta-usuario.primeiro-acesso');
            }
        } else {

            flash('E-mail ou senha incorretos.')->warning();

            return redirect()->back();
        }

        return redirect()->intended(route('site.cardapio'));

    }

    public function register()
    {
        return view('usuario.cadastro');
    }

    public function storeRegister(RegistroUsuarioRequest $request)
    {

        $confirmation_code = str_random(30);

        $usuario = User::create([
            'name'  => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'confirmation_code' => $confirmation_code
        ]);

        Mail::to($usuario)
            ->send(new NovoUsuarioMail($usuario));

        return redirect()->route('usuario.sucesso',   session(['idUsuario' => $usuario->id]));

    }

    public function reSendConfirmationEmail($id)
    {

        $usuario = User::find($id);

        Mail::to($usuario)
            ->send(new NovoUsuarioMail($usuario));

        return redirect(route('usuario.sucesso'))->with('idUsuario', $usuario->id);

    }

    public function confirmationEmail($confirmationCode)
    {

        if (!$confirmationCode)
        {
            throw new FatalErrorException('');
        }

        $usuario = User::whereConfirmationCode($confirmationCode)->first();

        if (!$usuario) {
            return redirect(route('usuario.login'))->with('message', 'Link inválido.');
        }

        $usuario->confirmed = 1;
        $usuario->confirmation_code = null;
        $usuario->save();

        Mail::to($usuario)
            ->send(new UsuarioConfirmadoMail());

        return redirect(route('usuario.login'))->with('message', 'Dados confirmados com sucesso.');

    }

    public function registerSuccess()
    {

        return view('usuario.sucesso');
    }

}
