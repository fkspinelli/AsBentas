<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Model\Empresa;
use AsBentas\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\VarDumper\VarDumper;

class SocialLoginController extends Controller
{
    public function login($provider)
    {
        return Socialite::driver($provider)->asPopup()->redirect();

    }

    public function callback($provider)
    {

        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, 'facebook');

        Auth::login($authUser, true);

        $user = Auth::user();

        if (! $user->data_primeiro_acesso)
        {
            return redirect()->intended(route('conta-usuario.primeiro-acesso'));
        }

        $empresaAtiva = Empresa::findOrFail($user->empresaAtiva()->pivot->empresa_id);


        if ($empresaAtiva->status == 'pre') {
            Auth::logout();
            return redirect(route('site.index'))->with('message', 'O Registro de sua empresa está em análise, em breve entraremos em contato.');
        }

        return redirect(route('site.cardapio'));

    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }
}
