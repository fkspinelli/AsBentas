<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Http\Requests\AdicionarEnderecoRequest;
use AsBentas\Http\Requests\AtualizarMeusDadosRequest;
use AsBentas\Http\Requests\CadastrarEnderecoRequest;
use AsBentas\Http\Requests\ConfirmarEmpresaRequest;
use AsBentas\Http\Requests\NovaSenhaRequest;
use AsBentas\Http\Requests\PreCadastroEmpresaRequest;
use AsBentas\Model\Empresa;
use AsBentas\Model\Endereco;
use AsBentas\Model\LocalEntrega;
use AsBentas\Model\Pedido;
use AsBentas\Model\User;
use AsBentas\Repositories\EmpresaRepository;
use AsBentas\Repositories\EnderecoRepository;
use AsBentas\Repositories\LocalEntregasRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

class ContaUsuarioController extends Controller
{
    /**
     * @var EmpresaRepository
     */
    private $empresaRepository;

    private $enderecoRepository;

    public function __construct(EmpresaRepository $empresaRepository,
                                EnderecoRepository $enderecoRepository,
                                LocalEntregasRepository $localEntregasRepository)
    {

        $this->empresaRepository = $empresaRepository;
        $this->enderecoRepository = $enderecoRepository;
        $this->localEntregasRepository = $localEntregasRepository;
        $this->middleware('auth:usuario');
    }

    public function primeiroAcesso(\Symfony\Component\HttpFoundation\Request $request, LocalEntrega $locais)
    {
        $locais = $this->localEntregasRepository->findAll()->where('status', '==', 'ativo');
        $user = Auth::user();

        $email = "";
        $cep = "";

        if ($request->session()->get('local')) {
            $email = $request->session()->get('local')['email'];
            $cep = $request->session()->get('local')['cep'];
        }

        if ($user->data_primeiro_acesso) {
            return redirect()->intended(route('site.cardapio'));
        }

        $empresas = $this->empresaRepository->findAtivas();

        return view('usuario.primeiro-acesso',

            ['nome' => $user->name,
                'empresas' => $empresas,
                'email' => $email,
                'cep' => $cep],

            ['locais' => $locais]);

    }

    public function listarEnderecosEmpresa(Request $request)
    {

        $empresa = $this->empresaRepository->find($request->query('empresa_id'));

        return response()->json($empresa->enderecos);

    }

    public function confirmarEmpresa(ConfirmarEmpresaRequest $request)
    {
        $user = Auth::user();

        $validatedData = $request->validate([
            'empresa_ativa' => 'required',
            'endereco_id' => 'required',
            'codigo_empresa' => 'required',
        ]);

        $empresaSelecionada = Empresa::findOrFail($request->empresa_id);

        if ($request->endereco_id == null )
        {
            flash('Informe o endereço da empresa.', 'warning');
            return back()->withInput();
        }

        if ($empresaSelecionada->codigo_empresa != $request->codigo_empresa) {
            return back()->withErrors(['codigo_empresa' => 'Código Informado não é válido.'])->withInput();
        }

        $user->empresas()->attach($request->empresa_id,
            ['endereco_id' => $request->endereco_id,
             'dt_inicio' => Carbon::now()
            ]);

        $user->data_primeiro_acesso = new Carbon();

        $user->save();

        return redirect()->intended(route('site.cardapio'));

    }

    public function cadastrarEmpresa(PreCadastroEmpresaRequest $request)
    {
        $user = Auth::user();

        $locaisEntrega = $this->localEntregasRepository->buscarIntervalo(preg_replace('/\D/', '', $request->cep));

        $redirect = $request->get('origem', 'site.index');

        $empresa = Empresa::create([
            'nome' => $request->nome,
            'contato' => $request->contato,
            'telefone' => $request->ddd . ' ' . $request->telefone,
            'email' => $request->email,
            'cep' => preg_replace('/\D/', '', $request->cep),
            'status' => 'pre',
            'usuario_pedido_cadastro_id' => $user->id
        ]);

        $endereco = Endereco::create([
            'nome' => $request->nome,
            'cep' => $request->cep,
            'logradouro' => $request->logradouro,
            'numero' => $request->numero,
            'bairro' => $request->bairro,
            'cidade' => $request->cidade,
            'estado' => $request->estado,
            'empresa_id' => $empresa->id
        ]);

        if ($redirect == 'site.index') {
            $user->empresas()->attach($empresa->id, ['dt_inicio' => Carbon::now()]);
            $user->data_primeiro_acesso = new Carbon();
            $user->save();
        }


        if ($locaisEntrega->count() == 0) {
            return redirect()->intended(route($redirect))->with('pre-cadastro', 'cep-invalido');
        }

        return redirect()->intended(route($redirect))->with('pre-cadastro', 'cep-valido');


    }

    public function meusDados(Request $request)
    {
        $user = Auth::user();

        $empresa = null;
        $enderecoId = null;
        $endereco = null;
        $enderecosEmpresa = [];

        $empresas = $this->empresaRepository->findAtivas();


        if (!$user->data_primeiro_acesso) {
            return redirect()->intended(route('conta-usuario.primeiro-acesso'));
        }



        if ($user->empresaAtiva()) {

            $enderecoId = $user->empresaAtiva()->pivot->endereco_id;

            $empresa = $user->empresaAtiva();
            $enderecosEmpresa = $empresa->enderecos;
            $endereco = $this->enderecoRepository->find($enderecoId);
        }
        else {

            if ($request->old('empresa_ativa', null)) {
                $empresaSelecionada = Empresa::findOrFail($request->old('empresa_ativa'));
                $enderecoId = $request->old('endereco_id', null);
                $enderecosEmpresa = $empresaSelecionada->enderecos;
            }

        }

        if(!$endereco == null){
            $listaEnderecosEcontrados = $user->enderecos;

            if ($user->empresaAtiva()->pivot->padrao) {
                $endereco->padrao = $user->empresaAtiva()->pivot->padrao;
            }

            $listaEnderecosEcontrados[] = $endereco;
        }
        else
        {
            $listaEnderecosEcontrados = $user->enderecos;
        }

        foreach ( $listaEnderecosEcontrados as $enderecoLista){


            $endereco = Endereco::find($enderecoLista->id);
            $cep = preg_replace("/[^0-9]/", "", 	$endereco->cep);
            $localAtivo = LocalEntrega::whereRaw('? between cep_inicial and cep_final and status = ?', [$cep, 'ativo'])->first();
            if($localAtivo){
                $endereco->status = 'ativo';
                $endereco->save();
            }
            else{
                if($endereco->status != 'inativo'){
                }
                $endereco->status = 'inativo';
                $endereco->save();
            }
        }

        return view('usuario.meus-dados', [
            'user' => $user,
            'empresa' => $empresa,
            'enderecoId' => $enderecoId,
            'enderecosEmpresa' => $enderecosEmpresa,
            'empresas' => $empresas,
            'listaEnderecosEcontrados' => $listaEnderecosEcontrados
        ]);
    }

    public function meusDadosAtualizar(AtualizarMeusDadosRequest $request)
    {
        $user = Auth::user();

        if ($request->password || $request->confirm_password) {
            $validatedData = Validator::make($request->all(), [
                    'password' => 'password|required|confirmed|max:20',
                'password_confirmation' => 'required'
            ],[
                'password.required' => 'A senha é obrigatória.',
                'password.password' => 'Formato de senha inválida.',
                'password.confirmed' => 'A senha precisa ser confirmada',
                'password_confirmation.required' => 'A confirmação da senha é necessária.'
            ]);

            if ($validatedData->fails()){
                return redirect()->intended(route('conta-usuario.meus-dados'))->withErrors($validatedData)->withInput();
            }

            $user->password = $request->password;
            $user->confirmed = 1;
            $user->save();
            flash('Senha alterada com sucesso!', 'success');
        }

        $dadosUsuario = [
            'name' => $request->name,
            'pin' => $request->pin,
            'telefone' => trim($request->ddd . ' ' . $request->telefone),
            'receber_noticias' => !empty($request->receber_noticias)
        ];

        $usuario = User::updateOrCreate(['id' => $user->id], $dadosUsuario);

        $empresaUsuario = $user->empresaAtiva();

        if ($empresaUsuario) {

            $empresaUsuario->pivot->endereco_id = $request->endereco_id;
            $empresaUsuario->pivot->codigo_empresa = $request->codigo_empresa;
            $empresaUsuario->pivot->save();

        } else {

            //Trata Se estiver vinculando uma empresa nova

            $validatedData = $request->validate([
                'empresa_ativa' => 'required',
                'endereco_id' => 'required',
                'codigo_empresa' => 'required',
            ]);

            if($request->empresa_ativa != ""){

                $empresaSelecionada = Empresa::findOrFail($request->empresa_ativa);


                if ($request->endereco_id == null )
                {
                    flash('Informe o endereço da empresa.', 'warning');
                    return back()->withInput();
                }

                if ($empresaSelecionada->codigo_empresa != $request->codigo_empresa) {
                    return back()->withErrors(['codigo_empresa' => 'Código Informado não é válido.'])->withInput();
                }

                $user->empresas()->attach($request->empresa_ativa, [
                    'endereco_id' => $request->endereco_id,
                    'dt_inicio' => Carbon::now(),
                    'codigo_empresa' => $request->codigo_empresa
                ]);
            }


            $user->data_primeiro_acesso = new Carbon();

            $user->save();

        }

        return redirect()->intended(route('conta-usuario.meus-dados'));
    }

    public function adicionarEndereco(AdicionarEnderecoRequest $request)
    {
        $user = Auth::user();

        $endereco = Endereco::find($request->get('id', null));


        if (!$endereco) {
            $endereco = new Endereco();
        }

        $endereco->nome = $request->get('nome');
        $endereco->cep = $request->get('cep');
        $endereco->logradouro = $request->get('logradouro');
        $endereco->numero = $request->get('numero');
        $endereco->complemento = $request->get('complemento');
        $endereco->ddd = $request->get('endereco-ddd');
        $endereco->telefone = $request->get('endereco-telefone');
        $endereco->email = $request->get('email');
        $endereco->bairro = $request->get('bairro');
        $endereco->cidade = $request->get('cidade');
        $endereco->estado = $request->get('estado');
        $endereco->usuario_id = $user->id;

        //Testando se o CEP esta dentro ou não da área de atauação
        $cep = preg_replace("/[^0-9]/", "", $endereco->cep);
        $local = LocalEntrega::whereRaw('? between cep_inicial and cep_final and status = ?', [$cep, 'ativo'])->first();


        //Se o CEP estiver fora da Área de entrega
        if (!$local) {
            $referer = request()->headers->get('referer');

            $url = ($referer) ? $referer : route('conta-usuario.meus-dados');
            flash('Endereço salvo com sucesso', 'success');
            return redirect()->intended($url)->with('error', 'Não é possível usar esse endereço porque o CEP especificado ainda não é atendido pelas nossas rotas de entrega. Por favor insira outro CEP ou entre em contato concosco.');
        }

        $endereco->save();

        if (!$user->data_primeiro_acesso) {
            $user->data_primeiro_acesso = new Carbon();
            $user->save();
            return redirect()->intended(route('site.cardapio'));
        }

        $redirect = $request->get('redirect', null);

        flash('Endereço salvo com sucesso', 'success');

        if (!$redirect) {
            return redirect()->intended(route('conta-usuario.meus-dados'));
        } else {
            return redirect()->intended($redirect);
        }


    }

    public function removerEndereco($id)
    {
        $user = Auth::user();

        $endereco = Endereco::find($id);

        if ($endereco->padrao) {
            Endereco::where('usuario_id', $user->id)->first()->update(['padrao' => true]);
        }

        if ($endereco->empresa_id == null){
            // Verifica se existem referências para o endereço
            $pedido = Pedido::where('endereco_entrega_id', '=', $endereco->id)
                ->get()
            ;

            if ($pedido) {
                flash('O endereço não pode ser removido, pois já foi usado em algum pedido.', 'warning');
            } else {
                $endereco->delete();
                flash('Endereço removido com sucesso', 'success');
            }
        }
        else{
            flash('Endereços de Empresas não podem ser removidos.', 'warning');
        }

        return redirect()->intended(route('conta-usuario.meus-dados'));


    }

    public function enderecoPrincipal($id)
    {
        $user = Auth::user();

        $endereco = $this->enderecoRepository->find($id);

        Endereco::where('usuario_id', '=', $user->id)
            ->update(['padrao' => 0]);

        if($endereco->empresa_id){

            $user->empresaAtiva()->pivot->padrao = 1;
            $user->empresaAtiva()->pivot->save();

        } else {

            Endereco::where('id', '=', $id)
                ->update(['padrao' => 1]);

            if ($user->empresaAtiva()) {
                $user->empresaAtiva()->pivot->padrao = 0;
                $user->empresaAtiva()->pivot->save();
            }

        }


        flash('Endereço Padrão salvo com sucesso.', 'success');

        return redirect()->intended(route('conta-usuario.meus-dados'));


    }

    public function desvincularEmpresa($id)
    {

        //Adicionar Data Termino 

        $user = Auth::user();

        $empresaAtiva = $user->empresaAtiva();
        $empresaAtiva->pivot->dt_termino = new Carbon();
        $empresaAtiva->pivot->save();


        flash('Empresa foi desvinculada.', 'success');

        return redirect()->intended(route('conta-usuario.meus-dados'));

    }


}

