<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Http\Controllers\Admin\ConfiguracaoController;
use AsBentas\Http\Requests\ValidacaoCepRequest;
use AsBentas\Model\Combo;
use AsBentas\Repositories\ComboRepository;
use Carbon\Carbon;
use AsBentas\Model\FAQ;
use AsBentas\Model\Empresa;
use Illuminate\Http\Request;
use AsBentas\Model\Newsletter;
use AsBentas\Mail\ContatoMail;
use AsBentas\Model\ConsultaCep;
use AsBentas\Model\Configuracao;
use AsBentas\Model\LocalEntrega;
use AsBentas\Repositories\LocalEntregasRepository;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use AsBentas\Http\Requests\ContatoRequest;
use AsBentas\Model\DescontoProgressivo;
use AsBentas\Repositories\CardapioRepository;
use AsBentas\Repositories\ConfiguracaoRepository;
use AsBentas\Repositories\AcompanhamentoRepository;
use AsBentas\Repositories\CategoriaAcompanhamentoRepository;



class SiteController extends Controller
{
    const DIA_SEXTA = 5;
    const DIA_SABADO = 6;
    const DIA_DOMINGO = 0;

    private $localEntregasRepository;
    private $cardapioRepository;
    private $categoriaAcompanhamentoRepository;


    public function __construct(LocalEntregasRepository $localEntregasRepository,
                                CardapioRepository $cardapioRepository,
                                CategoriaAcompanhamentoRepository $categoriaAcompanhamentoRepository,
                                ConfiguracaoRepository $configuracaoRepository,
                                ComboRepository $comboRepository)
    {
        $this->localEntregasRepository = $localEntregasRepository;
        $this->cardapioRepository = $cardapioRepository;
        $this->categoriaAcompanhamentoRepository = $categoriaAcompanhamentoRepository;
        $this->configuracaoRepository = $configuracaoRepository;
        $this->comboRepository = $comboRepository;
    }

    public function index(LocalEntrega $locais)
    {
        $locais = $this->localEntregasRepository->findAll()->where('status', '==', 'ativo');
        return view('site.index', ['locais' => $locais]);
    }

    public function montarCardapios($primeiro, $ultimo) {


        $empresa = null;

        $diaSemana = ['segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira'];

        if (Auth::check()) {

            $user = Auth::user();
            $empresas = null;
            if ($user->empresaAtiva()) {
                $empresa = $user->empresaAtiva()->id;
            }

        }

        $acompanhamentos = $this->categoriaAcompanhamentoRepository->buscarPreco($empresa);

        $novaData = $primeiro->copy();
        $cardapios = [];

        do {

            $itens = [];
            $bebidas = [];

            $cardapioDia = \AsBentas\Model\Cardapio::whereDate('data_cardapio', '=', $novaData->toDateString())->get()->first();

            //Se não tiver cardádio setado para esse dia, skip para próximo dia
            if (!$cardapioDia) {
                $novaData->modify("+1 day");
                continue;
            }

            //Se esse dia for sábado ou domingo, skip para próximo dia
            if ($cardapioDia->dia_da_semana == '0' || $cardapioDia->dia_da_semana == '6' ){
                $novaData->modify("+1 day");
                continue;
            }

            $itens = $this->cardapioRepository->buscarPorData($novaData, $empresa);
            $bebidas = $this->cardapioRepository->buscarBebidasPorCardapio($novaData);

            $dia = ($novaData->format('w') - 1);

            $cardapios[$novaData->format('d/m')] = [
                'info' => [
                    'data' => $novaData->format('d/m'),
                    'passado' => $novaData->lte(new Carbon()),
                    'semana' => $diaSemana[$dia],
                    'status' => $cardapioDia->status
                ],
                'bebidas' => $bebidas,
                'itens' => $itens
            ];
            
            $novaData->modify("+1 day");


        } while ($novaData <= $ultimo);
        

        return $cardapios;
    }

    public function cardapio(Request $request)
    {
        $local = $request->session()->get('local', []);

        $cepInformado = false;

        if (isset($local['cep']) && strlen($local['cep']) > 0) {
            $cepInformado = true;
        }

        if (Auth::check()) {

            $user = Auth::user();

            if (! $user->data_primeiro_acesso)
            {
                return redirect()->intended(route('conta-usuario.primeiro-acesso'));
            }

        }


        //Define o primeiro dia;
        $primeiro =  Carbon::parse('today');

        //Define o último dia;
        $ultimo = Carbon::parse($this->cardapioRepository->ultimoCardapio());

        //Formatações para ordenar os cardápios
        $primeiroDia = $primeiro->format('d/m');
        $ultimoDia = $ultimo->format('d/m');

        $cardapios = $this->montarCardapios($primeiro, $ultimo);


        $empresa = null;
        if (Auth::check()) {

            $user = Auth::user();
            $empresas = null;
            if ($user->empresaAtiva()) {
                $empresa = $user->empresaAtiva()->id;
            }

        }

        $configuracoes = $this->configuracaoRepository->find(1);

        $acompanhamentos = $this->categoriaAcompanhamentoRepository->buscarPreco($empresa);

        $combos = $this->comboRepository->all();

        $jsonCombos = [];

        foreach($combos as $combo) {
            $valorCombo = (float) $combo->valor;
            if ($empresa) {
                $valorEmpresa = $combo->precoEmpresa($empresa)->get()->first();
                if ($valorEmpresa) {
                    $valorCombo = (float) $valorEmpresa->pivot->valor;
                }
            }
            $jsonCombos[$combo->id] = [];
            $jsonCombos[$combo->id]['valor'] = $valorCombo;
            $jsonCombos[$combo->id]['itens'][] = "principal";
            foreach($combo->categorias as $categoria) {
                $jsonCombos[$combo->id]['itens'][] = strtolower($categoria->nome);
            }
        }

        $descontos = [];

        foreach(DescontoProgressivo::orderBy('dia', 'asc')->get() as $desconto) {
            $descontos[$desconto->dia] = $desconto;
        }

        return view('site.cardapio', [
            'carrinho' => $request->session()->get('carrinho', []),
            'cardapios' => $cardapios,
            'acompanhamentos' => $acompanhamentos,
            'primeiro_dia' => $primeiroDia,
            'ultimo_dia' => $ultimoDia,
            'cepInformado' => $cepInformado,
            'configuracoes' => $configuracoes->valor,
            'combos' => json_encode($jsonCombos),
            'descontos' => $descontos
        ]);
    }

    public function sobre()
    {
        return view('site.sobre');
    }

    public function termosPolitica()
    {
        return view('site.termos-e-politica-de-privacidade');
    }

    public function facaSeuPedido()
    {
        return view('site.faca-seu-pedido');
    }

    public function comoFunciona()
    {
        return view('site.como-funciona');
    }

    public function contato()
    {
        return view('site.contato');
    }

    public function faq()
    {

        $faqs = FAQ::where('status', '=', 'ativo')->orderBy('ordem')->get();

        return view('site.faq', ['faqs' => $faqs]);
    }

    public function enviarMensagem(ContatoRequest $request)
    {

        $emailConfiguracao = Configuracao::where('chave', '=', 'email_contato')->first();

        Mail::to($emailConfiguracao->valor)
            ->send(new ContatoMail($request->all()));


        return back()->with('message', 'Sua mensagem foi enviada com sucesso! Agradecemos seu contato e em breve retornaremos. Abraços!');

    }

    public function newsletter(Request $request)
    {

        try {
            $nome = $request->get('nome');
            $email = $request->get('email');

            $jaExiste = Newsletter::where('email', '=',$email)->get()->count();

            if (! $jaExiste) {
                $newsletter = new Newsletter();
                $newsletter->nome = $nome;
                $newsletter->email = $email;
                $newsletter->save();
            }


            return response()->json([
                'message' => 'Cadastro realizado com sucesso'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Erro realizar cadastro.'
            ], 500);
        }

    }

    public function validarCep(ValidacaoCepRequest $request)
    {
        $email = $request->get('email');

        $cep = preg_replace("/[^0-9]/", "", $request->get('cep'));

        $local = LocalEntrega::whereRaw('? between cep_inicial and cep_final', [$cep])
            ->where('status', '=', 'ativo')
            ->first();

        if (! $local) {
            //Grava Tabela de Prospeccao
            if (strlen($email) > 0 && strlen($cep) > 0) {

                $consultas = ConsultaCep::where('email', '=', $email)
                    ->where('cep', '=', $cep)
                    ->get();

                if ($consultas->count() == 0) {


                    $consultaCep = ConsultaCep::create([
                        'email' => $email,
                        'cep'   => $cep
                    ]);
                }
            }
            return redirect()->intended(route('site.cep-recusado'));
        }

        $request->session()->put('local', [
            'email' => $email,
            'cep'   => $cep
        ]);


        return redirect()->intended(route('usuario.cadastro'));

    }

    public function cepRecusado()
    {

        $locais = $this->localEntregasRepository->findAll()->where('status', '==', 'ativo');
        return view('site.cep-recusado', ['locais' => $locais]);
    }

    public function exceptionGetValidarCep()
    {
        return redirect()->route('site.index');
    }

}
