<?php

namespace AsBentas\Http\Controllers;

use AsBentas\Repositories\LocalEntregasRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private $localEntregasRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LocalEntregasRepository $localEntregasRepository)
    {
        $this->localEntregasRepository = $localEntregasRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        return view('home');
    }

    public function validarCep($cep)
    {

        $locaisEntrega = $this->localEntregasRepository->buscarIntervalo(preg_replace('/\D/', '', $cep));

        if ($locaisEntrega->count() == 0) {
            return response()->json(['message' => 'Cep nao encontrado'], 500);
        }

        return response()->json(['message' => 'CEP Encontrado'], 200);

        //return redirect()->intended(route('site.index'));
    }
}
