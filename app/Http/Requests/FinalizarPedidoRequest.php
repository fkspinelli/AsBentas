<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FinalizarPedidoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'pin' => 'required|min:4|max:6',
            'endereco_entrega' => 'required'
        ];
    }


    public function messages()
    {
        return [
//            'pin.required' => 'O PIN deve ser informado.',
//            'pin.min' => 'O PIN deve conter entre 4 e 6 números',
//            'pin.max' => 'O PIN deve conter entre 4 e 6 números',
            'endereco_entrega.required' => 'O Endereço de Entrega deve ser informado.',
        ];
    }
}
