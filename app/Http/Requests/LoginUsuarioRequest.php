<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|max:20'
        ];
    }


    public function messages()
    {
        return [
            'email.required'  => 'O e-mail deve ser informado.',
            'email.email'  => 'Formato de e-mail inválido.',
            'password.required'  => 'A senha deve ser informada.',
            'password.max'  => 'A senha deve ter no máximo 20 caracteres.',
        ];
    }


}
