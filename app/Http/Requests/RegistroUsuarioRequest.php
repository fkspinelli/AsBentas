<?php

namespace AsBentas\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class RegistroUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:160',
            'email' => 'required|unique:users|email:max:160',
            'password' => 'password|required|confirmed|max:20',
            'password_confirmation' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'O nome deve ser informado.',
            'name.min' => 'O nome deve ter no mínimo 3 caracteres.',
            'name.max' => 'O nome deve ter no máximo 160 caracteres.',
            'email.required'  => 'O e-mail deve ser informado.',
            'email.email'  => 'Formato de e-mail inválido.',
            'email.max'  => 'O email deve ter no máximo 160 caracteres.',
            'email.unique'  => 'O e-mail informado já encontra-se cadastrado.',
            'password.required'  => 'A senha deve ser informada.',
            'password.confirmed'  => 'A senha deve ser confirmada.',
            'password.max'  => 'A senha deve ter no máximo 20 caracteres.',
            'password_confirmation.required'  => 'A confirmação de senha deve ser informada.',
        ];
    }



}
