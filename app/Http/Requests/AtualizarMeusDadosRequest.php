<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class AtualizarMeusDadosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = Auth::user();

        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' =>[
                'nullable',
                'email',
                Rule::unique('users')->ignore($user->id),
                ]
//            ,
//            'pin' => 'nullable|min:4|max:6'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'O nome deve ser informado.',
            'name.regex' => 'Por favor insira um nome válido.',
            'email.unique' => 'O email informado já esta em uso.',
//            'email.required' => 'O email deve ser informado.',
            'email.email' => 'Por favor insira um email válido.',
            'password.password' => 'A senha informada não é válida.',
            'confirm_password.password_confirmed' => 'A confirmação de senha não é válida.',
            'pin.min' => 'Verifique seu PIN',
            'pin.max' => 'Verifique seu PIN',
        ];
    }
}
