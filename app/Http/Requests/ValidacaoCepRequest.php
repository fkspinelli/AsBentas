<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacaoCepRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email:max:160',
            'cep' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cep.required' => 'Informe o CEP.',
            'cep' => 'CEP inválido',
            'email.required'  => 'O e-mail deve ser informado.',
            'email.email'  => 'Formato de e-mail inválido.',
            'email.max'  => 'O email deve ter no máximo 160 caracteres.',
        ];
    }
}
