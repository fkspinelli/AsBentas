<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContatoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email',
            'assunto' => 'required',
            'mensagem' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'Informe seu nome.',
            'email.required' => 'Informe seu e-mail',
            'email.email' => 'O e-mail informado não é valido.',
            'assunto.required' => 'Informe o Assunto',
            'mensagem.required' => 'Informe a mensagem'
        ];
    }
}
