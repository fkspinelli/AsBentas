<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'email' => 'email|nullable'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'O campo Nome/Razão Social deve ser informado.',
            'nome.max' => 'Insira um nome válido.',
            'email.email' => 'Insira um email válido',
            'telefone.required' => 'O campo telefone deve ser informado.',
            'codigo_empresa.required' => 'O campo código para validação deve ser informado.',
        ];
    }
}
