<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfirmarEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa_id' => 'required',
            'endereco_id' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'empresa_id.required' => 'A Empresa deve ser informada.',
            'endereco_id.required' => 'O Polo deve ser informado.',
        ];
    }
}
