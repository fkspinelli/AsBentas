<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdicionarEnderecoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required',
            'cep' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'cep.required' => 'O cep deve ser informado.',
            'logradouro.required' => 'O logradouro deve ser informado.',
            'numero.required' => 'O número deve ser informado.',
            'nome.required' => 'O nome deve ser informado.'
        ];
    }
}
