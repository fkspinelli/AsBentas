<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NovaSenhaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'password|required|confirmed',
            'password_confirmation' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'password.required'  => 'A senha deve ser informada.',
            'password.confirmed'  => 'A senha deve ser confirmada.',
            'password_confirmation.required'  => 'A confirmação de senha deve ser informada.',
        ];
    }
}
