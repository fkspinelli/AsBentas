<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastrarEnderecoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cep' => 'required',
            'logradouro' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'nome' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'cep.required' => 'O CEP deve ser informado.',
            'logradouro.required' => 'O logradouro deve ser informado.',
            'numero.required' => 'O número deve ser informado.',
            'bairro.required' => 'O bairro deve ser informado.',
            'cidade.required' => 'A cidade deve ser informado.',
            'estado.required' => 'O estado deve ser informado.',
            'nome.required' => 'O nome deve ser informado.'
        ];
    }
}
