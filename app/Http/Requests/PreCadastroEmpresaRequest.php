<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PreCadastroEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'contato' => 'required',
            'ddd' => 'required',
            'telefone' => 'required',
            'email' => 'required|email',
            'cep' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'O Nome da Empresa deve ser informada.',
            'contato.required' => 'O Contato deve ser informado.',
            'ddd.required' => 'O DDD deve ser informado',
            'telefone.required' => 'O Telefone deve ser informado.',
            'email.required' => 'O Email deve ser informado.',
            'email.email' => 'O formato do Email deve ser válido.',
            'cep.required' => 'O Campo CEP deve ser informado.',
        ];
    }
}
