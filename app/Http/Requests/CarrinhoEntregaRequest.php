<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarrinhoEntregaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'horario' => 'required',
            'endereco' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'horario.required' => 'O Horário deve ser informado.',
            'endereco.required'  => 'O Endereço deve ser informado.'
        ];
    }
}
