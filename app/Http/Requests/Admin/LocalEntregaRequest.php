<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class LocalEntregaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cep_inicial' => 'required|size:9',
            'cep_final' => 'required|size:9',
            'bairro' => 'required',
            'valor_entrega' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'cep_inicial.required' => 'O CEP inicial deve ser informado.',
            'cep_inicial.size' => 'O CEP inicial deve ser informado corretamente.',
            'cep_final.required'  => 'O CEP final deve ser informado.',
            'cep_final.size'  => 'O CEP final deve ser informado corretamente.',
            'bairro.required'  => 'O bairro deve ser informado',
            'valor_entrega.required'  => 'O bairro deve ser informado.',
        ];
    }
}
