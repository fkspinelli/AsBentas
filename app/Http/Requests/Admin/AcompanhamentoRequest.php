<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AcompanhamentoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|min:3',
            'categoria_id' => 'required',
            'valor_sugerido' => 'required|money',
            'status' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'O nome deve ser informado.',
            'nome.min' => 'O nome deve ser informado.',
            'categoria_id.required'  => 'A categoria deve ser informada.',
            'valor_sugerido.required'  => 'O valor sugerido deve ser informado.',
            'valor_sugerido.money'  => 'O valor sugerido não foi preenchido corretamente.',
            'status.required'  => 'A situação deve ser informada.',
        ];
    }
}
