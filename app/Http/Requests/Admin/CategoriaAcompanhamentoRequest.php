<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaAcompanhamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome' => 'required|min:3',
            'status' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'O nome deve ser informado.',
            'nome.min' => 'O nome deve ser informado.',
            'status.required'  => 'A situação deve ser informada.',
        ];
    }
}
