<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class EditarClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'password|confirmed',
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'O nome deve ser informado.',
            'name.min' => 'O nome deve ser informado.',
            'email.required'  => 'O e-mail deve ser informado.',
            'email.email'  => 'Formato de e-mail inválido.',
            'password.confirmed'  => 'A senha deve ser confirmada.',
        ];
    }
}
