<?php

namespace AsBentas\Http\Requests\Admin;

use AsBentas\Model\Cupom;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Symfony\Component\VarDumper\VarDumper;

class CupomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $tipo = $this->get('tipo');

        if($tipo == '$')
        {
            return [
                'codigo' => [
                    'required',
                    Rule::unique('cupons')->ignore($this->get('id'))
                ],
                'tipo' => 'required',
                'valor' => 'required|money',
                'data_expiracao' => 'nullable|date_format:d/m/Y|after:tomorrow'
            ];
        }
        else{
            return [
                'codigo' => [
                    'required',
                    Rule::unique('cupons')->ignore($this->get('id')),'max:40'
                ],
                'tipo' => 'required',
                'valor' => 'required|porcentagem',
                'data_expiracao' => 'nullable|date_format:d/m/Y|after:tomorrow'
            ];
        }




    }

    public function messages()
    {
        return [
            'codigo.required' => 'O código deve ser informado.',
            'codigo.unique' => 'Este código já foi cadastrado.',
            'codigo.max' => 'O código deve possuir no máximo 40 caracteres.',
            'tipo.required'  => 'O tipo deve ser informado.',
            'valor.required'  => 'O valor deve ser informado.',
            'valor.porcentagem'  => 'O valor deve ser igual ou maior a 0% e menor ou igual a 100% caso for do tipo porcentagem (%).',
            'valor.numeric'  => 'O valor deve conter apenas números.',
            'data_expiracao.date_format' => 'Insira uma data válida.',
            'data_expiracao.after' => 'Insira uma data posterior a hoje.'
        ];
    }
}
