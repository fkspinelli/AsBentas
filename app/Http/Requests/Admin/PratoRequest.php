<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PratoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return [
            'nome' => 'required',
            'descricao' => 'required',
            'icone_id' => 'required',
            'valor_sugerido' => 'required|money',
//            'codigo' => [
//                'required',
//                Rule::unique('pratos')->ignore($this->get('id'))
//            ],
            'codigo' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'nome.required' => 'O nome deve ser informado.',
            'codigo.required' => 'O código deve ser informado.',
            'codigo.unique' => 'Este código já foi cadastrado.',
            'descricao.required'  => 'A descrição deve ser informada.',
            'icone_id.required'  => 'O ícone deve ser informado.',
            'valor_sugerido.required' => 'O valor sugerido deve ser informado.',
            'valor_sugerido.money' => 'O valor sugerido deve ser válido.',

        ];
    }
}
