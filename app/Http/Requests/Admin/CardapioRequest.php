<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CardapioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_cardapio' => 'required|date_format:d/m/Y|unique:cardapios'
        ];
    }

    public function messages()
    {
        return [
            'data_cardapio.required' => 'A data deve ser informada.',
            'data_cardapio.unique' => 'Já existe um cardápio cadastrado para este dia!'
        ];
    }
}
