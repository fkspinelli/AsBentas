<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tipo' => 'required|max:50|min:3',
            'status' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tipo.required' => 'A forma de pagamento deve ser informada.',
            'tipo.max' => 'A forma de pagamento deve conter no máximo 50 caracteres.',
            'tipo.min' => 'A forma de pagamento deve conter no mínimo 3 caracteres.',
            'status.required' => 'A situação deve ser informada.',
        ];
    }
}
