<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class DescontoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dia' => 'required|numeric|max:5|min:1',
            'desconto' => 'required|porcentagem|max:7'
        ];
    }


    public function messages()
    {
        return [
            'dia.required' => 'O dia do desconto deve ser informado.',
            'dia.numeric' => 'O dia do desconto deve ser numérico.',
            'dia.max' => 'O dia deve ser menor ou igual a 5.',
            'dia.min' => 'O dia deve ser maior ou igual a 1.',
            'desconto.required'  => 'O valor do desconto deve ser informado.',
            'desconto.porcentagem' => 'O descoto deve ser igual ou maior a 0% e menor ou igual a 100%.'
        ];
    }
}
