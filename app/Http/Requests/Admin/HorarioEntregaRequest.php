<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HorarioEntregaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'horario_inicial' => 'required',
            'horario_final' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'horario_inicial.required' => 'O horário inicial deve ser informado.',
            'horario_final.required'  => 'O horário final deve ser informado.'
        ];
    }
}
