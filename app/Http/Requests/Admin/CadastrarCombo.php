<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CadastrarCombo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'valor' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'O campo nome é obrigatírio.',
            'valor.required' => 'O campo valor é obrigatírio.'
        ];
    }
}
