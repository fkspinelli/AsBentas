<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NovoClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'password|required|confirmed',
            'password_confirmation' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'O nome deve ser informado.',
            'name.min' => 'O nome deve ser informado.',
            'email.required'  => 'O e-mail deve ser informado.',
            'email.email'  => 'Formato de e-mail inválido.',
            'password.required'  => 'A senha deve ser informada.',
            'password.confirmed'  => 'A senha deve ser confirmada.',
            'password_confirmation.required'  => 'A confirmação de senha deve ser informada.',
        ];
    }

}
