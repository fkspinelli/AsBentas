<?php

namespace AsBentas\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FAQRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pergunta' => 'required|min:3',
            'resposta' => 'required|min:3',
            'ordem' => 'numeric',
        ];
    }


    public function messages()
    {
        return [
            'pergunta.required' => 'A pergunta deve ser informada.',
            'pergunta.min' => 'A pergunta deve ser informada corretamente.',
            'resposta.required'  => 'A resposta deve ser informada.',
            'resposta.min'  => 'A resposta deve ser informada corretamente.',
            'ordem.numeric'  => 'A ordem deve ser numérica.',
        ];
    }
}
