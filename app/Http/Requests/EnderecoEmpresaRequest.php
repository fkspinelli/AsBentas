<?php

namespace AsBentas\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnderecoEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomeEndereco' => 'required',
            'cep' => 'required',
            'logradouro' => 'required',
            'bairro' => 'required',
            'numero' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nomeEndereco.required' => 'O nome do endereço deve ser informado.',
            'cep.required' => 'O CEP deve ser informado.',
            'logradouro.required' => 'O logradouro deve ser informado.',
            'bairro.required' => 'O bairro deve ser informado.',
            'numero.required' => 'O número deve ser informado'
        ];
    }
}
