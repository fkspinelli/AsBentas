<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Newsletter extends Model
{
    use Sortable;

    protected $fillable = ['id', 'nome', 'email'];

    public $sortable = ['id', 'nome', 'email'];
}
