<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\VarDumper\VarDumper;
use Kyslik\ColumnSortable\Sortable;

class Pedido extends Model
{
    use Sortable;
    protected $dates = ['data_entrega', 'created_at', 'updated_at'];

    protected $fillable = [
        'id', 'status', 'endereco_entrega_id', 'user_id', 'complemento_endereco', 'valor_total',
        'data_entrega', 'horario_id', 'valor_frete','pagamento_id','pagamento','fulfillment',
        'valor_cupom_desconto', 'endereco_entrega', 'endereco_entrega_complemento', 'horario_entrega'
    ];
    public $sortable = ['id', 'created_at', 'user_id','fulfillment', 'pagamento'];

    public function getIdAttribute($value)
    {
        return str_pad($value, 8, 0, STR_PAD_LEFT);
    }

    public function usuario()
    {
        return $this->hasOne('AsBentas\Model\User', 'id', 'user_id');
    }

    public function horario()
    {
        return $this->hasOne('AsBentas\Model\HorarioEntrega', 'id', 'horario_id');
    }

    public function formaPagamento()
    {
        return $this->hasOne('AsBentas\Model\Pagamento', 'id', 'pagamento_id');
    }

    public function endereco()
    {
        return $this->hasOne('AsBentas\Model\Endereco', 'id', 'endereco_entrega_id');
    }

    public function cupons()
    {
        //return $this->belongsToMany(Cupom::class, 'cupom_pedidos', 'id', 'pedido_id');
        return $this->hasMany(CupomPedido::class, 'pedido_id');
    }

    public function pedidoCardapios()
    {
        return $this->hasMany('AsBentas\Model\PedidoCardapio', 'pedido_id')
            ->orderBy('created_at', 'desc');
    }

    public function podeCancelar(PedidoCardapio $pedidoCardapio)
    {
        if (in_array($pedidoCardapio->pedido->fulfillment, ['cancelado', 'entregue','em_entrega', 'entregue'])) {
            return false;
        }

        if ($pedidoCardapio->fulfillment == 'cancelado')
        {
            return false;
        }

        return true;

    }

    public function lablePagamento(){

        if ($this->pagamento == 'pago'){
            return "Pago";
        }

        if ($this->pagamento == 'nao_pago'){
            return "Não pago";
        }

        if ($this->pagamento == 'pago_parcialmente'){
            return "Pago parcialmente";
        }

        return null;

    }

    public function lableFulfillment(){


        if ($this->fulfillment == 'colocado'){
            return "Colocado";
        }

        if ($this->fulfillment == 'cancelado'){
            return "Cancelado";
        }

        if ($this->fulfillment == 'programado'){
            return "Programado";
        }

        if ($this->fulfillment == 'congelado'){
            return "Congelado";
        }

        if ($this->fulfillment == 'recusado'){
            return "Recusado";
        }

        if ($this->fulfillment == 'em_entrega'){
            return "Em entrega";
        }

        if ($this->fulfillment == 'entregue'){
            return "Entregue";
        }

        if ($this->fulfillment == 'entrega_recusada'){
            return "Entrega recusada";
        }

        if ($this->fulfillment == 'retornado'){
            return "Retornado";
        }

        return null;

    }

    public function lableMixFulfillment($id){



        $index = 0;
        $arrayStatus = [];

        foreach($this->pedidoCardapios as $pedidoCardapio){

            $arrayStatus[$index] = $pedidoCardapio->fulfillment;

            if($index != 0){
                if ($arrayStatus[$index] != $arrayStatus[$index-1]){


                    $statusMisto = $this->lableFulfillment() . " *";
                    return $statusMisto;
                }
            }

            $index++;
        }

        $status = $this->lableFulfillment();
        return $status;
    }

    public function cupomUtilizado()
    {
        return $this->belongsTo(CupomPedido::class, 'id', 'pedido_id');
    }

    public function valorCupom() {
        $valorCupom = 0;

        if ($this->cupons->first()) {
            $cupom = $this->cupons->first();

            if ($cupom) {
                if ($cupom->cupom->tipo == '$') {
                    $valorCupom = $cupom->cupom->valor;
                }
    
                if ($cupom->cupom->tipo == '%') {
                    $valorCupom = $this->valor_total * ($cupom->cupom->valor / 100);
                }
            }
            
        }

        return $valorCupom;
    }

    public function valorFinal() {
        $valorTotal = $this->valor_total;
        
        $valorCupom = $this->valorCupom();
        
        $valorFinal = $valorTotal - $valorCupom;

        return ($valorFinal < 0) ? 0 : $valorFinal;
        
    }


}
