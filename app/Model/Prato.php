<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Prato extends Model
{

    use Sortable;

    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }

    protected $fillable = ['id', 'icone_id', 'nome', 'codigo', 'descricao', 'valor_sugerido', 'status'];

    public $sortable = ['id','icone_id', 'codigo', 'nome','status'];


    public function cardapios()
    {
        return $this->belongsToMany(Cardapio::class);
//        ,
//            'cardapio_prato', 'prato_id', 'cardapio_id'
//        );
    }

    public function caracteristicas()
    {
        return $this->belongsToMany('AsBentas\Model\CaracteristicaPrato',
            'prato_caracteristica',
            'prato_id', 'caracteristica_id');
    }

    public function empresasPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Empresa',
            'tabela_preco_prato_empresa',
            'prato_id', 'empresa_id')
            ->withPivot('valor')
            ->withTimestamps();
    }

    public function icone()
    {
        return $this->belongsTo('\AsBentas\Model\IconePrato');
    }

    public function atualizarPreco($empresaId, $options=[])
    {

        foreach($this->empresasPreco()->get() as $empresaPreco)
        {
            if ($empresaPreco->pivot->empresa_id == $empresaId) {
                $this->removerPreco($empresaId);
            }
        }

        return $this->empresasPreco()->attach($empresaId, $options);

    }

    public function removerPreco($empresaId)
    {
        return $this->empresasPreco()->detach($empresaId);
    }

}
