<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class TabelaPrecoAcompanhamentoEmpresa extends Model
{
    protected $table = 'tabela_preco_acompanhamento_empresa';

}
