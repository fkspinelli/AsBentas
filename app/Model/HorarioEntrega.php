<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class HorarioEntrega extends Model
{
    protected $fillable = ['id', 'horario_inicial', 'horario_final', 'observacao'];

    public function label()
    {
        return "Entre {$this->horario_inicial} e {$this->horario_final}";
    }
}
