<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class UserEmpresa extends Model
{
    protected $table= 'user_empresa';

    protected $fillable = [ 'user_id', 'empresa_id', 'dt_inicio', 'dt_termino', 'created_at',
        'updated_at', 'endereco_id', 'codigo_empresa', 'padrao'];
}
