<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this
            ->belongsToMany('AsBentas\Model\User')
            ->withTimestamps();
    }
}
