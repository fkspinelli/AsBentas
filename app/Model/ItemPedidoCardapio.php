<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class ItemPedidoCardapio extends Model
{
    protected $fillable = ['id', 'pedido_cardapio_id', 'descricao', 'quantidade', 'valor'];
    protected $table = 'item_pedido_cardapios';

    public function pedido()
    {
        return $this->belongsTo('AsBentas\Model\PedidoCardapio', 'pedido_cardapio_id', 'id');
    }
}
