<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class Configuracao extends Model
{
    protected $fillable = ['id', 'grupo', 'chave', 'valor','label', 'created_at', 'updated_at'];
}
