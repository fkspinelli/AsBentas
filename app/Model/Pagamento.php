<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    //
    protected $fillable = ['id', 'tipo', 'status'];

    public function pedidos()
    {
        return $this->hasMany('AsBentas\Model\Pedido', 'pagamento_id');
    }
}
