<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Empresa extends Model
{
    use Sortable;

    protected $table = 'empresas';

    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    protected $fillable = ['id', 'nome', 'cnpj', 'contato', 'telefone', 'email', 'area_atuacao', 'cep', 'status', 'codigo_empresa','frete_gratis','frete_gratis_funcionario','valor_frete'];

    public $sortable = ['id', 'nome', 'cnpj','codigo_empresa', 'status'];

    public function enderecos()
    {
        return $this->hasMany('AsBentas\Model\Endereco',  'empresa_id', 'id');
    }

    public function pratoPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Prato',
            'tabela_preco_prato_empresa',
            'empresa_id', 'prato_id')
            ->withPivot('valor')
            ->withTimestamps();
    }

    public function acompanhamentoPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Acompanhamento',
            'tabela_preco_acompanhamento_empresa',
            'empresa_id', 'acompanhamento_id')
            ->withPivot('valor')
            ->withTimestamps();
    }

    public function comboPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Combo',
            'tabela_preco_combo_empresa',
            'empresa_id', 'combo_id')
            ->withPivot('valor')
            ->withTimestamps();
    }


    public function users()
    {
        return $this->belongsToMany('AsBentas\Model\User')->withTimestamps();
    }

}
