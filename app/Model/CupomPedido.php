<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class CupomPedido extends Model
{
    
    public function cupom() {
        return $this->belongsTo(Cupom::class, 'cupom_id');
    }

    public function usuario() {
        return $this->belongsTo(Cupom::class, 'usuario_id');
    }

    public function pedido() {
        return $this->belongsTo(Pedido::class, 'pedido_id');
    }


}
