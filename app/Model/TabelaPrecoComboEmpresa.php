<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class TabelaPrecoComboEmpresa extends Model
{
    protected $table = 'tabela_preco_combo_empresa';
}
