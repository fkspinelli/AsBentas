<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Endereco extends Model
{
    use SoftDeletes;

    protected $fillable = ['id', 'nome', 'cep', 'complemento', 'logradouro', 'numero', 'bairro', 'cidade', 'estado', 'empresa_id', 'usuario_id', 'ddd', 'telefone', 'email', 'padrao'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function label()
    {
        return $this->logradouro . ($this->numero ? ", {$this->numero}" : "") . ($this->complemento ? " - {$this->complemento}" : "") . ' - ' .  $this->bairro; //$this->logradouro . ' ' . $this->bairro;
    }

    public function resumo()
    {
        return $this->logradouro . ($this->numero ? ", {$this->numero}" : "") . ($this->complemento ? " - {$this->complemento}" : "") . ' - ' .  $this->bairro;
    }

    public function usuarios()
    {
        return $this->belongsTo(User::class, 'usuario_id', 'id');
    }

    public function usuarioEmpresa()
    {
        return $this->hasMany(UserEmpresa::class,  'endereco_id', 'id');
    }

}
