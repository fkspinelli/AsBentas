<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class PrecoPrato extends Model
{
    protected $table = 'preco_prato';

    protected $fillable = ['id', 'data_cardapio', 'nome', 'descricao', 'icone', 'css', 'valor', 'prato_id', 'ppe.empresa_id'];

    public function prato()
    {
        return $this->belongsTo('\AsBentas\Model\Prato');
    }

    public function caracteristicas()
    {
        return $this->belongsToMany('AsBentas\Model\CaracteristicaPrato',
            'prato_caracteristica',
            'prato_id', 'caracteristica_id');
    }

    public function getValorAttribute($value)
    {
        return str_replace(".",",", $value);
    }
}
