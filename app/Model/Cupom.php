<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Cupom extends Model
{
    use Sortable;

    protected $table = 'cupons';
    protected $fillable = ['id', 'codigo', 'tipo', 'valor','data_expiracao', 'tipo_cupom', 'status'];
    protected $dates = ['data_expiracao','created_at', 'updated_at'];
    public $sortable = ['id', 'codigo', 'data_expiracao','tipo_cupom', 'status'];

    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    const CUPOM_CAMPANHA = 0;
    const CUPOM_UNICO = 1;

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }

    public function getTipoCupom($tipoCupom) 
    {
        $tipos = $this->tipos();
        return $tipos[$tipoCupom]; 
    }

    public function tipos()
    {
        return [
            self::CUPOM_CAMPANHA => 'Campanha',
            self::CUPOM_UNICO => 'Único'
        ];
    }

    public function pedidos()
    {
        return $this->hasMany(CupomPedido::class, 'cupom_id', 'id');
    }

}
