<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class CaracteristicaPrato extends Model
{
    protected $fillable = ['id', 'nome', 'status'];


    public function pratos()
    {
        return $this->belongsToMany('AsBentas\Model\Prato',
            'prato_caracteristica',
            'caracteristica_id', 'prato_id');
    }
}
