<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    protected $fillable=['id','nome','valor'];


    public function categorias()
    {
        return $this->belongsToMany('AsBentas\Model\CategoriaAcompanhamento',
            'combos_categoria_acompanhamentos',
            'combo_id', 'cat_acomp_id')
            ->withTimestamps();
    }

    public function tabelaPrecoCombo()
    {
        return $this->hasMany(TabelaPrecoComboEmpresa::class);
    }

    public function empresasPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Empresa',
            'tabela_preco_combo_empresa',
            'combo_id', 'empresa_id')
            ->withPivot('valor')
            ->withTimestamps();
    }

    public function precoEmpresa($idEmpresa)
    {
        return $this->belongsToMany('AsBentas\Model\Empresa',
            'tabela_preco_combo_empresa',
            'combo_id', 'empresa_id')
            ->withPivot('valor')
            ->where('empresa_id', '=', $idEmpresa)
            ->withTimestamps();
    }

    public function atualizarPreco($empresaId, $options=[])
    {

        foreach($this->empresasPreco()->get() as $empresaPreco)
        {
            if ($empresaPreco->pivot->empresa_id == $empresaId) {
                $this->removerPreco($empresaId);
            }
        }

        return $this->empresasPreco()->attach($empresaId, $options);

    }

    public function removerPreco($empresaId)
    {
        return $this->empresasPreco()->detach($empresaId);
    }

}
