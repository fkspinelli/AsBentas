<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Cardapio extends Model
{
    use Sortable;
    protected $dates = ['data_cardapio', 'created_at', 'updated_at'];
    protected $fillable = ['id', 'data_cardapio', 'dia_da_semana', 'status'];
    public $sortable = ['id', 'data_cardapio', 'status'];

    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }

    public function pratos()
    {
        return $this->belongsToMany('AsBentas\Model\Prato',
            'cardapio_prato',
            'cardapio_id', 'prato_id')
            ->withPivot('ordem')
            ->withTimestamps();
    }

    public function bebidas()
    {
        return $this->belongsToMany('AsBentas\Model\Acompanhamento',
            'cardapio_bebidas',
            'cardapio_id', 'acompanhamento_id')
            ->withTimestamps();
    }

}
