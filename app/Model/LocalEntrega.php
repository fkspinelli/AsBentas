<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class LocalEntrega extends Model
{
    use Sortable;
    protected $fillable = ['id', 'cep_inicial', 'cep_final', 'bairro', 'valor_entrega', 'status'];
    public $sortable = ['id', 'status', 'cep_inicial','cep_final', 'bairro'];

}
