<?php

namespace AsBentas\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Symfony\Component\VarDumper\VarDumper;

class User extends Authenticatable
{
    use Notifiable;
    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'provider', 'provider_id', 'confirmed', 'confirmation_code', 'telefone', 'receber_noticias', 'pin'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $sortable = ['id', 'name', 'email', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value){

        if (!empty($value)) {
            $this->attributes['password'] = bcrypt($value);
        }

    }

    public function newsletter()
    {
        return $this->hasOne('AsBentas\Model\Newsletter', 'email', 'email');
    }

    public function roles()
    {
        return $this
            ->belongsToMany('\AsBentas\Model\Role')
            ->withTimestamps();
    }

    public function pedidos($dias = 7)
    {


        return $this
            ->hasMany('AsBentas\Model\Pedido', 'user_id');
    }

    public function empresas()
    {
        return $this->belongsToMany('AsBentas\Model\Empresa', 'user_empresa', 'user_id', 'empresa_id')
            ->withPivot('dt_inicio', 'dt_termino', 'endereco_id');
    }


    public function empresaAtivas()
    {
        return $this->belongsToMany('AsBentas\Model\Empresa', 'user_empresa', 'user_id', 'empresa_id')
            ->whereNull('dt_termino')
            ->withPivot('dt_inicio', 'dt_termino', 'endereco_id', 'codigo_empresa', 'padrao');

    }

    public function empresaAtiva()
    {
        $empresas = $this->empresaAtivas;
        if ($empresas->count() == 0) {
            return false;
        }
        return $empresas->first();
    }

    public function enderecos()
    {
        return $this->hasMany(Endereco::class, 'usuario_id', 'id');
    }


}
