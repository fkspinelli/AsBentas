<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class CategoriaAcompanhamento extends Model
{
    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }

    protected $fillable = ['id', 'nome', 'status'];

    public function acompanhamentos()
    {
        return $this->hasMany('AsBentas\Model\Acompanhamento', 'categoria_id');
    }

}
