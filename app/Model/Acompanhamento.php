<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class   Acompanhamento extends Model
{
    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    use Sortable;

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }

    protected $fillable = ['id', 'nome', 'valor_sugerido', 'status', 'categoria_id'];


    public function categoria()
    {
        return $this->belongsTo('AsBentas\Model\CategoriaAcompanhamento');
    }

    public function empresasPreco()
    {
        return $this->belongsToMany('AsBentas\Model\Empresa',
            'tabela_preco_acompanhamento_empresa',
            'acompanhamento_id', 'empresa_id')
            ->withPivot('valor')
            ->withTimestamps();
    }

    public function precoEmpresa($idEmpresa)
    {
        return $this->belongsToMany('AsBentas\Model\Empresa',
            'tabela_preco_acompanhamento_empresa',
            'acompanhamento_id', 'empresa_id')
            ->withPivot('valor')
            ->where('empresa_id', '=', $idEmpresa)
            ->withTimestamps();
    }

    public function atualizarPreco($empresaId, $options=[])
    {

        foreach($this->empresasPreco()->get() as $empresaPreco)
        {
            if ($empresaPreco->pivot->empresa_id == $empresaId) {
                $this->removerPreco($empresaId);
            }
        }

        return $this->empresasPreco()->attach($empresaId, $options);

    }

    public function removerPreco($empresaId)
    {
        return $this->empresasPreco()->detach($empresaId);
    }

}
