<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class PrecoAcompanhamento extends Model
{
    protected $table = 'preco_acompanhamento';

    protected $fillable = ['id', 'nome', 'categoria_id', 'nome_categoria', 'valor', 'empresa_id'];

    public function getValorAttribute($value)
    {
        return str_replace(".",",", $value);
    }

}
