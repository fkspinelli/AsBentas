<?php

namespace AsBentas\Model;


use Symfony\Component\VarDumper\VarDumper;

class Carrinho
{
    private $itens;

    public function addPrato($idCardapio, $dia, $idPrato, $nome, $qtd)
    {

        if (! isset($this->itens[$idCardapio])) {
            $this->itens[$idCardapio] = [
                'data' => $dia,
                'pratos' => []
            ];

            $this->itens[$idCardapio]['pratos'][] = [
                'id' => $idPrato,
                'nome' => $nome,
                'qtd'  => $qtd,
                'acompanhamentos' => []
            ];

        } else {
            $this->pratos[$idCardapio]['pratos'][$idPrato]['qtd'] = $qtd;
        }
    }

    public function addAcompanhamento($idCardapio, $idPrato, $idAcompanhamento, $nome, $qtd)
    {
        if (! isset($this->itens[$idCardapio]['pratos'][$idPrato]['acompanhamentos'][$idAcompanhamento])) {
            $this->itens[$idCardapio]['pratos'][$idPrato]['acompanhamentos'][$idAcompanhamento] = [
                'nome' => $nome,
                'qtd'   => $qtd
            ];
        } else {
            $this->itens[$idCardapio]['pratos'][$idPrato]['acompanhamentos'][$idAcompanhamento]['qtd'] = $qtd;
        }
    }

    public function getItens()
    {
        return $this->itens;
    }
}
