<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class ConsultaCep extends Model
{
    use Sortable;

    protected $fillable = ['id', 'email', 'cep'];

    public $sortable = ['id', 'email', 'cep', 'created_at'];
}
