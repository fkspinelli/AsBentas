<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class DescontoProgressivo extends Model
{
    //
    protected $fillable = ['id', 'dia', 'desconto'];

    public function setDescontoAttribute($desconto) {
        $this->attributes['desconto'] = (float) str_replace(",", ".", $desconto);
    }
}
