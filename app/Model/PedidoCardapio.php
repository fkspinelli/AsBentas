<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;

class PedidoCardapio extends Model
{
    protected $fillable = ['id', 'cardapio_id', 'pedido_id', 'canceled_at', 'canceled_by', 'status', 'valor_frete',
        'desconto', 'justificativa_cancelamento' , 'subtotal', 'fulfillment', 'pagamento'];



    public function pedido()
    {
        return $this->belongsTo(Pedido::class);
    }

    public function itens()
    {
        return $this->hasMany('AsBentas\Model\ItemPedidoCardapio', 'pedido_cardapio_id');
    }

    public function cardapio()
    {
        return $this->hasOne(Cardapio::class, 'id', 'cardapio_id');
    }

    public function lablePagamento($id){

        $pedidoCardapio = PedidoCardapio::find($id);

        if ($pedidoCardapio->pagamento == 'pago'){
            return "Pago";
        }

        if ($pedidoCardapio->pagamento == 'nao_pago'){
            return "Não pago";
        }

    return null;

    }

    public function lableFulfillment($id){

        $pedidoCardapio = PedidoCardapio::find($id);

        if ($pedidoCardapio->fulfillment == 'colocado'){
            return "Colocado";
        }

        if ($pedidoCardapio->fulfillment == 'cancelado'){
            return "Cancelado";
        }

        if ($pedidoCardapio->fulfillment == 'programado'){
            return "Programado";
        }

        if ($pedidoCardapio->fulfillment == 'congelado'){
            return "Congelado";
        }

        if ($pedidoCardapio->fulfillment == 'recusado'){
            return "Recusado";
        }

        if ($pedidoCardapio->fulfillment == 'em_entrega'){
            return "Em entrega";
        }

        if ($pedidoCardapio->fulfillment == 'entregue'){
            return "Entregue";
        }

        if ($pedidoCardapio->fulfillment == 'entrega_recusada'){
            return "Entrega recusada";
        }

        if ($pedidoCardapio->fulfillment == 'retornado'){
            return "Retornado";
        }


        return null;

    }

    public function getQtdItensImpressao()
    {

        $qtdItens = count($this->itens);

        $descontoCombo = 0;
        $descontoFrequencia = 0;

        if ($this->desconto > 0) {
            $descontoFrequencia = 1;
        }

        if ($this->desconto_combo > 0) {
            $descontoCombo = 1;
        }

        return ($qtdItens + $descontoFrequencia + $descontoCombo);

    }

}