<?php

namespace AsBentas\Model;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class FAQ extends Model
{
    use Sortable;
    protected $table = 'faqs';

    protected $fillable = ['id', 'pergunta', 'resposta', 'ordem', 'status'];
    protected $sortable = ['id', 'pergunta', 'resposta', 'ordem'];

    const STATUS_ATIVO = 'ativo';
    const STATUS_INATIVO = 'inativo';

    public function situacoes()
    {
        return [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_INATIVO => 'Inativo',
        ];
    }
}
