<?php

namespace AsBentas\Providers;

use AsBentas\Validator;
use Illuminate\Support\ServiceProvider;

class ValidatorProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */

    public function boot()
    {
        $me = $this;
        $this->app['validator']->resolver(function ($translator, $data, $rules, $messages, $customAttributes) use($me)
        {
            $messages += $me->getMessages();

            return new Validator($translator, $data, $rules, $messages, $customAttributes);
        });
    }
    protected function getMessages()
    {
        return [
            'password'   => 'O campo :attribute não é uma senha válida'
        ];
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(){}
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
