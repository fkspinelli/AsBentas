<?php

namespace AsBentas;

use Illuminate\Validation\Validator as BaseValidator;

class Validator extends BaseValidator
{
    /**
     * Valida o formato do celular junto com o ddd
     * @param string $attribute
     * @param string $value
     * @return boolean
     */
    protected function validatePassword($attribute, $value)
    {
        // if (empty($value)) return true;

        $int = (filter_var($value, FILTER_SANITIZE_NUMBER_INT));
        $str = (filter_var($value, FILTER_SANITIZE_STRING));
        preg_match('/[^0-9.]/', $value, $matches, PREG_OFFSET_CAPTURE);


        if (strlen($value) >= 6 && strlen($int) > 0 && count($matches) > 0) {
            return true;
        }

        return false;
    }

    protected function validatePorcentagem($value)
    {
        $desconto = str_replace('%','',$value);
        $desconto = str_replace('.','',$value);

        if($desconto > 100 || $desconto < 0){
            return false;
        }

        return true;

    }

    protected function validateMoney( $value)
    {
        if ( $desconto = str_replace(',','.',$value))
        {
            return true;
        }
        else{
            return false;
        }

    }

}