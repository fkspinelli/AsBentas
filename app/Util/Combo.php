<?php

namespace AsBentas\Util;

use Symfony\Component\VarDumper\VarDumper;

class Combo
{

    public static function subtrairCombos($combo, $dia) {
        $id = key($combo);

        $comboAtributes = array_keys($combo[$id]['itens']);
        $diaAtributes = array_keys($dia);

        if (count($diaAtributes) < count($comboAtributes)) {
            $dia = false;
        } else {
            foreach($comboAtributes as $atributo) {
                if ($dia) {

                    if (! isset($combo[$id]['itens'][$atributo])) {
                        $dia = false;
                    } elseif ($combo[$id]['itens'][$atributo] == 0){
                        $dia = false;
                    }
                    else {

                        if (! in_array($atributo, $diaAtributes)) {
                            $dia = false;
                        }
                        else if($dia[$atributo] < $combo[$id]['itens'][$atributo]){
                            $dia = false;
                        }
                        else {
                            $dia[$atributo] -= $combo[$id]['itens'][$atributo];
                        }
                    }
                }
            }
        }

        return $dia;

    }

    public static function formarCombo ($model, $target, $combosFormados) {

        $novoValor = Combo::subtrairCombos($model, $target);

        if (! $novoValor) {
            return ['target' => $target, 'combosFormados' => $combosFormados];
        } else {
            $combosFormados++;
            return Combo::formarCombo($model, $novoValor, $combosFormados);
        }

    }

    public function getCombos ($combos) {


//        let combos = Object.keys(as_bentas_site_pedido.combos).map(function (res) {
//                let combos = {}
//            let valor = as_bentas_site_pedido.combos[res].valor
//            Object.keys(as_bentas_site_pedido.combos[res]['itens']).forEach(function (posicao) {
//                    combos[as_bentas_site_pedido.combos[res]['itens'][posicao]] = 1
//            })
//            return {
//                    id: res,
//                combo: combos,
//                valor: valor
//            }
//        })
//
//        combos.sort(function (a, b) {
//            a = Object.keys(a.combo).length
//            b = Object.keys(b.combo).length
//            if(a > b) {
//                return -1
//            }
//            if(a < b) {
//                return 1
//            }
//            return 0
//        })
//
//        return combos
    }

    public static function calcularValor ($itens, $combos) {

        $combosFormadosPorDia = [];
        $combosFormados = [];
        $valorPorDia = [];

        $comboAux = [];

        //Ordernar por mas item
        $nowData = null;


        foreach ($combos as $id => $combo) {
            $comboAux[] = [$id => $combo];
        }

        for ( $i = 0; $i < count($comboAux); $i++){
            for ($j = 0; $j < count($comboAux); $j++){
                $iKey = key($comboAux[$i]);
                $jKey = key($comboAux[$j]);
                if(count($comboAux[$i][$iKey]['itens']) > count($comboAux[$j][$jKey]['itens'])){
                    $nowData = $comboAux[$i];
                    $comboAux[$i] = $comboAux[$j];
                    $comboAux[$j] = $nowData;
                }
            }
        }

        $combos = $comboAux;

        foreach($itens as $idDia => $diaCarrinho) {

            foreach ($combos as $i => $combo) {
                $idCombo = key($combo);

                $formouCombo = Combo::formarCombo($combo, $diaCarrinho, 0);

                $combosFormados[] = [
                    'qtd_combos' => $formouCombo['combosFormados'],
                    'id_combo' => $idCombo,
                    'desconto_por_combo' => $combo[$idCombo]['valor']
                ];


                if (! isset($valorPorDia[$idDia])) {
                    $valorPorDia[$idDia] = 0;
                }

                if ($formouCombo['combosFormados'] > 0) {
                    $valorPorDia[$idDia] += (float) $combo[$idCombo]['valor'] * (int) $formouCombo['combosFormados'];
                }



                $diaCarrinho = $formouCombo['target'];

            }

            $combosFormadosPorDia = [
                'dia' => $idDia,
                'combos' => $combosFormados,
            ];
        }
        return $valorPorDia;
    }

}