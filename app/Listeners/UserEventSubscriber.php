<?php

namespace AsBentas\Listeners;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

class UserEventSubscriber
{
    public function onUserLogin($event) {
        VarDumper::dump(Auth::user());
        VarDumper::dump($event);
        exit;
    }

    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'AsBentas\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'AsBentas\Listeners\UserEventSubscriber@onUserLogout'
        );
    }


}