<?php

namespace AsBentas\Repositories;

use AsBentas\Model\CaracteristicaPrato;

class CaracteristicaPratoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return CaracteristicaPrato::all();
    }

    public function find($id)
    {
        return CaracteristicaPrato::findOrFail($id);
    }

    public function remove($id)
    {
        return CaracteristicaPrato::findOrFail($id)->delete();
    }

    public function findAtivos()
    {
        return CaracteristicaPrato::where('status', '=', 'ativo')->get();
    }

}