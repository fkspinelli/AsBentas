<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Acompanhamento;
use AsBentas\Model\CombosCategoriaAcompanhamento;
use AsBentas\Model\PrecoAcompanhamento;
use Symfony\Component\VarDumper\VarDumper;

class ComboCategoriaAcompanhamentoRepository implements RepositoryInterface
{



    public function findAll()
    {
        return CombosCategoriaAcompanhamento::all();
    }

    public function find($id)
    {
        return CombosCategoriaAcompanhamento::findOrFail($id);
    }


    public function findByCombo($id)
    {
        return CombosCategoriaAcompanhamento::where('combo_id', '=', "$id")->first();
    }

    public function remove($id)
    {
        return CombosCategoriaAcompanhamento::findOrFail($id)->delete();
    }


}