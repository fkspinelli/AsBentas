<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Combo;

class ComboRepository implements RepositoryInterface
{

    public function all()
    {
        return Combo::all();
    }

    public function findAll()
    {
        return Combo::orderBy('id', 'desc')->paginate(10);
    }

    public function find($id)
    {
        return Combo::findOrFail($id);
    }

    public function remove($id)
    {
        return Combo::findOrFail($id)->delete();
    }

    public function buscarPreco($idEmpresa = null)
    {
        $combos = [];

        $resultado = Combo::join('categoria_acompanhamentos', 'acompanhamentos.categoria_id', '=', 'categoria_acompanhamentos.id')
            ->where('categoria_acompanhamentos.status', '=', 'ativo')
            ->where('acompanhamentos.status', '=', 'ativo')
            ->where('categoria_acompanhamentos.nome', '<>', 'Bebidas')
            ->select('acompanhamentos.*')
            ->get();

        /** @var Acompanhamento $acompanhamento */
        foreach ($resultado as $acompanhamento) {

            if ($idEmpresa) {
                $preco = $acompanhamento->precoEmpresa($idEmpresa)->get()->first();
            } else {
                $preco = null;
            }

            $acompanhamentos[$acompanhamento->categoria->nome][] = [
                'id' => $acompanhamento->id,
                'categoria_id' => $acompanhamento->categoria->id,
                'nome' => $acompanhamento->nome,
                'valor' => ($preco) ? $preco->pivot->valor : $acompanhamento->valor_sugerido,
            ];
        }

        return $acompanhamentos;
    }

}