<?php

namespace AsBentas\Repositories;

use AsBentas\Model\PrecoAcompanhamento;
use AsBentas\Model\TabelaPrecoComboEmpresa;

class TabelaDePrecoComboRepository implements RepositoryInterface
{

    public function findAll()
    {
        return TabelaPrecoComboEmpresa::all();
    }

    public function find($id)
    {
        return TabelaPrecoComboEmpresa::findOrFail($id);
    }


    public function findByCombo($id)
    {
        return TabelaPrecoComboEmpresa::where('combo_id', '=', "$id")->first();
    }

    public function remove($id)
    {
        return TabelaPrecoComboEmpresa::findOrFail($id)->delete();
    }


}