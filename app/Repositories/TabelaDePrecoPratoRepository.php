<?php

namespace AsBentas\Repositories;

use AsBentas\Model\PrecoAcompanhamento;
use AsBentas\Model\TabelaPrecoPratoEmpresa;

class TabelaDePrecoPratoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return TabelaPrecoPratoEmpresa::all();
    }

    public function find($id)
    {
        return TabelaPrecoPratoEmpresa::findOrFail($id);
    }


    public function findByCombo($id)
    {
        return TabelaPrecoPratoEmpresa::where('prato_id', '=', "$id")->first();
    }

    public function remove($id)
    {
        return TabelaPrecoPratoEmpresa::findOrFail($id)->delete();
    }

    public function deleteByPratoId($id)
    {

        return TabelaPrecoPratoEmpresa::de($id)->delete();
    }


}