<?php

namespace AsBentas\Repositories;

use AsBentas\Model\LocalEntrega;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\VarDumper;

class LocalEntregasRepository implements RepositoryInterface
{

    public function findAll()
    {
        return LocalEntrega::all();
    }

    public function find($id)
    {
        return LocalEntrega::findOrFail($id);
    }

    public function remove($id)
    {
        return LocalEntrega::findOrFail($id)->delete();
    }

    public function findAtivos()
    {
        return LocalEntrega::where('status', '=', 'ativo')->get();
    }

    public function buscarIntervalo($cep)
    {

        return LocalEntrega::
            where('cep_inicial', '<=', $cep)
            ->where('cep_final', '>=', $cep)
            ->get();

    }



}