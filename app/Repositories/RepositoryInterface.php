<?php

namespace AsBentas\Repositories;

interface RepositoryInterface {

    public function findAll();

    public function find($id);

}