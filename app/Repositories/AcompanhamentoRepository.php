<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Acompanhamento;

class AcompanhamentoRepository implements RepositoryInterface
{



    public function findAll()
    {
        return Acompanhamento::orderBy('id', 'desc')->paginate(10);
    }

    public function find($id)
    {
        return Acompanhamento::findOrFail($id);
    }

    public function buscarPorCategoria($categoria)
    {
        return Acompanhamento::select('acompanhamentos.*')
            ->join('categoria_acompanhamentos', 'acompanhamentos.categoria_id', '=', 'categoria_acompanhamentos.id')
            ->where('categoria_acompanhamentos.nome', '=', $categoria)
            ->get();
    }

    public function remove($id)
    {
        return Acompanhamento::findOrFail($id)->delete();
    }

}