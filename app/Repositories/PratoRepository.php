<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Prato;

class PratoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return Prato::orderBy('id', 'desc')->get();
    }

    public function find($id)
    {
        return Prato::findOrFail($id);
    }

    public function remove($id)
    {
        return Prato::findOrFail($id)->delete();
    }

}