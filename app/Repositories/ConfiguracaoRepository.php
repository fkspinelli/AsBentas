<?php

namespace AsBentas\Repositories;
use AsBentas\Model\Configuracao;

class ConfiguracaoRepository implements RepositoryInterface
{


    public function findAll()
    {
        return Configuracao::all();
    }

    public function find($id)
    {
        return Configuracao::findOrFail($id);
    }

    public function remove($id)
    {
        return Configuracao::findOrFail($id)->delete();
    }



}