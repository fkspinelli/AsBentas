<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Acompanhamento;
use AsBentas\Model\CategoriaAcompanhamento;
use AsBentas\Model\PrecoAcompanhamento;
use Symfony\Component\VarDumper\VarDumper;

class CategoriaAcompanhamentoRepository implements RepositoryInterface
{



    public function findAll()
    {
        return CategoriaAcompanhamento::all();
    }

    public function find($id)
    {
        return CategoriaAcompanhamento::findOrFail($id);
    }

    public function findAtivos()
    {
        return CategoriaAcompanhamento::where('status', '=', 'ativo')->get();
    }

    public function remove($id)
    {
        return CategoriaAcompanhamento::findOrFail($id)->delete();
    }

    public function buscarPreco($idEmpresa = null)
    {
        $acompanhamentos = [];

        $resultado = Acompanhamento::join('categoria_acompanhamentos', 'acompanhamentos.categoria_id', '=', 'categoria_acompanhamentos.id')
            ->where('categoria_acompanhamentos.status', '=', 'ativo')
            ->where('acompanhamentos.status', '=', 'ativo')
            ->where('categoria_acompanhamentos.nome', '<>', 'Bebidas')
            ->select('acompanhamentos.*')
            ->get();

        /** @var Acompanhamento $acompanhamento */
        foreach ($resultado as $acompanhamento) {

            if ($idEmpresa) {
                $preco = $acompanhamento->precoEmpresa($idEmpresa)->get()->first();
            } else {
                $preco = null;
            }

            $acompanhamentos[$acompanhamento->categoria->nome][] = [
                'id' => $acompanhamento->id,
                'categoria_id' => $acompanhamento->categoria->id,
                'nome' => $acompanhamento->nome,
                'valor' => ($preco) ? $preco->pivot->valor : $acompanhamento->valor_sugerido,
            ];
        }

        return $acompanhamentos;
    }


}