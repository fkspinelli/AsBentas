<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Pagamento;

class PagamentoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return Pagamento::all()->where('status', '=', 'ativo');
    }

    public function find($id)
    {
        return Pagamento::findOrFail($id);
    }

    public function remove($id)
    {
        return Pagamento::findOrFail($id)->delete();
    }

}