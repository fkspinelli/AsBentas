<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Endereco;

class EnderecoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return Endereco::all();
    }

    public function find($id)
    {
        return Endereco::find($id);
    }

    public function remove($id)
    {
        return Endereco::find($id);

    }

    public function buscarPorEmpresa($idEmpresa)
    {
        return Endereco::where('empresa_id', '=', $idEmpresa)->get()->first();
    }

}