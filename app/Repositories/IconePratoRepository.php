<?php

namespace AsBentas\Repositories;

use AsBentas\Model\IconePrato;

class IconePratoRepository implements RepositoryInterface
{

    public function findAll()
    {
        return IconePrato::all();
    }

    public function find($id)
    {
        return IconePrato::findOrFail($id);
    }

    public function remove($id)
    {
        return IconePrato::findOrFail($id)->delete();
    }

}