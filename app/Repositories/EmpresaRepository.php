<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Empresa;
use Illuminate\Support\Facades\DB;

class EmpresaRepository implements RepositoryInterface
{

    public function findAll()
    {
        return Empresa::all();
    }

    public function find($id)
    {
        return Empresa::findOrFail($id);
    }

    public function remove($id)
    {
        return Empresa::findOrFail($id)->delete();
    }

    public function findAtivas()
    {
        return Empresa::where('status', '=', 'ativo')->get();
    }

    public function buscarPrecosAcompanhamento($id)
    {
        return DB::table('acompanhamentos')
            ->join('categoria_acompanhamentos', 'categoria_acompanhamentos.id', '=', 'acompanhamentos.categoria_id')
            ->leftJoin('tabela_preco_acompanhamento_empresa', function ($q) use ($id) {
                $q->on('tabela_preco_acompanhamento_empresa.acompanhamento_id', '=', 'acompanhamentos.id');
                $q->on('tabela_preco_acompanhamento_empresa.empresa_id', '=', DB::raw($id));
            })
            ->select('acompanhamentos.id', 'acompanhamentos.nome',
                DB::raw('ifnull(tabela_preco_acompanhamento_empresa.valor, acompanhamentos.valor_sugerido) valor'),
                'categoria_acompanhamentos.nome AS categoria', 'tabela_preco_acompanhamento_empresa.empresa_id')
            ->orderBy('categoria_acompanhamentos.nome')
            ->get();
        ;

    }

    public function buscarPrecosPrato($id)
    {
        return DB::table('pratos')
            ->leftJoin('tabela_preco_prato_empresa', function ($q) use ($id) {
                $q->on('tabela_preco_prato_empresa.prato_id', '=', 'pratos.id');
                $q->on('tabela_preco_prato_empresa.empresa_id', '=', DB::raw($id));
            })
            ->select('pratos.id', 'pratos.nome',
                DB::raw('ifnull(tabela_preco_prato_empresa.valor, pratos.valor_sugerido) valor'),
                'tabela_preco_prato_empresa.empresa_id')
            ->orderBy('pratos.nome')
            ->get();
        ;

    }

}