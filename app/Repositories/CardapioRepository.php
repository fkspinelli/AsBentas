<?php

namespace AsBentas\Repositories;

use AsBentas\Model\Acompanhamento;
use AsBentas\Model\Cardapio;
use AsBentas\Model\PrecoAcompanhamento;
use AsBentas\Model\PrecoPrato;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Symfony\Component\VarDumper\VarDumper;

class CardapioRepository implements RepositoryInterface
{

    public function findAll($orderBy = null, $orderDir = null)
    {
        return Cardapio::select()->orderBy($orderBy, $orderDir)->get();
    }

    public function buscarPorMes($mes, $ano = null)
    {
        return Cardapio::sortable()->select()
            ->whereRaw('MONTH(data_cardapio) = ? ', $mes)
            ->whereRaw('YEAR(data_cardapio) = ? ', ($ano) ? $ano : date('Y'))
            ->orderBy('data_cardapio')->get();
    }

    public function find($id)
    {
        return Cardapio::findOrFail($id);
    }

    public function remove($id)
    {
        return Cardapio::findOrFail($id)->delete();
    }

    public function findAtivos()
    {
        return Cardapio::where('status', '=', 'ativo')->get();
    }

    public function ultimoCardapio()
    {
        $today =  Carbon::parse('today');

        $cardapio = DB::table('cardapios')
            ->where('status', '=', 'ativo')
            ->where('data_cardapio' ,'>=', $today)
            ->latest('data_cardapio', 'asc')
            ->first();

        return $cardapio->data_cardapio;
    }

    public function buscarIntervalo($primeiro, $ultimo)
    {

        return Cardapio::whereBetween('data_cardapio', [$primeiro, $ultimo])->get();

    }

    public function buscarPorData($dataCardapio, $idEmpresa = null)
    {

        if (! $idEmpresa) {

            $cardapios =  Cardapio::select('cardapios.*', 'cardapio_prato.ordem')
                ->where('data_cardapio', '=', $dataCardapio->toDateString())
                ->join('cardapio_prato', 'cardapio_prato.cardapio_id', '=', 'cardapios.id')
                ->leftJoin('cardapio_bebidas', 'cardapio_bebidas.cardapio_id', '=', 'cardapios.id')
                ->join('pratos', 'pratos.id', '=', 'cardapio_prato.prato_id')
                ->join('icone_pratos', 'icone_pratos.id', '=', 'pratos.icone_id')
                ->orderBy('cardapio_prato.ordem')
                ->get()
            ;
            if ($cardapios->count() == 0 ) {
                return [];
            } else {

                $pratos = [];

                foreach ($cardapios->first()->pratos as $prato) {
                    $pratos[$prato->pivot->ordem] = $prato;
                }
                ksort($pratos);
                return $pratos;
            }

        } else {
            return PrecoPrato::where('data_cardapio', '=', $dataCardapio->toDateString())
                ->where(function ($q) use ($idEmpresa) {
                    $q->where('empresa_id', '=', $idEmpresa)
                        ->orWhereNull('empresa_id');
                })
                ->orderBy('created_at')
                ->get();
        }

    }

    public function buscarBebidasPorCardapio($dataCardapio, $idEmpresa = null)
    {

        $query =  Acompanhamento::join('cardapio_bebidas', 'cardapio_bebidas.acompanhamento_id', '=', 'acompanhamentos.id')
            ->join('cardapios', 'cardapio_bebidas.cardapio_id', '=', 'cardapios.id')
            ->join('categoria_acompanhamentos', 'categoria_acompanhamentos.id', '=', 'acompanhamentos.categoria_id')
            ->where('cardapios.data_cardapio', '=', $dataCardapio->toDateString())
            ->where('categoria_acompanhamentos.nome', '=', 'Bebidas');

        if (! $idEmpresa) {

            return $query->select('acompanhamentos.*')->get();

        } else {
            return $query
                ->select('preco_acompanhamento.id, preco_acompanhamento.nome, preco_acompanhamento.valor')
                ->join('preco_acompanhamento', 'preco_acompanhamento.id', '=', 'acompanhamentos.id')
                ->get();
        }

    }

    public function store($cardapio){

        return $cardapio->save();
    }



}