<?php

namespace AsBentas\Mail;

use AsBentas\Model\DescontoProgressivo;
use AsBentas\Model\Endereco;
use AsBentas\Model\HorarioEntrega;
use AsBentas\Model\User;
use AsBentas\Model\Pedido;
use AsBentas\Repositories\EnderecoRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Symfony\Component\VarDumper\VarDumper;

class PedidoFinalizado extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $dados = [])
    {
        $this->user = $user;
        $this->dados = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $dados = $this->dados;
        $user = Auth::user();
        $pedido = $dados['pedido'];
        //$pedido->endereco = Endereco::find($pedido->endereco_entrega_id)->first();
        //dd($pedido->endereco = Endereco::find($pedido->endereco_entrega_id)->first();
        // $pedido->endereco = $pedido->endereco->lozxxgradouro.", ".$pedido->endereco->bairro." - ".$pedido->endereco->cidade;
        $pedido->horario = HorarioEntrega::find($pedido->horario_id)->first();
        $pedido->horario = "Entre ".$pedido->horario->horario_inicial." e ".$pedido->horario->horario_final;


        $itensPedido = [];

        /** @var PedidoCardapio $pedidoCardapio */
        foreach($pedido->pedidoCardapios as $pedidoCardapio)
        {
            $itensPedido[$pedidoCardapio->cardapio->data_cardapio->format('d/m/Y')] = $pedidoCardapio;

        }
        ksort($itensPedido);

        $descontos = [];

        foreach(DescontoProgressivo::orderBy('dia', 'asc')->get() as $desconto) {
            $descontos[$desconto->dia] = $desconto;
        }

        return $this->view('usuario/mail/confirmacao-compra',
            [
                'user' => $this->user,
                'dados' => $dados['carrinho'],
                'pedido' => $pedido,
                'itensPedido' => $itensPedido,
                'frete' => $dados['frete'],
                'descontos' => $descontos
            ])
            ->subject("As Bentas - Pedido Finalizado");
    }
}
