<?php

namespace AsBentas\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use AsBentas\Model\Pedido;
use AsBentas\Model\PedidoCardapio;
use Symfony\Component\VarDumper\VarDumper;

class CancelarPedido extends Mailable
{
    use Queueable, SerializesModels;

    private $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PedidoCardapio $pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('site.mail.cancelar-pedido', ['pedido' => $this->pedido])
        ->subject("Cancelamento de Item de Pedido");
    }
}
