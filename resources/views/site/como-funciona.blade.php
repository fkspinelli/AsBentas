@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Sobre As Bentas</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box-txt text-center">
                    <h4>Praticidade e sabor para sua alimentação saudável em casa ou no trabalho. <br> Quer saber como funciona? Dê uma olhada e faça agora mesmo o seu pedido!</h4>
                </div>
            </div>
        </div>

        <div class="how-it-works list">

            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>1</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-cadastro.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Faça seu cadastro no site</h4>
                    <p>Primeiro pedido com a gente? Basta se cadastrar no nosso site, informando os dados solicitados para que possamos saber exatamente onde você está.</p>
                    <p><b>Se tiver alguma dúvida, chama a gente no chat!</b></p>
                </div>
            </div>

            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>2</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-cardapio.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Veja nosso cardápio, agende sua refeição e faça seu pedido</h4>
                    <p>Temos um cardápio variado e saudável para atender todos os gostos.  Agende o recebimento da sua refeição do dia ou aproveite para planejar sua semana completa e desfrutar de uma alimentação saudável sem complicação.</p>
                    <p><b>Invista em uma semana planejada!</b></p>
                </div>
            </div>
{{-- 
            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>3</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-pedido.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Agende seu pedido</h4>
                    <p>Agora é só agendar o recebimento da sua refeição do dia ou aproveitar para planejar sua semana completa e desfrutar de uma alimentação saudável sem complicação. Lembrando que é preciso escolher todas as refeições caso opte por receber durante toda a semana!</p>
                    <p><b>É prático e você se libera para o que realmente importa.</b></p>
                </div>
            </div> --}}

            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>3</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-entrega.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Preparamos e entregamos</h4>
                    <p>Agora é com a gente! Preparamos sua refeição com ingredientes frescos, livres de aditivos, orgânicos sempre que possível, para você ter muito mais energia no seu dia. Aí, é só relaxar e esperar seu box chegar recheado de sabor.</p>
                    <p><b>Mais facilidade e mobilidade pra você.</b></p>
                </div>
            </div>

            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>4</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-pagamento-2.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Pagamento</h4>
                    <p>O pagamento é feito na entrega da sua refeição! Aceitamos todos os cartões de débito e crédito e as principais bandeiras de alimentação. Confira abaixo:</p>
                    <img src="img/logo_alelo.png" class="icon-pagamento">
                    <img src="img/logo_sodexo.png" class="icon-pagamento">
                    <img src="img/logo_ticket.png" class="icon-pagamento">
                    <img src="img/logo_vr.png" class="icon-pagamento">
                </div>
            </div>

            <div class="row row-height">
                <div class="col-sm-1 col-sm-push-3 col-height col-top">
                    <div class="number">
                        <span>5</span>
                    </div>
                    <hr>
                </div>
                <div class="col-sm-3 col-sm-pull-1 col-height col-top">
                    <img src="img/icon-aqueca-e-aproveite.png">
                </div>
                <div class="col-sm-6 col-height col-top">
                    <h4>Aqueça e aproveite!</h4>
                    <p>As refeições são entregues resfriadas para que tudo chegue até você em perfeitas condições. Elas vêm dentro de um box totalmente sustentável e pronto para ser aquecido no micro-ondas. São só 2 minutinhos e pronto! Agora você pode se deliciar com uma alimentação saudável e funcional :)</p>
                </div>
            </div>

            <div class="row row-height">
                <div class="col-md-3 col-sm-push-5 col-sm-4">
                    <a href="{{ route('site.cardapio') }}" class="btn btn-success btn-block text-uppercase">Experimente agora</a>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection