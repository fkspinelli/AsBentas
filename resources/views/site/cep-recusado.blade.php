@extends('layouts.site')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-txt text-center">
                    <h4 class="m-b-0">Não é possível usar esse endereço!</h4>
                    <p>Não é possível usar esse endereço porque o CEP especificado não é servido pelas nossas rotas de entrega. Por favor, insira outro CEP ou entre em contato conosco.</p>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 20px;"></div>
        <div class="row mapa-content m">
            <div class="col-xs-1 text-center">
                <i class="fa fa-angle-left"></i>
            </div>
            <div class="col-xs-10">
                <div class="text-center mapa-cep">
                    <?php
                        $bairros = [];
                    ?>

                    @foreach ($locais as $local)

                    <?php

                        $i= 0;
                        $acumulator = 0;
                        $bairros[] = $local->bairro;

                        for($i= 0; $i < count($bairros); $i++)
                        {
                            if($local->bairro == $bairros[$i])
                            {
                               $acumulator += 1;
                            }
                        }

                    ?>
                        @if($acumulator == 1)
                            <div class="box-map">
                                <div class="mapa">
                                    <img src="{{asset('/img/mapa.png')}}" class="img-responsive img-cinza" draggable="false">
                                    <span class="size-20">
                                            {{$local->bairro}}
                                    </span>
                                </div>
                            </div>
                        @endif
                        
                    @endforeach
                </div>
            </div>
            <div class="col-xs-1 text-center">
                <i class="fa fa-angle-right"></i>
            </div>
        </div>
        <div style="margin-bottom: 60px;"></div>
        
        <div class="col-sm-10 col-sm-push-1">
            <div>
                <form id="frm-conferir-local" action="{{ route('site.validar-cep') }}" method="post" data-parsley-validate=""
                      novalidate>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input class="form-control" name="email" placeholder="E-MAIL" >
                                @if($errors->has('email'))
                                    <span class="help-inline text-red" style="color: #ff0000 !important;">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" id="cep" class="form-control cep" name="cep" placeholder="CEP" >
                                @if($errors->has('cep'))
                                    <span class="help-inline text-red" style="color: #ff0000 !important;"> {{ $errors->first('cep') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" id="btn-conferir-local"
                                        class="btn btn-success btn-lg btn-block text-uppercase">Consultar novamente
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div style="margin-bottom: 50px;"></div>
    </div>


@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script type="text/javascript" src="/js/cep_recusado.js"></script>
    <script type="text/javascript" src="/js/pt-br.js"></script>
    <script type="text/javascript" src="/js/index.js"></script>
@endsection

