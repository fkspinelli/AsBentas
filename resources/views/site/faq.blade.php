@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url({{ url('img/banner-interna.jpg') }});">
        <div class="container">
            <h1>FAQ</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-auth text-center">
                    <h4>As principais dúvidas você resolve aqui.  ;)</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="panel-group accordion" id="accordion">
                    @foreach($faqs as $faq)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$loop->index}}">
                                    {{ $faq->pergunta }} <i class="ion-android-arrow-down"></i></a>
                            </h4>
                        </div>
                        <div id="collapse{{$loop->index}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                {!!html_entity_decode($faq->resposta)!!}

                                <i class="ion-android-arrow-up" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$loop->index}}"></i>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-auth text-center">
                    <h4>Ainda com dúvida? Utilize o formulário abaixo.</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-auth">

                    <form action="{{ route('site.enviar-mensagem') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" placeholder="Nome" name="nome" required value="{{ old('nome') }}">
                                    @if($errors->has('nome'))
                                        <span class="help-inline">{{ $errors->first('nome') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                        <span class="help-inline">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group {{ $errors->has('mensagem') ? 'has-error' : '' }}">
                                    <textarea class="form-control no-resize" rows="3" name="mensagem" 
                                              placeholder="Escreva aqui sua dúvida">{{ old('mensagem') }}</textarea>
                                    @if($errors->has('mensagem'))
                                        <span class="help-inline">{{ $errors->first('mensagem') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-sm-push-3">
                                <input type="hidden" name="assunto" value="FAQ">
                                <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">Enviar Mensagem</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

@section('stylesheets')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('javascripts')

    @if (session('message'))
        <script>
            $(document).ready(function () {
                swal(
                    'Sua mensagem foi enviada com sucesso!',
                    'Agradecemos seu contato e em breve retornaremos. Abraços!',
                    'success'
                );
            });
        </script>
    @endif
@endsection