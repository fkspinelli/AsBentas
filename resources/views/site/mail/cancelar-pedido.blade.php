@extends('layouts.email')

@section('conteudo')
    <table width="100%" align="center" valign="top" border="0" cellpadding="68" cellspacing="0" bgcolor="#ffffff" class="padding-none">
        <tr>
            <td>
                <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                    <tr>
                        <td valign="top">
                            <font face="verdana" size="5" color="#981318">CANCELAMENTO ITEM PEDIDO <br><br></font>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>Cliente</b>: {{ $pedido->pedido->usuario->name }} <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>Pedido</b>: <a href="{{ route('admin.pedido.visualizar', ['id'=> $pedido->pedido->id]) }}"> {{ $pedido->pedido->id }}</a> <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>Data para Entrega</b>: {{ $pedido->cardapio->data_cardapio->format('d/m/Y')}} <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b></b><br><br><br></font>
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection