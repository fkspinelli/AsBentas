@extends('layouts.email')

@section('conteudo')
    <table width="100%" align="center" valign="top" border="0" cellpadding="68" cellspacing="0" bgcolor="#ffffff" class="padding-none">
        <tr>
            <td>
                <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                    <tr>
                        <td valign="top">
                            <font face="verdana" size="5" color="#981318">FORMULÁRIO DE CONTATO <br><br></font>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>NOME</b> {{ $dados['nome'] }}<br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>E-MAIL</b> {{ $dados['email'] }}<br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>ASSUNTO</b> {{ $dados['assunto'] }}<br><br><br></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {{ $dados['mensagem'] }}
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection