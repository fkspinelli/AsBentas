@extends('layouts.site')

@section('conteudo')
    <!-- banner -->
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Sobre As Bentas</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-txt">
                    {{-- <h4>Boa é a refeição que te liberta e te dá mais energia!</h4>
                    <p>As Bentas é a arte de tornar possível a alimentação saudável no seu dia-a-dia.</p>
                    <p>É um lifestyle com o cotidiano mais livre e leve em que a prioridade é estimular a sua vida com qualidade. É escolher planejar e se cuidar para viver melhor o hoje, é viver o presente com atenção plena, experimentando sensações únicas que geram bem-estar, bom humor e vitalidade. <b>É buscar o agora.</b> </p>
                    <p>As Bentas é seu compromisso semanal de cuidado e respeito ao seu próprio ritmo de maneira saudável e sustentável.</p> --}}
                    <h4>Comida de verdade</h4>
                    <p>Fazemos comida de verdade com ingredientes selecionados, sem adicionar nenhum produto industrializado. Na nossa cozinha não entram caldos, temperos prontos, conservantes e aditivos. Não abrimos vidros nem latas. Utilizamos insumos simples: proteínas, legumes, grãos e verduras para preparar as refeições. </p>
                    <p>Comida saudável pode e deve ser saborosa, temperada e gostosa.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="box-image" style="background-image: url(/img/escolha-planejar.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-push-5">
                    <div class="box-text">
                        <div class="border">
                            <h4>Escolha planejar para viver melhor o hoje</h4>
                            <p>Com a vida num ritmo cada vez mais acelerado, sabemos como é complicado dar conta de tudo e ainda manter uma alimentação saudável.</p>
                            <p>As Bentas oferecem cardápios saudáveis com antecedência pra você planejar seu almoço semanal no trabalho sem precisar de correria ou dor de cabeça. Basta fazer seu pedido e ter a certeza de que todos os dias você terá comida funcional de verdade e da melhor qualidade {{--<b>para o seu corpo estar sempre no seu ritmo.</b>--}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-image" style="background-image: url(/img/mais-saude.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="box-text">
                        <div class="border">
                            {{-- <h4>Mais saúde e mais energia. Seu corpo agradece</h4>
                            <p>Quando você se planeja, poupa a si mesmo de estresse e de más decisões para o seu corpo. Melhor ainda se seu planejamento incluir uma <b>refeição mais saudável</b> e bem elaborada <b>com alimentos orgânicos</b> para atender às suas necessidades alimentares. Você ganha mais energia e aproveita uma vida mais completa <b>pra se concentrar no que realmente importa.</b></p> --}}
                            <h4>Nutrição e sabor juntos nos boxes As Bentas</h4>
                            <p>Cardápios elaborados por chefs e nutricionistas garantem para você o melhor sabor aliado à melhor composição nutricional. Elaboramos nossos pratos considerando as necessidades de micro e macronutrientes que seu organismo precisa. Queremos que você almoce e fique desperto e saciado. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-image" style="background-image: url(/img/de-onde-vem-a-comida.png);">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-sm-push-5">
                    <div class="box-text">
                        {{-- <div class="border">
                            <h4>Você sabe de onde sua comida vem? <br> Com a gente, sim!</h4>
                            <p>Além de todos os benefícios proporcionados por uma alimentação saudável, aqui você sabe exatamente <b>de onde ela vem.</b> </p>
                            <br>
                            <div class="list-icon">
                                <ul>
                                    <li>Alimentos orgânicos totalmente <b>rastreáveis</b></li>
                                    <li>Produção <b>sustentável</b></li>
                                    <li>Estímulo ao trabalho local através da compra de <b>produtores familiares</b></li>
                                </ul>
                            </div>
                        </div> --}}
                        <div class="border">
                            <h4>Gastronomia sustentável</h4>
                            <p>Na hora de elaborar nossos cardápios, perguntamos à Natureza: ¨O que temos por aí esta semana?¨. Trabalhar com produtos da safra significa melhor qualidade e sabor e aproveitar o que está já produzido e colhido. Sustentabilidade é um assunto sério para As Bentas.</p>
                            <p>Preferimos fornecedores locais, agricultura familiar e pequenos produtores. Nosso box é feito de material compostável e os talheres são de Bio-plástico de amido de milho.  Incentivamos o consumo limitado e consciente de proteína animal. O planeta em que vivemos é nossa casa e acreditamos que é nosso dever ajudar a preservá-lo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="box-image caixa">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box-text">
                        <div class="border">
                            <h4>Nós criamos dentro da caixa pra você poder pensar fora</h4>
                            <div class="list-icon">
                                <ul>
                                    <li>Box feito para conservar sua comida perfeitamente</li>
                                    <li>Feitos de materiais recicláveis</li>
                                    <li>Totalmente sustentável</li>
                                    <li>Comida fresca com ingredientes naturais e orgânicos</li>
                                    <li>Mobilidade pra você comer onde quiser</li>
                                    <li>Praticidade pra você não ter que se preocupar com sua comida</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="/img/embalagens.png">
                </div>
            </div>
        </div>
    </div> --}}
    <div class="container">
        <div class="title text-center">
            <h4>Comer é escolher. Escolha mais qualidade de vida!</h4>
        </div>
    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

