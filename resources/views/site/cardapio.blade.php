@extends('layouts.site')

@section('conteudo')
{{--     <div class="internal-banner">
        <div class="container">
            <h1>Cardápio da Semana!</h1>
        </div>
    </div> --}}

    <style type="text/css">
        nav.navbar.navbar-inverse.navbar-fixed-top {
            position: inherit;
        }

        .container.date-affix.affix + .container {
            padding-top: 100px;
        }

        section {
            min-height: 650px;
        }
    </style>
    <div class="container date-affix" data-spy="affix" data-offset-top="110">

        <div class="slick-date" style="display: none;">

            <!-- calendário -->
            @if(count($cardapios) == 0)
                <div class="alert alert-info text-center " role="alert" style="margin-top:20px">
                    Obrigado pela visita! Em breve o cardápio da próxima semana será publicado.
                </div>
            @endif
            <div class="dates clearfix">

                @foreach($cardapios as $cardapio)
                    <div class="{{ (count($cardapio['itens']) == 0) ? 'no-items' : '' }}">
                        <div class="date">
                            <span>Dia</span>
                            <span>{{ $cardapio['info']['data'] }}</span>
                            <span>{{ $cardapio['info']['semana'] }}</span>
                            <i class="ion-android-checkmark-circle"></i>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- /calendário -->
        </div>

        {{--<!-- barra semana -->--}}
        {{--<div class="week hidden-sm hidden-xs">--}}
            {{--<div class="container text-center text-uppercase">--}}
                {{--<span>Cardápio da Semana:</span> {{ $primeiro_dia }} a {{ $ultimo_dia }}--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
    <div id="menu" class="container" style="margin-top: 30px;">

        <form method="post" action="{{ url(route('carrinho.atualizar')) }}" id="form-carrinho" style="position: relative; /*min-height: 300px;*/">
        {{ csrf_field() }}

            <img src="{{asset('img/spinner.svg')}}" style="position: absolute; top: 50%; left: 50%; margin-top: -60px; margin-left: -60px;" class="spinner">

            <!-- container do slider do menu -->
            <div class="slick-menu" style="display: none;">
                @php ($i = 0)

                @forelse($cardapios as $data => $cardapio)

                <!-- menu da semana -->
                <div class="slick-menu-item" data-dia="{{ $data }}">

                    @if ($cardapio['info']['passado'])
                    <div class="alert alert-warning text-center" role="alert">
                        Aceitamos pedidos apenas com antecedência de {{$configuracoes}} horas do horário de entrega desejado.
                    </div>
                    @endif

                    @if (isset($cardapio['info']['status']) && $cardapio['info']['status'] == 'inativo')
                        <div class="alert alert-warning text-center" role="alert">
                            <!-- Infelizmente não temos entregas programadas para este dia.-->
                            Em breve pedidos online.
                        </div>
                    @endif


                    @forelse($cardapio['itens'] as $prato)
                        @php ($i++)
                        <div class="item {{ $prato->css ? $prato->css : $prato->icone->css }}" data-id-cardapio="{{ $prato->id }}"><!-- tipo de prato -->
                            <div class="border">
                                <div class="row">
                                    <div class="col-sm-1">
                                        @if((Auth::check() || $cepInformado) && $cardapio['info']['status'] == 'ativo')
                                            @if (!$cardapio['info']['passado'])
                                                <div class="check hidden-sm hidden-xs">
                                                    <input type="checkbox" class="ios8-switch" id="checkbox-{{ $i }}">
                                                    <label for="checkbox-{{ $i }}"></label>
                                                </div>
                                            @endif
                                        @endif

                                        <img src="/img/{{ is_object($prato->icone) ? $prato->icone->icone : $prato->icone }}" class="icon">
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="item-name">
                                            <div>
                                                <h3>{{ $prato->nome }}</h3>
                                                <p>{{ $prato->descricao }}</p>
                                            </div>
                                            <div>
                                                <div class="text-center">

                                                    @if(Auth::check() || $cepInformado)
                                                        <span class="price value ">R${{ str_replace(".",",", ($prato->valor_sugerido ? $prato->valor_sugerido : $prato->valor)) }}</span><!-- preço do prato principal (default) -->
                                                    @endif
                                                </div>
                                                <div class="text-center">
                                                    <span class="prev"></span>
                                                    <div>

                                                        @if((Auth::check() || $cepInformado) && $cardapio['info']['status'] == 'ativo')

                                                            @if (!$cardapio['info']['passado'])
                                                                <a class="btn btn-info btn-sm text-uppercase" data-toggle="collapse" data-target="#opt_{{ $i }}">Escolher opções</a>
                                                            @endif
                                                        {{--@else--}}
                                                            {{--<a class="btn btn-info btn-sm text-uppercase" href="{{ route('usuario.login') }}">--}}
                                                                {{--Quero esse prato--}}
                                                            {{--</a>--}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="tags">
                                            @if($prato->caracteristicas)
                                                @foreach($prato->caracteristicas as $caracteristica)
                                                <div class="tag">
                                                    <div>
                                                        <div>
                                                            {!! $caracteristica->nome !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                @if(Auth::check() || $cepInformado)

                                    <div id="opt_{{ $i }}" class="opt collapse">
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-10 col-sm-push-1">
                                            <h4>Agora defina as quantidades e os complementos :)</h4>
                                            <div data-type="principal"
                                                 data-id="{{ $prato->prato_id ? $prato->prato_id : $prato->id }}"
                                                 data-price="{{ str_replace(".",",", ($prato->valor_sugerido ? $prato->valor_sugerido : $prato->valor)) }}">
                                                <h5>Prato Principal</h5>
                                                <div class="menu-header">
                                                    <div>
                                                        <div>&nbsp;</div>
                                                        <div class="text-center">
                                                            <span>Quantidade</span>
                                                        </div>
                                                        <div class="text-center">
                                                            <span>Valor Unitário</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="item-option">
                                                    <li> <!-- preço do prato -->
                                                        <div>
                                                            <div>
                                                                <h6>{{ $prato->nome }}</h6>
                                                            </div>
                                                            <div>
                                                                <div class="integer">
                                                                    <button type="button" data-operator="less">-</button>
                                                                    <input name="prato[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id }}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}]"
                                                                           type="number" value="{{ $carrinho[$prato->pivot ? $prato->pivot->cardapio_id : $prato->id]['pratos'][$prato->prato_id ? $prato->prato_id : $prato->id]['qtd'] or '0' }}" readonly>
                                                                    <button type="button" data-operator="more">+</button>
                                                                </div>
                                                            </div>
                                                            <div class="text-center">
                                                                <span class="price-2"  data-item-price="{{ str_replace(".",",", ($prato->valor_sugerido ? $prato->valor_sugerido : $prato->valor)) }}">
                                                                    R$ {{ str_replace(".",",",($prato->valor_sugerido ? $prato->valor_sugerido : $prato->valor)) }}
                                                                    <input name="prato_valor[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id }}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}]" type="hidden" value="{{ str_replace(".",",", ($prato->valor_sugerido ? $prato->valor_sugerido : $prato->valor)) }}">
                                                                </span> <!-- preço do prato -->
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- Bebidas -->

                                            @if(count($cardapio['bebidas']) > 0)
                                            <div data-type="bebidas">
                                                <h5>Bebidas</h5>
                                                <div class="menu-header">
                                                    <div>
                                                        <div>&nbsp;</div>
                                                        <div class="text-center">
                                                            <span>Quantidade</span>
                                                        </div>
                                                        <div class="text-center">
                                                            <span>Valor Unitário</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="item-option">
                                                    @foreach($cardapio['bebidas'] as $bebida)
                                                        <li data-id="{{ $bebida->id }}"
                                                            data-id-prato="{{ $prato->prato_id ? $prato->prato_id : $prato->id }}"
                                                            data-price="{{ str_replace(".",",", ($bebida->valor) ? $bebida->valor : $bebida->valor_sugerido) }}"
                                                            data-item-type="bebidas"
                                                            >

                                                            <div>
                                                                <div>
                                                                    <h6>{{ $bebida->nome }}</h6>
                                                                </div>
                                                                <div>
                                                                    <div class="integer">
                                                                        <button type="button" data-operator="less">-</button>
                                                                        <input name="acompanhamento[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id}}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}][{{ $bebida->id }}]"
                                                                               type="number" value="{{ $carrinho[$prato->pivot ? $prato->pivot->cardapio_id : $prato->id]['pratos'][$prato->prato_id ? $prato->prato_id : $prato->id]['acompanhamentos'][$bebida->id]['qtd'] or '0' }}" readonly>
                                                                        <button type="button" data-operator="more">+</button>
                                                                    </div>
                                                                </div>
                                                                <div class="text-center">
                                                            <span class="price-2" data-item-price="{{ str_replace(".",",", ($bebida->valor) ? $bebida->valor : $bebida->valor_sugerido) }}">
                                                                R$ {{  str_replace(".",",", ($bebida->valor) ? $bebida->valor : $bebida->valor_sugerido) }}
                                                                <input name="acompanhamento_valor[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id }}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}][{{ $bebida->id }}]"
                                                                       type="hidden" value="{{($bebida->valor) ? $bebida->valor : $bebida->valor_sugerido }}">
                                                            </span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif


                                            @foreach($acompanhamentos as $nome => $acompanhamento)
                                            <div data-type="{{ strtolower($nome) }}">
                                                <h5>{{ $nome }}</h5>
                                                <div class="menu-header">
                                                    <div>
                                                        <div>&nbsp;</div>
                                                        <div class="text-center">
                                                            <span>Quantidade</span>
                                                        </div>
                                                        <div class="text-center">
                                                            <span>Valor Unitário</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="item-option">
                                                    @foreach($acompanhamento as $item)
                                                    <li data-id="{{ $item['id'] }}"
                                                        data-id-prato="{{ $prato->prato_id ? $prato->prato_id : $prato->id }}"
                                                        data-price="{{ str_replace(".",",", $item['valor']) }}"
                                                        data-item-type="{{ strtolower($nome) }}"
                                                        >

                                                        <div>
                                                            <div>
                                                                <h6>{{ $item['nome'] }}</h6>
                                                            </div>
                                                            <div>
                                                                <div class="integer">
                                                                    <button type="button" data-operator="less">-</button>
                                                                    <input name="acompanhamento[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id }}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}][{{ $item['id'] }}]"
                                                                           type="number" value="{{ $carrinho[$prato->pivot ? $prato->pivot->cardapio_id : $prato->id]['pratos'][$prato->prato_id ? $prato->prato_id : $prato->id]['acompanhamentos'][$item['id']]['qtd'] or '0' }}" readonly>
                                                                    <button type="button" data-operator="more">+</button>
                                                                </div>
                                                            </div>
                                                            <div class="text-center">
                                                                <span class="price-2" data-item-price="{{ $item['valor'] }}">
                                                                    R$ {{ str_replace(".",",",($item['valor'])) }}
                                                                    <input name="acompanhamento_valor[{{ $prato->pivot ? $prato->pivot->cardapio_id : $prato->id }}][{{ $prato->prato_id ? $prato->prato_id : $prato->id }}][{{ $item['id'] }}]"
                                                                           type="hidden" value="{{ $item['valor'] }}">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="btn-close" data-toggle="collapse" data-target="#opt_1">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 290 20">
                                    <path d="M0,20C57.65,20,102,0,145,0s86.69,20,145,20Z"/>
                                </svg>
                                <i class="ion-chevron-up"></i>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
                @empty
                @endforelse
                <!-- /menu da semana -->

            </div>
        </form>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="box-descontos">
                    <div class="box-content">
                        <div class="box-border">
                            <h3>
                                <img src="img/icon-box-descontos-left.png" class="hidden-xs">
                                Campanha Planeje sua Semana!
                                <img src="img/icon-box-descontos-right.png" class="hidden-xs">
                            </h3>
                            <p>Criamos descontos especiais para <br> compras com frequência.</p>
                            <a href="#" class="btn btn-white" data-toggle="modal" data-target="#modal-descontos">CONFIRA!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="box-descontos orange">
                    <div class="box-content">
                        <div class="box-border">
                            <h3>
                                Conheça nossos Combos!
                            </h3>
                            <p><b>SELECIONE Prato + Bebida ou <br> Prato + Bebida + Lanche</b> <br> e ganhe descontos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-descontos" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <div class="clearfix">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <h3>Descontos por frequência</h3>
            <div class="descontos">
                @foreach($descontos as $desconto)
                <div class="desc clearfix">
                    <div class="left">{{$desconto->dia}}º Dia</div>
                    <div class="right">{{number_format($desconto->desconto,0)}}%</div>
                </div>
                @endforeach
            </div>
            <div class="obs">
                Todos os descontos serão aplicados no prato do dia em questão.
            </div>
          </div>
        </div>
      </div>
    </div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
    <div class="bar-total hide">
        <div class="container">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="total">Valor Total <b>R$ <span class="value" id="value">0,00</span></b></div>
                        <span class="prev"></span>
                    </div>
                    <div class="col-lg-3  text-center" >
                        <a href="#" class="btn btn-info btn-block btn-lg text-uppercase" id="btn-continuar-comprando">
                            Continuar comprando
                        </a>
                    </div>
                    <div class="col-lg-3  text-center" >
                        <a href="#" class="btn btn-success btn-block btn-lg text-uppercase" id="btn-carrinho">
                            VER CARRINHO
                        </a>

                        <span class="btn-limpar-carrinho " data-url-limpar-carrinho="{{ route('carrinho.limpar') }}">
                            <i class="fa fa-times" aria-hidden="true"></i>LIMPAR CARRINHO
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endsection


@section('javascripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
    <script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
    <script>
        as_bentas_site_pedido.combos = <?php echo $combos; ?>
    </script>
@endsection
