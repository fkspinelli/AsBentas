@extends('layouts.site')

@section('conteudo')
    <!-- banner -->
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Termos e Política de Privacidade</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-txt">
                    <h4>Política de Privacidade e Termo de Uso</h4>

                    <p>As Bentas tem o compromisso com a privacidade e a segurança de seus clientes durante todo o processo de navegação e compra pelo site. </p>

                    <p>Os dados cadastrais dos clientes não são vendidos, trocados ou divulgados para terceiros, exceto quando essas informações são necessárias para o processo de entrega, para cobrança, ou para participação em promoções solicitadas pelos clientes.</p> 

                    <p>Seus dados pessoais são peça fundamental para que seu pedido chegue em segurança, na sua casa, de acordo com nosso prazo de entrega.</p>

                    <p>O site As Bentas utiliza cookies e informações de sua navegação (sessão do browser) com o objetivo de traçar um perfil do público que visita o site e aperfeiçoar sempre nossos serviços, produtos, conteúdos e garantir as melhores ofertas e promoções para você. </p>

                    <p>Durante todo este processo mantemos suas informações em sigilo absoluto. Para que estes dados permaneçam intactos, nós desaconselhamos expressamente a divulgação de sua senha a terceiros, mesmo a amigos e parentes.</p>

                    <p>Ao se cadastrar no site www.asbentas.com, o usuário concorda com os termos dispostos no presente documento.</p>

                    <p>O cadastro de usuário é gratuito e será necessário para finalização do processo de compra. O usuário deverá informar os dados solicitados, além de se responsabilizar integralmente pela veracidade e exatidão dos dados fornecidos.  Em caso de informações incorretas fornecidas pelo usuário, As Bentas estará isenta de qualquer responsabilidade decorrente de informações cadastrais incompletas e/ou inverídicas.</p>

                    <p>Uma vez efetuado o cadastro com sucesso, o usuário poderá acessar o portal por meio de login (e-mail de cadastro) e senha, dados estes que o usuário se compromete a não divulgar a terceiros, pois será integralmente responsável por toda e qualquer solicitação que seja feita com seu login e senha.</p>

                    <p>As alterações sobre nossa política de privacidade e termo de uso serão devidamente informadas neste espaço</p>
                </div>
            </div>
        </div>

        <div style="margin-bottom: 50px;"></div>

        <div class="row">
            <div class="col-sm-8 col-sm-push-2">
                <div class="box-txt text-center">
                    <h4>Alguma dúvida? Utilize o formulário abaixo.</h4>
                </div>
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nome">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="E-mail">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin-bottom: 30px;">
                                <textarea class="form-control no-resize" placeholder="Escreva aqui sua dúvida" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block text-uppercase">Enviar Mensagem</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

