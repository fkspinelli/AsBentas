@extends('layouts.site')

@section('conteudo')

        <div id="banner" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                {{-- <div class="item active" style="background-image: url(/img/banner-home.png);"></div> --}}
                <div class="item active" style="background-image: url(/img/banner-frango-grelhado.png);"></div>
                <div class="item" style="background-image: url(/img/banner-boxes-1.png);"></div>
                <div class="item" style="background-image: url(/img/banner-almoco.png);"></div>
            </div>

            <div class="carousel-box-form">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-5">
                            <div class="content-box-text">
                                <div class="box-text absolute-center text-center">
                                    <div class="border">
                                        <h1>Comida saudável e saborosa é possível.</h1>
                                        <p>Levamos até você diariamente, sua refeição saborosa, balanceada e saudável, preparada na hora com ingredientes selecionados, e entregue em um box prático. Coloque o seu box 2min no microondas e aproveite!</p>
                                        <br>
                                        <p>Delivery corporativo e residencial</p>
                                        <div style="margin-bottom: 35px;"></div>
                                        <form id="frm-conferir-local" action="{{ route('site.validar-cep') }}" method="post" data-parsley-validate="" novalidate>
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-push-1">
                                                    <div class="form-group">
                                                        <input class="form-control" name="email" placeholder="E-MAIL" value="{{ old('email') }}" >
                                                        @if($errors->has('email'))
                                                            <span class="help-inline text-red" style="color: #ff0000 !important;">{{ $errors->first('email') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-sm-push-1">
                                                    <div class="form-group">
                                                        <input type="text" id="cep" class="form-control cep" name="cep" placeholder="CEP" value="{{ old('cep')}}">
                                                        @if($errors->has('cep'))
                                                            <span class="help-inline text-red" style="color: #ff0000 !important;"> {{ $errors->first('cep') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-10 col-sm-push-1">
                                                    <button type="button" id="btn-conferir-local"
                                                            class="btn btn-success btn-lg btn-block text-uppercase">CONFERIR LOCAL DE ENTREGA</button>
                                                            <div class="box-auth text-center" style="margin-top: 20px;">
                                                                <p>Já possui cadastro?  <a href="http://192.168.33.21/usuario/cadastro"> Faça o login</a>.</p>
                                                            </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="how-it-works">
            <div class="block">
                <div class="container">
                    <div class="box-itens">
                        <div class="itens">
                            <div class="item">
                                <div>
                                    <div><img src="img/step-1.png" alt="Passo 1"></div>
                                    <h4>Escolha sua refeição e agende seu pedido</h4>
                                    <p>Um cardápio variado e saudável para atender todos os gostos. Peça a sua refeição do dia ou planeje sua semana.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div><img src="img/step-3-b.png" alt="Passo 3"></div>
                                    <h4>Nós Preparamos e entregamos</h4>
                                    <p>Preparamos sua refeição com ingredientes frescos e equilibrados. Aí é só relaxar e esperar seu box chegar.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <div><img src="img/step-4.png" alt="Passo 4"></div>
                                    <h4>aí é só Aquecer e aproveitar!</h4>
                                    <p>Refeições frescas para que nenhum nutriente seja perdido! Entregues dentro de um box totalmente sustentável e pronto para ser aquecido no micro-ondas.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="padding-bottom: 30px;"></div>

                    <div class="row margin-">
                        <div class="col-sm-4 col-sm-push-4">
                            <a href="{{ route('site.faca-seu-pedido') }}" class="btn btn-gray btn-block text-uppercase margin-como-funcionamos">Saiba como funciona</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Onde entregamos -->
        <div class="places" style="background-image: url(img/bg-onde-entregamos.jpg);">
            <div class="block text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-push-2">
                            <h2>Onde entregamos</h2>
                            <div class="box-text">
                                <div align="center">
                                    <div class="icon-img plant-2" style="background: url(img/tray.png) no-repeat center; width: 50px; height: 50px; display: table-row;"></div>
                                </div>
                                <br>
                                <p>Entregamos no Rio de Janeiro, atualmente nos bairros:</p>


                                <h4>
                                    @php($bairros = [])
                                    @foreach($locais as $local)
                                        @php
                                            $bairros[] = $local->bairro;
                                        @endphp
                                    @endforeach

                                    @php
                                        $bairros = implode(", ", array_unique($bairros));
                                    @endphp
                                    {{ $bairros }}
                                </h4>
                                <p>De segunda à sexta-feira (exceto feriado).</p>

                                <div class="row" style="margin-top: 40px;">
                                    <div class="col-sm-6 col-sm-push-3">
                                        <a href="{{ route('site.cardapio') }}" class="btn btn-success btn-block text-uppercase">Faça o seu pedido agora</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-as-bentas">
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2>Comer é escolher. Escolha ter uma vida mais saudável!</h2>
                            <div class="box-caixa">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <img src="{{asset('/img/box-2.png')}}" class="img-responsive">
                                    </div>
                                    <div class="col-sm-7">
                                        <h3>Solução perfeita para sua refeição diária, eventos ou sua reunião de trabalho</h3>
                                        <div class="list-icon">
                                            <ul>
                                                <li>Refeição pronta, completa, balanceada e saudável</li>
                                                <li>Box prático para você consumir em qualquer lugar </li>
                                                <li>Pratos variados para que sua refeição seja sempre um prazer</li>
                                                <li>Ingredientes selecionados, provenientes da agricultura familiar, sazonais, não super-processados e livres de aditivos</li>
                                                <li>Opções para seu paladar e sua filosofia alimentar (veggie, vegan, low-carb, diversas proteínas)</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Os benefícios -->
        <div class="benefits" style="background-image: url(/img/bg-bike.jpg);">
            <div class="block text-center">
                <div class="container">
                    <h2>Os benefícios:</h2>
                    <div class="row">
                        <div class="col-sm-10 col-sm-push-1">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="box-text text-left">
                                        <div class="border">
                                            <img src="img/heart.png">
                                            <h2>Saúde em primeiro lugar</h2>
                                            <p>Uma refeição baseada em ingredientes naturais, frescos, sem aditivos e conservantes é um dos pilares fundamentais para preservar sua saúde.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="box-text text-right">
                                        <div class="border">
                                            <img src="img/energy.png">
                                            <h2>Aumente o nível de energia</h2>
                                            <p>Quando a refeição é equilibrada e rica em ingredientes de alta qualidade, você tem mais disposição para encarar os desafios do dia a dia.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="box-text text-left">
                                        <div class="border">
                                            <img src="img/hourglass.png">
                                            <h2>Recupere seu tempo</h2>
                                            <p>Preparar seu almoço ou procurar opções saudáveis requer um bom tempo do seu dia. De forma prática e deliciosa, restituímos esse tempo para você.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="box-text text-right">
                                        <div class="border">
                                            <img src="img/seal.png">
                                            <h2>Selo As Bentas de qualidade</h2>
                                            <p>Comida saudável, sustentável e saborosa, preparada com técnicas que preservam as propriedades benéficas dos ingredientes. Nós garantimos!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Os benefícios -->

        <div class="testimonials-trustpilot">
            <div class="block">
                <div class="container">
                    <h2>O que dizem sobre nós</h2>
                    <div class="row">
                        <div class="col-xs-1">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </div>
                        <div class="col-xs-10">
                            <div class="slick-testimonials">
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/59f9c651f5cddd029469066d" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>Muito satisfeita!</h4>
                                            <p>Só tenho elogios a fazer. A comida d’As Bentas faz parte da minha rotina desde janeiro/2017, é muito variada, fresca e saborosa.</p>
                                            <p><b>ANA</b></p>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/59fbac0f3943a90650312008" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>Comida deliciosa e excelente serviço</h4>
                                            <p>Comida deliciosa e excelente serviço. Provei o feijão tropeiro bento e estava maravilhoso! </p>
                                            <p><b>Maíra Silva</b></p>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/59fc69ed31302a07249d7f66" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>De extrema boa qualidade todas as …</h4>
                                            <p>De extrema boa qualidade todas as refeições! Eu recomendo e aprovo!</p>
                                            <p><b>Sérgio Pastore</b></p>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/5a05d49ffc7e9b0048abbb7e" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>Refeições leves e saborosas</h4>
                                            <p>Refeições leves e saborosas em embalagem prática. Ótimo para o dia-a-dia. Poderiam pensar em oferecer uma saladinha de entrada.</p>
                                            <p><b>Caroline Telles</b></p>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/5a1575b804017d03f4c276a1" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>Uma marmita diferenciada, gostosa e saudável</h4>
                                            <p>Descobri As Bentas através de uma amiga e logo de cara, gostei da apresentação dos pratos.</p>
                                            <p><b>Márcia Makino</b></p>
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="https://br.trustpilot.com/reviews/5a1edef0c0c9900ddc5ae5fa" target="_blank">
                                        <div class="content">
                                            <div class="stars">
                                                <ul>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                    <li><i class="fa fa-star" aria-hidden="true"></i></li>
                                                </ul>
                                            </div>
                                            <h4>Ótima experiência</h4>
                                            <p>Comida de qualidade, bem servida e muito bem temperada. Excelente opção para quem deseja se alimentar de forma leve e saudável.</p>
                                            <p><b>Leonardo Lohmann</b></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-push-4">
                            <a href="https://br.trustpilot.com/review/asbentas.com" class="btn btn-gray btn-block text-uppercase" target="_blank" style="margin-top: 40px;">VER TODOS COMENTÁRIOS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 item-testimonials">
                        <div class="row row-height">
                            <div class="col-sm-4 col-height">
                                <div class="photo">
                                    <img src="img/foto1.png">
                                </div>
                            </div>
                            <div class="col-sm-8 col-height">
                                <p>Não podemos esquecer que o desafiador cenário globalizado promove a alavancagem das posturas.</p>
                                <p><b>Liliane Santos</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 item-testimonials">
                        <div class="row row-height">
                            <div class="col-sm-4 col-height">
                                <div class="photo">
                                    <img src="img/foto2.png">
                                </div>
                            </div>
                            <div class="col-sm-8 col-height">
                                <p>Não podemos esquecer que o desafiador cenário globalizado promove a alavancagem das posturas.</p>
                                <p><b>Roberto Sanches</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

        <div class="news text-center" style="background-image: url(/img/bg-news.png);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-push-1">
                        <p>Cadastre-se no site e fique por <dentr></dentr>o das novidades do nosso mundo saudável. <br>
                            Ao efetuar o cadastro, você ganha um brinde logo na próxima compra!</p>
                    </div>
                </div>
                <div class="box-form">
                    <form id="form-newsletter" method="post" onsubmit="return false;" data-url="{{ route('site.newsletter') }}">
                        <div class="clearfix">
                            <div class="form-group left">
                                <input name="nome" id="nome" type="text" placeholder="Nome" class="form-control" required>
                            </div>
                            <div class="form-group right">
                                <input name="email" id="email" type="email" placeholder="Email" class="form-control" required>
                            </div>
                            <button type="submit" class="transition"><img src="img/send.png"></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script type="text/javascript" src="/js/home.js"></script>


    @if (session('message'))
        <script>
            $(document).ready(function () {
                swal(
                    'Olá!',
                    'Sua empresa está em processo de cadastramento, em breve entraremos em contato!',
                    'success'
                );
            });
        </script>
    @endif

    @if (session('pre-cadastro'))

        <script>
            $(document).ready(function () {
                @if(session('pre-cadastro') == 'cep-invalido')
                    swal({
                        title: 'Olá! Vimos que seu CEP não está dentro da nossa área de atendimento no Rio de Janeiro',
                        text: 'Infelizmente no momento não estamos fazendo entregas para seu raio geográfico. Vamos efetuar o seu cadastro para avisá-lo sobre nosso processo de expansão \\o/',
                        type: 'warning'
                    }, function () {
                        document.getElementById('logout-form').submit();
                    });
                @endif
                @if(session('pre-cadastro') == 'cep-valido')
                    swal({
                        title:  'Olá!',
                        text: 'Obrigado por cadastrar sua empresa. Faremos contato em breve para enviar nossa apresentação. Abraços!',
                        type: 'success'
                    }, function () {
                        document.getElementById('logout-form').submit();
                    });
                @endif
            });
        </script>
    @endif

@endsection

