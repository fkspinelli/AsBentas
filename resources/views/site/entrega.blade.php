@extends('layouts.site')

@section('conteudo')
    <div class="container">

        <div class="row">

            <div class="col-md-10 col-md-push-1">
                <div class="box-txt text-center">
                    <h4>Escolha o horário desejado e o endereço de entrega.</h4>
                </div>
            </div>
        </div>
        <div class="box-summary">
            @include('flash::message')

            @php($enderecosInvalidos = [])
            @foreach($enderecosInativos as $enderecoInativo)
                @php
                    $enderecosInvalidos[] = $enderecoInativo->nome;
                @endphp
            @endforeach

            @php
                $enderecosInvalidos = implode(", ", array_unique($enderecosInvalidos))  . ".";
            @endphp

            @if($enderecosInvalidos != ".")
                <div class="alert alert-mini alert-warning margin-bottom-30"><!-- WARNING -->
                    <strong>Atenção!</strong> Os seguintes endereços não fazem mais parte de nossos locais de entrega: {{$enderecosInvalidos}}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-6 col-sm-push-3">
                    <form method="post" action="{{ route('carrinho.salvarEntrega') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label><i class="ion-clock"></i> Faixa de horario para entrega:</label>
                            <select class="form-control" name="horario">
                                @forelse($horarios->sortBy('horario_inicial') as $horario)
                                    <option value="{{ $horario->id }}" @if($horario->id == $entrega['horario']) selected @endif>
                                        Entre {{ $horario->horario_inicial }} e {{ $horario->horario_final }}
                                    </option>
                                @empty
                                    <option selected>Nenhum Horário Disponível</option>
                                @endforelse
                            </select>
                            @if($errors->has('horario'))
                                <span class="help-inline">{{ $errors->first('horario') }}</span>
                            @endif
                        </div>
                        <div style="margin-bottom: 50px;"></div>
                        <div class="form-group">
                            <label><i class="ion-ios-location"></i>  Entregar no endereço:</label>
                            <select class="form-control location select-address" name="endereco">
                                @forelse($enderecos as $endereco)

                                    <optgroup label="{{ $endereco->nome }}">
                                        <option value="{{ $endereco->id }}" @if($endereco->id == $entrega['endereco']) selected @endif>
                                            {{ $endereco->label() }}
                                        </option>
                                    </optgroup>

                                @empty
                                    <option value='' selected>Nenhum Endereço Disponível</option>
                                @endforelse
                            </select>
                            @if($errors->has('endereco'))
                                <span class="help-inline">{{ $errors->first('endereco') }}</span>
                            @endif
                        </div>
                        <div style="margin-bottom: 30px;"></div>
                        <div class="form-group">
                            <label>Complemento do endereço de entrega:</label>
                            <input class="form-control" name="complemento" value="{{ $entrega['complemento'] }}"
                                   placeholder="Referência para a entrega: departamento, ramal, andar, etc.">
                        </div>
                        <div class="row text-center">
                            <div class="col-sm-6 text-right">
                                <button type="button" class="btn btn-gray text-uppercase open-modal-address" data-action="add">Cadastrar um novo endereço</button>
                            </div>

                            <div class="col-sm-6 text-right">
                                <button type="submit" class="btn btn-success text-uppercase">Continuar</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Dados -->
    <div id="modalDados" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('conta-usuario.meus-dados.adicionarEndereco') }}" method="post"  data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-8 col-sm-push-2">
                                <input type="hidden" class="form-control" name="id">
                                <input type="hidden" class="form-control" name="redirect" value="{{ route('carrinho.salvarEntrega') }}">
                                <div class="form-group">
                                    <input type="text" class="form-control name nome-local" name="nome" placeholder="Nome do endereço" required
                                           data-parsley-required-message="Informe o Nome do Endereço">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control cep" name="cep" placeholder="CEP" required id="cep"
                                           data-parsley-required-message="Informe o CEP">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control rua address" name="logradouro" placeholder="Endereço" required id="logradouro"
                                           data-parsley-required-message="Informe o Endereço">
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control number" name="numero" placeholder="Nº" required
                                                   data-parsley-required-message="Informe o número">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control complement" name="complemento" placeholder="Complemento">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control neighborhood" name="bairro" placeholder="Bairro" id="bairro">
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control locality" name="cidade" placeholder="Cidade" id="cidade">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control uf" name="estado" placeholder="Estado" maxlength="2" id="estado">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control ddd" name="ddd" placeholder="DDD">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control tel phone" name="telefone" placeholder="Telefone">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="box-dados-id">
                                <input type="hidden" class="neighborhood">
                                <input type="hidden" class="locality">
                                <input type="hidden" class="uf">
                                <button type="submit" class="btn btn-success btn-block btn-lg text-uppercase save-modal-address" data-action="edit">
                                    Salvar Endereço
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pedido')
        @php($qtdPedidos = 0)
        @php($acompanhamentosIncluidos = [])
        @php($subtotal = 0)
        @php($total = 0)
        @php($dia = 1)
        @forelse($carrinho as $id => $cardapio)
            @php($subtotal = 0)
            @if($id == "cupom") @continue @endif
            @if(count($cardapio['pratos']) > 0)
                @php($qtdPedidos++)
        
                @foreach($cardapio['pratos'] as $idPrato => $item)
    
                    @foreach($item['acompanhamentos'] as $idAcompanhamento => $itemAcompanhamento)
                            @php($acompanhamentosIncluidos[] = $idAcompanhamento)
                            @php($subtotal += $itemAcompanhamento['valor'])
                                                
                    @endforeach
                @endforeach
                                    
                @if($valorFrete > 0)
                    @php($subtotal += $valorFrete)
                @endif
                
                @if(isset($descontos[$dia]) && $descontos[$dia]['desconto'] > 0)
                    @php($subtotal -= $descontos[$dia]['desconto'])
                @endif
                                    
                @php($dia++)
                
            @endif

            @php($total += $subtotal + $valorFrete)
                            
        @empty
        @endforelse


    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script type="text/javascript" src="/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/js/entrega.js"></script>
    <script>
        $(document).ready(function (){
            @if(session('error'))
                swal(
                'Ops!',
                '{{ session('error') }}',
                'warning'
            );
            @endif
        })
    </script>
@endsection

