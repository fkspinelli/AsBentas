@extends('layouts.site')

@section('conteudo')
    <!--div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Meu Carrinho</h1>
            <h2>Comer é escolher, e você pode escolher qualidade de vida e vitalidade.</h2>
            <h2>É possível comer bem no trabalho ;) </h2>
        </div>
    </div-->
    <div class="container carrinho">
        <form method="post" action="{{ url(route('carrinho.finalizar')) }}" id="form-carrinho"
              data-url-atualizar="{{ url(route('carrinho.atualizar')) }}"
              data-url-cupom="{{ url(route('carrinho.cupom')) }}">
            <input type="hidden" name="redirect" id="redirect" value="">
            <input type="hidden" name="add-item-id-cardapio" id="add-item-id-cardapio" value="">
            <input type="hidden" name="add-item-id-acompanhamento" id="add-item-id-acompanhamento" value="">
            {{ csrf_field() }}
            <div class="text-center text-top">
                <h3>Confira o seu pedido e clique em Finalizar.</h3>
            </div>
            {{--<div class="box-pin {{ $errors->has('pin') ? 'has-error' : '' }}">
                <div class="row">
                    <div class="col-sm-10 col-sm-push-1">
                        <div class="row">
                            <div class="col-sm-5">
                                <label>
                                    Cadastre um PIN para sua entrega:
                                    <a href="#" data-toggle="tooltip" title="O PIN serve para retirar seu pedido. Ele deve ter de 4 a 6 números."><i class="ion-help-circled"></i></a>
                                </label>
                                <input type="text" name="pin" id="pin" class="form-control pin"
                                       value="{{ old('pin', $pin) }}"
                                       placeholder="De 4 a 6 númeoros de sua combinação" maxlength="6">
                                @if($errors->has('pin'))
                                    <span class="help-inline has-error-pin">{{ $errors->first('pin') }}</span>
                                @endif
                            </div>
                            <div class="col-sm-7">
                                <p>Você precisará desse PIN para retirar seu pedido com o nosso entregador. Lembre-se de utilizar uma combinação fácil de números para você não esquecer.  : )</p>
                            </div>
                        </div>
                    </div>
                </div>
            {{--</div>--}}
            <div class="box-summary">
                <div class="row">
                    <div class="col-sm-10 col-sm-push-1" class="resumo-pedido">
                        <h3>Resumo do Pedido</h3>
                        <div class="row" style="margin-bottom: 40px;">
                            <div class="col-sm-8">
                                <div class="address-checkout">
                                    <label>
                                        <input type="hidden" name="endereco_entrega" value="{{ $enderecoEntregaId }}">
                                        <input type="hidden" name="horario_entrega" value="{{ $horarioEntregaId }}">
                                        <i class="ion-ios-location"></i> Entregar no endereço: <span>{{ $enderecoEntrega->resumo() }}</span>
                                        <br>
                                        <i class="ion-ios-location"></i> Complemento: <span>{{ $entrega['complemento'] }}</span>
                                        <br>
                                        <i class="ion-clock"></i> Horário: <span>{{ $horarioEntrega->label() }}</span>
                                    </label>
                                </div>
                                <div style="margin-bottom: 5px;"></div>
                                <a href="#" class="btn btn-info btn-xs btn-alterar-entrega" style="margin-left: 13px;"
                                    data-url="{{ route('carrinho.entrega') }}">
                                    Alterar Entrega
                                </a>
                            </div>
                            <div class="col-sm-3 col-sm-push-1">
                                <label style="width: 100%; margin-bottom: 20px;">
                                    <i class="fa fa-usd" style="font-size: 17px; margin: 0 1px 10px 0;"></i>
                                    Como deseja pagar?
                                    <select name="forma-pagamento" class="form-control">
                                        @foreach($pagamentos as $pagamento)
                                            <option value="{{ $pagamento->id }}"
                                                @if(isset($entrega['formaPagamento']) && $entrega['formaPagamento'] == $pagamento->id)
                                                    selected
                                                @endif
                                            >
                                                {{ $pagamento->tipo }}
                                            </option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>
                                    <i class="ion-ios-pricetag"></i>
                                    Cupom Desconto
                                    <div class="input-group" style=" margin-top: 8px; ">
                                        <input type="text" class="form-control" style="text-transform: uppercase;" id='cupom' name="cupom"
                                        @if(isset($carrinho['cupom'])) value='{{ $carrinho['cupom']->codigo }}' @endif
                                        >
                                        <div class="input-group-btn">
                                            <button class="btn btn-default ativarCupom" type="button"><i class="ion-arrow-right-c"></i></button>
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>

                        @include('flash::message')
                        
                        @php($qtdPedidos = 0)
                        @php($acompanhamentosIncluidos = [])
                        @php($subtotal = 0)
                        @php($total = 0)
                        @php($dia = 1)
                        @forelse($carrinho as $id => $cardapio)
                            @php($subtotal = 0)
                            @if($id == "cupom") @continue @endif
                            @if(count($cardapio['pratos']) > 0)
                                @php($qtdPedidos++)
                                <div class="summary-day" id="dia_{{ ($dia - 1) }}" data-id="{{ $id }}">
                                    <div class="bar-image text-uppercase size-14 clearfix">Pedido para o dia {{ $cardapio['data']->format('d/m/Y') }}
                                        <i class="ion-trash-a btn-remover-item-carrinho"
                                            data-url="{{ route('carrinho.removerItem', ['id' => $id])  }}"></i>
                                    </div>
                                    <div class="menu-header">
                                        <div>
                                            <div class="text-left">
                                                <span>Ítens</span>
                                            </div>
                                            <div class="text-center ">
                                                <span>Quantidade</span>
                                            </div>
                                            <div class="text-center">
                                                <span>Valor Unitário</span>
                                            </div>
                                            <div class="text-center">
                                                <span>Valor Total</span>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="item-option">

                                        {{--Loop para preencher a lista de pratos principais!--}}
                                        @foreach($cardapio['pratos'] as $idPrato => $item)
                                            @if(isset($item['valor']))
                                            <li data-item-price1="{{ $item['valor'] }}" data-type="principal">
                                                <div>
                                                    <div>
                                                        <h6>{{ $item['nome'] }}</h6>
                                                    </div>
                                                    <div>
                                                        <div class="integer">
                                                            <button type="button" data-operator="less">-</button>
                                                            <input name="prato[{{ $id }}][{{ $idPrato }}]"
                                                                type="number" value="{{ $item['qtd'] }}" readonly="">
                                                            <button type="button" data-operator="more">+</button>
                                                        </div>
                                                    </div>
                                                    <div class="text-right">
                                                        <span class="price-2" data-item-price="{{ $item['valor'] }}"> <span class="tit-mob visible-sm visible-xs">Valor Unitário </span> R$ {{ $item['valor'] }}</span>
                                                        <input name="prato_valor[{{ $id }}][{{ $idPrato }}]" class="valor"
                                                            type="hidden" value="{{ $item['valor'] }}">
                                                    </div>
                                                    <div class="text-right">
                                                        <span class="price-2"><span class="tit-mob visible-sm visible-xs">Valor Total </span> R$ <span class="price-2 valor-total">0,00</span></span>
                                                    </div>
                                                </div>
                                            </li>
                                            @endif
                                            {{--Loop para preencher a lista de acompanhamentos!--}}
                                            @foreach($item['acompanhamentos'] as $idAcompanhamento => $itemAcompanhamento)
                                                @php($acompanhamentosIncluidos[] = $idAcompanhamento)
                                                <li data-item-price1="{{ $itemAcompanhamento['valor'] }}" data-type="{{ strtolower($itemAcompanhamento['tipo']) }}">
                                                    <div>
                                                        <div>
                                                            <h6>{{ $itemAcompanhamento['nome'] }}</h6>
                                                        </div>
                                                        <div class="integer">
                                                            <button type="button" data-operator="less">-</button>
                                                            <input name="acompanhamento[{{ $id }}][{{ $idPrato }}][{{ $idAcompanhamento }}]"
                                                                type="number" value="{{ $itemAcompanhamento['qtd'] }}" readonly="">
                                                            <button type="button" data-operator="more">+</button>
                                                        </div>
                                                        <div>
                                                            <div class="text-right">
                                                                <span class="price-2" data-item-price="{{ $itemAcompanhamento['valor'] }}"><span class="tit-mob visible-sm visible-xs">Valor Unitário </span> R$ {{ number_format($itemAcompanhamento['valor'], 2, ',' , '') }}</span>
                                                                <input name="acompanhamento_valor[{{ $id }}][{{ $idPrato }}][{{ $idAcompanhamento }}]"
                                                                    type="hidden" value="{{ $itemAcompanhamento['valor'] }}" class="valor">
                                                            </div>
                                                        </div>
                                                        <div class="text-right">
                                                            <span class="price-2"><span class="tit-mob visible-sm visible-xs">Valor Total </span> R$ <span class="price-2 valor-total">0,00</span></span>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endforeach
                                    </ul>
                                    <div class="btns clearfix div-acompanhamentos">
                                        @php($htmlBebidas = "")

                                        @if(isset($bebidas[$cardapio['data']->format('d/m')]) )
                                            @foreach($bebidas[$cardapio['data']->format('d/m')] as $acompanhamento)
                                                {{--@if(!in_array($acompanhamento['id'], $acompanhamentosIncluidos))--}}
                                                    @php
                                                        $htmlBebidas .= "<li>
                                                                <a href=\"#\" class=\"add-item-carrinho\" data-id=\"" .  $id . "\" data-id-acompanhamento=\"" .  $acompanhamento['id'] . "\">
                                                                " .  $acompanhamento['nome'] . "
                                                                </a>
                                                            </li>";
                                                    @endphp
                                                {{--@endif--}}
                                            @endforeach
                                        @endif

                                        @if(strlen($htmlBebidas) > 0)
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i> Bebidas</a>
                                                <ul class="dropdown-menu">
                                                    {!! $htmlBebidas !!}
                                                </ul>
                                            </div>
                                        @endif

                                        @php($htmlAcompanhamentos = "")


                                        @foreach($acompanhamentos as $categoria => $itens)
                                            @foreach($itens as $key => $acompanhamento)
                                                {{--@if(!in_array($acompanhamento['id'], $acompanhamentosIncluidos))--}}
                                                    @php
                                                        $htmlAcompanhamentos .= "<li>
                                                            <a href=\"#\" class=\"add-item-carrinho\" data-id=\"" . $id . "\" data-id-acompanhamento=\"" . $acompanhamento['id'] . "\">
                                                                " . $acompanhamento['nome'] . "
                                                            </a>
                                                        </li>";
                                                    @endphp
                                                {{--@endif--}}
                                            @endforeach
                                        @endforeach

                                        @if(strlen($htmlAcompanhamentos) > 0)
                                            <div class="dropdown">
                                                <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i>  {{$categoria}}</a>
                                                <ul class="dropdown-menu">
                                                    {!! $htmlAcompanhamentos !!}
                                                </ul>
                                            </div>
                                        @endif

                                    </div>
                                    
                                    <table class="box-total box-total-b" data-item-price="{{ $valorFrete }}" data-type="frete">
                                        <tr style="display: none">
                                            <td>Desconto Combo</td>
                                            <td><b>- R$ <span class='desconto-combo'></span></b></td>
                                        </tr>
                                        @if(isset($descontos[$dia]) && $descontos[$dia]['desconto'] > 0)
                                        <tr data-item-price="{{ $descontos[$dia]['desconto'] }}" data-type="desconto">
                                            {{--({{number_format($descontos[$dia]['desconto'], 0, ',' , '')}}%)--}}
                                            <td>Desconto Frequência</td>
                                            <td><b>- R$ <span class='desconto-item-pedido' id="desconto-item-pedido-{{ $id }}"></span></b></td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td>Frete</td>
                                            <td><b>R$ {{ number_format($valorFrete, 2, ',' , '') }}</b></td>
                                        </tr>

                                        <tr>
                                            <td>Valor do Dia</td>
                                            <td><b>R$ <span class='total-item-pedido' id="total-item-pedido-{{ $id }}"></span></b></td>
                                        </tr>
                                    </table>
                                    @php($dia++)
                                </div>
                            @endif

                                @php($total += $subtotal + $valorFrete)
                            
                        @empty
                        @endforelse
                    
                        @if($dia <= 5)
                        <div class="div-info-promo" style=" margin: 0; ">
                            <div class="menu-header">
                            
                            </div>
                            <ul class="item-option">
                            
                            </ul>
                            <div class="info" style=" color: #aa231f; font-size: 16px; ">Você sabia? Pedindo mais um dia você ganha {{ number_format($descontos[$dia]->desconto, 0) }}% de desconto \o/</div>
                            
                        </div>
                        @php(++$dia)
                        @endif
                        <hr>

                        <table class="box-total">
                            @php($valorCupom = 0)
                            @if(isset($carrinho['cupom']))
                            <tr>
                                <td>Cupom Desconto @if ($carrinho['cupom']->tipo == '%')({{  number_format($carrinho['cupom']->valor,0) }}%)@endif</td>
                                <td>- R$ <span class='cupom-pedido'></span></td>
                            </tr>
                            @endif
                            <tr data-item-type="@if(isset($carrinho['cupom'])) {{ $carrinho['cupom']->tipo }} @else - @endif" data-item-price="@if(isset($carrinho['cupom'])) {{$carrinho['cupom']->valor }} @else 0 @endif" data-type="cupom">
                                <td><b>Valor Total do Pedido</b></td>
                                <td ><b>R$ <span class='total-pedido'></span></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
    <div class="bar-total show">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-3">
                            <a href="{{ route('site.cardapio') }}" class="btn btn-info btn-block btn-lg text-uppercase size-14 btn-continue" style="margin-bottom: 5px;">
                                Continuar Comprando
                            </a>
                        </div>
                        <div class="col-md-3 col-md-push-3 col-xs-7">
                            <div class="total">Valor Total <b>R$ <span class="value"></span></b></div>
                            <span class="prev">Pedidos para os dias: </span>
                        </div>
                        <div class="col-md-3 col-md-push-3 col-xs-5 text-center">
                            @if($qtdPedidos > 0)
                            <a href="#" class="btn btn-success btn-block btn-lg text-uppercase" id="btn-carrinho">
                                Finalizar Pedido
                                <span class="hidden-xs">  <i class="ion-arrow-right-c"></i></span>
                            </a>
                            <span class="btn-limpar-carrinho text-center" data-url-limpar-carrinho="{{ route('carrinho.limpar') }}">
                                <i class="fa fa-times" aria-hidden="true"></i>LIMPAR CARRINHO
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endsection

@section('javascripts')
    <script>
        as_bentas_site_pedido.combos = <?php echo $combos; ?>
    </script>
    <script type="text/javascript" src="/js/carrinho.js"></script>
@endsection