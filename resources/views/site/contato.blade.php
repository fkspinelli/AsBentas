@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Contato</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-auth text-center">
                    <h4>Está com alguma dúvida? Utilize o formulário abaixo para entrar em contato conosco.</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-auth">

                    <form action="{{ route('site.enviar-mensagem') }}" method="post">
                        <div class="row">
                            <div class="col-sm-4 col-sm-push-4">
                                {{ csrf_field() }}

                                <div class="form-group {{ $errors->has('nome') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" placeholder="Nome" name="nome" required value="{{ old('nome') }}">
                                    @if($errors->has('nome'))
                                        <span class="help-inline">{{ $errors->first('nome') }}</span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                        <span class="help-inline">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('assunto') ? 'has-error' : '' }}">
                                    <select class="form-control" name="assunto">
                                        <option selected disabled>Assunto</option>
                                        <option value="Sugestão">Sugestão</option>
                                        <option value="Dúvida">Dúvida</option>
                                        <option value="Elogio">Elogio</option>
                                        <option value="Reclamação">Reclamação</option>
                                        <option value="Outros">Outros</option>
                                    </select>
                                    @if($errors->has('assunto'))
                                        <span class="help-inline">{{ $errors->first('assunto') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('mensagem') ? 'has-error' : '' }}">
                                    <textarea class="form-control" rows="5" name="mensagem" placeholder="Escreva aqui sua dúvida" required>{{ old('mensagem') }}</textarea>
                                    @if($errors->has('mensagem'))
                                        <span class="help-inline">{{ $errors->first('mensagem') }}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">
                                    Enviar Mensagem
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

@section('javascripts')
    @if (session('message'))
    <script>
        $(document).ready(function () {
            swal(
                'Sua mensagem foi enviada com sucesso!',
                'Agradecemos seu contato e em breve retornaremos. Abraços!',
                'success'
            );
        });
    </script>
    @endif
@endsection