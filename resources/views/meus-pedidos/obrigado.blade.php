@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url({{ url('/img/banner-interna.jpg') }});">
        <div class="container">
            <h1>Bom Apetite!</h1>
        </div>
    </div>
    <div class="container">
        <div class="text-center text-top">
            <h3>Obrigado! Seu pedido {{-- <a href="{{ url(route('meus-pedidos.resumo', ['id'=> $pedido])) }}" style=" font-size: 20px; "><b>{{ $pedido }}</b></a> --}} foi realizado com sucesso!</h3>
            {{--<h4>Por favor, anote o número abaixo e forneça ao nosso entregador no ato da entrega.</h4>--}}
        </div>
        {{--<div class="row">--}}
            {{--<div class="col-md-6 col-md-push-3">--}}
                {{--<div class="box-pin show-pin">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-10 col-md-push-1">--}}
                            {{--<h4>PIN: {{ $pin }}</h4>--}}
                            {{--<p><i>(No ato da entrega será debitado o valor total da sua compra)</i></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-sm-10 col-sm-push-1">
                <hr>

                <div style="height: 30px;"></div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="box-icon-text">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="ion-clock cl-666666"></i>
                                </div>
                                <div class="col-xs-10">
                                    <p>Seu pedido está programado e você poderá encontrá-lo na página de <a href="{{ route('meus-pedidos.index') }}">Meus Pedidos</a> caso precise efetuar cancelamento.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="box-icon-text">
                            <div class="row">
                                <div class="col-xs-2">
                                    <i class="ion-alert-circled cl-ffcc44"></i>
                                </div>
                                <div class="col-xs-10">
                                    <p><b>ATENÇÃO:</b> Cancelamentos de pedidos só poderão ser realizados com <b>{{ $conf }} horas de antecedência</b>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="height: 60px;"></div>

                <!-- div class="alert alert-info" style="margin-bottom: 50px;">
                    <div class="row">
                        <div class="col-sm-1 col-xs-2">
                            <i class="icon icon-ecommerce-wallet"></i>
                        </div>
                        <div class="col-sm-11 col-xs-10">
                            <p>Você sabia que é possível comprar créditos para sua carteira virtual? Isso pode agilizar o processo de pagamento do seu próximo pedido. <a href="#" target="_blank">Clique aqui</a> e conheça como funciona   : )</p>
                        </div>
                    </div>
                </div -->

                <h2 class="cl-95191a size-30 text-center">Bom Almoço!</h2>

                <div style="height: 35px;"></div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-md-push-2 col-sm-6">
                <a href="{{ url(route('site.termosPolitica')) }}" class="btn btn-gray btn-lg-2 btn-block text-uppercase btn-xs-margin-bottom" target="_blank">termos e condições</a>
            </div>
            <div class="col-md-3 col-md-push-4 col-sm-6">
                <a href="{{ url(route('meus-pedidos.resumo', ['id'=> $pedido])) }}#print" target="_blank" class="btn btn-gray btn-lg-2 btn-block text-uppercase btn-xs-margin-bottom">imprimir meu pedido</a>
            </div>
        </div>
    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection