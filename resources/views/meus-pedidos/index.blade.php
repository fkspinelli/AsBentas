@extends('layouts.site')

@section('conteudo')
    {{-- <div class="internal-banner" style="background-image: url(img/banner-interna.jpg);">
        <div class="container">
            <h1>Meus Pedidos</h1>
            <h2>Comer é escolher, e você pode escolher qualidade de vida e vitalidade.</h2>
            <h2>É possível comer bem no trabalho ;) </h2>
        </div>
    </div> --}}
    <div class="container">
        <div class="text-center text-top">
            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <h3>Nesse espaço você tem acesso a todo o histórico dos seus pedidos no site As Bentas. Fique totalmente à vontade para acompanhar os status ou mesmo fazer algum cancelamento.</h3>

                    <div class="alert alert-warning" style="margin-bottom: 50px;">
                        <div class="row">
                            <div class="col-sm-1 col-xs-2">
                                <i class="ion-alert-circled"></i>
                            </div>
                            <div class="col-sm-11 col-xs-10 text-left">
                                <p><b>Mas lembre-se:</b> cancelamentos de pedidos só poderão ser realizados com <br> <b>{{ $qtd_hora_cancelamento }} horas de antecedência</b>.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-push-1">
                <hr style="margin: 0 0 30px;">
            </div>
        </div>

        <div class="row">
            <div class="col-sm-10 col-sm-push-1">
                @if(session('pedido-cancelado'))
                    <div class="alert alert-info" style="margin-bottom: 50px;">
                        <div class="row">
                            <div class="col-sm-1 col-xs-2">
                                <i class="icon icon-basic-sheet-txt"></i>
                            </div>
                            <div class="col-sm-11 col-xs-10">
                                <p>Seu Pedido de número <b>{{ session('pedido-cancelado') }}</b> foi cancelado com sucesso! Que tal escolher um novo prato para esta data? Temos outras opções saudáveis e apetitosas para você   ; )</p>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-filter clearfix" style="margin: 30px 0;" >
                    <div>
                        <h4>Mostrar Pedidos</h4>
                    </div>
                    <div>
                        <div class="btn-group btn-group-justified">
                            <a href="{{ route('meus-pedidos.index') }}#meus-pedidos" class="btn btn-default @if($dias == "7")) active @endif">
                                7 dias
                            </a>
                            <a href="{{ route('meus-pedidos.index') }}?dias=15#meus-pedidos" class="btn btn-default @if($dias == "15")) active @endif">
                                15 dias
                            </a>
                            <a href="{{ route('meus-pedidos.index') }}?dias=30#meus-pedidos" class="btn btn-default @if($dias == "30")) active @endif">
                                30 dias
                            </a>
                        </div>
                    </div>
                </div>

                <table class="table-resumo" id="meus-pedidos">
                    <thead>
                    <tr>
                        <th>Número do Pedido</th>
                        <th>Data do Pedido</th>
                        <th>Valor R$</th>
                        <th>Situação</th>
                        <th>Pagamento</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($statusMisto = 0)

                    @forelse ($pedidos as $pedido )
                        
                        <tr>
                            <td><a href="{{ url(route('meus-pedidos.resumo', ['id'=> $pedido->id])) }}">{{ $pedido->id }}</a></td>
                            <td>{{ $pedido->created_at->format('d/m/Y') }}</td>
                            <td>{{ number_format($pedido->valor_total, 2, ',', '') }}</td>
                            <td>{{ $pedido->lableMixFulfillment($pedido->id) }} </td>
                            <td>{{ $pedido->lablePagamento($pedido->id)}} </td>
                        </tr>

                    <?php
                        $pattern = "/*/";
                        $subject = $pedido->lableMixFulfillment($pedido->id);
                        if(strstr($subject, '*'))
                        {
                            $statusMisto = 1;
                        }
                    ?>


                    @empty
                        <tr>
                            <td colspan="5">
                                Nenhum pedido realizado
                            </td>
                        </tr>
                    @endforelse

                    @if($statusMisto == 1)
                        <tr>
                            <td colspan="5" style="text-align: left !important"> <small>* Esse pedido possui itens com variação de status.</small></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection