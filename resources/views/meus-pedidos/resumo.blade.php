@extends('layouts.site')

@section('conteudo')
    <div class="container">
        <img src="/img/logo-black.png" class="visible-print" alt="As Bentas">
    </div>

    {{-- <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Meus Pedidos</h1>
            <h2>Comer é escolher, e você pode escolher qualidade de vida e vitalidade.</h2>
            <h2>É possível comer bem no trabalho ;) </h2>
        </div>
    </div> --}}
    <div class="container meus-pedidos">
        <div class="text-center text-top">
            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <h3>Nesse espaço você tem acesso a todo o histórico dos seus pedidos no site As Bentas. Fique totalmente à vontade para acompanhar os status ou mesmo fazer algum cancelamento.</h3>

                    <div class="alert alert-warning" style="margin-bottom: 50px;">
                        <div class="row">
                            <div class="col-sm-1 col-xs-2">
                                <i class="ion-alert-circled"></i>
                            </div>
                            <div class="col-sm-11 col-xs-10 text-left">
                                <p><b>Mas lembre-se:</b> cancelamentos de pedidos só poderão ser realizados com <br> <b>{{ $qtd_hora_cancelamento }} @if ($qtd_hora_cancelamento <2 ) hora @else horas @endif de antecedência</b>.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-push-1">
                <hr style="margin-top: 0;">
            </div>
        </div>
            <div class="box-summary bg-transparent" style="background-color: #FFFFFF !important;">
            <div class="row resumo-pedido">
                <div class="col-sm-10 col-sm-push-1">

                    <div class="topbar">
                        <div>
                            <h3>Resumo do Pedido</h3>
                        </div>
                        <div>
                            <a href="{{ route('meus-pedidos.index') }}" class="btn btn-gray btn-block text-uppercase"><i class="icon icon-basic-sheet-txt"></i>  lista de pedidos</a>
                        </div>
                    </div>

                    <table class="table-resumo" id="resumo">
                        <thead>
                        <tr>
                            <th>Número do Pedido</th>
                            <th>Data do Pedido</th>
                            <th>Valor R$</th>
                            <th>Cupom</th>
                            <th>Pagamento</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{ $pedido->id }}</td>
                            <td>{{ $pedido->created_at->format('d/m/Y') }}</td>
                            <td>{{ number_format($pedido->valor_total, 2, ',', '') }}</td>
                            <td>{{ number_format($pedido->valor_cupom_desconto, 2, ',', '') }}</td>
                            <td>{{ $pedido->lablePagamento($pedido->id) }}</td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="row" style="margin-bottom: 30px;">
                        <div class="col-sm-6">
                            <label><i class="ion-ios-location"></i> Entregar no endereço:</label>
                            <input class="form-control" value="{{ $pedido->endereco_entrega }}" readonly>
                        </div>
                        <div class="col-sm-6">
                            <label>Complemento do endereço de entrega:</label>
                            <input class="form-control" value="{{ $pedido->complemento_endereco }}" readonly>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 30px;">
                        <div class="col-sm-6">
                            <label><i class="ion-clock"></i> Faixa de horario para entrega:</label>
                            <input class="form-control" value="{{$pedido->horario_entrega}}" readonly>
                        </div>

                        <div class="col-sm-6">
                            <label><i class="ion-social-usd"></i> Forma de pagamento</label>
                            <input class="form-control" value="{{$pedido->formaPagamento->tipo}}" readonly>
                        </div>
                    </div>
                    @foreach($itensPedido as $pedidoCardapio )
                        @if($pedidoCardapio->cardapio)
                        <div class="summary-day">

                            <div class="bar-image text-uppercase size-14 clearfix">

                                Pedido para o dia {{ $pedidoCardapio->cardapio->data_cardapio->format('d/m/Y') }}
                            </div>

                            <div class="row" style="margin-bottom: 30px;">
                                <div class="col-sm-6">
                                    <label>Situação:</label>
                                    <input class="form-control" value="{{$pedidoCardapio->lableFulfillment($pedidoCardapio->id)}}" readonly>
                                </div>

                                <div class="col-sm-6">
                                    <label></i></i> Pagamento</label>
                                    <input class="form-control" value="{{$pedidoCardapio->lablePagamento($pedidoCardapio->id)}}" readonly>
                                </div>
                            </div>



                            <div class="menu-header">
                                <div class="row">
                                    <div class="text-left">
                                        <span>Ítens</span>
                                    </div>
                                    <div class="text-center">
                                        <span>Quantidade</span>
                                    </div>
                                    <div class="text-right">
                                        <span>Valor</span>
                                    </div>
                                    <div class="text-right">
                                        <span>Valor Total</span>
                                    </div>
                                </div>
                            </div>
                            <ul class="item-option">

                               @foreach($pedidoCardapio->itens as $item)
                                <li data-item-price="{{ $item->valor }}" data-item-type="drinks">
                                    <div>
                                        <div>
                                            <h6>{{ $item->descricao }}</h6>
                                        </div>
                                        <div>
                                            <div class="integer">
                                                <input type="number" value="{{ $item->quantidade }}" readonly="" disabled>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <span class="price-2" data-item-price="{{ number_format(($item->valor / $item->quantidade), 2, ',','')  }}"><span class="tit-mob visible-sm visible-xs">Valor Unitário </span> R$ {{ number_format(($item->valor / $item->quantidade), 2, ',','')  }}</span>
                                        </div>
                                        <div class="text-right">
                                            <span class="price-2"><span class="tit-mob visible-sm visible-xs">Valor Total </span>
                                                R$ <span class="price-2 valor-total"></span></span>
                                        </div>
                                    </div>
                                </li>

                                @endforeach

                            </ul>

                            <table class="box-total" style="background-color: #FFFFFF !important;" data-type="frete">

                                @if(isset($pedidoCardapio->desconto_combo) && $pedidoCardapio->desconto_combo > 0)
                                    <tr>
                                        <td>Desconto Combo</td>
                                        <td><b>-R$ {{number_format($pedidoCardapio->desconto_combo, 2, ',','')}} </b></td>
                                    </tr>

                                @endif
                                @if(isset($pedidoCardapio->desconto) && $pedidoCardapio->desconto > 0)
                                    <tr>
                                         <td>Desconto Frequência</td>
                                         <td><b>- R$ {{number_format($pedidoCardapio->desconto, 2, ',','')}} </b></td>
                                    </tr>

                                @endif


                                <tr>
                                    <td>Frete</td>
                                    <td><b>R$ {{ number_format($pedidoCardapio->valor_frete, 2, ',','') }} </b></td>
                                </tr>

                                <tr>
                                    <td>Valor do Dia</td>
                                    <td>
                                        <b>R$ {{number_format($pedidoCardapio->subtotal, 2, ',','')}}</b>

                                    </td>
                                </tr>

                            </table>

                            @if($pedido->podeCancelar($pedidoCardapio))
                                <div class="clearfix">
                                    <a class="btn btn-danger btn-sm text-uppercase btn-cancelar-pedido pull-right" data-url-exclusao="{{ route('meus-pedidos.cancelar-item', ['id' => $pedidoCardapio->id]) }}"
                                       data-url="{{ route('meus-pedidos.cancelar-item', ['id' => $pedido->id]) }}">
                                    Cancelar pedido &nbsp; <i class="ion-trash-a"></i>
                                    </a>
                                </div>
                            @else
                                <div class="alert alert-danger" style="margin-bottom: 50px;">
                                    <div class="row">
                                        <p style="color: #000">Atenção item cancelado!</p>
                                    </div>
                                </div>
                            @endif

                        </div>
                        @endif
                    @endforeach

                    <hr>

                    <table class="box-total">

                        @if($pedido->valor_cupom_desconto > 0)
                        <tr>
                            <td>Cupom Desconto</td>
                            <td> - R$ {{ number_format($pedido->valor_cupom_desconto, 2, ',', '') }}</td>
                        </tr>
                        @endif

                        <tr>
                            <td><b>Valor Total do Pedido</b></td>
                            <td><b>R$ {{ number_format($pedido->valor_total, 2, ',', '.') }}</b></td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        @media print {
            * {
                color: #000 !important;
                text-decoration: none !important;
            }

            body {
                position: inherit !important;
                width: auto !important;
                height: auto !important;
                top: inherit !important;
                left: inherit !important;
            }
            section {
                padding: 0 !important;
                background-color: #fff !important;
            }

            header,
            .internal-banner,
            .text-top,
            .topbar > div:nth-child(2),
            .bottom,
            footer,
            hr,
            .bar-image .ion-trash-a,
            .ion-help-circled {
                display: none !important;
            }

            .navbar-brand {
                margin: 0 !important;
                width: inherit !important;
                padding-left: 0 !important;
            }

            .visible-print {
                display: block !important;
                margin-bottom:  50px !important;
            }

            .navbar {
                background-color: #fff !important;
                height: auto !important;
            }

            .navbar > .container .navbar-brand {
                box-shadow: none !important;
                -webkit-box-shadow: none !important;
                background-color: #fff !important;
                position: inherit  !important;
                margin: 0  !important;
            }

            .navbar-fixed-bottom, .navbar-fixed-top {
                position: inherit  !important;
            }

            .col-sm-10.col-sm-push-1 {
                width: 100%  !important;
                left: 0  !important;
            }

            table { border-collapse: separate !important; }
            td {border-top: 2px solid #aaaaaa !important; border-bottom: 2px solid #aaaaaa !important; background-color: #fff !important;}
            td:first-child { border-top-left-radius: 6.5px !important; border-left: 2px solid #aaaaaa !important;}
            td:last-child { border-top-right-radius: 6.5px !important; border-right: 2px solid #aaaaaa !important;}
            tr:last-child td:first-child { border-bottom-left-radius: 6.5px !important; border-left: 2px solid #aaaaaa !important;}
            tr:last-child td:last-child { border-bottom-right-radius: 6.5px !important; border-right: 2px solid #aaaaaa !important;}


            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                background-color: #fff !important;
                border: none !important;
            }

            .alert-2,
            .item-option li {
                border-radius: 6.5px !important;
                border: 2px solid #aaaaaa !important;
                background-color: #fff !important;
            }

            .bar-image {
                background: none !important;
            }

            .integer input,
            .item-option .price-2 {
                color: #666666 !important;
            }

            .bar-image {
                font-weight: 600 !important;
            }

            .container {
                width: 1040px !important;
                max-width: inherit !important;
            }

            .item-option > li > div > div:nth-child(1) {
                width: 60% !important;
                float: left !important;
            }
            .item-option > li > div > div:nth-child(2), .item-option > li > div > div:nth-child(3) {
                width: 20% !important;
                text-align: center !important;

            }

            .item-option > li > div > div {
                float: left !important;
            }

            .col-sm-6 {
                width: 50% !important;
                float: left !important;
            }

            .bg-white{
                background-color: #FFFFFF !important;
            }

            .bg-white{
                background-color: #FFFFFF !important;
            }
            .alert-x{
                color: #a94442 !important;
                background-color: #f2dede;
                border-color: #ebccd1;
            }



        }
    </style>

@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection


@section('javascripts')
    <script type="text/javascript" src="/js/meus_pedidos_resumo.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
@endsection