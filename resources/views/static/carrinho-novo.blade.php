@extends('layouts.site')

@section('conteudo')

         <div class="container">
            <div class="text-center text-top">
               <h3>Confira o seu pedido e clique em Finalizar. <b>Bom Apetite!</b></h3>
            </div>
            <div class="box-pin">
               <div class="row">
                  <div class="col-sm-10 col-sm-push-1">
                     <div class="row">
                        <div class="col-sm-5">
                           <label>Cadastre um PIN para sua entrega:   <a href="#" data-toggle="tooltip" title="O PIN serve para retirar seu pedido. Ele deve ter de 4 a 6 números."><i class="ion-help-circled"></i></a></label>
                           <input type="" class="form-control pin" placeholder="De 4 a 6 númeoros de sua combinação">
                        </div>
                        <div class="col-sm-7">
                           <p>Você precisará desse PIN para retirar seu pedido com o nosso entregador. Lembre-se de utilizar uma combinação fácil de números para você não esquecer.  : )</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="box-summary">
               <div class="row">
                  <div class="col-sm-10 col-sm-push-1">
                     
                     <h3>Resumo do Pedido</h3>
                     <div class="row" style="margin-bottom: 40px;">
                        <div class="col-sm-8">
                            <div>
                                <label>
                                    <i class="ion-ios-location"></i> Entregar no endereço abaixo: <span>Estrada dos Bandeirantes, 6670 - Jacarepagua</span>
                                    <br>
                                    <i class="ion-ios-location" style="visibility: hidden;"></i> Horário: <span>Entre 09h00 e 09h30</span>
                                </label>
                            </div>
                            <div style="margin-bottom: 5px;"></div>
                            <a href="#" class="btn btn-info btn-xs" style="margin-left: 13px;">Alterar Entrega</a>
                        </div>
                        <div class="col-sm-3 col-sm-push-1">
                          <label>
                            <i class="ion-ios-pricetag"></i>
                                 Cupom Desconto
                                <div class="form-group" style=" margin-top: 8px; ">
                                  <input type="" class="form-control">
                                </div>
                            </label>
                        </div>
                     </div>


                     <div class="summary-day">
                        <div class="bar-image text-uppercase size-14 clearfix">Pedido para o dia 21/06 <i class="ion-trash-a"></i></div>
                        <div class="menu-header">
                           <div>
                              <div class="text-left">
                                 <span>Ítens</span>
                              </div>
                              <div class="text-center">
                                 <span>Quantidade</span>
                              </div>
                              <div class="text-center">
                                 <span>Valor</span>
                              </div>
                           </div>
                        </div>
                        <ul class="item-option">
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Fajitas de carne com molho</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="27.00">R$ 27,00</span>
                                 </div>
                              </div>
                           </li>
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Suco de melancia com água de côco</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="10.00">R$ 10,00</span>
                                 </div>
                              </div>
                           </li>
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Brownie de banana</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="10.00">R$ 10,00</span>
                                 </div>
                              </div>
                           </li>
                        </ul>
                        <div class="btns clearfix">
                           <div class="dropdown">
                             <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i>  Bebidas</a>
                             <ul class="dropdown-menu">
                               <li><a href="#">Marte artesanal</a></li>
                               <li><a href="#">Suco de melancia com água de côco</a></li>
                             </ul>
                           </div>
                           <div class="dropdown">
                             <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i>  Lanches</a>
                             <ul class="dropdown-menu">
                               <li><a href="#">Lanche 1</a></li>
                               <li><a href="#">Lanche 2</a></li>
                             </ul>
                           </div>
                        </div>
                        <table class="box-total box-total-b">
                            <tr>
                                <td>Frete</td>
                                <td><b>R$ 94,00</b></td>
                            </tr>
                            <tr>
                                <td>Valor do Dia</td>
                                <td><b>R$ 10,00</b></td>
                            </tr>
                        </table>
                     </div>


                     <div class="summary-day">
                        <div class="bar-image text-uppercase size-14 clearfix">Pedido para o dia 22/06 <i class="ion-trash-a"></i></div>
                        <div class="menu-header">
                           <div>
                              <div class="text-left">
                                 <span>Ítens</span>
                              </div>
                              <div class="text-center">
                                 <span>Quantidade</span>
                              </div>
                              <div class="text-center">
                                 <span>Valor</span>
                              </div>
                           </div>
                        </div>
                        <ul class="item-option">
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Fajitas de carne com molho</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="27.00">R$ 27,00</span>
                                 </div>
                              </div>
                           </li>
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Mate artesanal</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="10.00">R$ 10,00</span>
                                 </div>
                              </div>
                           </li>
                           <li data-item-price="10.00" data-item-type="drinks">
                              <div>
                                 <div>
                                    <h6>Chips de beterraba</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less">-</button>
                                       <input type="number" value="1" readonly="">
                                       <button type="button" data-operator="more">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2" data-item-price="10.00">R$ 10,00</span>
                                 </div>
                              </div>
                           </li>
                        </ul>
                        <div class="btns clearfix">
                           <div class="dropdown">
                             <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i>  Bebidas</a>
                             <ul class="dropdown-menu">
                               <li><a href="#">Marte artesanal</a></li>
                               <li><a href="#">Suco de melancia com água de côco</a></li>
                             </ul>
                           </div>
                           <div class="dropdown">
                             <a href="#" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown"><i class="ion-plus"></i>  Lanches</a>
                             <ul class="dropdown-menu">
                               <li><a href="#">Lanche 1</a></li>
                               <li><a href="#">Lanche 2</a></li>
                             </ul>
                           </div>
                        </div>
                     </div>


                     <div class="summary-day" style=" margin: 0; ">
                        <div class="menu-header">
                           <div>
                              <div class="text-left">
                                 <span>
                                    Ítens Promoção Fidelidade
                                    <a href="#" data-toggle="tooltip" title="" data-original-title="Item de promoção de fidelidade.">
                                       <i class="ion-help-circled"></i>
                                    </a>  
                                 </span>
                              </div>
                              <div class="text-center">
                                 <span>Quantidade</span>
                              </div>
                              <div class="text-center">
                                 <span>Valor</span>
                              </div>
                           </div>
                        </div>
                        <ul class="item-option">
                           <li class="promo">
                              <div>
                                 <div>
                                    <h6>Chips de beterraba</h6>
                                 </div>
                                 <div>
                                    <div class="integer">
                                       <button type="button" data-operator="less" disabled="">-</button>
                                       <input type="number" value="1" disabled="">
                                       <button type="button" data-operator="more" disabled="">+</button>
                                    </div>
                                 </div>
                                 <div class="text-center">
                                    <span class="price-2">GRÁTIS</span>
                                 </div>
                              </div>
                           </li>
                        </ul>
                        <div class="info">Quer continuar ganhando? Peça para mais um dia e ganhe mais uma bebida.</div>
                     </div>

                     <table class="box-total box-total-b">
                            <tr>
                                <td>Frete</td>
                                <td><b>R$ 94,00</b></td>
                            </tr>
                            <tr>
                                <td>Valor do Dia</td>
                                <td><b>R$ 10,00</b></td>
                            </tr>
                        </table>

                        <hr>

                        <table class="box-total">
                            <tr>
                                <td>Cupom Desconto</td>
                                <td>R$ 94,00</td>
                            </tr>
                            <tr>
                                <td><b>Valor Total do Pedido</b></td>
                                <td><b>R$ 10,00</b></td>
                            </tr>
                        </table>
                  </div>
               </div>
            </div>

         </div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

