@extends('layouts.site')

@section('conteudo')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-push-2">
            <div class="box-txt text-center">
                <h4>Aqui você pode gerenciar suas informações, definir novo endereço padrão de entrega e personalizar seu PIN para pagamento. </h4>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 20px;"></div>
    <form>
      <div class="row">
          <div class="col-sm-4">
              <div class="box-dados dados">
                <h4>Dados Pessoais</h4>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Antonio dos Santos">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" value="antonio_santos@gmail.com" readonly>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" value="******" readonly>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <a href="#" class="btn btn-info btn-block text-uppercase">Alterar e-mail</a>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <a href="#" class="btn btn-info btn-block text-uppercase">Alterar senha</a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-4">
                    <input type="text" class="form-control" value="21" placeholder="DDD">
                  </div>
                  <div class="col-xs-8">
                    <input type="text" class="form-control tel" value="98865-4563" placeholder="Telefone">
                  </div>
                </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="box-dados dados">
                <h4>Empresa</h4>
                <div class="form-group">
                  <select class="form-control">
                    <option selected disabled>Selecione sua empresa</option>
                    <option>Michelin</option>
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control">
                    <option selected disabled>Estr. dos Bandeirantes, 6670 - Ja...</option>
                    <option>Michelin</option>
                  </select>
                  <span>Escolha o seu endereço de entrega padrão. Esses endereços são cadastrados pela sua empresa.</span>
                </div>
                <div style="margin-bottom: 46px;"></div>
                <div class="form-group m-b-0">
                  <label>Código da empresa</label>
                  <input type="text" class="form-control" value="956AER45SWT" placeholder="Digite o código da empresa">
                </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="box-dados dados">
                <h4>PIN de Entrega</h4>
                <div class="form-group">
                  <input type="text" class="form-control" value="673244" placeholder="Digite seu PIN">
                  <span>Combinação de 4 a 6 números. Lembre-se de utilizar uma combinação fácil para você não esquecer. :)</span>
                </div>
                <div style="margin-bottom: 30px;"></div>
                <div class="form-group">
                  <label class="checkbox"><input type="checkbox" name="checkbox-inline" checked><i></i>Aceito receber promoções e <br> novidades das Bentas.</label>
                </div>
              </div>
          </div>
      </div>
      <div style="margin-bottom: 46px;"></div>
      <h4 class="title-box-dados">Meus Endereços de Entrega</h4>
      <div class="row content-address">
        <div class="col-sm-4">
          <div id="box-dados-1" class="box-dados address">
            <div class="tools">
              <div class="text-right">
                <a href="#" class="open-modal-address" data-action="edit"><i class="fa fa-pencil"></i></a>
                <a href="#" class="open-modal-address" data-action="delete"><i class="fa fa-trash-o"></i></a> 
              </div>
            </div>
            <h5>Michelin</h5>
            <p><span class="label-address">Av. das Américas</span> <span class="label-number">, 700</span> <span class="label-complement">- Bloco 2 - Segundo Andar</span></p>
            <p><span class="label-cep">22640-100</span>  <span class="label-neighborhood">- Barra da Tijuca</span></p>
            <p><span class="label-locality">Rio de Janeiro</span>/<span class="label-uf">RJ</span></p>
            <div style="margin-bottom: 40px;"></div>
            
            <div class="radio-font-awesome">
              <input type="radio" name="default_address" id="default_address_1" checked>
              <label for="default_address_1">Endereço Padrão</label>
            </div>

            <input type="hidden" class="cep" value="22640-100">
            <input type="hidden" class="neighborhood" value="Barra da Tijuca">
            <input type="hidden" class="address" value="Av. das Américas">
            <input type="hidden" class="number" value="700">
            <input type="hidden" class="complement" value="Bloco 2 - Segundo Andar">
            <input type="hidden" class="ddd" value="21">
            <input type="hidden" class="phone" value="99999-9999">
            <input type="hidden" class="email" value="fulano@email.com">
            <input type="hidden" class="name" value="Nome do endereço">
            <input type="hidden" class="locality" value="Rio de Janeiro">
            <input type="hidden" class="uf" value="RJ">
          </div>
        </div>
        <div class="col-sm-4">
          <div id="box-dados-2" class="box-dados address">
            <div class="tools">
              <div class="text-right">
                <a href="#" class="open-modal-address" data-action="edit"><i class="fa fa-pencil"></i></a>
                <a href="#" class="open-modal-address" data-action="delete"><i class="fa fa-trash-o"></i></a> 
              </div>
            </div>
            <h5>Michelin</h5>
            <p><span class="label-address">Av. das Américas</span> <span class="label-number">, 700</span> <span class="label-complement">- Bloco 2 - Segundo Andar</span></p>
            <p><span class="label-cep">22640-100</span>  <span class="label-neighborhood">- Barra da Tijuca</span></p>
            <p><span class="label-locality">Rio de Janeiro</span>/<span class="label-uf">RJ</span></p>
            <div style="margin-bottom: 40px;"></div>
            
            <div class="radio-font-awesome">
              <input type="radio" name="default_address" id="default_address_2">
              <label for="default_address_2">Endereço Padrão</label>
            </div>

            <input type="hidden" class="cep" value="22640-100">
            <input type="hidden" class="neighborhood" value="Barra da Tijuca">
            <input type="hidden" class="address" value="Av. das Américas">
            <input type="hidden" class="number" value="700">
            <input type="hidden" class="complement" value="Bloco 2 - Segundo Andar">
            <input type="hidden" class="ddd" value="21">
            <input type="hidden" class="phone" value="99999-9999">
            <input type="hidden" class="email" value="fulano@email.com">
            <input type="hidden" class="name" value="Nome do endereço">
            <input type="hidden" class="locality" value="Rio de Janeiro">
            <input type="hidden" class="uf" value="RJ">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="box-dados address empty open-modal-address" data-action="add">
            <div class="content">
              <!-- content -->
            </div>
          </div>
        </div>
      </div>
      <div style="padding: 30px 0;">
        <div class="row">
          <div class="col-sm-4 col-sm-push-4">
            <button type="submit" class="btn btn-success btn-block btn-lg text-uppercase">Salvar Meus Dados</button>
          </div>
        </div>
      </div>
    </form>
</div>

<!-- Modal Dados -->
<div id="modalDados" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-8 col-sm-push-2">
            <div class="form-group">
                <input type="text" class="form-control cep" placeholder="CEP">
            </div>
            <div class="form-group">
                <input type="text" class="form-control rua address" placeholder="Endereço">
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="text" class="form-control number" placeholder="Nº">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control complement" placeholder="Complemento">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="text" class="form-control ddd" placeholder="DDD">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" class="form-control tel phone" placeholder="Telefone">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <input type="email" class="form-control email" placeholder="E-mail do contato">
            </div>
            <div class="form-group">
                <input type="text" class="form-control name nome-local" placeholder="Nome do endereço">
            </div>
            <div style="margin-bottom: 30px;"></div>
            <input type="hidden" class="box-dados-id">
            <input type="hidden" class="neighborhood">
            <input type="hidden" class="locality">
            <input type="hidden" class="uf">
            <a href="#" class="btn btn-success btn-block btn-lg text-uppercase save-modal-address disabled" data-action="edit">Salvar Endereço</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

