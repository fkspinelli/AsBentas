@extends('layouts.site')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-txt text-center">
                    <h4>Escolha o horário desejado e o endereço de entrega.</h4>
                </div>
            </div>
        </div>
        <div class="box-summary">
           <div class="row">
                <div class="col-sm-6 col-sm-push-3">
                    <form>
                        <div class="form-group">
                            <label><i class="ion-clock"></i> Faixa de horario para entrega:</label>
                            <select class="form-control">
                                <option selected>Entre 09h00 e 09h30</option>
                            </select>
                        </div>
                        <div style="margin-bottom: 50px;"></div>
                        <div class="form-group">
                            <label><i class="ion-ios-location"></i>  Entregar no endereço abaixo:</label>
                            <select class="form-control location select-address">
                                <option selected>Estr. dos Bandeirantes, 6670 - Jacarepagua</option>
                            </select>
                        </div>
                        <div style="margin-bottom: 30px;"></div>
                        <div class="form-group">
                            <label>Complemento do endereço de entrega:</label>
                            <input class="form-control" placeholder="Referência para a entrega: departamento, ramal, andar, etc.">
                        </div>
                        <div class="row">
                            <div class="col-sm-8 col-sm-push-2">
                                <button type="submit" class="btn btn-gray btn-block text-uppercase">Cadastrar um novo endereço</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

