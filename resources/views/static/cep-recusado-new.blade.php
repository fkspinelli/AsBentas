@extends('layouts.site')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-txt text-center">
                    <h4 class="m-b-0">Ops! Ainda não chegamos aí!sdf</h4>
                    <p>Dê uma olhada na nossa área de entrega e insira um endereço válido.</p>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 20px;"></div>
        <div class="row">
            <div class="col-sm-3">
                <div class="mapa">
                    <img src="{{asset('/img/mapa.png')}}" class="img-responsive">
                    <span>Barra da Tijuca</span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="mapa">
                    <img src="{{asset('/img/mapa.png')}}" class="img-responsive">
                    <span>Jacarepaguá</span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="mapa">
                    <img src="{{asset('/img/mapa.png')}}" class="img-responsive">
                    <span>Vargem Grande</span>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="mapa">
                    <img src="{{asset('/img/mapa.png')}}" class="img-responsive">
                    <span>Centro</span>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 60px;"></div>
        <form action="{{ route('site.validar-cep') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-2 col-sm-push-2">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="E-MAIL">
                    </div>
                </div>
                <div class="col-sm-2 col-sm-push-2">
                    <div class="form-group">
                        <input type="text" class="form-control cep" placeholder="CEP">
                    </div>
                </div>
                <div class="col-sm-4 col-sm-push-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">Consulte novamente</button>
                    </div>
                </div>
            </div>
        </form>
        <div style="margin-bottom: 50px;"></div>
    </div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

