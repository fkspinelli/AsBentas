@extends('layouts.site')

@section('conteudo')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-push-1">
                <div class="box-auth text-center">
                    <h4>Olá {{ $nome }}!</h4>
                    <h5>Vimos que você ainda não cadastrou seu local de entrega :) <br> Indique se prefere receber em
                        casa ou tenha vantagens cadastrando-se com sua empresa. Entregamos no Rio de Janeiro, atualmente
                        nos bairros: </h5>
                    <br>
                    <h6 class="cl-aa231f">Barra da Tijuca, Jacarepaguá, Vargem Grande e Centro</h6>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 80px;"></div>


        <div class="row">
            <div class="col-sm-6 col-sm-push-3">
                <ul class="navtabs">
                    <li class="active"><a data-toggle="pill" href="#tab1">Residencial</a></li>
                    <li><a data-toggle="pill" href="#tab2">Comercial</a></li>
                </ul>
            </div>
        </div>

        <div class="box-summary box-navtabs">
            <div class="tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    <form id="frm-pre-cadastro-endereco" action="{{ route('conta-usuario.pre-cadastro-endereco') }}"
                          method="post" data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-4 col-sm-push-4">
                                <div class="form-group">
                                    <input type="text"
                                           class="form-control name nome-local {{ $errors->has('nome') ? 'has-error' : '' }}"
                                           name="nome" placeholder="Nome do endereço" required>
                                    @if($errors->has('nome'))
                                        <span class="help-inline">{{ $errors->first('nome') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                           class="form-control cep {{ $errors->has('cep') ? 'has-error' : '' }}"
                                           name="cep" placeholder="CEP" required>
                                    @if($errors->has('cep'))
                                        <span class="help-inline">{{ $errors->first('cep') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                           class="form-control rua address {{ $errors->has('logradouro') ? 'has-error' : '' }}"
                                           name="logradouro" placeholder="Endereço" required>
                                    @if($errors->has('logradouro'))
                                        <span class="help-inline">{{ $errors->first('logradouro') }}</span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control number {{ $errors->has('numero') ? 'has-error' : '' }}"
                                                   name="numero" placeholder="Nº" required>
                                            @if($errors->has('numero'))
                                                <span class="help-inline">{{ $errors->first('numero') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control complement" name="complemento"
                                                   placeholder="Complemento">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text"
                                           class="form-control {{ $errors->has('bairro') ? 'has-error' : '' }}"
                                           name="bairro" placeholder="Bairro" required>
                                    @if($errors->has('bairro'))
                                        <span class="help-inline">{{ $errors->first('bairro') }}</span>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('cidade') ? 'has-error' : '' }}"
                                                   name="cidade" placeholder="Cidade" required>
                                            @if($errors->has('cidade'))
                                                <span class="help-inline">{{ $errors->first('cidade') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control {{ $errors->has('estado') ? 'has-error' : '' }}"
                                                   name="estado" placeholder="Estado" maxlength="2" required>
                                            @if($errors->has('estado'))
                                                <span class="help-inline">{{ $errors->first('estado') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control ddd" name="ddd" placeholder="DDD">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control tel phone" name="telefone"
                                                   placeholder="Telefone">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control email" name="email"
                                           placeholder="E-mail do contato">
                                </div>
                                <div style="margin-bottom: 30px;"></div>
                                <button type="submit" class="btn btn-success btn-block btn-lg text-uppercase"
                                        id="btn-cadastrar-endereco">
                                    Cadastrar Endereço
                                </button>
                            </div>
                    </form>
                </div>
            </div>
            <div id="tab2" class="tab-pane fade">
                    <div class="row">
                        <div class="col-sm-10 col-sm-push-1">
                            <div class="row">
                                <form id="frm-confirmar-empresa" action="{{ route('conta-usuario.confirmar-empresa') }}" method="post"
                                      data-parsley-validate="">
                                    {{ csrf_field() }}
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Selecione sua empresa</label>
                                            <select name="empresa_id" class="form-control"
                                                    id="select-empresa"
                                                    data-url="{{ url(route('conta-usuario.listar-endereco-empresa'))}}">
                                                <option selected value="">Selecione sua empresa</option>
                                                @foreach($empresas as $empresa)
                                                    <option value="{{ $empresa->id }}">{{ $empresa->nome }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select name="endereco_id" class="form-control opacity"
                                                    id="select-polos" required data-parsley-required-message="Informe o Polo da Empresa">
                                                <option selected value="">Polo de entrega</option>
                                            </select>
                                        </div>
                                        <div style="margin-bottom: 80px;"></div>
                                        <label>Caso a sua empresa não esteja na lista, preencha o formulário ao lado
                                            para efetuarmos o cadastro.</label>
                                    </div>
                                </form>
                                <div class="col-sm-5 col-sm-push-2">
                                    <form id="frm-pre-cadastro" action="{{ route('conta-usuario.pre-cadastro-empresa') }}" method="post"
                                          data-parsley-validate="">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Cadastre sua empresa</label>
                                            <input type="text" class="form-control" name="nome"
                                                   placeholder="Nome da empresa" required
                                                   data-parsley-required-message="Informe o Nome da Empresa">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control cep" name="cep" maxlength="8"
                                                   placeholder="CEP da empresa" value="{{ old('cep') }}"
                                                   required data-parsley-required-message="Informe o CEP da Empresa"
                                            >
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control rua" name="logradouro"
                                                   placeholder="Endereço" value="{{ old('logradouro') }}" required
                                                   data-parsley-required-message="Informe o Endereço da Empresa">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="numero"
                                                           placeholder="Nº" required data-parsley-required-message="Informe o Número do Endereço">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="complemento"
                                                           placeholder="Complemento">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control ddd" placeholder="DDD"
                                                           name="ddd" style="padding-right: 8px;" required
                                                           maxlength="2" value="{{ old('ddd') }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control phone" name="telefone"
                                                           placeholder="Telefone" maxlength="9" required
                                                           value="{{ old('telefone') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email"
                                                   placeholder="E-mail do contato" value="{{ old('email') }}"
                                                   required data-parsley-required-message="Informe o E-mail da Empresa">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ old('contato') }}"
                                                   name="contato" placeholder="Com quem devemos falar?"
                                                   required data-parsley-required-message="Informe o Contato da Empresa">
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div style="margin-bottom: 50px;"></div>
                            <div class="row">
                                <div class="col-sm-4 col-sm-push-4">
                                    <button type="button" id="btn-cadastrar-empresa" class="btn btn-success btn-block btn-lg text-uppercase">
                                        Cadastrar empresa
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>


@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection

@section("javascripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script type="text/javascript" src="/js/primeiro-acesso.js"></script>
@endsection

