@extends('layouts.site')

@section('conteudo')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-txt text-center">
                    <h4>Aqui você pode gerenciar suas informações e definir novo endereço padrão de entrega. </h4>
                </div>
            </div>
        </div>
        <div style="margin-bottom: 20px;"></div>
        @include('flash::message')
        <form id="password-form" action="{{ route('conta-usuario.meus-dados.atualizar') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-4">
                    <div class="box-dados dados">
                        <h4>Dados Pessoais</h4>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" maxlength="160" value="{{ $user->name }}" placeholder="Nome" name="name">
                            @if($errors->has('name'))
                                <span class="help-inline">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" maxlength="160" name="email" id="email"
                                   value="{{ $user->email }}" placeholder="E-mail" @if($errors->isEmpty()) disabled @endif>
                            @if($errors->has('email'))
                                <span class="help-inline text-red">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" maxlength="20" name="password" id="password"
                                   value="" placeholder="Nova Senha"
                            @if(! ($errors->isEmpty() && !empty(old('password')))) {{-- disabled --}} @endif
                            >

                            @if($errors->has('password'))
                                <span class="help-inline text-red">{{ $errors->first('password') }}</span>
                                
                            @else
                                <p><small>Sua senha deve ter no mínimo seis caracteres, conter ao menos um número e uma letra.</small></p>
                            @endif
                        </div>



                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" maxlength="20" name="password_confirmation" id="password_confirmation"
                                   value="" placeholder="Confirmar Senha"
                            @if(! ($errors->isEmpty() && !empty(old('password_confirmation')))) {{-- disabled --}} @endif
                            >

                            @if($errors->has('password_confirmation'))
                                <span class="help-inline text-red">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-xs-4">
                                <input type="text" class="form-control ddd" name="ddd" id="ddd"
                                       value="{{ old('ddd', substr($user->telefone,0,2)) }}"
                                       placeholder="DDD" maxlength="2">
                            </div>
                            <div class="col-xs-8">
                                <input type="text" class="form-control phone" name="telefone" id="telefone"
                                       value="{{ old('telefone', substr($user->telefone,3)) }}"
                                       placeholder="Telefone" maxlength="9">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-block text-uppercase" style="margin-top: 15px;">Salvar Meus Dados</button>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="box-dados dados" style=" padding-bottom: 15px; ">
                        <h4>Empresa / Grupo</h4>
                        <div class="form-group">
                            @if($empresa)
                                <input type="text" class="form-control" value="{{ $empresa->nome }}" placeholder="Empresa" readonly>
                            @else
                            <p style="line-height: 15px;font-size: 15px;margin-bottom: 10px;">Você não possui vínculo com nenhum grupo nosso associado, verifique aqui os convênios disponíveis ou cadastre um novo grupo/empresa.</p>
                            
                            <select class="form-control" name="empresa_ativa" id="empresa-ativa"
                                    data-url="{{ url(route('conta-usuario.listar-endereco-empresa'))}}"
                            >
                                <option selected value="">Selecione sua empresa</option>


                                @foreach($empresas as $empresa)
                                    <option value="{{ $empresa->id }}" @if(old('empresa_ativa') == $empresa->id) selected @endif>
                                        {{ $empresa->nome }}
                                    </option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            
                            <select class="form-control {{ $errors->has('endereco_id') ? 'has-error' : '' }}" id="endereco_id" name="endereco_id">
                                <option value="">Selecione o Polo de Entrega</option>
                                @foreach($enderecosEmpresa as $endereco)
                                    <option value="{{ $endereco->id }}" @if($endereco->id == $enderecoId) selected @endif>{{ $endereco->nome }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('endereco_id'))
                                <span class="help-inline">{{ $errors->first('endereco_id') }}</span>
                            @endif
                        </div>
                        
                        <div class="form-group {{ $errors->has('codigo_empresa') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" name="codigo_empresa" value="{{ old('codigo_empresa', ($user->empresaAtiva()) ? $user->empresaAtiva()->pivot->codigo_empresa : null) }}"
                                   placeholder="Digite o código da empresa" maxlength="20"
                                   @if($user->empresaAtiva() && $user->empresaAtiva()->pivot->codigo_empresa) disabled @endif
                            >
                            @if($errors->has('codigo_empresa'))
                                <span class="help-inline">{{ $errors->first('codigo_empresa') }}</span>
                            @endif
                        </div>

                        @if(! $user->empresaAtiva())
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block text-uppercase">Salvar</button>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <a href="#" class="btn btn-info btn-block text-uppercase" data-toggle="modal" data-target="#modalCadastroEmpresa">nova empresa</a>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="form-group">
                            <a href="#" class="btn btn-info btn-block text-uppercase btn-desvincular-empresa" data-url-desvinculo="{{ route('conta-usuario.meus-dados.desvincular-empresa', ['id' => $empresa->id]) }}">
                                Quero desvincular
                            </a>
                        </div>
                        @endif

                    </div>
                </div>

            </div>

            <div style="margin-bottom: 46px;"></div>
            <h4 class="title-box-dados">Meus Endereços de Entregas</h4>

            <div class="row content-address">
                @foreach($listaEnderecosEcontrados as $endereco)

                <div class="col-sm-4">
                    <div id="box-dados-{{ $endereco->id }}" class="box-dados address @if($endereco->status == 'inativo') inativo @endif">
                        <div class="tools">
                            <div class="text-right">
                                @if(!$endereco->empresa_id)
                                    <a href="#" class="open-modal-address" data-action="edit" data-id="{{ $endereco->id }}"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="delete" data-action="delete" data-url="{{ route('conta-usuario.meus-dados.removerEndereco',['id' => $endereco->id]) }}"><i class="fa fa-trash-o"></i></a>
                                @else
                                    <a> <i class="fa fa-building"></i> </a>
                                @endif
                            </div>
                        </div>

                        <h5>{{ $endereco->nome }}</h5>
                        <p><span class="label-address">{{ $endereco->logradouro }}</span> <span class="label-number">, {{ $endereco->numero }}</span>
                            @if(strlen($endereco->complemento) > 0)<span class="label-complement">- {{ $endereco->complemento }}</span>@endif
                        </p>
                        <p><span class="label-cep">{{ $endereco->cep }}</span>  <span class="label-neighborhood">- {{ $endereco->bairro }}</span></p>
                        <p><span class="label-locality">{{ $endereco->cidade }}</span>/<span class="label-uf">{{ $endereco->estado }}</span></p>
                        <div style="margin-bottom: 40px;"></div>

                        <div class="radio-font-awesome endereco-padrao"
                             data-url="{{ route('conta-usuario.meus-dados.enderecoPrincipal', ['id' => $endereco->id]) }}">
                            <input type="radio" name="endereco-padrao"
                                    @if($endereco->padrao)checked @endif
                            >
                            <label for="default_address_1">Endereço Padrão</label>
                        </div>
                        <input type="hidden" name="id_endereco" class="cep" value="{{ $endereco->id }}">
                        <input type="hidden" name="cep" class="cep" value="{{ $endereco->cep }}">
                        <input type="hidden" name="bairro" class="neighborhood" value="{{ $endereco->bairro }}">
                        <input type="hidden" name="logradouro" class="address" value="{{ $endereco->logradouro }}">
                        <input type="hidden" name="numero" class="number" value="{{ $endereco->numero }}">
                        <input type="hidden" name="complemento" class="complement" value="{{ $endereco->complemento }}">
                        <input type="hidden" name="endereco-ddd" class="ddd" value="{{ $endereco->ddd }}">
                        <input type="hidden" name="endereco-telefone" class="phone" value="{{ $endereco->telefone }}">
                        <input type="hidden" name="nome" class="name" value="{{ $endereco->nome }}">
                        <input type="hidden" name="cidade" class="locality" value="{{ $endereco->cidade }}">
                        <input type="hidden" name="estado" class="uf" value="{{ $endereco->estado }}">
                    </div>
                </div>
                @endforeach

                <div class="col-sm-4">
                    <div class="box-dados address empty open-modal-address" data-action="add">
                        <div class="content">
                            <!-- content -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- Modal Dados -->
    <div id="modalDados" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="{{ route('conta-usuario.meus-dados.adicionarEndereco') }}" method="post"  data-parsley-validate="">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-8 col-sm-push-2">
                                <input type="hidden" class="form-control" name="id">
                                <input type="hidden" class="form-control" name="redirect" value="meus-dados">
                                <div class="form-group">
                                    <input type="text" class="form-control name nome-local" name="nome" placeholder="Nome do endereço" required
                                           data-parsley-required-message="Informe o Nome do Endereço">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control cep" name="cep" placeholder="CEP" required
                                    data-parsley-required-message="Informe o CEP" id="cep">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control rua address" name="logradouro" placeholder="Endereço" required
                                           data-parsley-required-message="Informe o Endereço" id="logradouro">
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control number" name="numero" placeholder="Nº" required
                                                   data-parsley-required-message="Informe o número">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control complement" name="complemento" placeholder="Complemento">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="bairro" placeholder="Bairro" id="bairro">
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="cidade" placeholder="Cidade" id="cidade">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="estado" placeholder="Estado" maxlength="2" id="estado">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control ddd" name="endereco-ddd" placeholder="DDD">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control tel phone" name="endereco-telefone" placeholder="Telefone">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="box-dados-id">
                                <input type="hidden" class="neighborhood">
                                <input type="hidden" class="locality">
                                <input type="hidden" class="uf">
                                <button type="submit" class="btn btn-success btn-block btn-lg text-uppercase save-modal-address" data-action="edit">
                                    Salvar Endereço
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


    <!-- Modal Cadastro Empresa -->
    <div id="modalCadastroEmpresa" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="frm-pre-cadastro" action="{{ route('conta-usuario.pre-cadastro-empresa') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="origem" value="conta-usuario.meus-dados">
                        <div class="row">
                            <div class="col-sm-8 col-sm-push-2">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="nome" placeholder="Nome da empresa" required="" data-parsley-required-message="Informe o Nome da Empresa">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control cep" name="cep" maxlength="9" placeholder="CEP da empresa" value="" required="" data-parsley-required-message="Informe o CEP da Empresa" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control rua" name="logradouro" placeholder="Endereço" value="" required="" data-parsley-required-message="Informe o Endereço da Empresa">
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="numero" placeholder="Nº" required="" data-parsley-required-message="Informe o Número do Endereço">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="complemento" placeholder="Complemento">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control ddd" placeholder="DDD" name="ddd" style="padding-right: 8px;" required="" maxlength="2" value="">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control phone" name="telefone" placeholder="Telefone" maxlength="10" required="" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="E-mail do contato" value="" required="" data-parsley-required-message="Informe o E-mail da Empresa">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="" name="contato" placeholder="Com quem devemos falar?" required="" data-parsley-required-message="Informe o Contato da Empresa">
                                </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg text-uppercase save-modal-address" data-action="edit">Cadastrar Empresa</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection


@section('javascripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.0/parsley.min.js"></script>
    <script type="text/javascript" src="js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="/js/meus-dados.js"></script>

    <script>
        $(document).ready(function (){

            @if(session('error'))
                swal(
                'Ops!',
                '{{ session('error') }}',
                'warning'
            );
            @endif
        })
    </script>

    @if (session('pre-cadastro'))

        <script>
            $(document).ready(function () {





                @if(session('pre-cadastro') == 'cep-invalido')
                    swal({
                        title: 'Olá! Vimos que seu CEP não está dentro da nossa área de atendimento no Rio de Janeiro',
                        text: 'Infelizmente no momento não estamos fazendo entregas para seu raio geográfico. Vamos efetuar o seu cadastro para avisá-lo sobre nosso processo de expansão \\o/',
                        type: 'warning'
                    }, function () {
                        
                    });
                @endif
                @if(session('pre-cadastro') == 'cep-valido')
                    swal({
                        title:  'Olá!',
                        text: 'Obrigado por cadastrar sua empresa. Faremos contato em breve para validar o seu endereço. Abraços!',
                        type: 'success'
                    }, function () {
                        
                    });
                @endif
            });
        </script>
    @endif

@endsection