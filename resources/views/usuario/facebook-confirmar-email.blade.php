@extends('layouts.email')
@section('conteudo')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <div class="box-auth text-center">
                        <h4>Olá Fulano!</h4>
                        <h5>Precisamos do seu e-mail para concluir o seu cadastro. Vamos lá?</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-4">
                    <div class="box-auth">
                        <form>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="E-mail">
                            </div>
                            <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">Confirmar e-mail</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="bottom" style="background-image: url(img/bg-news.png);"></div>
@endsection