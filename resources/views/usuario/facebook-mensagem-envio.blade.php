@extends('layouts.email')
@section('conteudo')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-push-2">
                    <div class="box-auth text-center">
                        <h4>Obrigado!</h4>
                        <h5>Agora acesse sua caixa de entrada e ative o cadastro.</h5>
                        <br>
                        <h5>Caso não receba nenhuma notificação, verifique sua caixa de SPAM e cadastre o <br> atendimento@asbentas.com.br como confiável ou:</h5>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-6 col-sm-push-3">
                                <a href="#" class="btn btn-info btn-lg-2 btn-block text-uppercase">Reenvie o e-mail de confirmação</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection