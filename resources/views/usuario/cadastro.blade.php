@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Cadastro</h1>
            <h2>Ser saudável no trabalho é possível.</h2>
            <h2>Planeje suas refeições diárias e tenha mais vitalidade para continuar o dia!</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-push-4">
                <div class="box-auth">
                    <form action="{{ route('usuario.salvar') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <input type="text" class="form-control" maxlength="160" name="name" placeholder="Nome" value="{{ old('name') }}">
                            @if($errors->has('name'))
                            <span class="help-inline">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" name="email" maxlength="160" placeholder="E-mail" value="{{ old('email', session('local') ?  session('local')['email'] : '' ) }}">
                            @if($errors->has('email'))
                                <span class="help-inline">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" name="password" maxlength="20" placeholder="Senha" value="{{ old('password') }}">
                            @if($errors->has('password'))
                                <span class="help-inline">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <p><small>Sua senha deve ter no mínimo seis caracteres, conter ao menos um número e uma letra.</small></p>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" maxlength="20" name="password_confirmation" placeholder="Confirmar Senha">
                            @if($errors->has('password_confirmation'))
                                <span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                        <p>
                            <small>Ao se cadastrar em nosso site você estará de acordo com nossos <a href="#" data-toggle="modal" data-target="#termos-e-condicoes">Termos e Condições</a> e com nossa
                                <a href="{{ url(route('site.termosPolitica')) }}" target="_blank">Política de Privacidade</a>.
                            </small>
                        </p>
                        <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">
                            Cadastre-se agora
                        </button>
                    </form>
                    <hr>
                    {{-- <a href="{{ route('social.login', [ 'provider' => 'facebook'] )  }}" class="btn btn-facebook btn-lg btn-block text-uppercase">
                        Cadastre-se pelo facebook
                    </a>
                    <hr> --}}
                    <div class="text-center">
                        <p>Já é cadastrado? <a href="{{ route('usuario.login') }}">Faça o seu login aqui.</a></p>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection