@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Nova Senha</h1>
            <h2>Planeje suas refeições diárias e abra-se ao novo!</h2>
            <h2>É possível comer de forma saudável no trabalho ;)</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-auth text-center">
                    <h4>Perfeito! Sua nova senha foi cadastrada com sucesso! </h4>
                    <h5>Agora é só se logar no site.</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-push-4">
                <div class="box-auth">
                    @include('flash::message')
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    <form action="{{ route('usuario.autenticar') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" maxlength="160" name="email" placeholder="E-mail">
                            @if($errors->has('email'))
                                <span class="help-inline">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" class="form-control" maxlength="20" name="password" placeholder="Senha">
                            @if($errors->has('password'))
                                <span class="help-inline">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <p>
                            <small>Esqueceu a senha? <a href="{{ route('usuario.esqueceu-senha') }}">Clique aqui</a>.</small>
                        </p>
                        <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">Entrar agora</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection