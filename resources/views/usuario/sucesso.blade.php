@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Cadastro</h1>
            <h2>Planeje suas refeições diárias e abra-se ao novo!</h2>
            <h2>É possível comer de forma saudável no trabalho ;)</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-push-2">
                <div class="box-auth text-center">
                    <h4>Obrigado por nos escolher!</h4>
                    <h5>Acesse sua caixa de entrada e ative o cadastro para poder efetuar seu primeiro pedido!</h5>
                    <br>
                    <h5>Caso não receba nenhuma notificação dentro de alguns minutos, verifique sua caixa de SPAM e cadastre o endereço admin@asbentas.com como confiável ou:</h5>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-3">
                            <a href="#" data-url="{{ route('usuario.reenviar_email', session('idUsuario' )) }}"
                               class="btn btn-info btn-lg-2 btn-block text-uppercase" id="btn-reenviar-email">
                                Reenvie o e-mail de confirmação
                            </a>
                        </div>
                    </div>
                    <br>
                    <br>
                    <h5>Se você é usuário de GMAIL, verifique também as ABAS Promoções e Social. Nosso e-mail pode estar lá.</h5>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>

        $(document).ready(function () {

            $('#btn-reenviar-email').on('click', function (e) {
                var url = $(this).attr('data-url')
                e.preventDefault();

                swal(
                    '',
                    'O e-mail de confirmação foi reenviado.',
                    'success'
                ).then((value) => {
                    window.location = url;
                });

            })

        });

    </script>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection