@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Esqueceu a sua senha?</h1>
            <h2>Calma que a gente te ajuda ;) </h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-auth text-center">
                    <h4>Nada de estresse! Não prejudique sua qualidade de vida por isso!</h4>
                    <h5>Informe o e-mail cadastrado no campo abaixo para que possamos enviar o link para criação de uma nova senha.</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-push-4">
                <div class="box-auth">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    <form action="{{ route('usuario.esqueceu-senha.enviar') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="E-mail">
                        </div>
                        <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">REDEFINIR SENHA</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection