@extends('layouts.site')

@section('conteudo')
    <div class="internal-banner" style="background-image: url(/img/banner-interna.jpg);">
        <div class="container">
            <h1>Nova Senha</h1>
            <h2>Planeje suas refeições diárias e abra-se ao novo!</h2>
            <h2>É possível comer de forma saudável no trabalho ;)</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <div class="box-auth text-center">
                    <h4>Vamos refazer sua senha?</h4>
                    <h5>Cadastre aqui sua nova senha! Ela deve conter no mínimo seis caracteres, pelo menos um número e uma letra.</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-push-4">
                <div class="box-auth">
                    @include('flash::message')
                    <form action="{{ route('usuario.trocar-senha.salvar') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $usuario->id }}">
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                            <input type="password" name="password" maxlength="20" class="form-control" placeholder="Senha">
                            @if($errors->has('password'))
                                <span class="help-inline">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                            <input type="password" name="password_confirmation" maxlength="20" class="form-control" placeholder="Repetir Senha">
                            @if($errors->has('password_confirmation'))
                                <span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-success btn-lg btn-block text-uppercase">Salvar nova Senha</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pedido')
    <div class="bottom" style="background-image: url(/img/bg-news.png);"></div>
@endsection