@extends('layouts.email')

@section('conteudo')

    <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
        <tr>
            <td valign="top">
                <font face="verdana" size="3" color="#343434"><br><br><b>Ol&#225; {{ $usuario->name }},</b> <br><br></font>
                <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;">Recebeu este e-mail? Ent&#227;o quer dizer que voc&#234; fez um cadastro l&#225; no nosso site!  Por favor, confirme seu cadastro para termos certeza de que voc&#234; &#233; voc&#234; mesmo ;) Tudo isso &#233; para garantirmos que a nossa comunica&#231;&#227;o chegar&#225; at&#233; voc&#234; sem nenhum problema. <br><br><br></font>
            </td>
        </tr>
        <tr>
            <td>
                <table width="250" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                    <tr>
                        <td height="36" bgcolor="#3391ce" align="center" style="border-radius: 3px;">
                            <a href="{{ route('usuario.confirmar_email', ['code' => $usuario->confirmation_code]) }}" target="_blank" style="display: block; line-height: 36px; text-decoration: none;">
                                <font face="verdana" size="1" color="#ffffff" style="font-size: 12px;"><b>CONFIRMA&#199;&#195;O DE CADASTRO</b></font>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><br><br> Cadastre o e-mail admin@asbentas.com em sua lista de contatos confi&#225;veis. Essa &#233; uma forma de garantirmos que nosso e-mail chegar&#225; na sua caixa de entrada. <br><br><br></font>
                <font face="verdana" size="1" color="#343434" style="font-size: 11px; line-height: 18px;"><i>Se voc&#234; acredita que houve algum engano ou voc&#234; n&#227;o teve a inten&#231;&#227;o de se inscrever em nosso site, ignore esta mensagem e nada acontecer&#225;.</i></font>
            </td>
        </tr>
        <tr>
            <td height="60"></td>
        </tr>
    </table>

@endsection