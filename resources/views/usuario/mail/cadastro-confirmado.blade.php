@extends('layouts.email')

@section('conteudo')

    <table width="100%" align="center" valign="top" border="0" cellpadding="68" cellspacing="0" bgcolor="#ffffff" class="padding-none">
        <tr>
            <td>
                <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                    <tr>
                        <td valign="top">
                            <font face="verdana" size="5" color="#981318">SEU CADASTRO FOI CONFIRMADO COM SUCESSO! <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;"><b>Estamos muito felizes em ter voc&#234; conosco!</b> <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;">Comer &#233; escolher, e voc&#234; escolheu ter uma vida mais saud&#225;vel e produtiva. A correria do dia a dia de trabalho muitas vezes nos impossibilita de manter um padr&#227;o de qualidade de vida, mas estamos aqui para provar o contrario! <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;">Ao optar por ser mais saud&#225;vel, voc&#234; opta tamb&#233;m por um estilo de vida diferente. Ent&#227;o, vamos come&#231;ar?<br><br><br></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="250" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                                <tr>
                                    <td height="36" bgcolor="#3ac734" align="center" style="border-radius: 3px;">
                                        <a href="{{ route('site.cardapio') }}" target="_blank" style="display: block; line-height: 36px; text-decoration: none;">
                                            <font face="verdana" size="1" color="#ffffff" style="font-size: 12px;"><b>FAZER MEU PRIMEIRO PEDIDO</b></font>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection