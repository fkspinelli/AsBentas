@extends('layouts.email')

@section('conteudo')
    <table width="100%" align="center" valign="top" border="0" cellpadding="68" cellspacing="0" bgcolor="#ffffff" class="padding-none">
        <tr>
            <td>
                <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                    <tr>
                        <td valign="top">
                            <font face="verdana" size="3" color="#343434"><b>Ol&#225; {{ $usuario->name }},</b> <br><br></font>
                            <font face="verdana" size="5" color="#981318">ESTAMOS AQUI PARA TE AJUDAR! <br><br></font>
                            <font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;">Voc&#234; est&#225; recebendo esse e-mail porque clicou na solicita&#231;&#227;o <b>“Esqueci Minha Senha”</b> Por favor, clique no bot&#227;o abaixo para criar uma nova senha e guarde-a com voc&#234; ;) <br><br><br></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="250" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                                <tr>
                                    <td height="36" bgcolor="#3391ce" align="center" style="border-radius: 3px;">
                                        <a href="{{ route('usuario.trocar-senha', ['id' => $usuario->id]) }}" target="_blank" style="display: block; line-height: 36px; text-decoration: none;">
                                            <font face="verdana" size="1" color="#ffffff" style="font-size: 12px;"><b>QUERO CRIAR UMA NOVA SENHA</b></font>
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <font face="verdana" size="1" color="#343434" style="font-size: 11px; line-height: 18px;"><br><br><i>Se voc&#234; acredita que houve algum engano ou voc&#234; n&#227;o teve a inten&#231;&#227;o refazer sua senha, ignore esta mensagem e nada acontecer&#225;.</i></font>
                        </td>
                    </tr>
                    <tr>
                        <td height="60"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


@endsection