@extends('layouts.email')

@section('conteudo')
		<table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
			<tr>
				<td valign="top">
					<font face="verdana" size="3" color="#343434"><b>Ol&#225;, {{$user->name}},</b> <br><br></font>
					<font face="verdana" size="3" color="#343434" style="font-size: 14px; line-height: 21px;">Confirmamos o recebimento do seu pedido. Confira abaixo o resumo ou se preferir, acompanhe atrav&#233;s da nossa plataforma: <br><br><br></font>
				</td>
			</tr>
			<tr>
				<td>
					<font face="verdana" size="4" color="#272727" style="font-size: 20px;">Resumo do Pedido <br><br></font>
					<font face="verdana" size="1" color="#343434" style="font-size: 12px; line-height: 21px;">
						<b>N&#176; do Pedido:</b> {{$pedido->id}} <br>
						<b>Data:</b> {{$pedido->created_at->format('d/m/Y')}} <br>
						<b>Status:</b> {{$pedido->fulfillment}} <br>
						<b>Endere&#231;o de entrega:</b> {{$pedido->endereco->label()}} <br>
						<b>Hor&#225;rio:</b> {{$pedido->horario}} <br>
						<b>Forma de Pagamento: </b> {{ $pedido->formaPagamento->tipo }}
					</font>
				</td>
			</tr>
			<tr>
				<td height="35"></td>
			</tr>
			@php($dia = 1)
			@foreach($itensPedido as $pedidoCardapio)
				@php(++$dia)
				<tr>
					<td>
						<table width="100%" align="center" valign="top" border="0" cellpadding="7" cellspacing="0" bgcolor="#ffffff">
							<tr>
								<td align="center" bgcolor="#535353" style="border-radius: 3px;">
									<font face="Arial" size="1" color="#ffffff" style="font-size: 12px; line-height: 21px;">PEDIDO PARA O DIA {{ $pedidoCardapio->cardapio->data_cardapio->format('d/m/Y') }}</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" align="center" valign="top" border="0" cellpadding="20" cellspacing="0" bgcolor="#ffffff">
							<tr>
								<td width="700" align="left"   style="padding-top: 13px; padding-bottom: 5px;"><font face="Arial" size="1" color="#878787" style="font-size: 11px;">&#205;TENS</font></td>
								<td width="104" align="center" style="padding-top: 13px; padding-bottom: 5px;"><font face="Arial" size="1" color="#878787" style="font-size: 11px;">QUANTIDADE</font></td>
								<td width="104" align="center" style="padding-top: 13px; padding-bottom: 5px;"><font face="Arial" size="1" color="#878787" style="font-size: 11px;">VALOR</font></td>
								<td width="104" align="right" style="padding-top: 13px; padding-bottom: 5px;"><font face="Arial" size="1" color="#878787" style="font-size: 11px;">TOTAL</font></td>
							</tr>
						</table>
					</td>
				</tr>


				@foreach($pedidoCardapio->itens as $item)
					<tr>
						<td>
							<table width="100%" align="center" valign="top" border="0" cellpadding="5" cellspacing="0" bgcolor="#f1f1f1" style="border-radius: 3px;">
								<tr>
									<td align="left" style="padding-left: 20px;"><font face="Arial" size="1" color="#272727" style="font-size: 12px;">{{$item->descricao }}</font></td>
									<td width="40" align="center">
										<table width="100%" align="center" valign="top" border="0" cellpadding="10" cellspacing="0" bgcolor="#ffffff" style="border-radius: 3px;">
											<tr>
												<td align="center">
													<font face="Arial" size="2" color="#535353" style="font-size: 14px;">{{ $item->quantidade }}</font>
												</td>
											</tr>
										</table>
									</td>
									<td width="1"></td>
									<td width="85" align="center"><font face="Arial" size="2" color="#535353" style="font-size: 14px;">R$ {{number_format( ($item->valor  / $item->quantidade), 2, ',', '.')}}</font></td>
									<td width="85" align="right"><font face="Arial" size="2" color="#535353" style="font-size: 14px;">R$ {{number_format( $item->valor , 2, ',', '.')}}</font></td>
								</tr>
							</table>
						</td>
					</tr>

				@endforeach


					<tr>
						<td>
							<table width="100%" align="center" valign="top" border="0" cellpadding="5" cellspacing="0">

								<tr>
									<td height="20"></td>
								</tr>

								@if($pedidoCardapio->desconto_combo != 0)
									<tr>
										<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Desconto Combo</b></font></td>
										<td align="right" width="110"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">- R$ {{number_format($pedidoCardapio->desconto_combo, 2, ',', '.')}}</font></td>
										<td width="18">&nbsp;</td>
									</tr>
								@endif

								@if($pedidoCardapio->desconto != 0)
								<tr>
									<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Desconto Frequência</b></font></td>
									<td align="right" width="110"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">- R$ {{number_format($pedidoCardapio->desconto, 2, ',', '.')}}</font></td>
									<td width="18">&nbsp;</td>
								</tr>
								@endif

								<p></p>


								<tr>
									<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Frete</b></font></td>
									<td align="right" width="110"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">R$ {{number_format($frete, 2, ',', '.')}}</font></td>
									<td width="18">&nbsp;</td>
								</tr>

								<tr>
									<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Valor do Dia</b></font></td>
									<td align="right" width="120"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">R$ {{number_format($pedidoCardapio->subtotal, 2, ',', '.') }}</font></td>
									<td width="18">&nbsp;</td>
								</tr>

								<tr>
									<td height="20"></td>
								</tr>
							</table>
						</td>
					</tr>
				<tr>
					<td height="2"></td>
				</tr>
			@endforeach

			<tr>
				<td height="10"></td>
			</tr>


			<tr>
				<td>
					<table width="100%" align="center" valign="top" border="0" cellpadding="5" cellspacing="0" >
						<tr>
							<hr>
						</tr>

						<tr>
							<td height="10"></td>
						</tr>


						@if($dia <= 5)
						<tr bgcolor="">
							<td align="center" colspan="3">
								<font color="#aa231f">Você sabia? Pedindo mais um dia você ganha {{ number_format($descontos[$dia]->desconto, 0) }}% de desconto \o/</font>
							</td>
						</tr>
						<tr>
							<td height="10"></td>
						</tr>
						@endif
						@if($pedido->valor_cupom_desconto != 0)
							<tr bgcolor="#f1f1f1">
								<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Cupom</b></font></td>
								<td align="right" width="110"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">- R$ {{number_format($pedido->valor_cupom_desconto, 2, ',', '.')}}</font></td>
								<td width="18">&nbsp;</td>
							</tr>
						@endif

						<tr bgcolor="#f1f1f1">
							<td align="right"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;"><b>Valor Total do Pedido</b></font></td>
							<td align="right" width="110"><font face="verdana" size="2" color="#272727" style="font-size: 14px; line-height: 21px;">R$ {{number_format($pedido->valor_total, 2, ',', '.')}}</font></td>
							<td width="18">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td height="50"></td>
			</tr>

			<tr>
				<td>
					<table width="250" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" class="padding-none">
						<tr>
							<td height="36" bgcolor="#3391ce" align="center" style="border-radius: 3px;">
								<a href="{{ route('meus-pedidos.index') }}"	 target="_blank" style="display: block; line-height: 36px; text-decoration: none;">
									<font face="verdana" size="1" color="#ffffff" style="font-size: 12px;"><b>ACOMPANHAR MEUS PEDIDOS</b></font>
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="40"></td>
			</tr>
		</table>




@endsection