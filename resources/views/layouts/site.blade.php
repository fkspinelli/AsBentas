<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="image/x-icon" rel="shortcut icon" href="{{ url('/img/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Bad+Script|Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
    <title>As Bentas - Comida Saudável</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/beauty-font.css">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/css/basictable.css">
    <link rel="stylesheet" type="text/css" href="/css/index.css">
    <link rel="stylesheet" type="text/css" href="/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/sweetalert.css">
    @yield('stylesheets')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Facebook Pixel Code -->
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="dbe5ba46-240f-44d0-91bc-96844d1e4fc1";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
     fbq('init', '245974725908971'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
     <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=245974725908971&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-102189494-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
     
      gtag('config', 'UA-102189494-1');
    </script>
</head>
<body>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('site.index') }}">
                            <img src="/img/logo-as-bentas-header.png" alt="As Bentas">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li><a href="{{ url(route('site.sobre')) }}" class="hover">Sobre As Bentas</a></li>
                            <li><a href="{{ url(route('site.como-funciona')) }}" class="hover">Como Funciona</a></li>
                            <li><a href="{{ url(route('site.cardapio')) }}" class="hover">Cardápio</a></li>
                            <li><a href="{{ url(route('site.faq')) }}" class="hover">FAQ</a></li>
                            <li><a href="{{ url(route('site.contato')) }}" class="hover">Contato</a></li>
                        </ul>
                        @if (Auth::check())
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-dropdown">
                                <a href="#" class="dropdown-toggle nav-dropdown clearfix" data-toggle="dropdown">
                                    <div class="left">
                                        <img src="/img/user.png">
                                    </div>
                                    <div class="right">
                                        <div>
                                            Olá, {{ strlen(Auth::user()->name)>=23 ? substr(Auth::user()->name,0,20) . '...' : Auth::user()->name}}
                                        </div>
                                        <div>
                                            Minha Conta
                                            <i class="ion-chevron-down"></i>
                                        </div>
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('conta-usuario.meus-dados') }}">Meus Dados <i class="ion-chevron-right"></i></a></li>
                                    <!--li><a href="#">Minha Carteira <i class="ion-chevron-right"></i></a></li-->
                                    <li><a href="{{ route('meus-pedidos.index') }}">Meus Pedidos <i class="ion-chevron-right"></i></a></li>
                                    <li><a href="{{ route('usuario.sair') }}">Sair </a></li>
                                </ul>
                            </li>
                        </ul>
                        @else
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="{{ route('usuario.login') }}" class="btn btn-default">Login</a></li>
                                <li><a href="{{ route('usuario.cadastro') }}" class="btn btn-danger">Cadastre-se</a></li>
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
<section>
    @yield('conteudo')
</section>

@yield('pedido')
<footer class="remove">
    <div class="footer-middle hide-footer-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <img src="{{ url('img/logo-footer-2.png') }}" clas="img-responsive">
                </div>
                <div class="col-sm-5">
                    <ul>
                        <li><a href="{{ url(route('site.sobre')) }}" >Sobre As Bentas</a></li>
                        <li><a href="{{ url(route('site.cardapio')) }}">Cardápio</a></li>
                        <li><a href="{{ url(route('site.faq')) }}">FAQ</a></li>
                        <li><a href="{{ url(route('site.contato')) }}">Contato</a></li>
                    </ul>
                </div>
                <div class="col-sm-1">
                    <div class="footer-img-center">
                        <a target="_blank" href="https://www.instagram.com/asbentas/"><img src="{{ url('img/instagram-2.png') }}"></a>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="footer-img-center">
                        <a target="_blank" href="https://www.facebook.com/asbentas"><img src="{{ url('img/facebook-2.png') }}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <span class="copyright">Copyright 2017 - As Bentas - Todos os direitos reservados.</span>
                </div>

                <div class="col-md-3 col-sm-4 hide-footer-2">
                    <div class="text-right">
                        <a href="{{ url(route('site.termosPolitica')) }}">Termos e Política de Privacidade </a>
                    </div>
                </div>
                {{-- <div class="col-md-2 col-sm-3 hide-footer-2">
                    <div class="text-right">
                        <a href="#" data-toggle="modal" data-target="#termos-e-condicoes">Política de privacidade</a>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="footer-bottom footer-bottom-b">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-6">
                    <span class="copyright">Copyright 2017 - As Bentas - Todos os direitos reservados.</span>
                </div>
                <div class="col-sm-1">
                    <div class="footer-img-center">
                        <a target="_blank" href="https://www.instagram.com/asbentas/"><img src="{{ url('img/instagram-2.png') }}"></a>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="footer-img-center">
                        <a target="_blank" href="https://www.facebook.com/asbentas"><img src="{{ url('img/facebook-2.png') }}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/slick.js"></script>
<script type="text/javascript" src="/js/select2.min.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="/js/jquery.basictable.min.js"></script>
<script type="text/javascript" src="/js/index.js"></script>
<script type="text/javascript" src="/assets/js/sweetalert.min.js"></script>
<script type="text/javascript" src="/js/chat.js"></script>

{{--<script id='chat-2-desk-widget-code' type="text/javascript">--}}
    {{--function chat2deskWidgetRun() {--}}
    {{--window['cha'+'t2d'+'e'+'skID'] = '0a780a2b39a8b3c134b3d3808dc88a80';--}}
    {{--window.domain = 'https://web.chat2desk.com';--}}
    {{--var sc = document.createElement('script');--}}
    {{--sc.type = 'text/javascript';--}}
    {{--sc.async = true;--}}
    {{--sc.src = window.domain + '/js/widget/new/dist/widget.min.js';--}}

    {{--var c = document['getElement'+'sByTagNa'+'me']('script')[0];--}}
    {{--if ( c ) c['p'+'arent'+'Node']['inser'+'tB'+'efo'+'re'](sc, c);--}}
    {{--else document['docu'+'me'+'ntEle'+'m'+'ent']['f'+'i'+'r'+'s'+'tChi'+'ld']['appe'+'nd'+'C'+'hild'](sc);--}}
    {{--}--}}
    {{--window.chat2deskWidgetCanRun = true;--}}
    {{--if (window.chat2deskWidgetCanRun) {--}}
        {{--chat2deskWidgetRun();--}}
{{--}--}}
{{--</script>--}}

<div id="termos-e-condicoes" class="modal fade" role="dialog">
  <div class="modal-dialog" style=" width: 600px; max-width: 90%; margin: 30px auto 0; ">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Política de Privacidade e Termo de Uso</h4>
      </div>
      <div class="modal-body" style="padding: 15px;">
        <p>As Bentas tem o compromisso com a privacidade e a segurança de seus clientes durante todo o processo de navegação e compra pelo site. </p>

        <p>Os dados cadastrais dos clientes não são vendidos, trocados ou divulgados para terceiros, exceto quando essas informações são necessárias para o processo de entrega, para cobrança, ou para participação em promoções solicitadas pelos clientes.</p> 

        <p>Seus dados pessoais são peça fundamental para que seu pedido chegue em segurança, na sua casa, de acordo com nosso prazo de entrega.</p>

        <p>O site As Bentas utiliza cookies e informações de sua navegação (sessão do browser) com o objetivo de traçar um perfil do público que visita o site e aperfeiçoar sempre nossos serviços, produtos, conteúdos e garantir as melhores ofertas e promoções para você. </p>

        <p>Durante todo este processo mantemos suas informações em sigilo absoluto. Para que estes dados permaneçam intactos, nós desaconselhamos expressamente a divulgação de sua senha a terceiros, mesmo a amigos e parentes.</p>

        <p>Ao se cadastrar no site www.asbentas.com, o usuário concorda com os termos dispostos no presente documento.</p>

        <p>O cadastro de usuário é gratuito e será necessário para finalização do processo de compra. O usuário deverá informar os dados solicitados, além de se responsabilizar integralmente pela veracidade e exatidão dos dados fornecidos.  Em caso de informações incorretas fornecidas pelo usuário, As Bentas estará isenta de qualquer responsabilidade decorrente de informações cadastrais incompletas e/ou inverídicas.</p>

        <p>Uma vez efetuado o cadastro com sucesso, o usuário poderá acessar o portal por meio de login (e-mail de cadastro) e senha, dados estes que o usuário se compromete a não divulgar a terceiros, pois será integralmente responsável por toda e qualquer solicitação que seja feita com seu login e senha.</p>

        <p>As alterações sobre nossa política de privacidade e termo de uso serão devidamente informadas neste espaço</p>
      </div>
    </div>

  </div>
</div>
@if(Auth::check())
    <script>
        $crisp.push(["set", "user:email", "{{ Auth::user()->email }}"]);
    </script>
@endif
@yield('javascripts')
</body>


</html>