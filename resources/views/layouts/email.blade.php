<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial scale=1.0"/>
    <title>As Bentas</title>
</head>
<body>
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="container" bgcolor="#ffffff" style="max-width: 600px;">
    <tr>
        <td width="100%">
            <!--Content wrapper-->
            <table width="94%" cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td height="25"></td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td width="100%">
                                    <!-- Content -->
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                        {{-- <tr>
                                            <td>
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                                    <tr>
                                                        <td valign="top">
                                                            <img draggable="false" src="{{URL::asset('/img/email/img_02.png')}}" width="68" height="100" style="display:block; border:none;">
                                                        </td>
                                                        <td valign="top">
                                                            <a href="http://asbentas.com" target="_blank">
                                                                <img draggable="false" src="{{ url('/img/email/img_03.png') }}" width="201" height="150" alt="As Bentas" style="display:block; border:none;">
                                                            </a>
                                                        </td>
                                                        <td valign="top">
                                                            <img draggable="false" src="{{ url('/img/email/img_04.png') }}" width="331" height="100" style="display:block; border:none;">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr> --}}
                                        <tr>
                                            <td align="center">
                                                <img draggable="false" src="{{URL::asset('/img/email/img_b_03.png')}}" width="100" height="76" style="display:block; border:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <img draggable="false" src="{{URL::asset('/img/email/img_b_07.png')}}" width="549" height="50" style="display:block; border:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                                    <tr>
                                                        <td width="70"></td>
                                                        <td>
                                                            <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                                                <tr>
                                                                    <td>
                                                                        @yield('conteudo')
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="70"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td height="30"></td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <img draggable="false" src="{{ url('/img/email/img_13.png') }}" style="display:block; border:none;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="1" bgcolor="#eeeeee"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" align="center" valign="top" border="0" cellpadding="25" cellspacing="0" bgcolor="#ffffff" class="padding-none">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" align="center" valign="top" border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <font face="verdana" size="1" color="#aa231f" style="font-size: 11px; line-height: 18px;">Copyright 2017 - As Bentas - Todos os direitos reservados.</font>
                                                                                </td>
                                                                                <td>
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <a href="https://www.instagram.com/asbentas/" target="_blank">
                                                                                                    <img draggable="false" src="{{ url('/img/email/img_b_15.png') }}" width="35" height="34" style="display:block; border:none;">
                                                                                                </a>
                                                                                            </td>
                                                                                            <td width="10"></td>
                                                                                            <td>
                                                                                                <a href="https://www.facebook.com/asbentas" target="_blank">
                                                                                                    <img draggable="false" src="{{ url('/img/email/img_b_17.png') }}" width="35" height="34" style="display:block; border:none;">
                                                                                                </a>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>