@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Horários
@endsection

@section('content')
<form class="validate" action="{{ $horario->id  ? route('admin.horarios-entrega.atualizar', ['id' => $horario->id]) : route('admin.horarios-entrega.gravar') }}" method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Horário</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $horario->id }}">

                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('horario_inicial') ? 'has-error' : '' }}">
                                    <label>Horário Inicial *</label>
                                    <input type="text" name="horario_inicial" value="{{ old('horario_inicial', $horario->horario_inicial) }}"
                                           class="form-control horario required">
                                    @if($errors->has('horario_inicial'))
                                        <span class="help-inline text-red">{{ $errors->first('horario_inicial') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group {{ $errors->has('horario_final') ? 'has-error' : '' }}">
                                    <label>Horário Final *</label>
                                    <input type="text" name="horario_final" value="{{ old('horario_final', $horario->horario_final) }}"
                                           class="form-control horario required">
                                    @if($errors->has('horario_final'))
                                        <span class="help-inline text-red">{{ $errors->first('horario_final') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.horarios-entrega.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</form>
@endsection


@section('javascripts')
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/horario_form.js"></script>
@endsection
