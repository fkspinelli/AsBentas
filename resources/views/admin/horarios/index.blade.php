@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Horários
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-horario">
                    <thead>
                    <tr>
                        <th class="width-30">#</th>
                        <th>Início</th>
                        <th>Término</th>
                        <th width="20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($horarios as $horario)
                        <tr>
                            <td class="text-center">
                                {{ $horario->id }}
                            </td>
                            <td>
                                {{ $horario->horario_inicial }}
                            </td>
                            <td>
                                {{ $horario->horario_final }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.horarios-entrega.editar', [ 'id' => $horario->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-horario" data-url="{{ route('admin.horarios-entrega.excluir', [ 'id' => $horario->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">Nenhum horário cadastrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.horarios-entrega.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/horario_index.js"></script>
@endsection