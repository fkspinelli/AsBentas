@extends('admin.layouts.admin')
@section('page-title')
    Buscas por CEP
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem de Buscas por CEP</strong>
            </span>
            <ul class="options pull-right list-inline">
                <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                <li>
                    <a href="{{ route('admin.buscascep.download') }}" class="opt panel_download" title="Fazer Download">
                        Fazer Download<i class="fa fa-download"></i>
                    </a>
                </li>
            </ul>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, cep ou email..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.buscascep.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-buscas">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id', 'id')</th>
                        <th>@sortablelink('cep', 'CEP')</th>
                        <th>@sortablelink('email', 'e-mail')</th>
                        <th>@sortablelink('created_at', 'data')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($buscas as $busca)
                        <tr>
                            <td class="text-center">
                                {{ $busca->id }}
                            </td>
                            <td>
                                {{ $busca->cep }}
                            </td>
                            <td>
                                {{ $busca->email }}
                            </td>
                            <td>
                                {{ $busca->created_at->format('d/m/Y') }}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">Nenhuma busca de cep encontrada.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $buscas->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!--pre code-->


            <!-- /pre code -->

        </div>

    </div>
@endsection

@section('javascripts')

@endsection