@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Pratos
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, código, nome ou descrição..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.prato.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-prato">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id', 'id')</th>
                        <th class="5%">@sortablelink('codigo', 'Código')</th>
                        <th width="5%">@sortablelink('icone_id', 'Ícone')</th>
                        <th>@sortablelink('nome', 'Nome')</th>
                        <th>@sortablelink('status', 'Situação')</th>
                        <th width="30%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($pratos as $prato)
                        <tr>
                            <td class="text-center">
                                {{ $prato->id }}
                            </td>
                            <td class="text-center">
                                {{ $prato->codigo }}
                            </td>
                            <td>
                                <img src="{{ url('/assets/images/icons/' . $prato->icone->icone) }}">
                            </td>
                            <td>
                                {{ $prato->nome }}
                            </td>
                            <td>
                                {{ $prato->status }}
                            </td>

                            <td class="text-center">

                                <a href="{{ route('admin.prato.tabela-preco', [ 'id' => $prato->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-table white"></i> Preços
                                </a>
                                <a href="{{ route('admin.prato.editar', [ 'id' => $prato->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-prato" data-url="{{ route('admin.prato.excluir', [ 'id' => $prato->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">Nenhum prato encontrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $pratos->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.prato.novo') }}" class="btn btn-primary">
                    Novo Prato
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/prato_index.js"></script>
@endsection