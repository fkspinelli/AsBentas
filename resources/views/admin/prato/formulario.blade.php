@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Pratos
@endsection

@section('content')
<form class="validate" action="{{ $prato->id  ? route('admin.prato.atualizar', ['id' => $prato->id]) : route('admin.prato.gravar') }}"
      id="form-prato" method="post" enctype="multipart/form-data"
      >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Pratos</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $prato->id }}">

                        <div class="row">
                            <div class="form-group ">
                                <div class="col-md-2 col-sm-12 {{ $errors->has('codigo') ? 'has-error' : '' }}">
                                    <label>Código *</label>
                                    <input type="text" name="codigo" value="{{ old('codigo', $prato->codigo) }}" class="form-control" maxlength="10">
                                    @if($errors->has('codigo'))
                                        <span class="help-inline text-red">{{ $errors->first('codigo') }}</span>
                                    @endif
                                </div>

                                <div class="col-md-6 col-sm-12 {{ $errors->has('nome') ? 'has-error' : '' }}">
                                    <label>Nome *</label>
                                    <input type="text" name="nome" value="{{ old('nome', $prato->nome) }}"  maxlength="80" class="form-control required ">
                                    @if($errors->has('nome'))
                                        <span class="help-inline text-red">{{ $errors->first('nome') }}</span>
                                    @endif
                                </div>

                                <div class="col-md-4 col-sm-12 {{ $errors->has('valor_sugerido') ? 'has-error' : '' }}">
                                    <label>Valor Sugerido *</label>
                                    <input type="text" name="valor_sugerido" value="{{ old('valor_sugerido', $prato->valor_sugerido) }}"
                                           class="form-control money required {{ $errors->has('valor_sugerido') ? 'has-error' : '' }}">
                                    @if($errors->has('valor_sugerido'))
                                        <span class="help-inline text-red">{{ $errors->first('valor_sugerido') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group ">
                                <div class="col-md-12 col-sm-12 {{ $errors->has('descricao') ? 'has-error' : '' }}">
                                    <label>Descrição *</label>
                                    <textarea name="descricao"
                                              class="form-control required {{ $errors->has('descricao') ? 'has-error' : '' }}" maxlength="350" >{{ old('descricao', $prato->descricao) }}  </textarea>
                                    @if($errors->has('descricao'))
                                        <span class="help-inline text-red">{{ $errors->first('descricao') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-5 col-sm-4">
                                    <label>Características</label> <br>
                                @foreach($caracteristicas as $caracteristica)
                                    <label class="switch switch">
                                        <input type="checkbox" name="caracteristica_id[]" value="{{ $caracteristica->id }}"
                                               @if($prato->caracteristicas->contains($caracteristica->id) || in_array($caracteristica->id, old('caracteristica_id', [])))checked @endif>
                                        <span class="switch-label" data-on="Sim" data-off="Não"></span>
                                        <span> {!! $caracteristica->nome !!}</span>
                                    </label>
                                @endforeach
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-12">
                                    <label>Icone *</label>
                                    <input type="hidden" name="icone_id" id="icone_id">
                                    <select name="icone" id="dropdown-icone">
                                        @foreach($icones as $icone)
                                            <option value="{{ $icone->id }}"
                                                    data-imagesrc="{{ url('/assets/images/icons/' . $icone->icone) }}"
                                                    data-description="{{ $icone->nome }}"
                                                    @if(old('icone_id') == $icone->id || $prato->icone && $prato->icone->id == $icone->id) selected @endif>
                                                {{ $icone->nome }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 col-sm-2">
                                    <label>Situação</label><br>
                                    <label class="radio">
                                        <input type="radio" name="status" value="ativo" @if(old('status') == 'ativo' || $prato->status == 'ativo' || $prato->status == null)checked @endif>
                                        <i></i> Ativo
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="status" value="inativo" @if(old('status') == 'inativo' || $prato->status == 'inativo')checked @endif>
                                        <i></i> Inativo
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.prato.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('stylesheets')

@endsection

@section('javascripts')
    <script src="/assets/plugins/ddslick/jquery.ddslick.min.js"></script>
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/prato_form.js"></script>
@endsection
