@extends('admin.layouts.admin')
@section('page-title')
    Newsletter
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem de E-mails</strong>
            </span>
            <ul class="options pull-right list-inline">
                <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                <li>
                    <a href="{{ route('admin.newsletter.download') }}" class="opt panel_download" title="Fazer Download">
                        Fazer Download<i class="fa fa-download"></i>
                    </a>
                </li>
            </ul>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, nome ou email..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.newsletter.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-newsletter">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id', 'id')</th>
                        <th>@sortablelink('nome', 'nome')</th>
                        <th>@sortablelink('email', 'e-mail')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($newsletters as $newsletter)
                        <tr>
                            <td class="text-center">
                                {{ $newsletter->id }}
                            </td>
                            <td>
                                {{ $newsletter->nome }}
                            </td>
                            <td>
                                {{ $newsletter->email }}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">Nenhum registro encontrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>

            <div class="text-center">
                {{ $newsletters->appends(request()->input())->links() }}
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!--pre code-->


            <!-- /pre code -->

        </div>

    </div>
@endsection

@section('javascripts')

@endsection