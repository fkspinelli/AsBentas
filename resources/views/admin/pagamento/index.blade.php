@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Pagamento
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-pagamento">
                    <thead>
                    <tr>
                        <th class="width-30">Id</th>
                        <th> Tipo </th>
                        <th> Situação</th>
                        <th class="width-400"> </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($pagamentos as $pagamento)

                        <tr>

                            <td>
                                {{ $pagamento->id}}
                            </td>
                            <td>
                                {{ $pagamento->tipo}}
                            </td>
                            <td>
                                {{ $pagamento->status}}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.pagamento.editar', [ 'id' => $pagamento->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-pagamento" data-url="{{ route('admin.pagamento.excluir', [ 'id' => $pagamento->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $pagamentos->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.pagamento.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/pagamento_index.js"></script>
@endsection