@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Pagamentos
@endsection

@section('content')
    <form class="validate"
          action="{{ $pagamento->id  ? route('admin.pagamento.atualizar', ['id' => $pagamento->id]) : route('admin.pagamento.gravar') }}"
          method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-transparent">
                        <strong>Registro de Pagamento</strong>
                    </div>

                    <div class="panel-body">

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $pagamento->id }}">

                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-md-3 col-sm-3 {{ $errors->has('tipo') ? 'has-error' : '' }}">
                                        <label>Tipo *</label>
                                        <input name="tipo" id="tipo" value="{{ old('tipo', $pagamento->tipo) }}"
                                               class="form-control required">
                                        @if($errors->has('tipo'))
                                            <span class="help-inline text-red">{{ $errors->first('tipo') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <label>Status *</label><br>
                                        <label class="radio">
                                            <input type="radio" name="status" value="ativo"
                                                   @if($pagamento->status == 'ativo' || $pagamento->status == '')checked @endif
                                                   @if(old('status', $pagamento->status)  == 'ativo') checked @endif>
                                            <i></i> Ativo
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="status" value="inativo"
                                                   @if($pagamento->status == 'inativo' && old('status', $pagamento->status) == '')checked @endif
                                                   @if(old('status', $pagamento->status) == 'inativo' ) checked @endif>
                                            <i></i> Inativo
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group {{ $errors->has('valor') ? 'has-error' : '' }}">
                                    
                                </div>
                            </div>

                            


                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.pagamento.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="/assets/plugins/pikaday/pikaday.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/desconto_form.js"></script>
@endsection
