@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Descontos
@endsection

@section('content')
    <form class="validate"
          action="{{ $desconto->id  ? route('admin.desconto.atualizar', ['id' => $desconto->id]) : route('admin.desconto.gravar') }}"
          method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-transparent">
                        <strong>Registro de Desconto</strong>
                    </div>

                    <div class="panel-body">

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $desconto->id }}">

                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-md-2 col-sm-2 {{ $errors->has('dia') ? 'has-error' : '' }}">
                                        <label>Dia *</label>
                                        <input type="number" name="dia" id="dia" value="{{ old('dia', $desconto->dia) }}"
                                               class="form-control required">
                                        @if($errors->has('dia'))
                                            <span class="help-inline text-red">{{ $errors->first('dia') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-sm-3 {{ $errors->has('desconto') ? 'has-error' : '' }}">
                                        <label>Desconto *</label>
                                        <input type="text" name="desconto" id="desconto" value="{{ old('desconto', $desconto->desconto) }}"
                                               class="form-control required percent">
                                        @if($errors->has('desconto'))
                                            <span class="help-inline text-red">{{ $errors->first('desconto') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group {{ $errors->has('valor') ? 'has-error' : '' }}">
                                    
                                </div>
                            </div>

                            


                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.desconto.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="/assets/plugins/pikaday/pikaday.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/desconto_form.js"></script>
@endsection
