@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Cupons
@endsection
@section('content')

    <div id="" class="panel panel-default">
        

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label>Código </label>
                        <input type="text" value="{{ $cupom->codigo }}" class="form-control" disabled>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label>Expira em </label>
                        <input type="text" value="{{ $cupom->data_expiracao ? $cupom->data_expiracao->format('d/m/Y') : null}}" class="form-control" disabled>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label>Tipo</label>
                        <input type="text" value="{{ $cupom->tipo == '$' ? 'Valor' : 'Porcentagem' }}" class="form-control" disabled>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <label>Aplicado em</label>
                        <input type="text" value="{{ $cupom->tipo_cupom == 0 ? 'Campanha' : 'Único' }}" class="form-control" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem de Utilizações</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-cupom">
                    <thead>
                    <tr>
                        <th>Usuário</th>
                        <th>Data Utilização</th>
                        <th>Pedido</th>
                        
                        <th>Valor Pedido</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cupom->pedidos as $pedido)
                        <tr>
                            <td>
                                {{ $pedido->pedido->usuario->name }}
                            </td>
                            <td>
                                {{ $pedido->created_at->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ $pedido->pedido->id }}
                            </td>
                            <td>
                                {{ $pedido->pedido->valor_total }}
                            </td>
                            
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/cupom_index.js"></script>
@endsection