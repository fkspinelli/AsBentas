@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Descontos
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-desconto">
                    <thead>
                    <tr>
                        <th class="width-30">Dia</th>
                        <th >Valor</th>
                        <th class="width-400"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($descontos as $desconto)
                        <tr>
                        
                            <td>
                                {{ $desconto->dia}}
                            </td>
                            <td>
                                {{ $desconto->desconto}}%
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.desconto.editar', [ 'id' => $desconto->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-desconto" data-url="{{ route('admin.desconto.excluir', [ 'id' => $desconto->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $descontos->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.desconto.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/desconto_index.js"></script>
@endsection