@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Cardápio
@endsection

@section('content')
<form class="validate" action="{{ $cardapio->id  ? route('admin.cardapio.atualizar', ['id' => $cardapio->id]) : route('admin.cardapio.gravar') }}"
      method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Cardápio</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $cardapio->id }}">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-3 col-sm-12 {{ $errors->has('data_cardapio') ? 'has-error' : '' }}">
                                    <label>Data *</label>
                                    <input type="text" name="data_cardapio" id="data-cardapio" 
                                            value="{{ (!empty($cardapio->data_cardapio)) ? $cardapio->data_cardapio->format('d/m/Y') : '' }}" class="form-control required date">
                                    @if($errors->has('data_cardapio'))
                                        <span class="help-inline text-red">{{ $errors->first('data_cardapio') }}</span>
                                    @endif
                                    <input type="text" name="dia_da_semana" id="dia_da_semana" hidden="">
                                </div>

                                <div class="col-md-3 col-sm-4">
                                    <label>Situação </label>
                                    <select class="selectize" name="status" id="situacao">
                                        <option value="ativo" @if($cardapio->status == 'ativo' || empty($cardapio->status)) selected @endif >Ativo</option>
                                        <option value="inativo" @if($cardapio->status == 'inativo') selected @endif>Inativo</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-12">
                                    <strong>Pratos</strong>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <strong>Bebidas</strong>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-12">
                                    <label>Selecione </label>
                                    <select class="selectize" id="select-prato">
                                        <option></option>
                                        @foreach($pratos as $prato)
                                            <option value="{{ $prato->id }}">{{ $prato->nome }}</option>
                                        @endforeach
                                    </select>
                                    <a class="btn btn-primary btn-sm" id="btn-adicionar-prato">
                                        adicionar
                                    </a>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <label>Selecione </label>
                                    <select class="selectize" id="select-bebida">
                                        <option value=""></option>
                                        @foreach($bebidas as $bebida)
                                            <option value="{{ $bebida->id }}">{{ $bebida->nome }}</option>
                                        @endforeach
                                    </select>
                                    <a class="btn btn-primary btn-sm" id="btn-adicionar-bebida">
                                        adicionar
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-vertical-middle nomargin" id="tabela-pratos">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th width="20%">Ordem</th>
                                            <th width="20%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($cardapio->pratos as $prato)
                                                <tr id="tr_prato_{{ $prato->id }}">
                                                    <td>
                                                        <input type="hidden" name="prato_id[{{$prato->id }}]" value="{{ $prato->id }}" id="prato_id_{{ $prato->id }}">
                                                        {{ $prato->nome }}
                                                    </td>
                                                    <td>
                                                        <input name="prato_ordem[{{$prato->id }}]"  type="number" value="{{ $prato->pivot->ordem }}"  class="form-control" id="prato_ordem_{{ $prato->id }}">
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-default btn-xs excluir-prato" data-id="{{ $prato->id }}">
                                                            <i class="fa fa-times white"></i> Remover
                                                        </a>
                                                    </td>
                                                </tr>
                                            @empty

                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-vertical-middle nomargin" id="tabela-bebidas">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th width="20%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @forelse($cardapio->bebidas as $bebida)
                                            <tr id="tr_bebida_{{ $bebida->id }}">
                                                <td>
                                                    <input type="hidden" name="bebida_id[]" value="{{ $bebida->id }}" id="bebida_id_{{ $bebida->id }}">
                                                    {{ $bebida->nome }}
                                                </td>
                                                <td>
                                                    <a href="#" class="btn btn-default btn-xs excluir-bebida" data-id="{{ $bebida->id }}">
                                                        <i class="fa fa-times white"></i> Remover
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty

                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">


                            </div>
                        </div>
                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.cardapio.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.standalone.min.css" />
@endsection

@section('javascripts')
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/cardapio_form.js"></script>
@endsection
