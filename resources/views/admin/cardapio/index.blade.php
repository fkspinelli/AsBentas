@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Cardápios
@endsection
@section('content')

    <div id="panel-misc-portlet-l1" class="panel panel-default">



        <!-- panel content -->
        <div class="panel-body">

            <form class="validate" action="" method="post" >
                {{ csrf_field() }}
                <fieldset>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3">
                                <select name="mes" class="form-control">
                                    @foreach($meses as $codigo => $nome)
                                        <option value="{{ $codigo }}" @if($mes == $codigo) selected @endif>
                                            {{ $nome }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" name="ano" class="form-control ano" value="{{ $ano }}">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </fieldset>

            </form>

        </div>
        <!-- /panel content -->

    </div>

    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-cardapio">
                    <thead>
                    <tr>
                        <th class="width-50">
                            @sortablelink('id', '#')
                        </th>
                        <th>
                            @sortablelink('data_cardapio', 'Data')
                        </th>
                        <th>Pratos</th>
                        <th>Bebidas</th>
                        <th>
                            @sortablelink('status', 'Situação')
                        </th>
                        <th width="20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($cardapios as $cardapio)
                        <tr>
                            <td class="text-center">
                                {{ $cardapio->id }}
                            </td>
                            <td>
                                {{ date('d/m/Y', strtotime($cardapio->data_cardapio)) }}
                            </td>
                            <td>
                                {{ count($cardapio->pratos) }}
                            </td>
                            <td>
                                {{ count($cardapio->bebidas) }}
                            </td>
                            <td>
                                {{ $cardapio->status }}
                            </td>

                            <td class="text-center">
                                <a href="{{ route('admin.cardapio.editar', [ 'id' => $cardapio->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-cardapio" data-url="{{ route('admin.cardapio.excluir', [ 'id' => $cardapio->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!--pre code-->
            <div class="text-left">
                <a href="{{ route('admin.cardapio.novo') }}" class="btn btn-primary">
                    Novo Cardápio
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/cardapio_index.js"></script>
@endsection