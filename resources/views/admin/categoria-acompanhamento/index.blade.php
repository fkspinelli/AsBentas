@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Pratos - Categoria Acompanhamento
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-categoria">
                    <thead>
                    <tr>
                        <th class="width-30">#</th>
                        <th>Nome</th>
                        <th>Status</th>
                        <th width="20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categorias as $categoria)
                        <tr>
                            <td class="text-center">
                                {{ $categoria->id }}
                            </td>
                            <td>
                                {{ $categoria->nome }}
                            </td>
                            <td>
                                {{ $categoria->status }}
                            </td>

                            <td class="text-center">
                                <a href="{{ route('admin.categoria-acompanhamento.editar', [ 'id' => $categoria->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-categoria" data-url="{{ route('admin.categoria-acompanhamento.excluir', [ 'id' => $categoria->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">Nenhuma Categoria Cadastrada.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!-- pre code-->
            <div class="text-left">
                <a href="{{ route('admin.categoria-acompanhamento.novo') }}" class="btn btn-primary">
                    Nova Categoria
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/categoria_index.js"></script>
@endsection