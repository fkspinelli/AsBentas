@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Acompanhamentos - Categorias
@endsection

@section('content')
<form class="validate" action="{{ $categoria->id  ? route('admin.categoria-acompanhamento.atualizar', ['id' => $categoria->id]) : route('admin.categoria-acompanhamento.gravar') }}" method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Categorias</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $categoria->id }}">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label>Nome *</label>
                                    <input type="text" name="nome" value="{{ $categoria->nome }}" class="form-control required">
                                    @if($errors->has('nome'))
                                        <span class="help-inline text-red">{{ $errors->first('nome') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Situação *</label>
                                    <select name="status" class="form-control required">
                                        @foreach($categoria->situacoes() as $status => $label)
                                            <option value="{{ $status }}" @if($categoria->status == $status) selected @endif>{{ $label }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('status'))
                                        <span class="help-inline">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.categoria-acompanhamento.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('stylesheets')

@endsection

@section('javascripts')

@endsection
