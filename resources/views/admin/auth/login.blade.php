@extends('admin.layouts.login')


@section('content')
    <div class="padding-15">

        <div class="login-box">

            <!-- login form -->
            <form action="{{ route('admin.login') }}" method="POST" class="sky-form boxed">
                {{ csrf_field() }}

                <header><i class="fa fa-users"></i> Área Administrativa</header>

                <!--
                <div class="alert alert-danger noborder text-center weight-400 nomargin noradius">
                    Invalid Email or Password!
                </div>

                <div class="alert alert-warning noborder text-center weight-400 nomargin noradius">
                    Account Inactive!
                </div>

                <div class="alert alert-default noborder text-center weight-400 nomargin noradius">
                    <strong>Too many failures!</strong> <br />
                    Please wait: <span class="inlineCountdown" data-seconds="180"></span>
                </div>
                -->

                <fieldset>

                    <section>
                        <label class="label">E-mail</label>
                        <label class="input">
                            <i class="icon-append fa fa-envelope"></i>
                            <input type="email" name="email" value="{{ old('email') }}" required>
                            <span class="tooltip tooltip-top-right">Informe seu E-mail</span>
                        </label>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </section>

                    <section>
                        <label class="label">Senha</label>
                        <label class="input">
                            <i class="icon-append fa fa-lock"></i>
                            <input type="password" name="password" required>
                            <b class="tooltip tooltip-top-right">Informe sua Senha</b>
                        </label>
                        <label class="checkbox"><input type="checkbox" name="checkbox-inline" checked><i></i>Lembrar Login</label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-primary pull-right">Entrar</button>
                    <div class="forgot-password pull-left">
                        <a href="{{ route('admin.password.request')  }}">Esqueci a Senha</a> <br />
                    </div>
                </footer>
            </form>
            <!-- /login form -->

            <hr />
            <!--
            <div class="text-center">
                Or sign in using:
            </div>


            <div class="socials margin-top-10 text-center"><!-- more buttons: ui-buttons.html --
                <a href="#" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                <a href="#" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
            </div>
            -->

        </div>

    </div>

@endsection
