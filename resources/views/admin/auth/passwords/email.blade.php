@extends('admin.layouts.login')

@section('content')
    <div class="padding-15">

        <div class="login-box">

            <!--
            <div class="alert alert-danger noradius">Email not found!</div>
            <div class="alert alert-success noradius">Email sent!</div> -->
            @if (session('status'))
                <div class="alert alert-success noradius">
{{ session('status') }}
                        </div>
                    @endif


            <!-- password form -->
            <form method="post" class="sky-form boxed " action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <header><i class="fa fa-users"></i> Esqueci a senha</header>

                <fieldset>

                    <label class="label">E-mail</label>
                    <label class="input">
                        <i class="icon-append fa fa-envelope"></i>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                        <span class="tooltip tooltip-top-right">Type your Email</span>
                    </label>
                    <a href="{{ route('admin.login') }}">Voltar para o Login</a>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-refresh"></i> Reset Passsword</button>
                </footer>
            </form>
            <!-- /password form -->

            <div class="alert alert-default noradius">
                The reset link will be sent to your email address. Click the link and reset your account password.
            </div>

            <hr>

            <div class="text-center">
                Or sign up using:
            </div>


            <div class="socials margin-top-10 text-center"><!-- more buttons: ui-buttons.html -->
                <a href="#" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                <a href="#" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
            </div>

        </div>

    </div>
@endsection


@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
