@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Cupons
@endsection

@section('content')
    <form class="validate"
          action="{{ $cupom->id  ? route('admin.cupom.atualizar', ['id' => $cupom->id]) : route('admin.cupom.gravar') }}"
          method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <!-- ------ -->
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-transparent">
                        <strong>Registro de Cupom</strong>
                    </div>

                    <div class="panel-body">

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $cupom->id }}">

                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-md-3 col-sm-3 {{ $errors->has('codigo') ? 'has-error' : '' }}">
                                        <label>Código *</label>
                                        <input type="text" name="codigo" id="codigo" value="{{ old('codigo', $cupom->codigo) }}"
                                               class="form-control required" maxlength="40">
                                        @if($errors->has('codigo'))
                                            <span class="help-inline text-red">{{ $errors->first('codigo') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-4 col-sm-4 {{ $errors->has('valor') ? 'has-error' : '' }}">
                                        <label>Valor *</label>
                                        <input type="text" name="valor" id="valor" value="{{ old('valor', $cupom->valor) }}"
                                               class="form-control required valor" >
                                        @if($errors->has('valor'))
                                            <span class="help-inline text-red">{{ $errors->first('valor') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-5 col-sm-5 {{ $errors->has('data_expiracao') ? 'has-error' : '' }}">
                                            <label>Data  de Expiração</label>
                                            <input type="text" name="data_expiracao" id="data_expiracao" value="{{ old('data_expiracao', $cupom->data_expiracao) }}"
                                                   class="form-control required data_expiracao">
                                            @if($errors->has('data_expiracao'))
                                                <span class="help-inline text-red">{{ $errors->first('data_expiracao') }}</span>
                                            @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-md-3 col-sm-3">
                                        <label>Tipo *</label><br>
                                        <label class="radio">
                                            <input type="radio" name="tipo" value="%"
                                                   @if($cupom->tipo == '%' || $cupom->tipo == null)checked @endif
                                                   @if(old('tipo', $cupom->tipo) == '%') checked @endif >
                                            <i></i> %
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="tipo" value="$"
                                                   @if($cupom->tipo == '$' && old('tipo', $cupom->tipo) == '')checked @endif
                                                   @if(old('tipo', $cupom->tipo) == '$') checked @endif>
                                            <i></i> $
                                        </label>
                                    </div>
                                    
                                    <div class="col-md-4 col-sm-4">
                                        <label>Modo de Uso *</label><br>
                                        <label class="radio">
                                            <input type="radio" name="tipo_cupom" value="0"
                                                   @if($cupom->tipo_cupom == 0 || $cupom->tipo_cupom == null)checked @endif
                                                   @if(old('tipo_cupom', $cupom->tipo_cupom) == 0) checked @endif >

                                            <i></i> Campanha
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="tipo_cupom" value="1"
                                                   @if($cupom->tipo_cupom == 1 && old('tipo_cupom', $cupom->tipo_cupom) == null)checked @endif
                                                   @if(old('tipo_cupom', $cupom->tipo_cupom) == 1) checked @endif>
                                            <i></i> Único por Usuário
                                        </label>
                                    </div>
                                    <div class="col-md-5 col-sm-5">
                                        <label>Status *</label><br>
                                        <label class="radio">
                                            <input type="radio" name="status" value="ativo"
                                                   @if($cupom->status == 'ativo' || $cupom->status == '')checked @endif
                                                   @if(old('status', $cupom->status)  == 'ativo') checked @endif>
                                            <i></i> Ativo
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="status" value="inativo"
                                                   @if($cupom->status == 'inativo' && old('status', $cupom->status) == '')checked @endif
                                                   @if(old('status', $cupom->status) == 'inativo' ) checked @endif>
                                            <i></i> Inativo
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.cupom.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="/assets/plugins/pikaday/pikaday.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/cupom_form.js"></script>
@endsection
