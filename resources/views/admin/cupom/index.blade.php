@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Cupons
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id ou código..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.cupom.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-cupom">
                    <thead>
                    <tr>
                        <th class="width-50">
                            @sortablelink('id', '#')
                        </th>
                        <th>
                            @sortablelink('codigo', 'Código')
                        </th>
                        <th>Tipo</th>
                        <th>Valor</th>
                        <th>
                            @sortablelink('data_expiracao', 'Data de expiração')</th>
                        <th>
                            @sortablelink('status', 'Status')
                        </th>
                        <th>
                            @sortablelink('tipo_cupom', 'Tipo Cupom')
                        </th>
                        <th>Qtd Utilização</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($cupons as $cupom)
                        <tr>
                            <td class="text-center">
                                {{ $cupom->id }}
                            </td>
                            <td>
                                {{ $cupom->codigo}}
                            </td>
                            <td>
                                {{ $cupom->tipo}}
                            </td>
                            <td>
                                {{ number_format($cupom->valor,2,',','.') }}
                            </td>
                            <td>
                                {{ $cupom->data_expiracao ? $cupom->data_expiracao->format('d/m/Y H:i') : null}}
                            </td>
                            <td>
                                {{ $cupom->status }}
                            </td>
                            <td>
                                {{ $cupom->getTipoCupom($cupom->tipo_cupom) }}
                            </td>
                            <td>
                                {{ $cupom->pedidos->count() }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.cupom.utilizacao', [ 'id' => $cupom->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-eye white"></i> Utilização
                                </a>
                                <a href="{{ route('admin.cupom.editar', [ 'id' => $cupom->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                @if($cupom->pedidos->count() == 0)
                                    <a href="#" class="btn btn-default btn-xs excluir-cupom" data-url="{{ route('admin.cupom.excluir', [ 'id' => $cupom->id] ) }}">
                                        <i class="fa fa-times white"></i> Remover
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">Nenhum Cupom encontrado</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $cupons->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.cupom.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/cupom_index.js"></script>
@endsection