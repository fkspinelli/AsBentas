@extends('admin.layouts.admin')

@section('page-title')
    Gestão de FAQs
@endsection

@section('content')
<form class="validate" action="{{ $faq->id  ? route('admin.faq.atualizar', ['id' => $faq->id]) : route('admin.faq.gravar') }}" method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de FAQ</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $faq->id }}">

                        <div class="row">
                            <div class="form-group {{ $errors->has('pergunta') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Pergunta *</label>
                                    <input type="text" name="pergunta" value="{{ old('pergunta', $faq->pergunta) }}" class="form-control required" maxlength="170">
                                    @if($errors->has('pergunta'))
                                        <span class="help-inline text-red">{{ $errors->first('pergunta') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group {{ $errors->has('resposta') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Resposta *</label>
                                    <textarea name="resposta" class="form-control required" id="faq" maxlength="340">{{ old('resposta', $faq->resposta) }} </textarea>
                                    @if($errors->has('resposta'))
                                        <span class="help-inline text-red">{{ $errors->first('resposta') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group {{ $errors->has('ordem') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Ordem</label>
                                    <input type="number" name="ordem" value="{{ old('ordem', $faq->ordem) }}" class="form-control required">
                                    @if($errors->has('ordem'))
                                        <span class="help-inline text-red">{{ $errors->first('ordem') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label class="radio">

                                        <input type="radio" name="status" value="ativo" @if($faq->status == 'ativo' || $faq->status == null)checked @endif>
                                        <i></i> Ativo
                                    </label>
                                    <label class="radio">
                                        <input type="radio" name="status" value="inativo" @if($faq->status == 'inativo')checked @endif>
                                        <i></i> Inativo
                                    </label>
                                </div>
                            </div>
                        </div>


                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.faq.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</form>
@endsection

@section('stylesheets')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script type="text/javascript" src="/assets/js/admin/faq_form.js"></script>
@endsection
