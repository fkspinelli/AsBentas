@extends('admin.layouts.admin')
@section('page-title')
    Gestão de FAQs
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, pergunta, resposta..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.faq.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-faq">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id','id')</th>
                        <th>@sortablelink('pergunta', 'pergunta')</th>
                        <th>@sortablelink('resposta', 'resposta')</th>
                        <th width="6%">@sortablelink('ordem', 'ordem')</th>
                        <th width="20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($faqs as $faq)
                        <tr>
                            <td class="text-center">
                                {{ $faq->id }}
                            </td>
                            <td>
                                {{ substr($faq->pergunta,0,100) }}
                            </td>
                            <td>
                                {{ substr($faq->resposta, 0, 100) }}
                            </td>
                            <td class="text-center">
                                {{ $faq->ordem }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.faq.editar', [ 'id' => $faq->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-faq" data-url="{{ route('admin.faq.excluir', [ 'id' => $faq->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.faq.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/faq_index.js"></script>
@endsection