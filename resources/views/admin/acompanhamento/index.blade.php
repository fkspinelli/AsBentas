@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Acompanhamentos
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, código, nome ou descrição..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.acompanhamento.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>

            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-acompanhamento">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id', 'id')</th>
                        <th>@sortablelink('nome', 'nome')</th>
                        <th width="15%">@sortablelink('categoria', 'Categoria')</th>
                        <th width="10%">@sortablelink('status', 'situação')</th>
                        <th width="25%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($acompanhamentos as $acompanhamento)
                        <tr>
                            <td class="text-center">
                                {{ $acompanhamento->id }}
                            </td>
                            <td>
                                {{ $acompanhamento->nome }}
                            </td>
                            <td>
                                {{ $acompanhamento->categoria ? $acompanhamento->categoria->nome : ''}}
                            </td>
                            <td>
                                {{ $acompanhamento->status }}
                            </td>

                            <td class="text-center">
                                <a href="{{ route('admin.acompanhamento.tabela-preco', [ 'id' => $acompanhamento->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-table white"></i> Preços
                                </a>

                                <a href="{{ route('admin.acompanhamento.editar', [ 'id' => $acompanhamento->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-acompanhamento" data-url="{{ route('admin.acompanhamento.excluir', [ 'id' => $acompanhamento->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">Nenhum Acompanhamento encontrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $acompanhamentos->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.acompanhamento.novo') }}" class="btn btn-primary">
                    Novo Acompanhamento
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/acompanhamento_index.js"></script>
@endsection