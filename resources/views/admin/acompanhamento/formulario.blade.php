@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Acompanhamentos
@endsection

@section('content')
<form class="validate" action="{{ $acompanhamento->id  ? route('admin.acompanhamento.atualizar', ['id' => $acompanhamento->id]) : route('admin.acompanhamento.gravar') }}" method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Acompanhamentos</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $acompanhamento->id }}">

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 {{ $errors->has('nome') ? 'has-error' : '' }}">
                                        <label>Nome *</label>
                                        <input type="text" name="nome" value="{{ $acompanhamento->nome }}" class="form-control required" maxlength="80">
                                        @if($errors->has('nome'))
                                            <span class="help-inline text-red">{{ $errors->first('nome') }}</span>
                                        @endif
                                </div>
                                <div class="col-md-6 col-sm-6 {{ $errors->has('valor_sugerido') ? 'has-error' : '' }}">
                                    <label>Valor Sugerido *</label>
                                    <input type="text" name="valor_sugerido" value="{{ $acompanhamento->valor_sugerido }}" class="form-control money required">
                                    @if($errors->has('valor_sugerido'))
                                        <span class="help-inline text-red">{{ $errors->first('valor_sugerido') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="form-group ">


                                <div class="col-md-6 col-sm-6 ">
                                    <label>Categoria *</label>
                                        <select name="categoria_id" class="form-control required ">
                                            @foreach($categorias as $categoria)
                                                <option value="{{ $categoria->id }}" @if($acompanhamento->categoria == $categoria) selected @endif>
                                                    {{ $categoria->nome }}
                                                </option>
                                            @endforeach
                                        </select>
                                    @if($errors->has('categoria_id'))
                                        <span class="help-inline ">{{ $errors->first('categoria_id') }}</span>
                                    @endif
                                </div>

                                <div class="col-md-6 col-sm-6 {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label>Situação *</label>

                                    <select name="status" class="form-control required">
                                        {{--@foreach($acompanhamento->situacoes() as $status => $label)--}}
                                        {{--<option>{{ $label }}</option>--}}
                                        {{--@endforeach--}}
                                        <option>Ativo</option>
                                        <option>Inativo</option>
                                    </select>

                                    @if($errors->has('status'))
                                        <span class="help-inline">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.acompanhamento.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/select2/css/select2.min.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@endsection

@section('javascripts')
    
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/plugins/select2/js/select2.min.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/acompanhamento_form.js"></script>
@endsection
