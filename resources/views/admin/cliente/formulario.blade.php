@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Clientes
@endsection

@section('content')
<form class="validate" action="{{ $usuario->id  ? route('admin.clientes.atualizar', ['id' => $usuario->id]) : route('admin.usuarios.gravar') }}" method="post" enctype="multipart/form-data" >
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Clientes</strong>
                </div>

                <div class="panel-body">

                    <fieldset>
                        <!-- required [php action request] -->
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $usuario->id }}">

                        <div class="row">
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Nome *</label>
                                    <input type="text" name="name" value="{{ $usuario->name }}" class="form-control required">
                                    @if($errors->has('name'))
                                        <span class="help-inline">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Email *</label>
                                    <input type="email" name="email" value="{{ $usuario->email }}" class="form-control required">
                                    @if($errors->has('email'))
                                        <span class="help-inline">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if ($usuario->provider)
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12">
                                    <label>Login Social </label>
                                </div>

                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Senha </label>
                                    <input type="password" name="password" value="" class="form-control" >
                                    @if($errors->has('password'))
                                        <span class="help-inline">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                <div class="col-md-12 col-sm-12">
                                    <label>Confirmar Senha </label>
                                    <input type="password" name="password_confirmation" value="" class="form-control" >
                                    @if($errors->has('password_confirmation'))
                                        <span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>

                            </div>
                        </div>
                        @endif



                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12 margin-top-30">
                            <button class="btn btn-primary" type="submit">
                                Salvar
                            </button>
                            <a class="btn btn-warning" href="{{ route('admin.clientes.index') }}">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Empresas</strong>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical-middle nomargin" id="tabela-cliente">
                            <thead>
                            <tr>
                                <th class="width-30">#</th>
                                <th>Empresa</th>
                                <th>Data Início</th>
                                <th>Data Término</th>
                                <!-- th width="20%"></th-->
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($usuario->empresas as $empresa)

                                <tr>
                                    <td class="text-center">
                                        {{ $empresa->id }}
                                    </td>
                                    <td>
                                        {{ $empresa->nome }}
                                    </td>
                                    <td>
                                        {{ date('d/m/Y', strtotime($empresa->pivot->dt_inicio)) }}
                                    </td>
                                    <td>
                                        @if($empresa->pivot->dt_termino)
                                            {{ date('d/m/Y', strtotime($empresa->pivot->dt_termino)) }}
                                        @endif
                                    </td>
                                    <!-- td class="text-center">
                                        <a href="{{ route('admin.clientes.editar', [ 'id' => $usuario->id] ) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-ban white"></i> Desativar
                                        </a>
                                        <a href="{{ route('admin.clientes.editar', [ 'id' => $usuario->id] ) }}" class="btn btn-default btn-xs">
                                            <i class="fa fa-check white"></i> Ativar
                                        </a>
                                    </td !-->
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/select2/select2.min.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="/assets/plugins/select2/select2.min.js"></script>
    <script src="/assets/js/admin/cliente_form.js"></script>
@endsection
