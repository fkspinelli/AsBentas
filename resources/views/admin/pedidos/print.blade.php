@extends('admin.layouts.print')
@section('content')
    <div id="wrapper">

        <div class="padding-20">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-6 col-xs-6">

                            <h4>Detalhe do Cliente</h4>
                            <ul class="list-unstyled">
                                                <li><strong>Nome:</strong> {{ $pedido->usuario->name }}</li>
                                <li><strong>E-mail:</strong> {{ $pedido->usuario->email }}</li>
                                <li><strong>Telefone:</strong> {{ $pedido->usuario->telefone }}</li>
                            </ul>

                        </div>

                        <div class="col-md-6 col-xs-6 text-right">

                            <h4>Detalhes do <strong>Pedido</strong></h4>
                            <ul class="list-unstyled">
                                <li><strong>Código:</strong> {{ $pedido->id }}</li>
                                <li><strong>Situação:</strong> {{ $pedido->lablePagamento($pedido->id) }}</li>
                                <li><strong>Fulfillment:</strong> {{ $pedido->lableFulfillment($pedido->id) }}</li>
                                <li><strong>Local de Entrega:</strong> {{ $pedido->endereco->label() }}</li>
                                <li><strong>Complemento:</strong> {{ $pedido->complemento or 'Não Informado'}}</li>
                                <!--li><strong>PIN Entrega:</strong> {{ $pedido->usuario->pin }}</li-->
                            </ul>

                        </div>

                    </div>

                    @php
                        $total = 0;
                    @endphp
                    @foreach($pedido->pedidoCardapios as $cardapio)
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <strong>Pedido para o dia Data:</strong> {{ $cardapio->cardapio->data_cardapio->format('d/m/Y') }}<br>
                                <strong>Situação:</strong> {{$cardapio->lablePagamento($cardapio->id)}}<br>
                                <strong>Fulfillment:</strong> {{$cardapio->lableFulfillment($cardapio->id)}}
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-condensed nomargin">
                                <thead>
                                <tr>
                                    <th width="70%"></th>Item</th>
                                    <th width="20%">Quantidade</th>
                                    <th class="text-right">Valor</th>
                                    <th width="5%">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $subtotal = 0;
                                @endphp
                                @foreach($cardapio->itens as $item)
                                    <tr>
                                        <td>
                                            <strong>{{ $item->descricao }}</strong>
                                        </td>
                                        <td>{{ $item->quantidade }}</td>
                                        <td class="text-right">R$ {{ number_format($item->valor, 2, ',', '') }}</td>
                                        <td></td>
                                    </tr>
                                    @php
                                        $total += $item->valor;
                                        $subtotal += $item->valor;
                                    @endphp
                                @endforeach
                                @php($subtotal += $cardapio->valor_frete)
                                <tr>
                                    <td>
                                        <strong> Frete</strong>
                                    </td>
                                    <td></td>
                                    <td class="text-right">R$ {{ number_format($cardapio->valor_frete, 2, ',', '') }}</td>
                                    <td></td>
                                </tr>
                                @if($cardapio->desconto_combo > 0)
                                    @php($subtotal -= $cardapio->desconto_combo)
                                    <tr>
                                        <td>
                                            <strong> Desconto Combo</strong>
                                        </td>
                                        <td></td>
                                        <td class="text-right">- R$ {{ number_format($cardapio->desconto_combo, 2, ',', '') }}</td>
                                        <td></td>
                                    </tr>
                                @endif
                                @if($cardapio->desconto > 0)
                                    @php($subtotal -= $cardapio->desconto)
                                    <tr>
                                        <td>
                                            <strong> Desconto</strong>
                                        </td>
                                        <td></td>
                                        <td class="text-right">- R$ {{ number_format($cardapio->desconto, 2, ',', '') }}</td>
                                        <td></td>
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="2" class="text-right">
                                        <strong>SubTotal:</strong>
                                    </td>
                                    <td class="text-right">
                                        R$ {{ number_format($subtotal, 2, ',', '') }}
                                    </td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    @endforeach
                    <hr class="nomargin-top" />
                    <div class="table-responsive">
                        <table class="table table-condensed nomargin">
                            @if($pedido->cupomUtilizado)
                                <tr>
                                    <td colspan="2" class="text-right">
                                        <strong>Cupom ({{ $pedido->cupomUtilizado->cupom->codigo }}):</strong>
                                        R$ {{ number_format($pedido->valor_cupom_desconto, 2, ',', '' ) }}
                                    </td>
                                    <td class="text-right"></td>
                                    <td width="1%">&nbsp; </td>
                                </tr>
                            @endif
                            <tr>
                                <td colspan="2" class="text-right">
                                    <strong>Total:</strong>
                                    &nbsp; &nbsp; &nbsp; &nbsp;
                                    R$ {{ number_format($pedido->valor_total, 2, ',', '') }}
                                </td>
                                <td class="text-right"></td>
                                <td width="1%">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <hr class="nomargin-top" />

                    <div class="row">

                        <div class="col-xs-8">
                            <h4>Detalhe Entrega</h4>

                            <p class="nomargin nopadding">
                                {{ $pedido->endereco->nome }}
                            </p><br><!-- no P margin for printing - use <br> instead -->

                            <address>
                                CEP: {{ $pedido->endereco->cep }} <br />
                                Logradouro: {{ $pedido->endereco->logradouro }} <br />
                                Bairro: {{ $pedido->endereco->bairro }} <br>
                                Cidade: {{ $pedido->endereco->cidade }} <br>
                                UF: {{ $pedido->endereco->estado }} <br />

                            </address>

                        </div>

                        <div class="col-xs-6 text-right">


                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection

