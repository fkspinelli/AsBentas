@extends('admin.layouts.admin')
@section('page-title')

    <h1 class="page-header text-overflow">Gestão de Pedidos</h1>
    <div class="searchbox">
        <form method="post" action="" >
            {{ csrf_field() }}

        </form>
    </div>
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>

            <ul class="options pull-right list-inline">
                <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                <li>
                    <a href="{{ route('admin.pedidos.download', ['search' => $search, 'dataInicio' => $dataInicio, 'dataTermino' => $dataTermino]) }}" class="opt panel_download" title="Fazer Download">
                        Fazer Download<i class="fa fa-download"></i>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.pedidos.entrega', ['search' => $search, 'dataInicio' => $dataInicio, 'dataTermino' => $dataTermino]) }}" class="opt panel_download" title="Fazer Download">
                        Roteiro entrega <i class="fa fa-truck"></i>
                    </a>
                </li>

            </ul>
        </div>
        <div id="panel-misc-portlet-l1" class="panel panel-default">



        <!-- panel content -->
        <div class="panel-body">
            <form class="validate" action="{{ route('admin.pedido.index') }}" method="get" >
                {{ csrf_field() }}
                <fieldset>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-2 col-sm-2">
                                <input type="text" name="data-inicio" id="filtro-data-inicio"
                                      class="form-control date" value="{{ request()->get('data-inicio') }}" placeholder="Data Início do Pedido">
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <input type="text" name="data-termino" id="filtro-data-termino" class="form-control date"
                                value="{{ request()->get('data-termino') }}" placeholder="Data Término do Pedido">
                            </div>

                            {{--<div class="col-md-4 col-sm-4">--}}
                                    {{--<input type="text" name="search" placeholder="Busque por id ou nome do cliente..." class="form-control" value="">--}}
                            {{--</div>--}}

                            <div class="col-md-2 col-sm-2">
                                <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                    <i class="fa fa-search"></i>
                                </button>
                                <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                    <i class="fa fa-close" href="{{ route('admin.pedido.index') }}"> </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </fieldset>

            </form>

        </div>
        <!-- /panel content -->

    </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-pedidos">
                    <thead>
                    <tr>
                        <th class="width-50">
                            @sortablelink('id', '#')
                        </th>
                        <th>
                            @sortablelink('created_at', 'Data de Criação')
                        </th>
                        <th>
                            @sortablelink('user_id', 'Cliente')
                        </th>
                        <th>Valor Compra</th>
                        <th>Cupom</th>
                        <th>Possui News</th>
                        <th>
                            @sortablelink('fulfillment', 'Fulfillment')
                        </th>
                        <th>Pagamento</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                    @forelse ($pedidos as $pedido)

                        <tr>
                            <td class="text-center">
                                {{ $pedido->id }}
                            </td>

                            <td>
                                {{ $pedido->created_at->format('d/m/Y H:i:s') }}
                            </td>

                            <td>
                                {{ $pedido->usuario ? $pedido->usuario->name : '-' }}
                            </td>

                            <td>
                                {{ number_format($pedido->valor_total, 2, ',', '.') }}
                            </td>

                            <td>
                                {{ number_format($pedido->valor_cupom_desconto, 2, ',', '.') }}
                            </td>

                            <td>
                                {{  $pedido->usuario ? ($pedido->usuario->newsletter ? 'Sim': 'Não') : '-' }}
                            </td>

                            <td>
                                {{  $pedido->lableFulfillment()}}
                            </td>

                            <td>
                                {{  $pedido->lablePagamento()}}
                            </td>

                            <td class="text-center">
                                <a href="{{ route('admin.pedido.visualizar', [ 'id' => $pedido->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-eye white"></i> Visualizar
                                </a>

                                <a href="{{ route('admin.pedido.completo.imprimir.cozinha', [ 'id' => $pedido->id] ) }}"
                                   class="btn btn-default btn-xs" target="_blank">
                                    <i class="fa fa-print white"></i> Print
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9">
                                Nenhum Pedido encontrado.
                            </td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $pedidos->appends(request()->input())->links() }}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.standalone.min.css" />
@endsection

@section('javascripts')
    
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.pt-BR.min.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/pedido_index.js"></script>
@endsection
