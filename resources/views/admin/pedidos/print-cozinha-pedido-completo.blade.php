@extends('admin.layouts.pdf')
@section('content')
    <div id="wrapper">

        <div class="padding-20">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h4>As Bentas - Alimentação Saudável</h4>
                            <p>Pedido num.: {{ $pedido->id}}<br>
                               Status: {{ $pedido->lablePagamento($pedido->id) }} / {{$pedido->lableFulfillment($pedido->id)  }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <strong>{{$pedido->usuario->name}}</strong><br>
                            <strong>{{$pedido->endereco_entrega}} </strong><br>
                            @if(isset($pedido->endereco_entrega_complemento))
                                {{$pedido->endereco_entrega_complemento}}
                                <br>
                            @endif
                        </div>
                    </div>
                    @if($empresa != false)
                        <div class="row">
                            <div class="col-md-12">
                                Referência: {{$empresa->contato}}<br>
                                Empresa: {{$empresa->nome}}
                            </div>
                        </div>
                    @endif

                    <br>

                    @foreach($pedido->pedidoCardapios as $pedidoCardapio)
                    <p>
                        Pedido para o dia: {{date_format($pedidoCardapio->cardapio->data_cardapio, "d-m-Y")}}<br>
                        Status: {{ $pedidoCardapio->lablePagamento($pedidoCardapio->id) }} / {{ $pedidoCardapio->lableFulfillment($pedidoCardapio->id) }}
                    </p>
                    <table cellpadding="5" width="220">
                        <tr>
                            <td class="margem-down" width="45%">Item</td>
                            <td class="margem-down" align="right" width="5%">Qt</td>
                            <td class="margem-down" align="right" width="25%">Vu</td>
                            <td class="margem-down" align="right" width="25%">Total</td>
                        </tr>
                        @foreach($pedidoCardapio->itens as $item)
                        <tr>
                            <td><b> {{$item->descricao}}</b></td>
                            <td align="right" ><b> {{$item->quantidade}}</b></td>
                            <td align="right" > {{number_format($item->valor / $item->quantidade, 2, ',', '')}}</td>
                            <td align="right" class="padding-right"> {{number_format($item->valor, 2, ',', '')}}</td>
                        </tr>
                        @endforeach

                        <tr>
                            <td class="margem-up">Subtotal</td>
                            <td class="margem-up"></td>
                            <td colspan="2" align="right"  class="margem-up padding-right">

                                    @if($pedidoCardapio->desconto_combo != 0 && $pedidoCardapio->desconto == 0 )
                                        @php($subtotal = (($pedidoCardapio->subtotal + $pedidoCardapio->desconto_combo) - $pedidoCardapio->valor_frete ))

                                         {{number_format($subtotal, 2, ',', '')}}
                                    @else
                                        @if($pedidoCardapio->desconto_combo != 0 && $pedidoCardapio->desconto != 0)
                                            @php($subtotal = (($pedidoCardapio->subtotal + $pedidoCardapio->desconto_combo + $pedidoCardapio->desconto) - $pedidoCardapio->valor_frete ))
                                            {{number_format($subtotal, 2, ',', '')}}
                                        @else
                                            @php($subtotal = ($pedidoCardapio->subtotal - $pedidoCardapio->valor_frete ))
                                            {{number_format($subtotal, 2, ',', '')}}
                                        @endif
                                    @endif
                            </td>
                        </tr>

                        @if(isset($pedidoCardapio->desconto_combo) && $pedidoCardapio->desconto_combo > 0 )
                            <tr>
                                <td>Desconto Combo</td>
                                <td></td>
                                <td colspan="2" align="right" class="padding-right"> - {{number_format($pedidoCardapio->desconto_combo, 2, ',', '')}}</td>
                            </tr>
                        @endif

                        @if(isset($pedidoCardapio->desconto) && $pedidoCardapio->desconto > 0 )
                            <tr>
                                <td>Desconto Frequência</td>
                                <td></td>
                                <td colspan="2" align="right" class="padding-right"> - {{number_format($pedidoCardapio->desconto, 2, ',', '')}}</td>
                            </tr>
                        @endif

                        <tr>
                            <td >Frete</td>
                            <td ></td>
                            <td colspan="2" align="right" class="padding-right">{{number_format($pedidoCardapio->valor_frete, 2, ',', '')}}</td>
                        </tr>

                        <tr>
                            <td class="margem-up"><b>Total</b></td>
                            <td class="margem-up"></td>
                            <td class="margem-up" align="right" colspan="2">
                                <b> {{number_format($pedidoCardapio->subtotal, 2, ',', '')}} </b>
                            </td>
                        </tr>
                    </table>
                    @endforeach

                    {{--<p class="align-text-right ">--}}
                        {{--@if($pedido->valor_cupom_desconto$pedido->valor_cupom_desconto)--}}
                            {{--<b>Desconto Cupom: </b> <label>{{number_format($pedido->valor_cupom_desconto, 2, ',', '')}}</label><br>--}}
                        {{--@endif--}}
                        {{--<label class="margin-left--10">--}}

                        {{--</label>--}}
                    {{--</p>--}}
                    <br>
                    <table class="no-border" cellpadding="5" width="220">

                        @if($pedido->valor_cupom_desconto > 0)
                            <tr>
                                <td width="15%"></td>
                                <td width="55%" align="right"><b>Cupom Desconto: </b></td>
                                <td colspan="2" align="right" class="padding-right">
                                    - {{number_format($pedido->valor_cupom_desconto, 2, ',', '')}}
                                </td>
                            </tr>
                        @endif

                        <tr>
                            <td width="15%"></td>
                            <td width="50%" align="right"><b>Valor Total: </b></td>
                            <td colspan="2" align="right" >
                                <b> {{number_format($pedido->valor_total, 2, ',', '')}} </b>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
    <style>

        table {
            border: 1px solid #000;
            border-collapse: collapse;
        }

        td.margem-down{
            border-bottom: 1px solid #000 ;
        }

        td.margem-up{
            border-top: 1px solid #000 ;
        }
        th {
            border-right: 1px solid #000;
        }

        html {
            font-family: 'Helvetica';
        }

        td.padding-right{
            padding-right: 9px;
        }

        .no-border{
            border: none;
        }


    </style>
@endsection

