@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Pedidos
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">

                <div class="col-md-6 col-sm-6 text-left">

                    <h4>Detalhe do Cliente</h4>
                    <ul class="list-unstyled">
                        <li><strong>Nome:</strong> {{ $pedido->usuario->name }}</li>
                        <li><strong>E-mail:</strong> {{ $pedido->usuario->email }}</li>
                        <li><strong>Telefone:</strong> {{ $pedido->usuario->telefone }}</li>
                    </ul>

                </div>

                <div class="col-md-6 col-sm-6 text-right">
                    <h4>Detalhes do <strong>Pedido</strong> </h4>
                    <ul class="list-unstyled">
                        <li><strong>Código:</strong> {{ $pedido->id }}</li>
                        <li><strong>Forma de Pagamento:</strong> {{ $pedido->formaPagamento->tipo }}</li>
                        <li><strong>Endereço Entrega:</strong> {{$pedido->endereco_entrega}}</li>
                        @if($pedido->complemento_entrega != null )
                            <li><strong>Endereço Entrega Complemento:</strong> {{$pedido->complemento_entrega}}</li>
                        @endif
                        <li><strong>Horário Entrega:</strong> {{$pedido->horario_entrega}}</li>

                    </ul>


                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                    <form action="{{route('admin.pedido.mudarStatusPedido', ['id' => $pedido->id])}}">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                            <label><strong> Alterar Fulfillment:</strong></label>
                            <select name="fulfillment" class="form-control" style="padding-top: 4px">
                                <option value="colocado" @if($pedido->fulfillment == "colocado") selected @endif > Colocado </option>
                                <option value="programado" @if($pedido->fulfillment == "programado") selected @endif > Programado </option>
                                <option value="congelado"  @if($pedido->fulfillment == "congelado") selected @endif> Congelado </option>
                                <option value="recusado"   @if($pedido->fulfillment == "recusado") selected @endif> Recusado </option>
                                <option value="em_entrega" @if($pedido->fulfillment == "em_entrega") selected @endif> Em entrega </option>
                                <option value="entregue"   @if($pedido->fulfillment == "entregue") selected @endif> Entregue </option>
                                <option value="entrega_recusada" @if($pedido->fulfillment == "entrega_recusada") selected @endif> Entrega Recusada </option>
                                <option value="retornado" @if($pedido->fulfillment == "retornado") selected @endif> Retornado</option>
                                <option value="cancelado" @if($pedido->fulfillment == "cancelado") selected @endif> Cancelado</option>
                            </select>
                        </div>

                        <div class="col-md-3">
                            <label><strong> Alterar Pagamento: </strong> </label>
                            <select name="pagamento" class="form-control" style="padding-top: 4px" >
                                <option value="nao_pago" @if($pedido->pagamento == "nao_pago") selected @endif> Não Pago </option>
                                <option value="pago_parcialmente" @if($pedido->pagamento == "pago_parcialmente") selected @endif> Pago Parcialmente </option>
                                <option value="pago" @if($pedido->pagamento == "pago") selected @endif> Pago </option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button type="submit"  class="btn btn-danger margin-top-20">
                                OK
                            </button>
                        </div>
                    </form>
                    </div>

                </div>

                <hr>
                <?php
                $itensOrdenacao = [];

                foreach($pedido->pedidoCardapios as $pedidoDia) {
                    $itensOrdenacao[$pedidoDia->cardapio->data_cardapio->format('d/m/Y')] = $pedidoDia;
                }
                ksort($itensOrdenacao);
                ?>
                @foreach($itensOrdenacao as $pedidoDia)

                    <div style="border:2px solid #cc2222" class="margin-bottom-20 padding-20" >
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    Pedido para o Dia : <strong>{{ $pedidoDia->cardapio->data_cardapio->format('d/m/Y') }} </strong> <br>
                                    Fullfilment: <strong class="uppercase"> {{$pedidoDia->lableFulfillment($pedidoDia->id)}}</strong> <br>
                                    Pagamento: <strong class="uppercase"> {{$pedidoDia->lablePagamento($pedidoDia->id)}} </strong> <br>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <form action="{{route('admin.pedido.mudarStatusItemPedido', ['id' => $pedidoDia->id])}}">
                                {{ csrf_field() }}
                                <div class="col-md-3">
                                    <label> Alterar Fulfillment:</label>
                                    <select name="fulfillment" class="form-control" style="padding-top: 4px">
                                        <option value="colocado" @if($pedidoDia->fulfillment == "colocado") selected @endif > Colocado </option>
                                        <option value="cancelado" @if($pedidoDia->fulfillment == "cancelado") selected @endif > Cancelado </option>
                                        <option value="programado" @if($pedidoDia->fulfillment == "programado") selected @endif > Programado </option>
                                        <option value="congelado"  @if($pedidoDia->fulfillment == "congelado") selected @endif> Congelado </option>
                                        <option value="recusado"   @if($pedidoDia->fulfillment == "recusado") selected @endif> Recusado </option>
                                        <option value="em_entrega" @if($pedidoDia->fulfillment == "em_entrega") selected @endif> Em entrega </option>
                                        <option value="entregue"   @if($pedidoDia->fulfillment == "entregue") selected @endif> Entregue </option>
                                        <option value="entrega_recusada" @if($pedidoDia->fulfillment == "entrega_recusada") selected @endif> Entrega Recusada </option>
                                        <option value="retornado" @if($pedidoDia->fulfillment == "retornado") selected @endif> Retornado</option>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label> Alterar Pagamento:</label>
                                    <select name="pagamento" class="form-control" style="padding-top: 4px" >
                                        <option value="pago" @if($pedidoDia->pagamento == "pago") selected @endif> Pago </option>
                                        <option value="nao_pago" @if($pedidoDia->pagamento == "nao_pago") selected @endif> Não Pago </option>
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="submit"  class="btn btn-danger margin-top-20">
                                        OK
                                    </button>
                                </div>
                            </form>
                        </div>

                        <hr>

                        <div class="table-responsive">
                            <table class="table table-condensed nomargin" id="tabela-pedidos">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Quantidade</th>
                                    <th>Valor Unitário</th>
                                    <th class="text-right">Valor</th>
                                    <th width="5%">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($pedidoDia->itens as $item)

                                    <tr>
                                        <td>
                                            <strong>{{ $item->descricao }}</strong>
                                        </td>
                                        <td>{{ $item->quantidade }}</td>
                                        <td>R$ {{ number_format($item->valor / $item->quantidade, 2, ',', '') }}</td>
                                        <td class="text-right">R$ {{ number_format($item->valor, 2, ',', '') }}</td>
                                        <td></td>

                                    </tr>

                                @endforeach

                                @if($pedidoDia->desconto_combo > 0)
                                    <tr>
                                        <td colspan="3" class="text-right">
                                            <strong> Desconto Combo</strong>
                                        </td>
                                        <td class="text-right"><strong>- R$ {{ number_format($pedidoDia->desconto_combo, 2, ',', '') }}</strong></td>
                                        <td></td>
                                    </tr>
                                @endif

                                @if($pedidoDia->desconto > 0)
                                    <tr>
                                        <td colspan="3" class="text-right">
                                            <strong> Desconto Frequência</strong>
                                        </td>
                                        <td class="text-right"><strong>- R$ {{ number_format($pedidoDia->desconto, 2, ',', '') }}</strong></td>
                                        <td></td>
                                    </tr>
                                @endif


                                <tr>
                                    <td colspan="3" class="text-right">
                                        <strong>Frete</strong>
                                    </td>
                                    <td class="text-right"><strong>R$ {{ number_format($pedidoDia->valor_frete, 2, ',', '') }}</strong></td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td colspan="3" class="text-right">
                                        <strong>Valor do Dia:</strong>
                                    </td>
                                    <td class="text-right">
                                        <strong>R$ {{ number_format($pedidoDia->subtotal, 2, ',', '') }}</strong>
                                    </td>
                                    <td></td>
                                </tr>

                                </tbody>
                            </table>
                            <div class="pull-left">
                                <a class="btn btn-info margin-right-20"
                                   href="{{ route('admin.pedido.imprimir.cozinha', ['id' => $pedidoDia->id]) }}"
                                   target="_blank">
                                    <i class="fa fa-print"></i> Imprimir Cozinha
                                </a>
                            </div>

                        </div>
                    </div>
                @endforeach

                <table class="table table-condensed nomargin">
                    <tbody>

                    @if($pedido->valor_cupom_desconto > 0)
                        <tr>
                            <td class="text-right" width=75%">
                                <strong>Cupom ({{ $pedido->cupomUtilizado->cupom->codigo }}): </strong>
                            </td>

                            <td class="text-right">
                                <strong><span>R$ {{ number_format($pedido->valor_cupom_desconto, 2, ',', '') }}</span></strong>
                            </td>

                            <td width=6.5%"></td>
                        </tr>
                    @endif

                    <tr>
                        <td class="text-right" width=75%">
                            <strong>Total:</strong>
                        </td>

                        <td class="text-right">
                            <strong><span>R$ {{ number_format($pedido->valor_total, 2, ',', '') }}</span></strong>
                        </td>

                        <td width=6.5%"></td>
                    </tr>

                    </tbody>
                </table>

                <hr class="nomargin-top">

                <div class="row">


                    <div class="col-sm-12 text-center">

                        <a class="btn btn-danger margin-right-6" href="{{ route('admin.pedido.index') }}">
                            <i class="fa fa-long-arrow-left"></i> Voltar
                        </a>

                        <a class="btn btn-info margin-right-6" href="{{ route('admin.pedido.completo.imprimir.cozinha', [ 'id' => $pedido->id] ) }}" target="_blank">
                            <i class="fa fa-print"></i> Imprimir Cupom
                        </a>

                        <a class="btn btn-success margin-right-6" href="{{ route('admin.pedido.imprimir', ['id' => $pedido->id]) }}" target="_blank">
                            <i class="fa fa-print"></i> Imprimir
                        </a>

                    </div>

                </div>


            </div>
        </div>
    </div>
        @endsection

        @section('javascripts')
            <script src="/assets/js/admin/pedido_show.js"></script>
@endsection
