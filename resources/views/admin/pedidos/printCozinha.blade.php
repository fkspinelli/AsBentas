@extends('admin.layouts.pdf')
@section('content')
    <div id="wrapper">

        <div class="padding-20">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h4>As Bentas - Alimentação Saudável</h4>
                            <p>Pedido num.: {{ $pedido->id}}<br>
                                Data: {{date_format($pedidoCardapio->cardapio->data_cardapio, "d-m-Y")}} <br>
                                Status: {{ $pedidoCardapio->lablePagamento($pedidoCardapio->id) }} / {{ $pedidoCardapio->lableFulfillment($pedidoCardapio->id) }}
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <strong>{{$pedido->usuario->name}}</strong><br>
                            <strong>{{$pedido->endereco_entrega}} </strong><br>
                            @if(isset($pedido->endereco_entrega_complemento))
                                {{$pedido->endereco_entrega_complemento}}
                                <br>
                            @endif
                            <br>
                        </div>
                    </div>
                    @if($empresa != false)
                        <div class="row">
                            <div class="col-md-12">
                                Referência: {{$empresa->contato}}<br>
                                Empresa: {{$empresa->nome}}
                            </div>
                        </div>
                    @endif

                    <br>
                    <table cellpadding="5" width="100%">
                        <tr>
                            <td class="margem-down">Item</td>
                            <td class="margem-down" align="center">Qt</td>
                            <td class="margem-down" align="center">Vu</td>
                            <td class="margem-down" align="center">Total</td>
                        </tr>
                        @php($subtotalItens = 0)
                        @foreach($pedidoCardapio->itens as $item)
                            <tr>
                                <td><b> {{$item->descricao}}</b></td>
                                <td align="right" ><b> {{$item->quantidade}}</b></td>
                                <td align="right" > {{number_format($item->valor / $item->quantidade, 2, ',', '')}}</td>
                                <td align="right" style="padding-right: 15px"> {{number_format($item->valor, 2, ',', '')}}</td>
                            </tr>
                            @php($subtotalItens += $item->valor * $item->quantidade)
                        @endforeach

                        <tr>
                            <td class="margem-up">Subtotal</td>
                            <td class="margem-up"></td>
                            <td colspan="2" align="right" style="padding-right: 15px" class="margem-up">
                                {{number_format($subtotalItens, 2, ',', '')}}
                            </td>
                        </tr>

                        @if(isset($pedidoCardapio->desconto_combo) && $pedidoCardapio->desconto_combo > 0 )
                            <tr>
                                <td>Desconto Combo</td>
                                <td></td>
                                <td colspan="2" align="right" style="padding-right: 15px"> - {{number_format($pedidoCardapio->desconto_combo, 2, ',', '')}}</td>
                            </tr>
                        @endif

                        @if(isset($pedidoCardapio->desconto) && $pedidoCardapio->desconto > 0 )
                            <tr>
                                <td>Desconto Frequência</td>
                                <td></td>
                                <td colspan="2" align="right" style="padding-right: 15px"> - {{number_format($pedidoCardapio->desconto, 2, ',', '')}}</td>
                            </tr>
                        @endif

                        <tr>
                            <td >Frete</td>
                            <td ></td>
                            <td colspan="2" align="right" style="padding-right: 15px">{{number_format($pedidoCardapio->valor_frete, 2, ',', '')}}</td>
                        </tr>

                        <tr>
                            <td class="margem-up"><b>Total</b></td>
                            <td class="margem-up"></td>
                            <td class="margem-up" align="right" colspan="2">
                                <b> {{number_format($pedidoCardapio->subtotal, 2, ',', '')}} </b>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <style>

        table {
            border: 1px solid #000;
            border-collapse: collapse;
        }

        td.margem-down{
            border-bottom: 1px solid #000 ;
        }

        td.margem-up{
            border-top: 1px solid #000 ;
        }
        th {
            border-right: 1px solid #000;
        }

        html {
            font-family: 'Helvetica';
        }


    </style>
@endsection