<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>As Bentas - Admin</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    {{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>--}}

    {{--<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">--}}
    {{--<link href="/assets/css/essentials.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/assets/css/layout.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="/assets/css/jais.css" rel="stylesheet">--}}
    {{--<link href="/assets/css/demo/jais-demo-icons.css" rel="stylesheet">--}}
    {{--<link href="/assets/css/demo/jais-demo.css" rel="stylesheet">--}}

    @yield('stylesheets')
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
{{--<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>--}}
{{--<script src="/assets/plugins/jquery/jquery-2.1.4.min.js"></script>--}}
{{--<script src="/assets/js/app.js"></script>--}}

{{--<script type="text/javascript">--}}
    {{--window.print();--}}
{{--</script>--}}

@yield('javascripts')

</body>
</html>
