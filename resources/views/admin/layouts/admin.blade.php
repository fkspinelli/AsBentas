<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>As Bentas - Admin</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/essentials.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/jais.css" rel="stylesheet">
    <link href="/assets/css/demo/jais-demo-icons.css" rel="stylesheet">
    <link href="/assets/css/demo/jais-demo.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/assets/css/sweetalert.css">

    <link type="image/x-icon" rel="shortcut icon" href="{{ url('/img/favicon.png') }}">
    @yield('stylesheets')
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body>
<div id="container" class="effect aside-float aside-bright mainnav-lg">

    <!--NAVBAR-->

    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed">

            <!--Brand logo & name-->
            <!--================================-->
            <div class="navbar-header">
                <a href="{{ route('admin.dashboard.index') }}"  class="navbar-brand">
                    <!-- img src="/assets/images/logo.png" alt="" class="brand-icon"-->
                    <div class="brand-title">
                        <span class="brand-text">As Bentas</span>
                    </div>
                </a>
            </div>
            <!--================================-->
            <!--End brand logo & name-->


            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content clearfix">
                <ul class="nav navbar-top-links pull-left">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="demo-pli-view-list"></i>
                        </a>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Navigation toogle button-->



                    <!--Notification dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End notifications dropdown-->
                </ul>
                <ul class="nav navbar-top-links pull-right">
                    <!--User dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="pull-right">
                                    <img class="img-circle img-user media-object" src="/img/user.png" alt="Profile Picture">
                                </span>
                            <div class="username hidden-xs">{{ Auth::user()->name }}</div>
                        </a>


                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right panel-default">

                            <!-- Dropdown heading  -->



                            <!-- User dropdown menu ->
                            <ul class="head-list">
                                <li>
                                    <a href="#">
                                        <i class="demo-pli-male icon-lg icon-fw"></i> Perfil
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="badge badge-danger pull-right">9</span>
                                        <i class="demo-pli-mail icon-lg icon-fw"></i> Messages
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="demo-pli-gear icon-lg icon-fw"></i> Settings
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="demo-pli-information icon-lg icon-fw"></i> Help
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="demo-pli-computer-secure icon-lg icon-fw"></i> Lock screen
                                    </a>
                                </li>
                            </ul>

                            <!-- Dropdown footer -->
                            <div class="pad-all text-right">
                                <form action="{{ url('/admin/logout') }}" method="POST">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-primary">
                                        <i class="demo-pli-unlock"></i> Sair
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End user dropdown-->


                </ul>
            </div>
            <!--================================-->
            <!--End Navbar Dropdown-->

        </div>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">



        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">

            <!--Page Title-->
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <div id="page-title">
                <h1 class="page-header text-overflow">@yield('page-title')</h1>

                @yield('searchbox')
                <!--Searchbox
                <div class="searchbox">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" placeholder="Search..">
                        <span class="input-group-btn">
                                <button class="text-muted" type="button"><i class="demo-pli-magnifi-glass"></i></button>
                            </span>
                    </div>
                </div>-->
            </div>
            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
            <!--End page title-->

            <!--Page content-->
            <!--===================================================-->

            <div id="content" class="padding-20">

                @yield('content')

            </div>
            <!--===================================================-->
            <!--End page content-->


        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->



        <!--ASIDE-->
        <!--===================================================-->

        <!--===================================================-->
        <!--END ASIDE-->


        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <div id="mainnav">

                <!--Menu-->
                <!--================================-->
                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">

                            <!--Profile Widget-->
                            <!--================================-->



                            <!--Shortcut buttons-->
                            <!--================================-->

                            <!--================================-->
                            <!--End shortcut buttons-->

                            <ul id="mainnav-menu" class="list-group">

                                <!--Category name-->
                                <li class="list-header">Menu de Navegação</li>

                                <!--Menu list item-->
                                <li>
                                    <a href="{{ route('admin.dashboard.index') }}">
                                        <i class="fa fa-tachometer"></i>
                                        <span class="menu-title">
												<strong>Dashboard</strong>
											</span>
                                    </a>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-list-alt"></i>
                                        <span class="menu-title">Cadastros</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.usuarios.index') }}">Usuários</a></li>
                                        <li><a href="{{ route('admin.clientes.index') }}">Clientes</a></li>
                                        <li><a href="{{ route('admin.newsletter.index') }}">Newsletter</a></li>
                                        <li><a href="{{ route('admin.buscascep.index') }}">Buscas de CEP</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-building-o"></i>
                                        <span class="menu-title">Empresas</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.empresas.index') }}">Cadastradas</a></li>
                                        <li><a href="{{ route('admin.empresas.pre-cadastro') }}">Pré Cadastro</a></li>

                                    </ul>
                                </li>
				                <li>
                                    <a href="{{ route('admin.cupom.index') }}">
                                        <i class="fa fa-ticket"></i>
                                        <span class="menu-title">
                                            <strong>Cupons</strong>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-calendar-minus-o"></i>
                                        <span class="menu-title">
                                            <strong>Descontos</strong>
                                        </span>
                                        <i class="arrow"></i>
                                    </a>

                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.desconto.index') }}">Por frequência</a></li>
                                    </ul>

                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.combo.index') }}">Combos</a></li>
                                    </ul>
                                </li>
                                 <li>
                                    <a href="{{ route('admin.pedido.index') }}">
                                        <i class="fa fa-cart-arrow-down"></i>
                                        <span class="menu-title">
                                            <strong>Pedidos</strong>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-taxi"></i>
                                        <span class="menu-title">Entrega</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.local-entrega.index') }}">Locais de Entrega</a></li>
                                        <li><a href="{{ route('admin.horarios-entrega.index') }}">Horários de Entrega</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ route('admin.pagamento.index') }}">
                                        <i class="fa fa-dollar"></i>
                                        <span class="menu-title">
                                            <strong>Pagamentos</strong>
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ route('admin.cardapio.index') }}">
                                        <i class="fa fa-cutlery"></i>
                                        <span class="menu-title">
                                            <strong>Cardápios</strong>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.prato.index') }}">
                                        <i class="fa fa-glass"></i>
                                        <span class="menu-title">Pratos</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-glass"></i>
                                        <span class="menu-title">Acompanhamentos</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="{{ route('admin.categoria-acompanhamento.index') }}">Categorias</a></li>
                                        <li><a href="{{ route('admin.acompanhamento.index') }}">Acompanhamentos</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ route('admin.configuracao.index') }}">
                                        <i class="fa fa-wrench"></i>
                                        <span class="menu-title">
                                            <strong>Configurações</strong>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.faq.index') }}">
                                        <i class="fa fa-question"></i>
                                        <span class="menu-title">
                                            <strong>FAQ</strong>
                                        </span>
                                    </a>
                                </li>

                            <!--Widget-->
                            <!--================================--
                            <div class="mainnav-widget">

                                <!-- Show the button on collapsed navigation --
                                <div class="show-small">
                                    <a href="#" data-toggle="menu-widget" data-target="#demo-wg-server">
                                        <i class="demo-pli-monitor-2"></i>
                                    </a>
                                </div>

                                <!-- Hide the content on collapsed navigation --
                                <div id="demo-wg-server" class="hide-small mainnav-widget-content">
                                    <ul class="list-group">
                                        <li class="list-header pad-no pad-ver">Server Status</li>
                                        <li class="mar-btm">
                                            <span class="label label-primary pull-right">15%</span>
                                            <p>CPU Usage</p>
                                            <div class="progress progress-sm">
                                                <div class="progress-bar progress-bar-primary" style="width: 15%;">
                                                    <span class="sr-only">15%</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="mar-btm">
                                            <span class="label label-purple pull-right">75%</span>
                                            <p>Bandwidth</p>
                                            <div class="progress progress-sm">
                                                <div class="progress-bar progress-bar-purple" style="width: 75%;">
                                                    <span class="sr-only">75%</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="pad-ver"><a href="#" class="btn btn-success btn-bock">View Details</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--================================-->
                            <!--End widget-->
                            </ul>
                        </div>
                    </div>
                </div>
                <!--================================-->
                <!--End menu-->

            </div>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

    </div>


    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">

        <!-- Visible when footer positions are fixed -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <div class="show-fixed pull-right">
            You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>
        </div>




        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->
        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

        <p class="pad-lft">&#0169; 2017 </p>



    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->


    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->



<!-- SETTINGS - DEMO PURPOSE ONLY -->
<!--===================================================-->

<!--===================================================-->
<!-- END SETTINGS -->

<!--JAVASCRIPT-->
<!--=================================================-->
<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
<script src="/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/jais.js"></script>
<script src="/assets/js/demo/jais-demo.min.js"></script>
<script src="/assets/js/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>


@yield('javascripts')

</body>
</html>
