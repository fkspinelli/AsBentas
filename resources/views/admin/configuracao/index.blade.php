@extends('admin.layouts.admin')
@section('page-title')
    Configurações
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <form method="post" action="{{ route('admin.configuracao.salvar') }}">
            {{ csrf_field() }}
            <div class="panel-body">
                @include('flash::message')
                <div class="table-responsive">
                    <table class="table table-bordered table-vertical-middle nomargin" id="tabela-configuracao">
                        <thead>
                        <tr>
                            <th class="width-30">#</th>
                            <th>Configuração</th>
                            <th>Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($configuracoes as $configuracao)
                            <tr>
                                <td class="text-center">
                                    {{ $configuracao->id }}
                                </td>
                                <td>
                                    {{ $configuracao->label }}
                                </td>
                                <td>
                                    <input type="text" name="valor[{{$configuracao->id}}]" value="{{ $configuracao->valor }}" >
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /panel content -->

            <!-- panel footer -->
            <div class="panel-footer">

                <!--pre code-->
                <div class="text-left">
                    <button type="submit" class="btn btn-primary">
                        Salvar Configurações
                    </button>
                </div>

                <!-- /pre code -->

            </div>
        <!-- /panel footer -->
        </form>

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/cardapio_index.js"></script>
@endsection