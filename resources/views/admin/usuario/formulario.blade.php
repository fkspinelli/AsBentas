@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Usuários
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12">

            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Usuário</strong>
                </div>

                <div class="panel-body">

                    <form class="validate" action="{{ $usuario->id  ? route('admin.usuarios.atualizar', ['id' => $usuario->id]) : route('admin.usuarios.gravar') }}" method="post" enctype="multipart/form-data" >

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $usuario->id }}">

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label>Nome *</label>
                                        <input type="text" name="nome" value="{{ $usuario->name }}" class="form-control required">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label>Email *</label>
                                        <input type="email" name="email" value="{{ $usuario->email }}" class="form-control required">
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label>Senha </label>
                                        <input type="password" name="senha" value="" class="form-control">
                                    </div>

                                </div>
                            </div>

                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.usuarios.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
            <!-- /----- -->

        </div>



    </div>
@endsection
