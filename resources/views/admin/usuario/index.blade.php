@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Usuários
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong> <!-- panel title -->
            </span>


        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, nome ou email..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.usuarios.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-usuario">
                    <thead>
                    <tr>
                        <th class="width-50">@sortablelink('id', 'id')</th>
                        <th>@sortablelink('name', 'nome')</th>
                        <th>@sortablelink('email', 'e-mail')</th>
                        <th width="15%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse ($usuarios as $usuario)
                        <tr>
                            <td class="text-center">
                                {{ $usuario->id }}
                            </td>
                            <td>
                                {{ $usuario->name }}
                            </td>
                            <td>
                                {{ $usuario->email }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.usuarios.editar', [ 'id' => $usuario->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-usuario" data-url="{{ route('admin.usuarios.excluir', [ 'id' => $usuario->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">Nenhum usuário encontrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{ $usuarios->appends(request()->input())->links() }}
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!-- pre code -->
            <div class="text-left">
                <a href="{{ route('admin.usuarios.novo') }}" class="btn btn-primary">
                    Novo Usuário
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/usuario_index.js"></script>
@endsection