@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Locais de Entrega
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Local de Entrega</strong>
                </div>

                <div class="panel-body">

                    <form class="validate" action="{{ $local->id  ? route('admin.local-entrega.atualizar', ['id' => $local->id]) : route('admin.local-entrega.gravar') }}"
                          method="post" enctype="multipart/form-data" >

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $local->id }}">

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-12">
                                        <label>CEP Inicial *</label>
                                        <input type="text" name="cep_inicial" value="{{ old('cep_inicial', $local->cep_inicial) }}"
                                               class="form-control required cep {{ $errors->has('cep_inicial') ? 'error' : '' }}">
                                        @if($errors->has('cep_inicial'))
                                            <span class="help-inline text-red">{{ $errors->first('cep_inicial') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <label>CEP Final *</label>
                                        <input type="text" name="cep_final" value="{{ old('cep_final', $local->cep_final) }}"
                                               class="form-control required cep {{ $errors->has('cep_final') ? 'error' : '' }}">
                                        @if($errors->has('cep_final'))
                                            <span class="help-inline text-red">{{ $errors->first('cep_final') }}</span>
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-4">
                                        <label>Bairro *</label>
                                        <input type="text" name="bairro" value="{{ old('bairro', $local->bairro) }}"
                                               class="form-control required {{ $errors->has('bairro') ? 'error' : '' }}" maxlength="110">
                                        @if($errors->has('bairro'))
                                            <span class="help-inline text-red">{{ $errors->first('bairro') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-sm-3">
                                        <label>Valor do Frete *</label>
                                        <input type="text" name="valor_entrega" value="{{ old('valor_entrega', $local->valor_entrega) }}"
                                               class="form-control money {{ $errors->has('valor_entrega') ? 'error' : '' }}">
                                        @if($errors->has('valor_entrega'))
                                            <span class="help-inline text-red">{{ $errors->first('valor_entrega') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-3 col-sm-3">
                                        <label>Situação *</label><br>
                                        <label class="radio">
                                            <input type="radio" name="status" value="ativo"
                                            @if($local->status == 'ativo' || $local->status == null)checked @endif
                                            @if(old('status', $local->status) == 'ativo') checked @endif >
                                            <i></i> Ativo
                                        </label>
                                        <label class="radio">
                                            <input type="radio" name="status" value="inativo"
                                            @if($local->status == 'inativo' && old('status', $local->status) == '')checked @endif
                                            @if(old('status', $local->status) == 'inativo') checked @endif>
                                            <i></i> Inativo
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.local-entrega.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
            <!-- /----- -->
        </div>
    </div>

@endsection

@section('javascripts')

    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/local_entrega_form.js"></script>

@endsection