@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Locais de Entrega
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong> <!-- panel title -->
            </span>


        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, cep, bairro ou valor do frete..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.clientes.index') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-local-entrega">
                    <thead>
                    <tr>
                        <th class="width-50">
                            @sortablelink('id', '#')
                        </th>
                        <th>
                            @sortablelink('cep_inicial', 'CEP Inicial')
                        </th>
                        <th>
                            @sortablelink('cep_final', 'CEP Final')
                        </th>
                        <th>
                            @sortablelink('bairro', 'Bairro')
                        </th>
                        <th>Valor do Frete</th>
                        <th>@sortablelink('status', 'Situação')</th>
                        <th width="20%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($locais as $local)
                        <tr>
                            <td class="text-center">
                                {{ $local->id }}
                            </td>
                            <td>
                                {{ $local->cep_inicial }}
                            </td>
                            <td>
                                {{ $local->cep_final }}
                            </td>
                            <td>
                                {{ $local->bairro }}
                            </td>
                            <td>
                                R${{ str_replace('.', ',', $local->valor_entrega) }}
                            </td>
                            <td>
                                {{ $local->status }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.local-entrega.editar', [ 'id' => $local->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-local" data-url="{{ route('admin.local-entrega.excluir', [ 'id' => $local->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Nenhum Local de Entrega encontrado.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>

            <div class="text-center">
                {{ $locais->appends(request()->input())->links() }}
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">

            <!-- pre code -->
            <div class="text-left">
                <a href="{{ route('admin.local-entrega.novo') }}" class="btn btn-primary">
                    Novo Local
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/local_entrega_index.js"></script>
@endsection