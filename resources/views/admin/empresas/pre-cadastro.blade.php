@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Empresas - Pré Cadastro
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong> <!-- panel title -->
            </span>


        </div>

        <!-- panel content -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="validate" action="" method="post" >
                        {{ csrf_field() }}
                        <fieldset>

                            <div class="row">
                                <div class="form-group" >
                                    <div class="col-md-11 col-sm-2">
                                        <input type="text" name="search" placeholder="Busque por id, nome, contato, email ou cep..." class="form-control" value="">
                                    </div>
                                    <div class="col-md-1 nopadding-left">
                                        <button type="submit" class="btn btn-sm btn-primary margin-right-10" title="Pesquisar" style="margin-bottom: 10px;">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" title="Limpar Filtros" style="margin-bottom: 10px;">
                                            <i class="fa fa-close" href="{{ route('admin.empresas.pre-cadastro') }}"> </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-usuario">
                    <thead>
                    <tr>
                        <th class="width-50">
                            @sortablelink('id', '#')
                        </th>
                        <th>
                            @sortablelink('nome', 'Nome')
                        </th>
                        <th>
                            @sortablelink('contato', 'Contato')
                        </th>
                        <th>
                            @sortablelink('email', 'E-mail')
                        </th>
                        <th>Telefone</th>
                        <th>CEP</th>
                        <th width="25%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($empresas as $empresa)
                        <tr>
                            <td class="text-center">
                                {{ $empresa->id }}
                            </td>
                            <td>
                                {{ $empresa->nome }}
                            </td>
                            <td>
                                {{ $empresa->contato }}
                            </td>
                            <td>
                                {{ $empresa->email }}
                            </td>
                            <td>
                                {{ $empresa->telefone }}
                            </td>
                            <td>
                                {{ $empresa->cep }}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.empresas.ativar', [ 'id' => $empresa->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Ativar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-empresa" data-url="{{ route('admin.empresas.excluir', [ 'id' => $empresa->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Nenhuma empresa pré-cadastrada encontrada.</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>

        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/empresa_index.js"></script>
@endsection