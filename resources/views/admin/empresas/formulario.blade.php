@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Empresas
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Registro de Empresa</strong>
                </div>

                <div class="panel-body">

                    <form class="validate" action="{{ $empresa->id  ? route('admin.empresas.atualizar', ['id' => $empresa->id]) : route('admin.empresas.gravar') }}" method="post" enctype="multipart/form-data" >

                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $empresa->id }}">

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-8 col-sm-12 {{ $errors->has('nome') ? 'has-error' : '' }}">
                                        <label>Nome/Razão Social *</label>
                                        <input type="text" name="nome" value="{{ old('nome', $empresa->nome)  }}" maxlength="110" class="form-control required">
                                        @if($errors->has('nome'))
                                            <span class="help-inline text-red">{{ $errors->first('nome') }}</span>
                                        @endif
                                    </div>

                                    <div class="col-md-4 col-sm-12">
                                        <label>CNPJ</label>
                                        <input type="text" name="cnpj" value="{{ old('cnpj', $empresa->cnpj) }}" class="form-control required cnpj">
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label>Contato</label>
                                        <input type="text" name="contato" value="{{ old('contato', $empresa->contato) }}" class="form-control" maxlength="50">
                                    </div>
                                    <div class="col-md-2 col-sm-3 {{ $errors->has('telefone') ? 'has-error' : '' }}">
                                        <label>Telefone</label>
                                        <input type="text" name="telefone" value="{{ old('telefone', $empresa->telefone) }}" class="form-control phone">
                                        @if($errors->has('telefone'))
                                            <span class="help-inline text-red">{{ $errors->first('telefone') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <label>E-mail</label>
                                        <input type="email" name="email" value="{{ old('email', $empresa->email) }}" class="form-control" maxlength="80">
                                        @if($errors->has('email'))
                                            <span class="help-inline text-red">{{ $errors->first('telefone') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6">
                                        <label>Área de Atuação</label>
                                        <input type="text" name="area_atuacao" value="{{ old('area_atuacao', $empresa->area_atuacao) }}" class="form-control" maxlength="80">
                                    </div>
                                    <div class="col-md-4 col-sm-4 {{ $errors->has('codigo_empresa') ? 'has-error' : '' }}">
                                        <label>Código para Validação</label>
                                        <input type="text" name="codigo_empresa" value="{{ old('codigo_empresa', $empresa->codigo_empresa) }}" maxlength="50" class="form-control cod-validacao">
                                        @if($errors->has('codigo_empresa'))
                                            <span class="help-inline text-red">{{ $errors->first('codigo_empresa') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <label>Situação</label> <br>
                                        <select name="status" enable>
                                            <option value="ativo"
                                                    @if($empresa->status == 'ativo' || $empresa->status == null ) selected @endif
                                                    @if(old('status', $empresa->status == 'ativo')) selected @endif
                                            >
                                                Ativa
                                            </option>
                                            <option value="inativo"
                                                    @if($empresa->status == 'inativo' && old('status', $empresa->status == null)) selected @endif
                                                    @if(old('status', $empresa->status) == 'inativo') selected @endif
                                            >
                                                Inativa
                                            </option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-2 col-sm-2">
                                        <label class="switch switch">
                                            <span> Frete Grátis</span> <br>
                                            <input type="checkbox" name="frete_gratis" value="1"
                                                   @if($empresa->frete_gratis && old('frete_gratis', $empresa->frete_gratis) == null) checked @endif
                                                   @if(old('frete_gratis', $empresa->frete_gratis) == '1') checked @endif

                                            >
                                            <span class="switch-label" data-on="Sim" data-off="Não"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <label class="switch switch">
                                            <span> Frete Grátis para Funcionário</span> <br>
                                            <input type="checkbox" name="frete_gratis_funcionario" value="1"
                                                   @if($empresa->frete_gratis_funcionario && old('frete_gratis_funcionario', $empresa->frete_gratis_funcionario) == null ) checked @endif
                                                   @if(old('frete_gratis_funcionario', $empresa->frete_gratis_funcionario) == '1') checked @endif

                                            >
                                            <span class="switch-label" data-on="Sim" data-off="Não"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <label>Valor do Frete</label>
                                        <input type="text" name="valor_frete" value="{{ old('valor_frete', $empresa->valor_frete)}}" maxlength="50" class="form-control valor-frete">
                                    </div>

                                </div>
                            </div>

                            <!-- required [php action request] -->
                            @if(!$empresa->id)
                                <!--
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-12">
                                        <label>CEP</label>
                                        <input type="text" name="cep" value="" class="form-control cep">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <label>Logradouro</label>
                                        <input type="text" name="logradouro" value="" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4 col-sm-4">
                                        <label>Bairro</label>
                                        <input type="text" name="bairro" value="" class="form-control">
                                    </div>
                                    <div class="col-md-4 col-sm-3">
                                        <label>Cidade</label>
                                        <input type="text" name="cidade" value="" class="form-control">
                                    </div>
                                    <div class="col-md-4 col-sm-6">
                                        <label>Estado</label>
                                        <input type="text" name="estado" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
-->
                            @endif

                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.empresas.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>

                    </form>

                </div>

            </div>
            <!-- /----- -->
        </div>
    </div>

    @if($empresa->id)
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Endereços da Empresa</strong>
                    <button type="button" class="btn btn-primary" id="btn-modal-endereco" data-toggle="modal" data-target=".modal-endereco"
                            data-url="{{ route('admin.empresas.endereco.gravar') }}">
                        Novo
                    </button>
                </div>

                <div class="panel-body">

                    @include('flash::message')
                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical-middle nomargin" id="tabela-endereco">
                            <thead>
                            <tr>
                                <th class="width-30">#</th>
                                <th>Nome</th>
                                <th>Logradouro</th>
                                <th>Bairro</th>
                                <th width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse ($empresa->enderecos as $endereco)
                                <tr>
                                    <td class="text-center">
                                        {{ $endereco->id }}
                                    </td>
                                    <td>
                                        {{ $endereco->nome }}
                                    </td>
                                    <td>
                                        {{ $endereco->logradouro }}
                                    </td>
                                    <td>
                                        {{ $endereco->bairro }}

                                    </td>
                                    <td class="text-center">
                                        <a href="" class="btn btn-default btn-xs btn-editar-endereco" data-json="{{ json_encode($endereco) }}" data-url="{{ route('admin.empresas.endereco.atualizar') }}">
                                            <i class="fa fa-edit white"></i> Editar
                                        </a>

                                        @if($endereco->usuarioEmpresa()->count() == 0)
                                            <a href="#" class="btn btn-default btn-xs btn-excluir-endereco" data-url="{{ route('admin.empresas.endereco.excluir', [ 'id' => $endereco->id] ) }}">
                                                <i class="fa fa-times white"></i> Remover
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Nenhum endereço</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

{{--------------------------------------------------------------MODAL NOVO ENDEREÇO--------------------------------------------}}

    <div class="modal fade modal-endereco" id="modal-endereco" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- header modal -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myLargeModalLabel">Adicionar Endereço</h4>
                </div>

                <!-- body modal -->
                <div class="modal-body">
                    <form class="validate" id="form-endereco" action="" method="post" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="empresa_id" value="{{ $empresa->id }}">

                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-8 col-sm-12 {{ $errors->has('nomeEndereco') ? 'has-error' : '' }}">
                                    <label>Nome</label>
                                    <input type="text" name="nomeEndereco" value="{{ old('nomeEndereco')}}" class="form-control" maxlength="70">
                                    @if($errors->has('nomeEndereco'))
                                        <span class="help-inline text-red">{{ $errors->first('nomeEndereco') }}</span>
                                    @endif
                                </div>

                                <div class="col-md-4 col-sm-12 {{ $errors->has('cep') ? 'has-error' : '' }}">
                                    <label>CEP</label>
                                    <input type="text" name="cep" value="{{ old('cep')}}" class="form-control cep" maxlength="9" id="cep">
                                    @if($errors->has('cep'))
                                        <span class="help-inline text-red">{{ $errors->first('cep') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 {{ $errors->has('logradouro') ? 'has-error' : '' }}">
                                    <label>Logradouro</label>
                                    <input type="text" name="logradouro" value="{{ old('logradouro')}}" class="form-control" maxlength="110" id="logradouro">
                                    @if($errors->has('logradouro'))
                                        <span class="help-inline text-red">{{ $errors->first('logradouro') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4 col-sm-4 {{ $errors->has('numero') ? 'has-error' : '' }}">
                                    <label>Número</label>
                                    <input type="text" name="numero" value="{{ old('numero')}}" class="form-control" maxlength="110" id="numero">
                                    @if($errors->has('numero'))
                                        <span class="help-inline text-red">{{ $errors->first('numero') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4 col-sm-4 {{ $errors->has('complemento') ? 'has-error' : '' }}">
                                    <label>Complemento</label>
                                    <input type="text" name="complemento" value="{{ old('complemento')}}" class="form-control" maxlength="110" id="complemento">
                                    @if($errors->has('complemento'))
                                        <span class="help-inline text-red">{{ $errors->first('complemento') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 {{ $errors->has('bairro') ? 'has-error' : '' }}">
                                    <label>Bairro</label>
                                    <input type="text" name="bairro" value="{{ old('bairro')}}" class="form-control" maxlength="30" id="bairro">
                                    @if($errors->has('bairro'))
                                        <span class="help-inline text-red">{{ $errors->first('bairro') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4 col-sm-3 ">
                                    <label>Cidade</label>
                                    <input type="text" name="cidade" value="{{ old('cidade')}}" class="form-control" maxlength="30" id="cidade">
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <label>Estado</label>
                                    <input type="text" name="estado" value="{{ old('estado')}}" class="form-control" maxlength="2" id="estado">
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="button" id="btn-salvar-endereco">
                                    Salvar
                                </button>
                                <button class="btn btn-warning" type="button" id="btn-cancelar-endereco" data-dismiss="modal" >
                                    Cancelar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

{{--------------------------------------------------------------FIM MODAL ENDEREÇO--------------------------------------------}}

    <!-- Precos -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Preços</strong>
                </div>

                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical-middle nomargin" id="tabela-precos">
                            <legend>Prato</legend>
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Valor</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse ($precosPratos as $prato)
                                <tr>
                                    <td>
                                        {{ $prato->nome }}
                                    </td>
                                    <td>
                                        {{ $prato->valor }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Nenhum Preço</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>

                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical-middle nomargin" id="tabela-precos">
                            <legend>Acompanhamentos</legend>
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Categoria</th>
                                <th>Valor</th>
                            </tr>
                            </thead>
                            <tbody>

                            @forelse ($precosAcompanhamentos as $acompanhamento)
                                <tr>
                                    <td>
                                        {{ $acompanhamento->nome }}
                                    </td>
                                    <td>
                                        {{ $acompanhamento->categoria }}
                                    </td>
                                    <td>
                                        {{ $acompanhamento->valor }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Nenhum Preço</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>

    @endif


    <style>
        select{
            padding: 0px;
            background-color: #ffffff;
        }

    </style>

@endsection


@section('javascripts')

    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/empresa_form.js"></script>

@endsection