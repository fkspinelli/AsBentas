@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Combos
@endsection

@section('content')
    <form class="validate"
          action="{{ $combo->id  ? route('admin.combo.atualizar', ['id' => $combo->id]) : route('admin.combo.gravar') }}"
          method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-transparent">
                        <strong>Registro de Combo</strong>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <!-- required [php action request] -->
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $combo->id }}">

                            <div class="row">
                                <div class="form-group ">
                                    <div class="col-md-3 col-sm-3 {{ $errors->has('nome') ? 'has-error' : '' }}">
                                        <label>Nome *</label>
                                        <input type="text" name="nome" id="nome" value="{{ old('nome', $combo->nome) }}"
                                               class="form-control required" maxlength="30">
                                        @if($errors->has('nome'))
                                            <span class="help-inline text-red">{{ $errors->first('nome') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-sm-3 {{ $errors->has('valor') ? 'has-error' : '' }}">
                                        <label>Valor *</label>
                                        <input type="text" name="valor" id="valor" value="{{ old('valor', $combo->valor) }}"
                                               class="form-control required percent">
                                        @if($errors->has('valor'))
                                            <span class="help-inline text-red">{{ $errors->first('valor') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-12">
                                        <strong>Categorias</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-12">
                                        <label>Selecione </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-12">
                                        <select class="selectize" id="select-categoria">
                                            <option></option>
                                                @foreach($categorias as $categoria)
                                                    <option value="{{ $categoria->id }}">{{ $categoria->nome }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-12">
                                                <a class="btn btn-primary btn-sm" id="btn-adicionar-categoria">
                                                    adicionar
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-vertical-middle nomargin" id="tabela-combos">
                                        <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th width="20%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($combo->categorias as $categoria)
                                                <tr id="tr_categoria_{{ $categoria->id }}">
                                                    <td>
                                                        <input type="hidden" name="categoria_id[]" value="{{ $categoria->id }}" id="categoria_id_{{ $categoria->id }}">
                                                        {{ $categoria->nome }}
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-default btn-xs excluir-categoria" data-id="{{ $categoria->id }}">
                                                            <i class="fa fa-times white"></i> Remover
                                                        </a>
                                                    </td>
                                                </tr>
                                            @empty
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <div class="row text-center">
                            <div class="col-md-12 margin-top-30">
                                <button class="btn btn-primary" type="submit">
                                    Salvar
                                </button>
                                <a class="btn btn-warning" href="{{ route('admin.combo.index') }}">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </form>
@endsection

@section('stylesheets')
    <link href="/assets/plugins/pikaday/pikaday.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.css" rel="stylesheet">
    <link href="/assets/plugins/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@endsection

@section('javascripts')
    <script src="/assets/plugins/pikaday/pikaday.js"></script>
    <script src="/assets/plugins/selectize/js/standalone/selectize.min.js"></script>
    <script src="/assets/js/admin/combo_form.js"></script>
@endsection
