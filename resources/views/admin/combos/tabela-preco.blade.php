@extends('admin.layouts.admin')

@section('page-title')
    Gestão de Combos - Tabela de Descontos
@endsection

@section('content')
<form class="validate" action="{{ route('admin.combo.tabela-preco.salvar', ['id' => $combo->id]) }}"
      id="form-tabela-preco" method="post" enctype="multipart/form-data"
      >
    {{ csrf_field() }}
    <input type="hidden" name="combo_id" value="{{ $combo->id }}">
    <div class="row">
        <div class="col-md-12">
            <!-- ------ -->
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-transparent">
                    <strong>Combo: {{ $combo->nome }}</strong>
                </div>

                <div class="panel-body">
                    @include('flash::message')
                    <div class="table-responsive">
                        <table class="table table-bordered table-vertical-middle nomargin" id="tabela-preco-combo">
                            <thead>
                            <tr>
                                <th class="width-30">#</th>
                                <th>Nome</th>
                                <th>Situação</th>
                                <th>Preço</th>
                                <th width="30%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($empresas as $empresa)
                                <tr>
                                    <td class="text-center">
                                        {{ $empresa->id }}
                                    </td>
                                    <td>
                                        {{ $empresa->nome }}
                                    </td>
                                    <td>
                                        {{ $empresa->status }}
                                    </td>
                                    <td>
                                        @if(count($empresa->comboPreco) > 0)
                                            {{ $empresa->comboPreco[0]->pivot->valor }}
                                        @else
                                            Nenhum preço registrado.
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a href="#" class="btn btn-default btn-xs btn-adicionar-preco"
                                           data-toggle="modal" data-target="#modalPreco"
                                           data-empresa="{{ $empresa->nome }}" data-empresa-id="{{ $empresa->id }}"
                                           data-combo-valor="{{ (isset($empresa->comboPreco[0])) ? $empresa->comboPreco[0]->pivot->valor : $combo->valor_sugerido }}"
                                        >
                                            <i class="fa fa-table white"></i> Alterar Preço
                                        </a>
                                        @if(count($empresa->comboPreco) > 0)
                                            <a href="{{ route('admin.combo.tabela-preco.excluir',['idEmpresa' => $empresa->id, 'idCombo' => $combo->id ])}}" class="btn btn-default btn-xs">
                                                <i class="fa fa-times white"></i> Remover Preço
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4">Nenhum Combo Cadastrado.</td>
                                </tr>
                            @endforelse

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         aria-hidden="true" id="modalPreco">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <!-- header modal -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mySmallModalLabel">Registrar Valor</h4>
                </div>

                <!-- body modal -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="valor" class="control-label">Valor</label>
                        <input type="text" class="form-control money" name="valor" id="valor">
                    </div>
                    <input type="hidden" name="empresa_id" id="empresa_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
                    <button type="button" class="btn btn-primary" id="btn-salvar-preco">Salvar</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('stylesheets')

@endsection

@section('javascripts')
    <script src="/assets/js/jquery.mask.min.js"></script>
    <script src="/assets/js/admin/combo_index.js"></script>
@endsection
