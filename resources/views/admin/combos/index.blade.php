@extends('admin.layouts.admin')
@section('page-title')
    Gestão de Combos
@endsection
@section('content')
    <div id="panel-1" class="panel panel-default">
        <div class="panel-heading">
            <span class="title elipsis">
                <strong>Listagem</strong>
            </span>
        </div>

        <!-- panel content -->
        <div class="panel-body">
            @include('flash::message')
            <div class="table-responsive">
                <table class="table table-bordered table-vertical-middle nomargin" id="tabela-combo">
                    <thead>
                    <tr>
                        <th class="width-30">Id</th>
                        <th> Nome</th>
                        <th> Valor</th>
                        <th class="width-300"> </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($combos as $combo)
                        <tr>
                            <td>
                                {{ $combo->id}}
                            </td>

                            <td>
                                {{$combo->nome}}
                            </td>
                            <td>
                                {{ number_format($combo->valor,2,',','.')}}

                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.combo.tabela-preco', [ 'id' => $combo->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-table white"></i> Preços
                                </a>
                                <a href="{{ route('admin.combo.editar', [ 'id' => $combo->id] ) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-edit white"></i> Editar
                                </a>
                                <a href="#" class="btn btn-default btn-xs excluir-combo" data-url="{{ route('admin.combo.excluir', [ 'id' => $combo->id] ) }}">
                                    <i class="fa fa-times white"></i> Remover
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <div class="text-center">
                {{--{{ $descontos->links() }}--}}
            </div>
        </div>
        <!-- /panel content -->

        <!-- panel footer -->
        <div class="panel-footer">


            <div class="text-left">
                <a href="{{ route('admin.combo.novo') }}" class="btn btn-primary">
                    Novo Item
                </a>
            </div>

            <!-- /pre code -->

        </div>
        <!-- /panel footer -->

    </div>
@endsection

@section('javascripts')
    <script src="/assets/js/admin/combo_index.js"></script>
@endsection