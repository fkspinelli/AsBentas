<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddCamposEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('cep', 8)->nullable();
            DB::statement("ALTER TABLE empresas MODIFY status ENUM('ativo', 'inativo', 'pre') NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('cep');
            DB::statement("ALTER TABLE empresas MODIFY status ENUM('ativo', 'inativo') NOT NULL");
        });
    }
}
