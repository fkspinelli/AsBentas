<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposPedidoCardapio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            $table->enum('status', ['ativo', 'cancelado'])->default('ativo')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->string('canceled_by')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('canceled_at');
            $table->dropColumn('canceled_by');
        });
    }
}
