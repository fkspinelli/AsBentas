<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNewFieldsStatusPedidosCardapios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            $table->enum('pagamento', ['pago', 'nao_pago','pago_parcialmente'])->default('nao_pago')->nullable();
            $table->enum('fulfillment', ['colocado', 'cancelado', 'programado', 'congelado', 'recusado', 'em_entrega', 'entregue', 'entrega_recusada', 'retornado'])->default('colocado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            $table->dropColumn('pagamento');
            $table->dropColumn('fulfillment');
        });
    }
}
