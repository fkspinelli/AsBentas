<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTabelaPreco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabela_preco_prato_empresa', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('prato_id')->unsigned()->nullable();
            $table->foreign('prato_id')->references('id')
                ->on('pratos')
                ->onDelete('cascade');
            $table->integer('empresa_id')->unsigned()->nullable();
            $table->foreign('empresa_id')->references('id')
                ->on('empresas')
                ->onDelete('cascade');
            $table->decimal('valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabela_preco_prato_empresa');
    }
}
