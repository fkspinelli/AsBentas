<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoComboToTableItensCombo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itens_combo', function (Blueprint $table) {
            $table->integer('id_combo')->unsigned();
            $table->foreign('id_combo')
                ->references('id')
                ->on('combos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itens_combo', function (Blueprint $table) {
            $table->dropForeign('codigo_combo');
        });
    }
}
