<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusPedidoCardapios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement("ALTER TABLE pedidos MODIFY status ENUM('programado', 'entregue', 'cancelado','pago','congelado') DEFAULT 'programado'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement("ALTER TABLE pedidos MODIFY status ENUM('programado', 'entregue', 'cancelado') DEFAULT 'programado'");
        });
    }
}
