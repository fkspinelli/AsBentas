<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescontoProgressivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desconto_progressivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dia');
            $table->float('desconto');
            $table->timestamps();
        });

        Schema::table('pedido_cardapios', function (Blueprint $table) {
            
            $table->float('desconto')->default(0)->nullable();
            
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desconto_progressivos');
        
        Schema::table('pedido_cardapios', function (Blueprint $table) {
            
            $table->dropColumn('desconto');
            
        });

    }
}
