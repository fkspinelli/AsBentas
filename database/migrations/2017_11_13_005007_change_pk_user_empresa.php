<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePkUserEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_empresa', function (Blueprint $table) {
            // $table->dropPrimary(['user_id', 'empresa_id']);
            // $table->primary(['user_id', 'empresa_id', 'dt_inicio']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_empresa', function (Blueprint $table) {
            // $table->dropPrimary(['user_id', 'empresa_id', 'dt_inicio']);
            // $table->primary(['user_id', 'empresa_id']);
        });
    }
}
