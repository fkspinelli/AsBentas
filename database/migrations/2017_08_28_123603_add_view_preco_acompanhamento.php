<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddViewPrecoAcompanhamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW preco_acompanhamento as 
select a.id, a.nome, a.categoria_id, c.nome nome_categoria , IFNULL(pae.valor , a.valor_sugerido) valor, pae.empresa_id
from acompanhamentos a 
inner join categoria_acompanhamentos c on c.id = a.categoria_id
left join tabela_preco_acompanhamento_empresa pae on a.id = pae.acompanhamento_id
where a.status = 'ativo' 
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW preco_acompanhamento");
    }
}
