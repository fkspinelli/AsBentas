<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePratoCaracteristica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prato_caracteristica', function (Blueprint $table) {

            $table->primary(['prato_id', 'caracteristica_id']);

            $table->integer('prato_id')->unsigned()->nullable();
            $table->foreign('prato_id')->references('id')
                ->on('pratos')
                ->onDelete('cascade');

            $table->integer('caracteristica_id')->unsigned()->nullable();
            $table->foreign('caracteristica_id')->references('id')
                ->on('caracteristica_pratos')
                ->onDelete('cascade');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prato_caracteristica');
    }
}
