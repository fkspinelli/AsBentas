<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksToTableCombosCategoriaAcompanhamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('combos_categoria_acompanhamentos', function (Blueprint $table) {
            $table->integer('combo_id')->unsigned();
            $table->integer('cat_acomp_id')->unsigned();

            $table->foreign('combo_id')->references('id')->on('combos')->onDelete('cascade');
            $table->foreign('cat_acomp_id')->references('id')->on('categoria_acompanhamentos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('combos_categoria_acompanhamentos', function (Blueprint $table) {
            //
        });
    }
}
