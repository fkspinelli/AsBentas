<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusPedidoCorrecao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement("ALTER TABLE pedidos MODIFY status ENUM('programado', 'entregue', 'cancelado','pago','congelado') NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            \Illuminate\Support\Facades\DB::statement("ALTER TABLE pedidos MODIFY status ENUM('programado', 'entregue', 'cancelado','pago','congelado') NOT NULL");
        });
    }
}
