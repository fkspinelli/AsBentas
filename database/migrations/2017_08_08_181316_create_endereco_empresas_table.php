<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecoEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endereco_empresas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('cep', 10);
            $table->string('logradouro', 255);
            $table->string('complemento', 100)->nullable();
            $table->string('bairro', 100);
            $table->string('cidade', 100);
            $table->string('estado', 2);
            $table->enum('status', ['ativo', 'inativo'])->default('ativo');

            $table->integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')
                ->references('id')->on('empresas')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endereco_empresas');
    }
}
