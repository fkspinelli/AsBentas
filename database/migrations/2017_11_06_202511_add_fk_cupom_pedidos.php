<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkCupomPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cupom_pedidos', function (Blueprint $table) {
            
            

            $table->foreign('cupom_id', 'fk_cupom_pedidos_cupons_cupom')->references('id')->on('cupons');
            $table->foreign('pedido_id', 'fk_cupom_pedidos_cupons_pedido')->references('id')->on('pedidos');
            $table->foreign('usuario_id', 'fk_cupom_pedidos_cupons_usuario')->references('id')->on('users');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cupom_pedidos', function (Blueprint $table) {
            
            $table->dropForeign(['cupom_id', 'pedido_id', 'usuario_id']);
            
        });
    }
}
