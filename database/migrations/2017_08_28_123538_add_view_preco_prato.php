<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddViewPrecoPrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW preco_prato as 
        SELECT c.id, c.data_cardapio,  p.nome, p.descricao, ip.icone, ip.css, IFNULL(ppe.valor, p.valor_sugerido) valor, p.id prato_id, ppe.empresa_id
        FROM cardapios c inner join cardapio_prato cp on c.id = cp.cardapio_id
        inner join pratos p on p.id = cp.prato_id and p.status =  'ativo' 
        inner join icone_pratos ip on ip.id = p.icone_id
        left join tabela_preco_prato_empresa ppe on ppe.prato_id = cp.prato_id
        where c.status = 'ativo' ;
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW preco_prato");
    }
}
