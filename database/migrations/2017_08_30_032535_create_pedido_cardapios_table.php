<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoCardapiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_cardapios', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cardapio_id')->unsigned();

            $table->foreign('cardapio_id')->references('id')
                ->on('cardapios')
                ->onDelete('cascade');

            $table->integer('pedido_id')->unsigned();

            $table->foreign('pedido_id')->references('id')
                ->on('pedidos')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_cardapios');
    }
}
