<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconePratos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('pratos', function(Blueprint $table) {
            $table->integer('icone_id')->unsigned();
            $table->foreign('icone_id')
                ->references('id')->on('icone_pratos')
                ->onDelete('cascade');
        });
        //

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pratos', function (Blueprint $table) {
            $table->dropColumn(['icone_id']);
        });
    }
}
