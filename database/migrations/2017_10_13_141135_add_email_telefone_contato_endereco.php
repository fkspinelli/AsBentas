<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailTelefoneContatoEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->string('ddd', 2)->nullable();
            $table->string('telefone', 10)->nullable();
            $table->string('email')->nullable();
            $table->string('numero')->nullable();
            $table->boolean('padrao')->default(false);

            \Illuminate\Support\Facades\DB::statement('
            ALTER TABLE `enderecos` 
            CHANGE COLUMN `bairro` `bairro` VARCHAR(100)  NULL ,
            CHANGE COLUMN `cidade` `cidade` VARCHAR(100)  NULL ,
            CHANGE COLUMN `empresa_id` `empresa_id` INT(10) UNSIGNED NULL ,
            CHANGE COLUMN `estado` `estado` VARCHAR(2)  NULL ;
');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enderecos', function (Blueprint $table) {
            $table->dropColumn('ddd');
            $table->dropColumn('telefone');
            $table->dropColumn('email');
            $table->dropColumn('numero');
            $table->dropColumn('padrao');
        });
    }
}
