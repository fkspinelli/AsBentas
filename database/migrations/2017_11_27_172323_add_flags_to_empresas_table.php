<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagsToEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->boolean('frete_gratis')->nullable();
            $table->boolean('frete_gratis_funcionario')->nullable();
            $table->decimal('valor_frete')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->dropColumn('frete_gratis');
            $table->dropColumn('frete_gratis_funcionario');
            $table->dropColumn('valor_frete');
        });
    }
}
