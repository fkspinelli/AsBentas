<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsuarioEndereco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('endereco_empresas', function (Blueprint $table) {
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->foreign('usuario_id', 'fk_endereco_usuario')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('endereco_empresas', function (Blueprint $table) {
            $table->dropColumn('usuario_id');
        });
    }
}
