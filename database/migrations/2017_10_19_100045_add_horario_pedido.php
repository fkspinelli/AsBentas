<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHorarioPedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pedidos', function (Blueprint $table) {

            $table->integer('horario_id')->unsigned()->nullable();

            $table->foreign('horario_id', 'fk_horarioentrega_pedido')
                ->references('id')
                ->on('horario_entregas')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function (Blueprint $table) {
            $table->dropColumn('horario_id');
            $table->dropForeign('fk_horarioentrega_pedido');
        });
    }
}
