<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPedidoCardapiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pedido_cardapios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedido_cardapio_id')->unsigned();

            $table->foreign('pedido_cardapio_id')->references('id')
                ->on('pedido_cardapios')
                ->onDelete('cascade');

            $table->string('descricao');
            $table->integer('quantidade');
            $table->decimal('valor');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_pedido_cardapios');
    }
}
