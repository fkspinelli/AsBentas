<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardapioPrato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardapio_prato', function (Blueprint $table) {
            $table->primary(['prato_id', 'cardapio_id']);

            $table->integer('prato_id')->unsigned()->nullable();
            $table->foreign('prato_id')->references('id')
                ->on('pratos')
                ->onDelete('cascade');

            $table->integer('cardapio_id')->unsigned()->nullable();
            $table->foreign('cardapio_id')->references('id')
                ->on('cardapios')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardapio_prato');
    }
}
