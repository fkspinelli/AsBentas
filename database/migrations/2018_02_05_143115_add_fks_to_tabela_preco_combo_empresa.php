<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFksToTabelaPrecoComboEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tabela_preco_combo_empresa', function (Blueprint $table) {
            $table->integer('combo_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->foreign('combo_id')->references('id')->on('combos')->onDelete('cascade');
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tabela_preco_combo_empresas', function (Blueprint $table) {
            $table->dropForeign('combo_id');
            $table->dropForeign('empresa_id');

        });
    }
}
