<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            //
            $table->integer('usuario_pedido_cadastro_id')->unsigned()->nullable();

            $table->foreign('usuario_pedido_cadastro_id')
                ->references('id')->on('users')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            //
            $table->dropForeign('usuario_pedido_cadastro_id');
            $table->dropColumn('usuario_pedido_cadastro_id');

        });
    }
}
