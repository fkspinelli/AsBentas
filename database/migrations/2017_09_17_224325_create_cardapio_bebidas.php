<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardapioBebidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardapio_bebidas', function (Blueprint $table) {
            $table->primary(['acompanhamento_id', 'cardapio_id'],'pk_cardapio_bebidas');

            $table->integer('acompanhamento_id')->unsigned();
            $table->integer('cardapio_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardapio_bebidas');
    }
}
