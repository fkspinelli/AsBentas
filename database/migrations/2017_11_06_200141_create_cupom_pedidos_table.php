<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupom_pedidos', function (Blueprint $table) {
            
            $table->primary(['cupom_id', 'pedido_id', 'usuario_id'],'pk_cupom_pedidos');
            $table->integer('cupom_id')->unsigned()->index();
            $table->integer('pedido_id')->unsigned()->index();
            $table->integer('usuario_id')->unsigned()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupom_pedidos');
    }
}
