<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnderecoUserEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_empresa', function (Blueprint $table) {
            $table->integer('endereco_id')->nullable();
            $table->string('pin_entrega', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_empresa', function (Blueprint $table) {
            $table->dropColumn('endereco_id');
            $table->dropColumn('pin_entrega');
        });
    }
}
