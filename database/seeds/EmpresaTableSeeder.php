<?php

use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa = \AsBentas\Model\Empresa::create([
            'nome' => 'EMPRESA TESTE',
            'cnpj' => '123456789000',
            'contato' => 'CONTATO',
            'telefone' => '24-992590000',
            'email' => 'email@teste.com',
            'area_atuacao' => 'TI',
            'status' => 'ativo'
        ]);

    }
}
