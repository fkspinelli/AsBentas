<?php

use Illuminate\Database\Seeder;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_cliente = \AsBentas\Model\Role::where('name', '=', 'Cliente')->first();

        $admin = new \AsBentas\Model\User();
        $admin->name = "Cliente";
        $admin->email = "cliente@asbentas.com";
        $admin->password = bcrypt('senha');
        $admin->save();

        $admin->roles()->attach($role_cliente);
    }
}
