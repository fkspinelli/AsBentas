<?php

use Illuminate\Database\Seeder;



class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \AsBentas\Model\Role();
        $role->name = 'Cliente';
        $role->description = 'Perfil de Cliente';
        $role->save();

        $role = new \AsBentas\Model\Role();
        $role->name = 'Administrador';
        $role->description = 'Perfil de Administrador';
        $role->save();
    }
}