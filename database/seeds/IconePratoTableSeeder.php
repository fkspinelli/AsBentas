<?php

use Illuminate\Database\Seeder;

class IconePratoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $iconeCarne = \AsBentas\Model\IconePrato::create([
            'nome' => 'Icone Carne',
            'icone' => 'icone-carne.png',
            'css' => 'carne',
        ]);

        $iconeVeggie = \AsBentas\Model\IconePrato::create([
            'nome' => 'Icone Veggie',
            'icone' => 'icone-veggie.png',
            'css' => 'veggie',
        ]);

        $iconePeixe = \AsBentas\Model\IconePrato::create([
            'nome' => 'Icone Peixe',
            'icone' => 'icone-peixe.png',
            'css' => 'peixe',
        ]);

        $iconePeixe = \AsBentas\Model\IconePrato::create([
            'nome' => 'Icone Frango',
            'icone' => 'icone-frango.png',
            'css' => 'peixe',
        ]);
    }
}
