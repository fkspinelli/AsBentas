<?php

use Illuminate\Database\Seeder;

class CaracteristicasPratoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $glutenFree = \AsBentas\Model\CaracteristicaPrato::create([
            'nome' => 'GLUTEN FREE',
            'status' => 'ativo'
        ]);

        $glutenFree = \AsBentas\Model\CaracteristicaPrato::create([
            'nome' => 'LAC FREE',
            'status' => 'ativo'
        ]);

        $glutenFree = \AsBentas\Model\CaracteristicaPrato::create([
            'nome' => 'VEGGIE',
            'status' => 'ativo'
        ]);
    }
}
