<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// static
Route::get('cep-recusado-new', array('as' => 'cep-recusado-new', function() {
    return View::make('static.cep-recusado-new');
}));
// Route::get('entrega-horario', array('as' => 'entrega-horario', function() {
//     return View::make('static.entrega-horario');
// }));
// Route::get('primeiro-acesso-novo', array('as' => 'primeiro-acesso-novo', function() {
//     return View::make('static.primeiro-acesso-novo');
// }));
Route::get('carrinho-novo', array('as' => 'carrinho-novo', function() {
     return View::make('static.carrinho-novo');
}));
Route::get('meus-dados-novo', array('as' => 'meus-dados-novo', function() {
     return View::make('static.meus-dados-novo');
}));



Route::get('/', 'SiteController@index')->name('site.index');
Route::get('/sobre', 'SiteController@sobre')->name('site.sobre');
Route::get('/termos-e-politica-de-privacidade', 'SiteController@termosPolitica')->name('site.termosPolitica');
Route::get('/faca-seu-pedido', 'SiteController@facaSeuPedido')->name('site.faca-seu-pedido');
Route::get('/como-funciona', 'SiteController@comoFunciona')->name('site.como-funciona');
Route::get('/cardapios', 'SiteController@cardapio')->name('site.cardapio');
Route::get('/faq', 'SiteController@faq')->name('site.faq');
Route::get('/contato', 'SiteController@contato')->name('site.contato');
Route::post('/contato', 'SiteController@enviarMensagem')->name('site.enviar-mensagem');
Route::post('/newsletter', 'SiteController@newsletter')->name('site.newsletter');
Route::post('/validar-cep', 'SiteController@validarCep')->name('site.validar-cep');
Route::get('/cep-recusado', 'SiteController@cepRecusado')->name('site.cep-recusado');
Route::get('/validar-cep', 'SiteController@exceptionGetValidarCep')->name('site.validar-cep');


Route::get('/usuario/sair', 'UsuarioController@sair')->name('usuario.sair');
Route::get('/usuario/login', 'UsuarioController@login')->name('usuario.login');
Route::post('/usuario/login', 'UsuarioController@auth')->name('usuario.autenticar');
Route::get('/usuario/cadastro', 'UsuarioController@register')->name('usuario.cadastro');
Route::post('/usuario/salvar', 'UsuarioController@storeRegister')->name('usuario.salvar');
Route::get('/usuario/confirmar-email/{confirmation_code}', 'UsuarioController@confirmationEmail')->name('usuario.confirmar_email');
Route::get('/usuario/reenviar-email/{id}', 'UsuarioController@reSendConfirmationEmail')->name('usuario.reenviar_email');
Route::get('/usuario/cadastro-sucesso', 'UsuarioController@registerSuccess')->name('usuario.sucesso');
Route::get('/usuario/nova-senha-sucesso', 'UsuarioController@newPasswordSuccess')->name('usuario.sucesso-senha');

Route::get('/esqueceu-senha', 'UsuarioController@lostPassword')->name('usuario.esqueceu-senha');
Route::post('/esqueceu-senha', 'UsuarioController@lostPasswordSendEmail')->name('usuario.esqueceu-senha.enviar');

Route::get('/nova-senha', 'UsuarioController@newPassword')->name('usuario.nova-senha');
Route::post('/nova-senha', 'UsuarioController@newPasswordSend')->name('usuario.nova-senha.enviar');

Route::get('/trocar-senha/{id}', 'UsuarioController@changePassword')->name('usuario.trocar-senha');
Route::post('/trocar-senha', 'UsuarioController@changePasswordSave')->name('usuario.trocar-senha.salvar');

Route::get('/social/{provider}/login', 'SocialLoginController@login')->name('social.login');
Route::get('/social/{provider}/login/callback', 'SocialLoginController@callback')->name('social.callback');

Route::get('/carrinho', 'CarrinhoController@exibir')->name('carrinho.exibir');
Route::get('/carrinho/entrega', 'CarrinhoController@entrega')->name('carrinho.entrega');
Route::post('/carrinho/entrega', 'CarrinhoController@salvarEntrega')->name('carrinho.salvarEntrega');
Route::post('/carrinho/atualizar', 'CarrinhoController@atualizar')->name('carrinho.atualizar');
Route::post('/carrinho/aplicar-cupom', 'CarrinhoController@aplicarCupom')->name('carrinho.cupom');
Route::get('/carrinho/remover-item/{id}', 'CarrinhoController@removerItem')->name('carrinho.removerItem');
Route::get('/carrinho/add-complemento/{id}/{idAcompanhamento}', 'CarrinhoController@adicionarComplemento')->name('carrinho.adicionar-complemento');
Route::post('/carrinho/finalizar', 'CarrinhoController@finalizar')->name('carrinho.finalizar');
Route::get('/carrinho/limpar', 'CarrinhoController@limpar')->name('carrinho.limpar');
Route::get('/carrinho/quero-este-prato', 'CarrinhoController@queroEstePrato')->name('carrinho.queroEstePrato');

Route::middleware('auth:usuario')->group(function () {

    Route::get('/meus-dados', 'ContaUsuarioController@meusDados')->name('conta-usuario.meus-dados');
    Route::post('/meus-dados/atualizar', 'ContaUsuarioController@meusDadosAtualizar')->name('conta-usuario.meus-dados.atualizar');
    Route::post('/meus-dados/adicionar-endereco', 'ContaUsuarioController@adicionarEndereco')->name('conta-usuario.meus-dados.adicionarEndereco');
    Route::get('/meus-dados/remover-endereco/{id}', 'ContaUsuarioController@removerEndereco')->name('conta-usuario.meus-dados.removerEndereco');
    Route::get('/meus-dados/endereco-padrao/{id}', 'ContaUsuarioController@enderecoPrincipal')->name('conta-usuario.meus-dados.enderecoPrincipal');
    Route::get('/meus-dados/desvincular-empresa/{id}', 'ContaUsuarioController@desvincularEmpresa')->name('conta-usuario.meus-dados.desvincular-empresa');

    Route::get('/meus-enderecos', 'ContaUsuarioController@meusEnderecos')->name('conta-usuario.meus-enderecos');
    //Route::get('/meus-enderecos', 'ContaUsuarioController@meusEnderecos')->name('conta-usuario.meus-enderecos');

    Route::get('/meus-pedidos', 'MeusPedidosController@index')->name('meus-pedidos.index');
    Route::get('/meus-pedidos/obrigado/{id}', 'MeusPedidosController@obrigado')->name('meus-pedidos.obrigado');
    Route::get('/meus-pedidos/{id}', 'MeusPedidosController@resumo')->name('meus-pedidos.resumo');
    Route::post('/meus-pedidos/cancelar-item/{id}', 'MeusPedidosController@cancelarItem')->name('meus-pedidos.cancelar-item');


    Route::get('/primeiro-acesso', 'ContaUsuarioController@primeiroAcesso')->name('conta-usuario.primeiro-acesso');
    Route::get('/primeiro-acesso/lista-enderecos-empresa', 'ContaUsuarioController@listarEnderecosEmpresa')->name('conta-usuario.listar-endereco-empresa');
    Route::post('/primeiro-acesso/confirmar-empresa', 'ContaUsuarioController@confirmarEmpresa')->name('conta-usuario.confirmar-empresa');
    Route::post('/primeiro-acesso/cadastrar-empresa', 'ContaUsuarioController@cadastrarEmpresa')->name('conta-usuario.pre-cadastro-empresa');
    Route::post('/primeiro-acesso/cadastrar-endereco', 'ContaUsuarioController@adicionarEndereco')->name('conta-usuario.pre-cadastro-endereco');


});



Auth::routes();


Route::prefix('admin')->name('admin.')->namespace('Admin')->group(function () {
    Auth::routes();
    //Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::middleware('auth:admin')->group(function () {

        Route::get('/', 'DashboardController@index')->name('dashboard.index');

        #Usuarios
        Route::match(['get', 'post'],'/usuarios/', 'UsuarioController@index')->name('usuarios.index');
        Route::get('/usuarios/novo', 'UsuarioController@create')->name('usuarios.novo');
        Route::get('/usuarios/editar/{id}', 'UsuarioController@edit')->name('usuarios.editar');
        Route::post('/usuarios/gravar', 'UsuarioController@store')->name('usuarios.gravar');
        Route::post('/usuarios/atualizar/{id}', 'UsuarioController@update')->name('usuarios.atualizar');
        Route::get('/usuarios/excluir/{id}', 'UsuarioController@destroy')->name('usuarios.excluir');

        //Empresa
        Route::match(['get', 'post'],'/empresas/', 'EmpresaController@index')->name('empresas.index');
        Route::match(['get', 'post'],'/empresas/pre-cadastro', 'EmpresaController@preCadastro')->name('empresas.pre-cadastro');

        Route::get('/empresas/novo', 'EmpresaController@create')->name('empresas.novo');
        Route::get('/empresas/editar/{id}', 'EmpresaController@edit')->name('empresas.editar');
        Route::get('/empresas/ativar/{id}', 'EmpresaController@ativar')->name('empresas.ativar');
        Route::post('/empresas/gravar', 'EmpresaController@store')->name('empresas.gravar');
        Route::post('/empresas/atualizar/{id}', 'EmpresaController@update')->name('empresas.atualizar');
        Route::get('/empresas/excluir/{id}', 'EmpresaController@destroy')->name('empresas.excluir');

        #EnderecoEmpresa
        Route::post('/empresas/novo-endereco', 'EmpresaController@storeEndereco')->name('empresas.endereco.gravar');
        Route::post('/empresas/atualizar-endereco', 'EmpresaController@updateEndereco')->name('empresas.endereco.atualizar');
        Route::get('/empresas/excluir-endereco/{id}', 'EmpresaController@destroyEndereco')->name('empresas.endereco.excluir');

        #cupons
        Route::match(['get', 'post'], '/cupom', 'CupomController@index')->name('cupom.index');
        Route::get('/cupom/novo', 'CupomController@create')->name('cupom.novo');
        Route::get('/cupom/editar/{id}', 'CupomController@edit')->name('cupom.editar');
        Route::post('/cupom/gravar', 'CupomController@store')->name('cupom.gravar');
        Route::post('/cupom/atualizar/{id}', 'CupomController@update')->name('cupom.atualizar');
        Route::get('/cupom/excluir/{id}', 'CupomController@destroy')->name('cupom.excluir');

        //Descontos

        #Por frequência
        Route::get('/descontos', 'DescontoController@index')->name('desconto.index');
        Route::get('/descontos/novo', 'DescontoController@create')->name('desconto.novo');
        Route::get('/descontos/editar/{id}', 'DescontoController@edit')->name('desconto.editar');
        Route::post('/descontos/gravar', 'DescontoController@store')->name('desconto.gravar');
        Route::post('/descontos/atualizar/{id}', 'DescontoController@update')->name('desconto.atualizar');
        Route::get('/descontos/excluir/{id}', 'DescontoController@destroy')->name('desconto.excluir');

        #Combo
        Route::get('/combos', 'ComboController@index')->name('combo.index');
        Route::get('/combos/novo', 'ComboController@create')->name('combo.novo');
        Route::get('/combos/editar/{id}', 'ComboController@edit')->name('combo.editar');
        Route::post('/combos/gravar', 'ComboController@store')->name('combo.gravar');
        Route::post('/combos/atualizar/{id}', 'ComboController@update')->name('combo.atualizar');
        Route::get('/combos/excluir/{id}', 'ComboController@destroy')->name('combo.excluir');

        Route::get('/combos/{id}/tabela-preco', 'TabelaPrecoController@indexCombo')->name('combo.tabela-preco');
        Route::post('/combos/{id}/tabela-preco', 'TabelaPrecoController@storeCombo')->name('combo.tabela-preco.salvar');
        Route::get('/combos/{idEmpresa}/tabela-preco/{idCombo}/excluir', 'TabelaPrecoController@destroyCombo')->name('combo.tabela-preco.excluir');


        //Relatorios
        Route::get('/cupom/utilizacao/{id}', 'CupomController@utilizacao')->name('cupom.utilizacao');

        #Locais de Entrega
        Route::match(['get', 'post'],'/locais-entrega', 'LocalEntregaController@index')->name('local-entrega.index');
        Route::get('/locais-entrega/novo', 'LocalEntregaController@create')->name('local-entrega.novo');
        Route::get('/locais-entrega/editar/{id}', 'LocalEntregaController@edit')->name('local-entrega.editar');
        Route::post('/locais-entrega/gravar', 'LocalEntregaController@store')->name('local-entrega.gravar');
        Route::post('/locais-entrega/atualizar/{id}', 'LocalEntregaController@update')->name('local-entrega.atualizar');
        Route::get('/locais-entrega/excluir/{id}', 'LocalEntregaController@destroy')->name('local-entrega.excluir');

        #Pagamentos
        Route::get('/pagamentos', 'PagamentoController@index')->name('pagamento.index');
        Route::get('/pagamentos/novo', 'PagamentoController@create')->name('pagamento.novo');
        Route::get('/pagamentos/editar/{id}', 'PagamentoController@edit')->name('pagamento.editar');
        Route::post('/pagamentos/gravar', 'PagamentoController@store')->name('pagamento.gravar');
        Route::post('/pagamentos/atualizar/{id}', 'PagamentoController@update')->name('pagamento.atualizar');
        Route::get('/pagamentos/excluir/{id}', 'PagamentoController@destroy')->name('pagamento.excluir');

        #Horarios de Entrega
        Route::get('/horarios-entrega', 'HorarioEntregaController@index')->name('horarios-entrega.index');
        Route::get('/horarios-entrega/novo', 'HorarioEntregaController@create')->name('horarios-entrega.novo');
        Route::get('/horarios-entrega/editar/{id}', 'HorarioEntregaController@edit')->name('horarios-entrega.editar');
        Route::post('/horarios-entrega/gravar', 'HorarioEntregaController@store')->name('horarios-entrega.gravar');
        Route::post('/horarios-entrega/atualizar/{id}', 'HorarioEntregaController@update')->name('horarios-entrega.atualizar');
        Route::get('/horarios-entrega/excluir/{id}', 'HorarioEntregaController@destroy')->name('horarios-entrega.excluir');

        #FAQ
        Route::match(['get', 'post'],'/faq', 'FAQController@index')->name('faq.index');
        Route::get('/faq/novo', 'FAQController@create')->name('faq.novo');
        Route::get('/faq/editar/{id}', 'FAQController@edit')->name('faq.editar');
        Route::post('/faq/gravar', 'FAQController@store')->name('faq.gravar');
        Route::post('/faq/atualizar/{id}', 'FAQController@update')->name('faq.atualizar');
        Route::get('/faq/excluir/{id}', 'FAQController@destroy')->name('faq.excluir');


        #newsletter
        Route::match(['get', 'post'],'/newsletter/', 'NewsletterController@index')->name('newsletter.index');
        Route::get('/newsletter/download', 'NewsletterController@download')->name('newsletter.download');

        #BuscasCEP
        Route::match(['get', 'post'],'/buscascep/', 'BuscaCEPController@index')->name('buscascep.index');
        Route::get('/buscascep/download', 'BuscaCEPController@download')->name('buscascep.download');

        #Cliente
        Route::match(['get', 'post'],'/clientes/', 'ClienteController@index')->name('clientes.index');
        Route::get('/clientes/novo', 'ClienteController@create')->name('clientes.novo');
        Route::get('/clientes/editar/{id}', 'ClienteController@edit')->name('clientes.editar');
        Route::post('/clientes/gravar', 'ClienteController@store')->name('clientes.gravar');
        Route::post('/clientes/atualizar', 'ClienteController@update')->name('clientes.atualizar');
        Route::get('/clientes/excluir/{id}', 'ClienteController@destroy')->name('clientes.excluir');
        Route::get('/clientes/download', 'ClienteController@download')->name('clientes.download');

        #Categoria Acompanhamento
        Route::get('/categorias-acompanhamento/', 'CategoriaAcompanhamentoController@index')->name('categoria-acompanhamento.index');
        Route::get('/categoria-acompanhamento/novo', 'CategoriaAcompanhamentoController@create')->name('categoria-acompanhamento.novo');
        Route::get('/categoria-acompanhamento/editar/{id}', 'CategoriaAcompanhamentoController@edit')->name('categoria-acompanhamento.editar');
        Route::post('/categoria-acompanhamento/gravar', 'CategoriaAcompanhamentoController@store')->name('categoria-acompanhamento.gravar');
        Route::post('/categoria-acompanhamento/atualizar', 'CategoriaAcompanhamentoController@update')->name('categoria-acompanhamento.atualizar');
        Route::get('/categoria-acompanhamento/excluir/{id}', 'CategoriaAcompanhamentoController@destroy')->name('categoria-acompanhamento.excluir');

        #Acompanhamento
        Route::match(['get', 'post'],'/acompanhamentos/', 'AcompanhamentoController@index')->name('acompanhamento.index');
        Route::get('/acompanhamento/novo', 'AcompanhamentoController@create')->name('acompanhamento.novo');
        Route::get('/acompanhamento/editar/{id}', 'AcompanhamentoController@edit')->name('acompanhamento.editar');
        Route::post('/acompanhamento/gravar', 'AcompanhamentoController@store')->name('acompanhamento.gravar');
        Route::post('/acompanhamento/atualizar', 'AcompanhamentoController@update')->name('acompanhamento.atualizar');
        Route::get('/acompanhamento/excluir/{id}', 'AcompanhamentoController@destroy')->name('acompanhamento.excluir');

        Route::get('/acompanhamentos/{id}/tabela-preco', 'TabelaPrecoController@indexAcompanhamento')->name('acompanhamento.tabela-preco');
        Route::post('/acompanhamentos/{id}/tabela-preco', 'TabelaPrecoController@storeAcompanhamento')->name('acompanhamento.tabela-preco.salvar');
        Route::get('/acompanhamentos/{idEmpresa}/tabela-preco/{idAcompanhamento}/excluir', 'TabelaPrecoController@destroyAcompanhamento')->name('acompanhamento.tabela-preco.excluir');

        #Prato
        Route::match(['get', 'post'],'/pratos/', 'PratoController@index')->name('prato.index');
        Route::get('/pratos/novo', 'PratoController@create')->name('prato.novo');
        Route::get('/pratos/editar/{id}', 'PratoController@edit')->name('prato.editar');
        Route::post('/pratos/gravar', 'PratoController@store')->name('prato.gravar');
        Route::post('/pratos/atualizar', 'PratoController@update')->name('prato.atualizar');
        Route::get('/pratos/excluir/{id}', 'PratoController@destroy')->name('prato.excluir');

        Route::get('/pratos/{id}/tabela-preco', 'TabelaPrecoController@index')->name('prato.tabela-preco');
        Route::post('/pratos/{id}/tabela-preco', 'TabelaPrecoController@store')->name('prato.tabela-preco.salvar');
        Route::get('/pratos/{idEmpresa}/tabela-preco/{idPrato}/excluir', 'TabelaPrecoController@destroyPrato')->name('prato.tabela-preco.excluir');

        #Cardapio
        Route::match(['get', 'post'], '/cardapios', 'CardapioController@index')->name('cardapio.index');
        Route::get('/cardapios/novo', 'CardapioController@create')->name('cardapio.novo');
        Route::post('/cardapios/gravar', 'CardapioController@store')->name('cardapio.gravar');
        Route::get('/cardapios/editar/{id}', 'CardapioController@edit')->name('cardapio.editar');
        Route::post('/cardapios/atualizar/{id}', 'CardapioController@update')->name('cardapio.atualizar');
        Route::get('/cardapios/excluir/{id}', 'CardapioController@destroy')->name('cardapio.excluir');

        #Pedidos
        Route::match(['get', 'post'], '/pedidos/', 'PedidosController@index')->name('pedido.index');
        Route::get('/pedidos/{id}', 'PedidosController@show')->name('pedido.visualizar')->where('id', '[0-9]+');;
        Route::get('/pedidos/entregar/{id}', 'PedidosController@marcarEntregue')->name('pedido.entregue');
        Route::get('/pedidos/mudar-status-item/{id}', 'PedidosController@mudarStatusItemPedido')->name('pedido.mudarStatusItemPedido');
        Route::get('/pedidos/mudar-status/{id}', 'PedidosController@mudarStatusPedido')->name('pedido.mudarStatusPedido');
        Route::get('/pedidos/imprimir/{id}', 'PedidosController@printInvoice')->name('pedido.imprimir');
        Route::get('/pedidos/imprimir-pedido/{id}', 'PedidosController@printInvoicePedido')->name('pedido.imprimir.cozinha');
        Route::get('/pedidos/imprimir-pedido-completo/{id}', 'PedidosController@printInvoicePedidoCompleto')->name('pedido.completo.imprimir.cozinha');
        Route::get('/pedidos/download', 'PedidosController@download')->name('pedidos.download');
        Route::get('/pedidos/entrega', 'PedidosController@roteiroEntrega')->name('pedidos.entrega');

        #Configurações
        Route::get('/configuracoes/', 'ConfiguracaoController@index')->name('configuracao.index');
        Route::post('/configuracoes/', 'ConfiguracaoController@store')->name('configuracao.salvar');
    });
});

