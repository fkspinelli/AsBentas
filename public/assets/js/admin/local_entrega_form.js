$(document).ready(function () {
    setTimeout(as_bentas_admin_local_entrega_form.iniciar(), 100);
});


as_bentas_admin_local_entrega_form = {

    campos: [],
    tabelas: [],
    formularioEndereco: null,
    modal: [],


    iniciarElementos: function () {

        this.formularioEndereco = $('#form-local-entrega');

        this.tabelas.endereco = $('#tabela-endereco');


        this.campos.btnModalEndereco = $('#btn-modal-endereco');
        this.campos.btnSalvarEndereco = $('#btn-salvar-endereco');
        this.campos.btnCancelarEndereco = $('#btn-cancelar-endereco');

        this.modal.endereco = $('#modal-endereco');

    },

    iniciarAcoes: function () {

        var self = this;

        this.tabelas.endereco.on('click', '.btn-editar-endereco', function (e) {
            e.preventDefault();

            var endereco = $(this).data('json');

            self.popularModalEndereco(endereco);

            self.formularioEndereco.data('url-salvar', $(this).data('url'));

            self.modal.endereco.modal('show');
        });


        this.tabelas.endereco.on('click', '.btn-excluir-endereco', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este endereco?",
                    text: "Após a remoção desse endereço, ele não será mais capaz de ser usado no sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

        this.campos.btnModalEndereco.on('click', function () {

            self.formularioEndereco.data('url-salvar', $(this).data('url'));


        });


        this.campos.btnSalvarEndereco.on('click', function () {

            self.formularioEndereco.attr('action', self.formularioEndereco.data('url-salvar'));
            self.formularioEndereco.submit();


        })

        this.modal.endereco.on('hidden.bs.modal', function (e) {
            self.limparFormularioEndereco();
        })

    },

    iniciarMascaras: function () {

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone').mask(SPMaskBehavior, spOptions);

        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.cep').mask('00000-000');
        $('.money').mask("000,00", {reverse: true});

    },

    popularModalEndereco: function (endereco) {

        this.formularioEndereco.find('input[name=id]').val(endereco.id);
        this.formularioEndereco.find('input[name=nome]').val(endereco.nome);
        this.formularioEndereco.find('input[name=cep]').val(endereco.cep);
        this.formularioEndereco.find('input[name=logradouro]').val(endereco.logradouro);
        this.formularioEndereco.find('input[name=bairro]').val(endereco.bairro);
        this.formularioEndereco.find('input[name=cidade]').val(endereco.cidade);
        this.formularioEndereco.find('input[name=estado]').val(endereco.estado);
    },

    limparFormularioEndereco: function () {

        this.formularioEndereco.find('input[name=id]').val('');
        this.formularioEndereco.find('input[name=nome]').val('');
        this.formularioEndereco.find('input[name=cep]').val('');
        this.formularioEndereco.find('input[name=logradouro]').val('');
        this.formularioEndereco.find('input[name=bairro]').val('');
        this.formularioEndereco.find('input[name=cidade]').val('');
        this.formularioEndereco.find('input[name=estado]').val('');
    },

    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
