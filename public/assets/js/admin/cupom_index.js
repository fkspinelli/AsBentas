$(document).ready(function () {
    setTimeout(as_bentas_admin_cupom.iniciar(), 100);
});


as_bentas_admin_cupom = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.cupom = $('#tabela-cupom');

    },

    iniciarAcoes: function () {

        this.tabelas.cupom.on('click', '.excluir-cupom', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este cupom?",
                    text: "Após a remoção, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
