$(document).ready(function () {
    setTimeout(as_bentas_admin_desconto.iniciar(), 100);
});


as_bentas_admin_desconto = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.pagamento = $('#tabela-pagamento');

    },

    iniciarAcoes: function () {

        this.tabelas.pagamento.on('click', '.excluir-pagamento', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover esta forma de pagamento?",
                    text: "Após a remoção, ela não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
