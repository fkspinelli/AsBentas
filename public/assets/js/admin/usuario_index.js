$(document).ready(function () {
    setTimeout(as_bentas_admin_usuario.iniciar(), 100);
});


as_bentas_admin_usuario = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.usuario = $('#tabela-usuario');

    },

    iniciarAcoes: function () {

        this.tabelas.usuario.on('click', '.excluir-usuario', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este usuário?",
                    text: "Após a remoção desse usuário, ele não será mais capaz de acessar o sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
