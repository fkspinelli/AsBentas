$(document).ready(function () {
    setTimeout(as_bentas_admin_pedido.iniciar(), 100);
});


as_bentas_admin_pedido = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.cardapio = $('#tabela-pedidos');
        this.campos.dataFiltro = $('#filtro-data');
        $('.date').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });

    },

    iniciarAcoes: function () {

    },

    iniciarMascaras: function () {
        
        $('.date').mask("00/00/0000");
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
