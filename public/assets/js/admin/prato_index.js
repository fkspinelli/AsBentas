$(document).ready(function () {
    setTimeout(as_bentas_admin_prato.iniciar(), 100);
});


as_bentas_admin_prato = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.prato = $('#tabela-prato');

    },

    iniciarAcoes: function () {

        this.tabelas.prato.on('click', '.excluir-prato', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este prato?",
                    text: "Após a remoção desse prato, ele não será mais capaz de acessar o sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
