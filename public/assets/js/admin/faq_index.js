$(document).ready(function () {
    setTimeout(as_bentas_admin_faq.iniciar(), 100);
});


as_bentas_admin_faq = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.faq = $('#tabela-faq');

    },

    iniciarAcoes: function () {

        this.tabelas.faq.on('click', '.excluir-faq', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este FAQ?",
                    text: "Após a remoção, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
