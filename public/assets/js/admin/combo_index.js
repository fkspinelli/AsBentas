$(document).ready(function () {
    setTimeout(as_bentas_admin_desconto.iniciar(), 100);
});


as_bentas_admin_desconto = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {
        this.modal = $('#modalPreco');
        this.formulario = $('#form-tabela-preco');
        this.campos.empresaId = $('#empresa_id');
        this.campos.valor = $('#valor');
        this.campos.btnSalvarPreco = $('#btn-salvar-preco');
        this.tabelas.desconto = $('#tabela-combo');

    },

    iniciarAcoes: function () {
        var self = this;

        this.tabelas.desconto.on('click', '.excluir-combo', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este combo?",
                    text: "Após a remoção, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

        this.modal.on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var empresa = button.data('empresa');
            var empresaId = button.data('empresa-id');
            var valor = button.data('prato-valor');

            var modal = $(this);
            modal.find('.modal-title').text('Alterar Preço de ' + empresa);
            self.campos.empresaId.val(empresaId);
            self.campos.valor.val(valor);

        });

        this.campos.btnSalvarPreco.on('click', function () {
            if ($.trim(self.campos.valor.val()).length == 0) {
                self.campos.valor.addClass('error');
            } else {
                self.formulario.submit();
            }

        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
