$(document).ready(function () {
    setTimeout(as_bentas_admin_cliente.iniciar(), 100);
});


as_bentas_admin_cliente = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.cliente = $('#tabela-cliente');

    },

    iniciarAcoes: function () {

        this.tabelas.cliente.on('click', '.excluir-cliente', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este cliente?",
                    text: "Após a remoção desse cliente, ele não será mais capaz de acessar o sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
