$(document).ready(function () {
    setTimeout(as_bentas_admin_desconto.iniciar(), 100);
});


as_bentas_admin_desconto = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.desconto = $('#tabela-desconto');

    },

    iniciarAcoes: function () {

        this.tabelas.desconto.on('click', '.excluir-desconto', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este desconto?",
                    text: "Após a remoção, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
