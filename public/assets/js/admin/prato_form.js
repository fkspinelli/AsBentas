$(document).ready(function () {
    setTimeout(as_bentas_admin_prato_form.iniciar(), 100);
});


as_bentas_admin_prato_form = {

    campos: [],
    tabelas: [],
    formulario: null,

    iniciarElementos: function () {

        this.formulario = $('#form-prato');

        this.campos.icones = $('#dropdown-icone');
        this.campos.icone = $('#icone_id');
        this.campos.btnSalvarEndereco = $('#btn-salvar-form');
        this.campos.btnCancelarEndereco = $('#btn-cancelar-form');

    },

    iniciarAcoes: function () {

        var self = this;

        this.campos.icones.ddslick({
            onSelected: function(data){
                self.campos.icone.val(data.selectedData.value);
            }
        });
    },

    iniciarMascaras: function () {
        $('.money').mask("00000,00", {reverse: true});
    },



    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
