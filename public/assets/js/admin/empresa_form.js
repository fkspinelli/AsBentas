$(document).ready(function () {
    setTimeout(as_bentas_admin_empresa_form.iniciar(), 100);
});

as_bentas_admin_empresa_form = {

    campos: [],
    tabelas: [],
    formularioEndereco: null,
    modal: [],

    iniciarElementos: function () {

        this.formularioEndereco = $('#form-endereco');
        this.tabelas.endereco = $('#tabela-endereco');
        this.campos.btnModalEndereco = $('#btn-modal-endereco');
        this.campos.btnSalvarEndereco = $('#btn-salvar-endereco');
        this.campos.btnCancelarEndereco = $('#btn-cancelar-endereco');
        this.modal.endereco = $('#modal-endereco');
        this.campos.cep = $('#cep');
        this.campos.logradouro = $('#logradouro');
        this.campos.bairro = $('#bairro');
        this.campos.cidade = $('#cidade');
        this.campos.estado = $('#estado');
        this.campos.complemento = $('#complemento');
        this.campos.numero = $('#numero');

    },

    iniciarAcoes: function () {

        var self = this;

        this.tabelas.endereco.on('click', '.btn-editar-endereco', function (e) {
            e.preventDefault();
            var endereco = $(this).data('json');
            self.popularModalEndereco(endereco);
            self.formularioEndereco.data('url-salvar', $(this).data('url'));
            self.modal.endereco.modal('show');
        });

        this.tabelas.endereco.on('click', '.btn-excluir-endereco', function () {
            var urlExclusao = $(this).data('url');
            swal({
                    title: "Deseja realmente remover este endereco?",
                    text: "Após a remoção desse endereço, ele não será mais capaz de ser usado no sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

        this.campos.btnModalEndereco.on('click', function () {
            self.formularioEndereco.data('url-salvar', $(this).data('url'));
        });

        this.campos.btnSalvarEndereco.on('click', function () {
            self.formularioEndereco.attr('action', self.formularioEndereco.data('url-salvar'));
            self.formularioEndereco.submit();
        });

        this.modal.endereco.on('hidden.bs.modal', function (e) {
            self.limparFormularioEndereco();
        });

        this.campos.cep.on('focusout',function(e) {

            cep = ($(this).val()).replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;

            if(cep != "" && validacep.test(cep)){

                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $(self.campos.logradouro).val(dados.logradouro);
                        $(self.campos.bairro).val(dados.bairro);
                        $(self.campos.cidade).val(dados.localidade);
                        $(self.campos.estado).val(dados.uf);
                    }else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'warning'
                        )
                    }

                })
                .fail(function( jqxhr, textStatus, error ) {
                });

            }
            else{
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }

        });
    },

    iniciarMascaras: function () {

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
        $('.phone').mask(SPMaskBehavior, spOptions);
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.cep').mask('00000-000');
        $('.valor-frete').mask("000.00", {reverse: true});
        $('.cod-validacao').mask("000000");
        $('.cep-end').mask('00000-000');
    },

    popularModalEndereco: function (endereco) {

        this.formularioEndereco.find('input[name=id]').val(endereco.id);
        this.formularioEndereco.find('input[name=nomeEndereco]').val(endereco.nome);
        this.formularioEndereco.find('input[name=cep]').val(endereco.cep);
        this.formularioEndereco.find('input[name=logradouro]').val(endereco.logradouro);
        this.formularioEndereco.find('input[name=bairro]').val(endereco.bairro);
        this.formularioEndereco.find('input[name=cidade]').val(endereco.cidade);
        this.formularioEndereco.find('input[name=estado]').val(endereco.estado);
        this.formularioEndereco.find('input[name=complemento]').val(endereco.complemento);
        this.formularioEndereco.find('input[name=numero]').val(endereco.numero);
    },

    limparFormularioEndereco: function () {

        this.formularioEndereco.find('input[name=id]').val('');
        this.formularioEndereco.find('input[name=nomeEndereco]').val('');
        this.formularioEndereco.find('input[name=cep]').val('');
        this.formularioEndereco.find('input[name=logradouro]').val('');
        this.formularioEndereco.find('input[name=bairro]').val('');
        this.formularioEndereco.find('input[name=cidade]').val('');
        this.formularioEndereco.find('input[name=estado]').val('');
        this.formularioEndereco.find('input[name=complemento]').val('');
    },

    iniciar: function () {

        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }
};
