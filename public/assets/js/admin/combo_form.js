$(document).ready(function () {
    setTimeout(as_bentas_admin_cardapio_form.iniciar(), 100);
});


as_bentas_admin_cardapio_form = {

    campos: [],
    tabelas: [],
    modal: [],

    iniciarElementos: function () {

        this.campos.btnAdicionarCategoria = $('#btn-adicionar-categoria');
        this.campos.selectCategoria = $('#select-categoria');
        this.tabelas.combos = $('#tabela-combos');

        $('.selectize').selectize();

    },

    iniciarAcoes: function () {

        var self = this;

        this.campos.btnAdicionarCategoria.on('click', function () {
            if ($('#categoria_id_' + self.campos.selectCategoria.val()).length) {
                sweetAlert("Oops...", "Categoria já adicionada no combo!", "warning");
            } else {
                self.tabelas.combos.find('tbody').append('<tr id="tr_categoria_' + self.campos.selectCategoria.val() + '">' +
                    '<td><input name="categoria_id[]"  type="hidden" id="categoria_id_' + self.campos.selectCategoria.val() +
                    '" value="' + self.campos.selectCategoria.val() + '">' +
                    '' + self.campos.selectCategoria.text() + '</td>' +
                    '<td><a href="#" class="btn btn-default btn-xs excluir-categoria" data-id="' + self.campos.selectCategoria.val() + '"><i class="fa fa-times white"></i> Remover</a></td>' +
                    '</tr>'
                );
            }
        });

        this.tabelas.combos.on('click', '.excluir-categoria', function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
        });
    },

    iniciarMascaras: function () {
        $('#valor').mask("00000,00", {reverse: true});
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
