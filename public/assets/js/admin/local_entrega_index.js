$(document).ready(function () {
    setTimeout(as_bentas_admin_local_entrega.iniciar(), 100);
});


as_bentas_admin_local_entrega = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.usuario = $('#tabela-local-entrega');

    },

    iniciarAcoes: function () {

        this.tabelas.usuario.on('click', '.excluir-local', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este Local de Entrega?",
                    text: "Após a remoção desse Local, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
