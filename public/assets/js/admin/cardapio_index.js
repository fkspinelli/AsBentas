$(document).ready(function () {
    setTimeout(as_bentas_admin_cardapio.iniciar(), 100);
});


as_bentas_admin_cardapio = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.cardapio = $('#tabela-cardapio');

    },

    iniciarAcoes: function () {

        this.tabelas.cardapio.on('click', '.excluir-cardapio', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este item de cardápio?",
                    text: "Após a remoção desse item, ele não será mais exibido no sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },

    iniciarMascaras: function () {

        $('.ano').mask('0000');
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
