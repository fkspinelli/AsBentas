$(document).ready(function () {
    setTimeout(as_bentas_admin_empresa.iniciar(), 100);
});


as_bentas_admin_empresa = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.usuario = $('#tabela-usuario');

    },

    iniciarAcoes: function () {

        this.tabelas.usuario.on('click', '.excluir-empresa', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este empresa?",
                    text: "Após a remoção dessa empresa, ela não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
