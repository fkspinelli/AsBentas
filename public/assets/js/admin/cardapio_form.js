$(document).ready(function () {
    setTimeout(as_bentas_admin_cardapio_form.iniciar(), 100);
});


as_bentas_admin_cardapio_form = {

    campos: [],
    tabelas: [],
    modal: [],

    iniciarElementos: function () {

        this.campos.dataCardapio = $('#data-cardapio');
        this.campos.btnAdicionarPrato = $('#btn-adicionar-prato');
        this.campos.selectPrato = $('#select-prato');
        this.campos.btnAdicionarBebida = $('#btn-adicionar-bebida');
        this.campos.selectBebida = $('#select-bebida');
        this.campos.dia_da_semana = $('#dia_da_semana');
        this.tabelas.pratos = $('#tabela-pratos');
        this.tabelas.bebidas = $('#tabela-bebidas');

        this.campos.dataCardapio.datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR'
        });
        $('.selectize').selectize();

    },

    iniciarAcoes: function () {

        var self = this;

        this.campos.btnAdicionarPrato.on('click', function () {
            //console.log(self.campos.selectPrato.val() + " " + self.campos.selectPrato.text()) ;

            if ($('#prato_id_' + self.campos.selectPrato.val()).length) {
                sweetAlert("Oops...", "Prato já adicionado no cardápio!", "warning");
            } else {
                self.tabelas.pratos.find('tbody').append('<tr id="tr_prato_' + self.campos.selectPrato.val() + '">' +
                    '<td><input name="prato_id[]"  type="hidden" id="prato_id_' + self.campos.selectPrato.val() +
                    '" value="' + self.campos.selectPrato.val() + '">' +
                    '' + self.campos.selectPrato.text() + '</td>' +
                    '<td><input name="prato_ordem[]"  type="number" class="form-control" id="prato_ordem_' + self.campos.selectPrato.val() +
                '" value=""></td>' +
                    '<td><a href="#" class="btn btn-default btn-xs excluir-prato" data-id="' + self.campos.selectPrato.val() + '"><i class="fa fa-times white"></i> Remover</a></td>' +
                    '</tr>'
                );
            }
        });

        this.campos.btnAdicionarBebida.on('click', function () {
            //console.log(self.campos.selectPrato.val() + " " + self.campos.selectPrato.text()) ;

            if ($('#bebida_id_' + self.campos.selectBebida.val()).length) {
                sweetAlert("Oops...", "Bebida já adicionado no cardápio!", "warning");
            } else {
                self.tabelas.bebidas.find('tbody').append('<tr id="tr_bebida_' + self.campos.selectBebida.val() + '">' +
                    '<td><input name="bebida_id[]"  type="hidden" id="bebida_id_' + self.campos.selectBebida.val() +
                    '" value="' + self.campos.selectBebida.val() + '">' +
                    '' + self.campos.selectBebida.text() + '</td>' +
                    '<td><a href="#" class="btn btn-default btn-xs excluir-bebida" data-id="' + self.campos.selectBebida.val() + '"><i class="fa fa-times white"></i> Remover</a></td>' +
                    '</tr>'
                );
            }
        });

        this.tabelas.pratos.on('click', '.excluir-prato', function (e) {
            e.preventDefault();

            $(this).closest('tr').remove();

        });

        this.tabelas.bebidas.on('click', '.excluir-bebida', function (e) {
            e.preventDefault();

            $(this).closest('tr').remove();

        });

        this.campos.dataCardapio.on('focusout',function (e) {
            date = null;
            date = $(this).datepicker('getDate');
            if(date != null){
                var dayOfWeek = date.getUTCDay();
            }
            self.campos.dia_da_semana.val(dayOfWeek);
            console.log(self.campos.dia_da_semana.val());

        });

    },

    iniciarMascaras: function () {

        $('.date').mask("00/00/0000");
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
