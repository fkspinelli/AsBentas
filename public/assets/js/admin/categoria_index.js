$(document).ready(function () {
    setTimeout(as_bentas_admin_categoria.iniciar(), 100);
});


as_bentas_admin_categoria = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.categoria = $('#tabela-categoria');

    },

    iniciarAcoes: function () {

        this.tabelas.categoria.on('click', '.excluir-categoria', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover esta categoria?",
                    text: "Após a remoção dessa categoria, ela não será mais capaz de acessar o sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
