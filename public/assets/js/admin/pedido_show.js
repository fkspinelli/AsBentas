$(document).ready(function () {
    setTimeout(as_bentas_admin_pedido_show.iniciar(), 100);
});


as_bentas_admin_pedido_show = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.campos.btnEntregar = $('#btn-entregar');
        this.campos.btnCancelarPedido = $('#btn-cancelar-pedido');
        this.campos.btnPagar = $('#btn-pagar');
        this.campos.btnCongelar = $('#btn-congelar');

        this.tabelas.cliente = $('#tabela-cliente');

    },

    iniciarAcoes: function () {

        $('#tabela-pedidos').on('click', '.btn-cancelar-pedido', function(e) {
            e.preventDefault();

            var urlCancelamento = $(this).data('url');

            swal({
                    title: "Deseja realmente cancelar este Pedido?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlCancelamento;
                    }
                });

        });

        $('#tabela-pedidos').on('click', '.btn-entregar', function(e) {
            e.preventDefault();

            var urlEntregar = $(this).data('url');

            swal({
                    title: "Deseja realmente marcar este pedido como entregue?",
                    text: "Após marcar o pedido como entregue, não será possível desmarcar.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlEntregar;
                    }
                });

        });

        $('#tabela-pedidos').on('click', '.btn-pagar', function(e) {
            e.preventDefault();

            var urlPagar = $(this).data('url');

            swal({
                    title: "Deseja realmente marcar este pedido como pago?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlPagar;
                    }
                });

        });

        $('#tabela-pedidos').on('click', '.btn-congelar', function(e) {
            e.preventDefault();

            var urlCongelar= $(this).data('url');

            swal({
                    title: "Deseja realmente marcar este pedido como congelado?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlCongelar;
                    }
                });

        });

        this.tabelas.cliente.on('click', '.excluir-cliente', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este cliente?",
                    text: "Após a remoção desse cliente, ele não será mais capaz de acessar o sistema.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
