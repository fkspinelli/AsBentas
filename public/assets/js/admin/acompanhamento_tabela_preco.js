$(document).ready(function () {
    setTimeout(as_bentas_admin_acompanhamento_tabela_preco.iniciar(), 100);
});


as_bentas_admin_acompanhamento_tabela_preco = {

    campos: [],
    tabelas: [],
    modal: null,
    formulario: null,

    iniciarElementos: function () {

        this.formulario = $('#form-tabela-preco');
        this.tabelas.prato = $('#tabela-preco-prato');
        this.modal = $('#modalPreco');
        this.campos.empresaId = $('#empresa_id');
        this.campos.valor = $('#valor');
        this.campos.btnSalvarPreco = $('#btn-salvar-preco');
        this.modal.valor = $('#valor');
    },

    iniciarAcoes: function () {

        var self = this;

        this.modal.on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var empresa = button.data('empresa');
            var empresaId = button.data('empresa-id');
            var valor = button.data('prato-valor');

            var modal = $(this);
            modal.find('.modal-title').text('Alterar Preço de ' + empresa);
            self.campos.empresaId.val(empresaId);
            self.campos.valor.val(valor);

        });

        this.campos.btnSalvarPreco.on('click', function () {

            if ($.trim(self.campos.valor.val()).length == 0) {
                self.campos.valor.addClass('error');
            } else {
                self.formulario.submit();
            }

        });

        this.campos.valor.on('keyup', function () {

            if ($.trim(self.campos.valor.val()).length > 0) {
                self.campos.valor.removeClass('error');
            }

        });

    },

    aplicarMascaras: function () {
        $(this.modal.valor).mask("00000,00", {reverse: true});
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.aplicarMascaras();
    }


};
