$(document).ready(function () {
    setTimeout(as_bentas_admin_horario_entrega.iniciar(), 100);
});


as_bentas_admin_horario_entrega = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.horario = $('#tabela-horario');

    },

    iniciarAcoes: function () {

        this.tabelas.horario.on('click', '.excluir-horario', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este Horário de Entrega?",
                    text: "Após a remoção, ele não ficará mais disponível!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
