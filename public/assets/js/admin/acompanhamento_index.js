$(document).ready(function () {
    setTimeout(as_bentas_admin_acompanhamento.iniciar(), 100);
});


as_bentas_admin_acompanhamento = {

    campos: [],
    tabelas: [],

    iniciarElementos: function () {

        this.tabelas.acompanhamento = $('#tabela-acompanhamento');

    },

    iniciarAcoes: function () {

        this.tabelas.acompanhamento.on('click', '.excluir-acompanhamento', function () {

            var urlExclusao = $(this).data('url');

            swal({
                    title: "Deseja realmente remover este acompanhamento?",
                    text: "Após a remoção desse acompanhamento, ela não será mais capaz de acessar o sistema!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm){
                    if (isConfirm) {
                        window.location = urlExclusao;
                    }
                });
        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
