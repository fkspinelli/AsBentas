$(document).ready(function () {
    setTimeout(as_bentas_admin_cupom_form.iniciar(), 100);
});


as_bentas_admin_cupom_form = {

    campos: [],
    tabelas: [],
    modal: [],

    iniciarElementos: function () {

        this.campos.dataExpiracao = $('#data_expiracao');
        this.campos.valor = $('#valor');

        var picker = new Pikaday({
            field: document.getElementById('data_expiracao'),
            format: 'DD/MM/YYYY',
            i18n: {
                previousMonth : 'Anterior',
                nextMonth     : 'Próximo',
                months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
            },
            toString(date, format) {
                // you should do formatting based on the passed format,
                // but we will just return 'D/M/YYYY' for simplicity
                const day = date.getDate();
                const month = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
                const year = date.getFullYear();

               return `${day}/${month}/${year}`;
            },
        });
        this.iniciarMascaras();
        $('.selectize').selectize();

    },

    iniciarAcoes: function () {

        var self = this;

        //autocomplete

    },

    iniciarMascaras: function () {

        $('.data_expiracao').mask('00/00/0000');
        $('.valor').mask("00000,00", {reverse: true});
    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.iniciarMascaras();
    }


};
