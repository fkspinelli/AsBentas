$(document).ready(function () {
    setTimeout(as_bentas_site_meus_dados.iniciar(), 100);
});


as_bentas_site_meus_dados = {

    campos: [],
    tabelas: [],
    botoes: [],

    iniciarElementos: function () {

        this.campos.email = $('#email');
        this.campos.senha = $('#password');
        this.campos.alterarEmail = $('#alterar-email');
        this.campos.alterarSenha = $('#alterar-senha');
        this.campos.empresaAtiva = $('#empresa-ativa');
        this.campos.enderecoEmpresa = $('#endereco_id');
        this.botoes.alterarEmail = $('#btn-alterar-email');
        this.botoes.alterarSenha = $('#btn-alterar-senha');

        this.campos.cep = $('#cep');
        this.campos.logradouro = $('#logradouro');
        this.campos.bairro = $('#bairro');
        this.campos.cidade = $('#cidade');
        this.campos.estado = $('#estado');
        this.campos.estado = $('#endereco-ddd');
    },

    iniciarAcoes: function () {

    	self = this;

        this.botoes.alterarEmail.on('click', function (e) {
            e.preventDefault();
            self.campos.alterarEmail.val('true');
            self.campos.email.removeAttr('disabled');
            self.botoes.alterarEmail.attr( "disabled", "disabled" );
        });

        this.botoes.alterarSenha.on('click', function (e) {
            e.preventDefault();
            self.campos.alterarSenha.val('true');
            self.campos.senha.removeAttr('disabled');
            self.campos.senha.val('');
            self.botoes.alterarSenha.attr( "disabled", "disabled" );
        });

        this.campos.cep.on('focusout',function(e) {

            cep = ($(this).val()).replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;

            if(cep != "" && validacep.test(cep)){

                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $(self.campos.logradouro).val(dados.logradouro);
                        $(self.campos.bairro).val(dados.bairro);
                        $(self.campos.cidade).val(dados.localidade);
                        $(self.campos.estado).val(dados.uf);
                    }else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'warning'
                        )
                    }

                })
                    .fail(function( jqxhr, textStatus, error ) {
                    });

            }
            else{
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }

        });

        this.campos.empresaAtiva.on('change', function () {

            if ($(this).val().length > 0) {
                var ajax = $.ajax({
                    method: 'get',
                    url: self.campos.empresaAtiva.data('url') + "?empresa_id=" + $(this).val()
                });

                options = [];

                options.push('<option value=""> Selecione o Polo de Entrega</option>');

                ajax
                    .done(function (result) {

                        for (var i = 0; i < result.length; i++) {
                            options.push('<option value="',
                                result[i].id, '">',
                                result[i].nome, '</option>');
                        }
                        
                        
                        self.campos.enderecoEmpresa.html(options.join(''));

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    });
            }
        });

        $('.btn-desvincular-empresa').on('click', function (e) {
            e.preventDefault();
            var urlExclusao = $(this).data('url-desvinculo');
            
            swal({
                title: "Deseja realmente se desvincular de sua empresa?",
                text: "Após esta ação, você não poderá mais usar esta empresa para realizar pedidos!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm){
                if (isConfirm) {
                    window.location = urlExclusao;
                }
            });
        });

    },

    aplicarMascara: function () {

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('.phone').mask(SPMaskBehavior, spOptions);
        $('.ddd').mask('00');
        $('.pin').mask('000000');

    },

    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.aplicarMascara();
    }


};
