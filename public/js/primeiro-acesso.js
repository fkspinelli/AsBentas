$(document).ready(function () {
    setTimeout(as_bentas_site_primeiro_acesso.iniciar(), 400);
});


as_bentas_site_primeiro_acesso = {

    formulario: null,
    campos: [],
    tabelas: [],
    urls: [],

    iniciarElementos: function () {

//        this.formulario = $('#frm-pre-cadastro');
        this.formularioResidencial = $('#frm-pre-cadastro-endereco');
        this.formularioEmpresa = $('#frm-pre-cadastro');
        this.formularioConfirmarEmpresa = $('#frm-confirmar-empresa');


        this.urls.validarCep = $('#frm-pre-cadastro-endereco').data('url');
        this.campos.selectPolos = $('#select-polos');
        this.campos.selectEmpresa = $('#select-empresa');
        this.campos.codigoEmpresa = $('#codigo_empresa')
        this.campos.cep = $('#cep');
        this.campos.logradouro = $('#logradouro');
        this.campos.bairro = $('#bairro');
        this.campos.cidade = $('#cidade');
        this.campos.estado = $('#estado');
        this.campos.nomeEndereco = $('nome-endereco');

        this.btnEmpresa = $('#btn-cadastrar-empresa');
    },

    iniciarAcoes: function () {

    	self = this;

        this.campos.cep.on('focusout',function(e) {

            cep = ($(this).val()).replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;

            if(cep != "" && validacep.test(cep)){

                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $(self.campos.logradouro).val(dados.logradouro);
                        $(self.campos.bairro).val(dados.bairro);
                        $(self.campos.cidade).val(dados.localidade);
                        $(self.campos.estado).val(dados.uf);
                    }else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'warning'
                        )
                    }

                })
                    .fail(function( jqxhr, textStatus, error ) {
                    });

            }
            else{
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }

        });

        this.campos.selectEmpresa.on('change', function () {

            if ($(this).val().length > 0) {
                var ajax = $.ajax({
                    method: 'get',
                    url: self.campos.selectEmpresa.data('url') + "?empresa_id=" + $(this).val()
                });

                options = [];

                options.push('<option value=""> Polo de Entrega</option>');

                ajax
                    .done(function (result) {

                        for (var i = 0; i < result.length; i++) {
                            options.push('<option value="',
                                result[i].id, '">',
                                result[i].nome, '</option>');
                        }

                        self.campos.selectPolos.html(options.join(''));

                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    });
			}


		});

        this.btnEmpresa.on('click', function () {
            var select = self.campos.selectEmpresa.val() + self.campos.selectPolos.val()
            if (select.length == 0) {
                self.formularioEmpresa.submit();
            } else {
                self.formularioConfirmarEmpresa.submit();
            }
        })

    },

    aplicarMascara: function () {

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone').mask(SPMaskBehavior, spOptions);
        $('.ddd').mask('00');
        $('.cep').mask('00000-000');

    },

    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
        this.aplicarMascara();
    }


};
