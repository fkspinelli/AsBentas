$(document).ready(function() {
    setTimeout(as_bentas_site_meus_dados.iniciar(), 100);
});

    as_bentas_site_meus_dados = {

    campos: [],
    tabelas: [],
    botoes: [],

    iniciarElementos: function () {

        this.campos.cep = $('#cep');
        this.campos.logradouro = $('#logradouro');
        this.campos.bairro = $('#bairro');
        this.campos.cidade = $('#cidade');
        this.campos.estado = $('#estado');

    },

    iniciarAcoes: function () {

        self = this;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        this.campos.cep.on('focusout',function(e) {

            cep = ($(this).val()).replace(/\D/g, '');
            var validacep = /^[0-9]{8}$/;

            if(cep != "" && validacep.test(cep)){

                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {

                        $(self.campos.logradouro).val(dados.logradouro);
                        $(self.campos.bairro).val(dados.bairro);
                        $(self.campos.cidade).val(dados.localidade);
                        $(self.campos.estado).val(dados.uf);
                    }else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'warning'
                        )
                    }

                })
                    .fail(function( jqxhr, textStatus, error ) {
                    });

            }
            else{
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }

        });

        /*
         * modal address
         */

        // open modal
        $(document).on('click', '.open-modal-address', function(event){
            event.preventDefault()
            var action = $(this).attr('data-action')
            var id = $(this).attr('data-id')
            var box_data = $(this).parents('.box-dados.address')
            if (action == 'edit') {
                console.log(id);
                $('#modalDados').find('input[name="id"]').val(id);
                $('#modalDados').find('input[name="nome"]').val($('#box-dados-' + id).find('input[name="nome"]').val());
                $('#modalDados').find('input[name="cep"]').val($('#box-dados-' + id).find('input[name="cep"]').val());
                $('#modalDados').find('input[name="logradouro"]').val($('#box-dados-' + id).find('input[name="logradouro"]').val());
                $('#modalDados').find('input[name="numero"]').val($('#box-dados-' + id).find('input[name="numero"]').val());
                $('#modalDados').find('input[name="complemento"]').val($('#box-dados-' + id).find('input[name="complemento"]').val());
                $('#modalDados').find('input[name="ddd"]').val($('#box-dados-' + id).find('input[name="ddd"]').val());
                $('#modalDados').find('input[name="telefone"]').val($('#box-dados-' + id).find('input[name="telefone"]').val());
                $('#modalDados').find('input[name="email"]').val($('#box-dados-' + id).find('input[name="email"]').val());
                $('#modalDados').find('input[name="bairro"]').val($('#box-dados-' + id).find('input[name="bairro"]').val());
                $('#modalDados').find('input[name="cidade"]').val($('#box-dados-' + id).find('input[name="cidade"]').val());
                $('#modalDados').find('input[name="estado"]').val($('#box-dados-' + id).find('input[name="estado"]').val());



                $('.save-modal-address').attr('data-action', 'edit')
            } else if (action == 'delete') {
                if ( $('.content-address >[class*="col-"]').length > 2) {
                    $(box_data).parents('[class*="col-"]').fadeOut(function(){
                        $(this).remove()
                    });
                }
            } else {
                $('.save-modal-address').attr('data-action', 'add')
            }

            if (action != 'delete') {
                $('#modalDados').modal('show');
            }
        });

        $(document).on('click', '.delete', function(event){
            event.preventDefault()
            var action = $(this).attr('data-action')
            var id = $(this).attr('data-id')
            var url = $(this).attr('data-url')
            swal({
                    title: "Deseja realmente remover este endereço?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {

                    if (isConfirm) {
                        window.location = url;
                    }
                }
            );
        });

        $('.endereco-padrao').on('click', function(event){
            event.preventDefault()

            var url = $(this).attr('data-url')
            swal({
                    title: "Deseja realmente definir este endereço como principal?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {

                    if (isConfirm) {
                        window.location = url;
                    }
                }
            );
        });


        // clear modal
        $("#modalDados").on('hidden.bs.modal', function () {
            $(this).find('input').val('');
        });

    },

        iniciar: function () {
            this.iniciarElementos();
            this.iniciarAcoes();
        }




};


