$(document).ready(function() {

    setTimeout(as_bentas_site_pedido.iniciar(), 1000);


    $(document).on('click', '.nav li a', function(){
        var href = $(this).attr('href')
        $("html, body").animate({ scrollTop: $(href).offset().top }, 600)
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('.select-address').select2()

    $('.pin').mask("000000", {reverse: true})
    $('.cep').mask("00000-000", {reverse: true})

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '00000-0000' : '0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.tel').mask(SPMaskBehavior, spOptions);

    $('.mapa-content .col-xs-1').css({'height':$('.mapa img').height()});

    $('.mapa-cep').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        infinite: false,
        prevArrow: $('.mapa-content .fa-angle-left'),
        nextArrow: $('.mapa-content .fa-angle-right'),
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.slick-testimonials').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        infinite: false,
        prevArrow: $('.testimonials-trustpilot .fa-angle-left'),
        nextArrow: $('.testimonials-trustpilot .fa-angle-right'),
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.slick-date > .dates ').not('.slick-initialized').slick({
        slidesToShow: 3,
        slickGoTo: 1,
        centerMode: true,
        centerPadding: '0px',
        speed: 400,
        asNavFor: '.slick-menu',
        focusOnSelect: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    }).on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.spinner').hide();
        $('.slick-date, .slick-menu').fadeIn();
    });

    $('.slick-menu').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slick-date > .dates'
    });

    $('.table-resumo').basictable();

    // funcionamento dos botões Azuis!
    $('.integer > button').click(function(e){
        e.preventDefault();
        var operator = $(this).attr('data-operator')
        var currentValue = parseInt($(this).parent().find('input').val())
        var newValue = null

        if (operator === "less") {
            if (currentValue > 1) {
                newValue = currentValue - 1
            } else {
                newValue = 0
                if ($(this).parents('[data-type]').attr('data-type') === "principal") {
                    $(this).parents('.item').find('.integer input').val(0)
                }
            }
        } else if (operator === "more") {
            if ($(this).parents('[data-type]').attr('data-type') !== "principal") {
                var principal = $(this).parents('.item').find('[data-type="principal"] .integer input')
                if (principal.val() == 0) {
                    principal.val(1)
                    BootstrapDialog.alert({
                        title: 'Atenção',
                        message: '&nbsp;&nbsp;&nbsp;&nbsp;Não vendemos esse item sem o <b>prato principal</b>. <br>&nbsp;&nbsp;&nbsp;&nbsp;Adicionamos para você.',
                        type: BootstrapDialog.TYPE_WARNING
                    });
                }
            }
            newValue = currentValue + 1
        }

        $(this).parent().find('input').val(newValue)
    });

    $('.check input').change(function(){

        if (!$(this).is(':checked')) {
            $(this).parents('.item').find('[data-type="principal"] [data-operator="more"]').click()
        } else {
            $(this).parents('.item').find('.integer input').val(0)
        }
    });
    getQtdItensPorCategoria = function(categoryName){
        var cont = []


        $('.slick-menu-item, .summary-day').each(function () {
            cont.push(0);
            $(this).find('[data-type="' + categoryName + '"]').each(function () {
                $(this).find('input[type="number"]').each(function () {
                    cont[cont.length - 1] += itemCount = parseInt($(this).val());
                })
            })

        });
        return cont
    }
    getNomesCategorias = function(){
        let ret = []

        $('li[data-type], div[data-type]').each(function () {
            ret.push($(this).attr('data-type'))
        })

        return ret.filter(function(categoria, index){
            return ret.indexOf(categoria) == index
        })
    }

    getQtdPedidosItens = function(){
        diasItems = {}
        categorias = getNomesCategorias();

        categorias.forEach(function (categoria) {

            getQtdItensPorCategoria(categoria).forEach(function (quantidade, index) {
                if(typeof diasItems[index] == "object"){
                    diasItems[index][categoria] = quantidade
                }
                else {
                    diasItems[index] = {}
                    diasItems[index][categoria] = quantidade
                }
            })
        })

        return diasItems
    }
    subtrairObjetos = function (combo, dia) {
        var comboAtributes = Object.keys(combo)
        var diaAtributes = Object.keys(dia)

        if (diaAtributes.length < comboAtributes.length) {
            dia = false
        }
        else {
            $.each(comboAtributes, function (i, atributo) {
                if (dia) {
                    if (combo[atributo] == 0){
                        dia = false
                        console.error('Função mal utilizada!','Objeto combo não pode conter atributos com valores nulos.')
                    }
                    else {
                        if (diaAtributes.indexOf(atributo) == -1){
                            dia = false
                        }
                        else if(dia[atributo] < combo[atributo]){
                            dia = false
                        }
                        else {
                            dia[atributo] -= combo[atributo]
                        }
                    }
                }
            })
        }
        return dia
    }
    formarCombos = function (model, target, combosFormados) {
        // combosFormados = combosFormados || 0
        let novoValor = subtrairObjetos(model, Object.assign({}, target))
        if (!novoValor) {
            return {target, combosFormados}
        } else {
            combosFormados++
            return formarCombos(model, novoValor, combosFormados)
        }

    }

    getCombos = function () {
        let combos = Object.keys(as_bentas_site_pedido.combos).map(function (res) {
            let combos = {}
            let valor = as_bentas_site_pedido.combos[res].valor
            Object.keys(as_bentas_site_pedido.combos[res]['itens']).forEach(function (posicao) {
                combos[as_bentas_site_pedido.combos[res]['itens'][posicao]] = 1
            })
            return {
                id: res,
                combo: combos,
                valor: valor
            }
        })

        combos.sort(function (a, b) {
            a = Object.keys(a.combo).length
            b = Object.keys(b.combo).length
            if(a > b) {
                return -1
            }
            if(a < b) {
                return 1
            }
            return 0
        })

        return combos

    }

    combosPorDia = function (combos) {
        var combosFormadosPorDia = []
        var itensCarrinho = getQtdPedidosItens()
        var valorDesconto = 0
        $.each(itensCarrinho, function (i, item) {
            var itensValidar = item
            var combosFormados = []
            $.each(combos, function (j, dados) {
                var formouCombo = formarCombos(Object.assign({}, dados.combo),Object.assign({}, itensValidar), 0)
                combosFormados.push({
                    qtd_combo: formouCombo.combosFormados,
                    id_combo: dados.id,
                    desconto_por_combo: dados.valor
                })
                itensValidar = formouCombo.target
            });
            combosFormadosPorDia.push({
                dia: i,
                combos: combosFormados.filter(res => res.qtd_combo > 0),
                total: combosFormados.filter(res => res.qtd_combo > 0).reduce((a,b) => {
                return b.qtd_combo * parseInt(b.desconto_por_combo) + a
            }, 0)
        })
        });



        //Somar Desconto
        $.each(combosFormadosPorDia, function (i, item) {
            valorDesconto += item.total
            if (item.total > 0) {
                $('#dia_' + i).find('.desconto-combo').closest('tr').removeAttr( 'style' )
                $('#dia_' + i).find('.desconto-combo').html(item.total.toFixed(2).replace('.', ','))
                $('#dia_' + i).find('.desconto-combo').data('valor', item.total.toFixed(2));
            }
        })

        return valorDesconto
    }

    atualizar = function(){
        as_bentas_site_pedido.atualizar();
    };

    //Teste pra remoção de footer
    removerFooter = function () {
        const REMOVEFROMPATH = ['/cardapios','/carrinho/entrega','/carrinho']
        var path = window.location.pathname

        if(REMOVEFROMPATH.indexOf(path) >=0 ){
            $('.remove').addClass('hide');
        }
    }

    //Chama a função atualizar nos eventos abaixo listados
    $(document).on('click change load', '.integer > input, .integer > button, html', atualizar);


    //Chama a função atualizar e remover footer ao inicializar a página
    as_bentas_site_pedido.atualizar();
    removerFooter();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#btn-conferir-local').click(function(){
        var _this = document.getElementById("cep")
        var valor = $(_this).val();
        var cep = valor.replace(/\D/g, '');


        var _this = document.getElementById("email")
        var email =_this;


        if (cep !== "" && email !== "") {
            var validacep = /^[0-9]{8}$/;
            if(validacep.test(cep)) {
                $(_this).parents('form').find('.rua').val('...')
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {
                        //Se a cidade for fora do Rio de Janeiro
                        if (dados.localidade !== "Rio de Janeiro")
                        {
                            swal(
                                'Desculpe-nos!',
                                'Nossa área de atuação está concentrada na cidade do Rio de Janeiro. Agradecemos muito seu interesse pelas nossas refeições!',
                                'warning'
                            )
                        }
                        else
                        {
                            $('#frm-conferir-local').submit();
                        }
                    }
                    else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'error'
                        )
                    }
                })
                    .fail(function( jqxhr, textStatus, error ) {
                        $('#frm-conferir-local').submit();
                    });
            }
            else {
                $(_this).parents('form').find('.rua').val('')
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }
        }
        else{
            $('#frm-conferir-local').submit();
        }


    });

    $('.cep').blur(function(){
        var _this = $(this);
        var valor = $(_this).val();
        var cep = valor.replace(/\D/g, '');
        if (cep != "") {
            var validacep = /^[0-9]{8}$/;
            if(validacep.test(cep)) {
                $(_this).parents('form').find('.address').val('...')
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {

                        $(_this).parents('form').find('.rua').val(dados.logradouro);
                        $(_this).parents('form').find('.neighborhood').val(dados.bairro);
                        $(_this).parents('form').find('.locality').val(dados.localidade);
                        $(_this).parents('form').find('.uf').val(dados.uf);
                    }
                    else {
                        swal(
                            'Oops...',
                            'CEP não encontrado!',
                            'error'
                        )
                    }
                })
                    .fail(function( jqxhr, textStatus, error ) {
                        $('#frm-conferir-local').submit();
                    });
            }
            else {
                $(_this).parents('form').find('.address').val('')
                swal(
                    'Oops...',
                    'Informe um formato de CEP válido!',
                    'error'
                )
            }
        }

    });



    /*
    * modal address
    */

    // open modal
    $(document).on('click', '.open-modal-address', function(event){
        event.preventDefault()
        var action = $(this).attr('data-action')
        var id = $(this).attr('data-id')
        var box_data = $(this).parents('.box-dados.address')
        if (action == 'edit') {
            $('#modalDados').find('input[name="id"]').val(id);
            $('#modalDados').find('input[name="nome"]').val($('#box-dados-' + id).find('input[name="nome"]').val());
            $('#modalDados').find('input[name="cep"]').val($('#box-dados-' + id).find('input[name="cep"]').val());
            $('#modalDados').find('input[name="logradouro"]').val($('#box-dados-' + id).find('input[name="logradouro"]').val());
            $('#modalDados').find('input[name="numero"]').val($('#box-dados-' + id).find('input[name="numero"]').val());
            $('#modalDados').find('input[name="complemento"]').val($('#box-dados-' + id).find('input[name="complemento"]').val());
            $('#modalDados').find('input[name="endereco-ddd"]').val($('#box-dados-' + id).find('input[name="endereco-ddd"]').val());
            $('#modalDados').find('input[name="endereco-telefone"]').val($('#box-dados-' + id).find('input[name="endereco-telefone"]').val());
            $('#modalDados').find('input[name="email"]').val($('#box-dados-' + id).find('input[name="email"]').val());
            $('#modalDados').find('input[name="bairro"]').val($('#box-dados-' + id).find('input[name="bairro"]').val());
            $('#modalDados').find('input[name="cidade"]').val($('#box-dados-' + id).find('input[name="cidade"]').val());
            $('#modalDados').find('input[name="estado"]').val($('#box-dados-' + id).find('input[name="estado"]').val());



            $('.save-modal-address').attr('data-action', 'edit')
        } else if (action == 'delete') {
            if ( $('.content-address >[class*="col-"]').length > 2) {
                $(box_data).parents('[class*="col-"]').fadeOut(function(){
                    $(this).remove()
                });
            }
        } else {
            $('.save-modal-address').attr('data-action', 'add')
        }

        if (action != 'delete') {
            $('#modalDados').modal('show');
        }
    });

    //Remover endereço (Página Meus Dados)
    $(document).on('click', '.delete', function(event){
        event.preventDefault()
        var action = $(this).attr('data-action')
        var id = $(this).attr('data-id')
        var url = $(this).attr('data-url')
        swal({
                title: "Deseja realmente remover este endereço?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {

                if (isConfirm) {
                    window.location = url;
                }
            }
        );
    });

    //Fazer endereço se tornar padrão (Página Meus Dados)
    $('.endereco-padrao').on('click', function(event){
        event.preventDefault()

        var url = $(this).attr('data-url')
        swal({
                title: "Deseja realmente definir este endereço como principal?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim",
                cancelButtonText: "Não",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {

                if (isConfirm) {
                    window.location = url;
                }
            }
        );
    });

    // clear modal
    $("#modalDados").on('hidden.bs.modal', function () {
        $(this).find('input').val('');
    });

    // save modal
    /**
     *
     $(".save-modal-address").click(function(event){
        event.preventDefault();
        var action = $(this).attr('data-action')
        var box_data = $(this).parents('#modalDados')

        if (action == 'add') {
            var new_box = $('.content-address').find('>[class*="col-"]:last-child').prev().clone()

            var new_id = new_box.find('.box-dados.address').prop('id').replace('box-dados-', '')
            var new_id = parseInt(new_id)
            var new_id = new_id+1
            var new_box_dados_id = 'box-dados-'+new_id

            new_box.find('.box-dados.address').prop('id', new_box_dados_id).find('input').val('')
            new_box.find('.radio-font-awesome label').attr('for', 'default_address_'+new_id)
            new_box.find('.radio-font-awesome input').prop('id', 'default_address_'+new_id)
            $('.content-address').find('>[class*="col-"]:last-child').before(new_box)
            $(box_data).find('.box-dados-id').val(new_box_dados_id)
        }

        var box_dados_id =      $(box_data).find('.box-dados-id').val()
        var cep =               $(box_data).find('.cep').val()
        var neighborhood =      $(box_data).find('.neighborhood').val()
        var address =           $(box_data).find('.address').val()
        var number =            $(box_data).find('.number').val()
        var complement =        $(box_data).find('.complement').val()
        var ddd =               $(box_data).find('.ddd').val()
        var phone =             $(box_data).find('.phone').val()
        var email =             $(box_data).find('.email').val()
        var name =              $(box_data).find('.name').val()
        var locality =          $(box_data).find('.locality').val()
        var uf =                $(box_data).find('.uf').val()

        $('#'+box_dados_id).find('.cep').val(cep) // 82400-000
        $('#'+box_dados_id).find('.neighborhood').val(neighborhood)
        $('#'+box_dados_id).find('.address').val(address)
        $('#'+box_dados_id).find('.number').val(number)
        $('#'+box_dados_id).find('.complement').val(complement)
        $('#'+box_dados_id).find('.ddd').val(ddd)
        $('#'+box_dados_id).find('.phone').val(phone)
        $('#'+box_dados_id).find('.email').val(email)
        $('#'+box_dados_id).find('.name').val(name)
        $('#'+box_dados_id).find('.locality').val(locality)
        $('#'+box_dados_id).find('.uf').val(uf)

        $('#'+box_dados_id).find('.label-address').text(address)
        $('#'+box_dados_id).find('.label-number').text(', '+number)
        $('#'+box_dados_id).find('.label-complement').text(' - '+complement)
        $('#'+box_dados_id).find('.label-cep').text(cep)
        $('#'+box_dados_id).find('.label-neighborhood').text(' - '+neighborhood)
        $('#'+box_dados_id).find('.label-locality').text(locality)
        $('#'+box_dados_id).find('.label-uf').text(uf)

        $('#modalDados').modal('hide');
    });


     $('#modalDados input:not([type="hidden"])').keyup(function(){
        var empty = 0;
        $('#modalDados input:not([type="hidden"])').each(function(){
            if ($(this).val() == '') {
                empty++
            }
        });
        if (empty == 0) {
            $('#modalDados .save-modal-address').removeClass('disabled')
        }
    });
     */

    if (window.location.hash == '#print') {
        window.print();
    }

    $('.nome-local').tooltip({'trigger':'focus', 'title': 'Apelido do Endereço'});

    $('#btn-continuar-comprando').click(function(){
        $("html, body").animate({ scrollTop: $('#menu form').offset().top - $('.date-affix').height() - 60 }, 600);
        setTimeout(function(){
            $('.slick-date > .dates').slick('slickNext');
        }, 600);
    });
});

as_bentas_site_pedido = {

    pedidos: {},
    combos: {},
    botoes: [],
    formulario: null,
    url: null,
    urlPrato: null,
    urlAcompanhamento: null,

    iniciarCampos: function () {
        this.botoes.carrinho = $('#btn-carrinho');

        this.formulario = $('#form-carrinho');
        this.url = this.formulario.data('url');
        this.botoes.limpar = $('.btn-limpar-carrinho');
        this.urlPrato = this.formulario.data('url-adicionar-prato');
        this.urlAcompanhamento = this.formulario.data('url-adicionar-acompanhamento');
        this.urlLimparCarrinho = $('.btn-limpar-carrinho').data('url-limpar-carrinho');


        $('.slick-date > .dates').on("init", function() {
            setTimeout(function () {

                var now = new Date();
                var goto = 1;
                var dia = now.getDay();

                if (dia > 0 && dia < 6)
                    goto = dia + 1;

                $('.slick-date > .dates').slick("slickGoTo", 0, true);

            }, 0);


        });
    },

    acoes: function () {
        var self = this;

        this.botoes.carrinho.on('click', function (e) {
            var path = window.location.pathname
            if(path == '/cardapios'){
                e.preventDefault();
                self.formulario.submit();
            }
        });

        //Limpar Carrinho
        this.botoes.limpar.on('click', function () {
            swal({
                    title: "Deseja realmente limpar o carrinho de pedidos?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {

                    if (isConfirm) {
                        window.location = self.urlLimparCarrinho;
                    }
                }
            );
        });
    },

    atualizar: function () {

        combos = getCombos()

        var totalTypePrincipal = 0;
        var totalTypeBebida = 0;
        var totalTypeLanche = 0;

        //Valor do dia atual
        var amount = 0

        //Valor do item
        var valorItem = 0

        //Total de todos os itens da lista
        var itemTotal = 0;

        //Valor total de frete
        var freteTotal = 0;

        var valorCupom = 0;

        //Frete do dia atual
        var freteDia = 0;

        //Valor total do pedido (soma de todos os dias)
        var totalPedido = 0;

        //Valor do desconto do dia
        var valorDesconto = 0;

        //Quantidade de pratos principais
        qtdPratoPorDia = new Array();
        var cont = 1;

        //Quantidade de pratos principais
        var quantPratoPrincipalAtual = 0;


        $('.slick-menu-item > .item, .summary-day').each(function(index) {

            itemTotal = 0;
            amount = 0;
            valorDesconto = 0;
            valorDescontoFrequencia = 0;
            valorDescontoCombo = $(this).find('.desconto-combo').data('valor') || 0;
            quantPratoPrincipalAtual = 0;


            menuPrice = parseFloat($(this).find('[data-type="principal"] [data-item-price]').attr('data-item-price'))

            itemId = $(this).find('[data-id]').attr('data-id')

            //Procuro todos as quantidades e valores dos itens na lista
            $(this).find('.item-option>li').each(function(){
                itemPrice = parseFloat($(this).find('[data-item-price]').attr('data-item-price'))
                itemCount = parseInt($(this).find('.integer input').val())
                if (isNaN(itemCount)) {
                    itemCount = 1;
                }
                itemTotal += itemPrice * itemCount
                valorItem = itemPrice * itemCount

                // console.log("Item Valor" + itemTotal)
                // console.log("Valor item " + valorItem)
                // $(this).find('[data-item-price]').html('R$ ' + valorItem.toFixed(2).replace('.', ','))

                //Testo todos os 'li's' que foram encontrados para saber se é um prato principal
                if($(this).attr('data-type') == ('principal'))
                {
                    //Somo a quantidade de cada prato principal a quantidade de pratos do dia
                    quantPratoPrincipalAtual += itemCount = parseInt($(this).find('.integer input').val());
                }
                var qtd = parseInt($(this).find('.integer input').val());
                var valUnit = parseFloat($(this).find('[data-item-price]').attr('data-item-price'));
                var valTot = qtd * valUnit;
                $(this).find('.valor-total').text(valTot.toFixed(2).replace('.', ','));
            });

            //Preencho a posição do vetor com a quantidade de pratos principais do dia
            qtdPratoPorDia[cont] = quantPratoPrincipalAtual;

            //Seto como default a barra de total visível
            $('.btn-limpar-carrinho').show();
            $('.bar-total').removeClass('hide').addClass('show');

            //Varro o vetor que guarda todas as quantidades de pratos por dia
            for (var i = 1; i <= cont; i++) {
                //Caso eu encontre algum dia com qauntidade = 0, escondo a barra de finalizar pedido!
                if (qtdPratoPorDia[i] == 0) {
                    $('.btn-limpar-carrinho').hide();
                    $('.bar-total').removeClass('show').addClass('hide');

                    //Como o arquivo js da tela carrinho é o mesmo da tela cardápio, preciso testar qual tela estou pra
                    //poder enviar a mensagem de alerta.
                    var path = window.location.pathname
                    if(path == '/carrinho'){
                        swal(
                            'Oops...',
                            'Lembre-se de adicionar pratos principais a todos os dias!',
                            'error'
                        )
                        //Adicionei este remove class stop-scrolling aqui pois o component swal alert estava travando
                        // o scrolling da página após sua execução
                        $("body").removeClass('stop-scrolling');
                    }

                }
            }
            //Variável auxiliar
            cont += 1

            //Soma o total de tudo que foi comprado ao valor do dia atual
            amount += itemTotal


            // Aplica Desconto de Combo
            amount -= valorDescontoCombo
            //Calcula o valor do desconto
            if ($(this).find('[data-type="desconto"]').length) {

                var desconto = parseFloat($(this).find('[data-type="desconto"]').data('item-price'));
                var valorDesconto = amount * ((desconto/100));


                //Subtrai do valor do dia atual o valor do desconto
                amount -= valorDesconto;
                //Imprime pra tela o valor do desconto
                $(this).find('.desconto-item-pedido').html(valorDesconto.toFixed(2).replace('.', ','));
            }

            //Adiciona o frete do dia ao valor total do dia.
            if ($(this).find('[data-type="frete"]').data('item-price')) {
                freteDia = parseFloat($(this).find('[data-type="frete"]').data('item-price'));
                amount += freteDia
            }

            //Calculo do valor total do frete
            if ($(this).find('[data-type="frete"]').length) {

                //Pega o valor do frete do dia
                var freteDoDia = parseFloat($(this).find('[data-type="frete"]').data('item-price'));

                freteTotal += freteDoDia;
            }


            //Imprime pra tela o valor do dia autal já com o frete e o desconto calculado
            $(this).find('.total-item-pedido').html(((amount)).toFixed(2).replace('.', ','));

            //Incrementa o valor total do pedido com o valor do dia atual
            totalPedido += amount;

            if (itemTotal == 0) {
                $(this).find('.check input').prop('checked', false)
                $(this).find('.price > .value').text(menuPrice.toFixed(2).replace('.', ','))
            } else {
                $(this).find('.check input').prop('checked', true)
                $(this).find('.price > .value').text(itemTotal.toFixed(2).replace('.', ','))
            }


            var typePrincipal = 0
            var typeBebida = 0
            var typeLanche = 0
            $(this).find('.prev').empty()

            $(this).find('[data-type]').each(function(){
                var type = $(this).attr('data-type')
                $(this).find('.integer input').each(function(){
                    if ( $(this).val() > 0 ) {
                        switch (type.toLowerCase()) {
                            case 'principal':
                                typePrincipal += 1
                                totalTypePrincipal += 1
                                break;
                            case 'bebidas':
                                typeBebida += 1
                                totalTypeBebida += 1
                                break;
                            case 'lanches':
                                typeLanche += 1
                                totalTypeLanche += 1
                                break;
                        }
                    }
                });
            });

            if (typePrincipal > 0) {$(this).find('.prev').append('<span>Prato principal</span>') }
            if (typeBebida > 0) {$(this).find('.prev').append('<span> + bebida</span>') }
            if (typeLanche > 0) {$(this).find('.prev').append('<span> + lanche</span>') }


        });

        //confere se tem item adicionado para os dias e da um check no carousel de datas
        $('.slick-menu-item').each(function(){
            var prato = 0;
            $(this).find('.check input').each(function(){
                if ($(this).prop('checked')) {
                    prato++;
                }
            });

            var slickId = $(this).attr('data-slick-index')
            var color = '#cccccc';
            if (prato > 0) {
                color = '#55d069'
            }
            $('[data-slick-index="'+slickId+'"]').find('.ion-android-checkmark-circle').css('color', color);
        });


        //Esvazia a barra de itens perto do 'Finalizar Pedido'
        $('.bar-total .prev').empty()

        var descontoCombo = combosPorDia(combos)

        // Testa pra ver se o total do pedido é igual a zero se for debitado somente o valor do frete,
        // caso for a barra de Finalizar pedido é escondida.
        var path = window.location.pathname
        if( path == '/cardapios') {

            totalPedido -= descontoCombo

            if ((totalPedido - freteTotal) == 0) {
                $('.btn-limpar-carrinho').hide();
                $('.bar-total').removeClass('show').addClass('hide');
            } else {
                $('.btn-limpar-carrinho').show();
                $('.bar-total').removeClass('hide').addClass('show');
            }
        }

        //Calculando o valor do Cupom
        valorCupom = 0;
        //Desconto por combo

        //
        // if (descontoCombo > 0) {
        //     // $('.desconto-combo').closest('tr').removeAttr( 'style' )
        //     // $('.desconto-combo').html(descontoCombo.toFixed(2).replace('.', ','))
        // }


        if ($('.box-total').find('[data-type="cupom"]').length) {
            var tipoCupom = $('.box-total').find('[data-type="cupom"]').data('item-type');

            valorCupom = parseFloat($('.box-total').find('[data-type="cupom"]').data('item-price'));

            if ($.trim(tipoCupom) == "%") {
                var valorCupom = (totalPedido * (valorCupom / 100));
                totalPedido = totalPedido - valorCupom;
            } else {
                totalPedido = totalPedido - valorCupom;
            }

        }

        //Se o total do pedido for menor que 0, o total do pedido se torna 0
        if (totalPedido < 0) totalPedido = 0


        //Imprime na tela o valor do cupom
        if (valorCupom > 0) {
            $('.cupom-pedido').html(valorCupom.toFixed(2).replace('.', ','));
        }

        //Imprime na tela o valor do cupom
        $('.total-pedido').html(totalPedido.toFixed(2).replace('.', ','));

        //Adiciona itens de acordo com a categoria selecionada
        if (totalTypePrincipal > 0) {$('.bar-total .prev').append('<span>Prato principal</span>') }
        if (totalTypeBebida > 0) {$('.bar-total .prev').append('<span> + bebida</span>') }
        if (totalTypeLanche > 0) {$('.bar-total .prev').append('<span> + lanche</span>')}
        if (descontoCombo > 0) {$('.bar-total .prev').text('Parabéns, Combo adicionado!')}

        //Testa pra ver se o frete é maior que 0, se sim adiciona à barra de itens
        if (freteTotal > 0) {
            $('.bar-total .prev').append('<span> + frete</span>')
        }
        //É aqui que o elemento que tem a classe value (na barra de finalizar o pedido) recebe o valor de amount
        $('.bar-total .total .value').text(totalPedido.toFixed(2).replace('.', ','))
    },

    iniciar: function () {
        this.iniciarCampos();
        this.acoes();
    }

}





