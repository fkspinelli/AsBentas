$(document).ready(function () {
    setTimeout(as_bentas_site_home.iniciar(), 100);
});


as_bentas_site_home = {

    campos: [],
    tabelas: [],
    urls: [],
    botoes: [],
    formulario: null,
    formularioConferirLocal: null,

    iniciarElementos: function () {

        this.formulario = $('#form-newsletter');
        this.formularioConferirLocal = $('#frm-conferir-local');

        this.campos.nome = $('#nome');
        this.campos.email = $('#email');

        this.botoes.conferirLocal = $('#btn-conferir-local');

        this.urls.salvar = this.formulario.data('url');

    },

    iniciarAcoes: function () {

    	self = this;

        this.formulario.on('submit', function () {

            if (self.campos.nome.val().length == 0 || self.campos.email.val().length == 0) {
                swal(
                    'Atenção',
                    'Verifique se os campos Nome/E-mail foram preenchidos.',
                    'warning'
                );

            } else {

                data = {
                  'nome' : self.campos.nome.val(),
                  'email': self.campos.email.val()
                };

                $.post(self.urls.salvar, data, function (retorno) {
                    swal(
                        'Bem-vindo ao universo As Bentas!',
                        'Em breve você receberá conteúdos que ajudarão você a ter uma vida mais saudável \\o/',
                        'success'
                    );
                    self.campos.nome.val('');
                    self.campos.email.val('');
                }).fail(function () {
                    swal(
                        '',
                        'O correu um erro ao tentar realizar o cadastro.',
                        'error'
                    );
                });

            }


            return false;
        });

        window.Parsley.on('form:error', function () {
            swal(
                'Dados incorretos!',
                'Por favor, preencha os campos e-mail e CEP para vermos se é possível entregar na sua localidade!',
                'warning'
            );
        });


    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
