$(document).ready(function () {
    setTimeout(as_bentas_site_meus_pedidos_resumo.iniciar(), 100);
});


as_bentas_site_meus_pedidos_resumo = {

    campos: [],
    tabelas: [],
    botoes: [],

    iniciarElementos: function () {


    },

    iniciarAcoes: function () {

    	self = this;

        $('.resumo-pedido').on('click', '.btn-cancelar-pedido', function (e) {
            e.preventDefault();

            var urlExclusao = $(this).data('url-exclusao');
            var urlRedirect = $(this).data('url');
            
            swal({
                    title: "Deseja realmente remover este item do pedido?",
                    text: "Após a remoção desse item, ele será Cancelado!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                if (isConfirm){
                    bootbox.prompt({
                        title: "Que pena! Por favor, informe o motivo do cancelamento.",
                        className: "custom-bootbox",
                        inputType: 'textarea',
                        callback: function (result) {

                            $.post(urlExclusao, { 'justificativa' : result }, function( data ) {
                                window.location = window.location
                            }).fail(function(xhr, status, error) {
                               console.log(error)
                            })


                        }
                    });
                }
            });

        });

    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
