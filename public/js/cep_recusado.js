$(document).ready(function () {
    setTimeout(as_bentas_site_home.iniciar(), 100);
});


as_bentas_site_home = {

    campos: [],
    urls: [],
    botoes: [],
    formularioConferirLocal: null,

    iniciarElementos: function () {

        this.formularioConferirLocal = $('#frm-conferir-local');

        this.campos.email = $('#email');
        this.campos.cep = $('#cep');

        this.botoes.conferirLocal = $('#btn-conferir-local');

    },

    iniciarAcoes: function () {



    },


    iniciar: function () {
        this.iniciarElementos();
        this.iniciarAcoes();
    }


};
