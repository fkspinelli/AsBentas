$(document).ready(function() {

    setTimeout(as_bentas_site_carrinho.iniciar(), 1000);


});

as_bentas_site_carrinho = {

	pedidos: {},
	botoes: [],
	campos: [],
	formulario: null,
	url: null,
	urlAddItem: null,
	limparCarrinho: null,

	iniciarCampos: function () {
		this.botoes.carrinho = $('#btn-carrinho');
        this.botoes.limpar = $('.btn-limpar-carrinho');
        this.botoes.cupom = $('.ativarCupom')
		this.formulario = $('#form-carrinho');
		this.campos.pin = $('#pin');
        this.campos.redirect = $('#redirect');
        this.campos.idCardapio = $('#add-item-id-cardapio');
        this.campos.idAcompanhamento = $('#add-item-id-acompanhamento');
        this.campos.cupom = $('#cupom');
		this.url = this.formulario.data('url');
        this.urlAddItem = this.formulario.data('url-adicionar-item');

        $('html').trigger('click');

	},

	acoes: function () {
		var self = this;

		//Remover um dia/pedido do carrinho
        this.formulario.on('click', '.btn-remover-item-carrinho', function () {
            var urlRemoverItem = $(this).data('url');
            swal({
                    title: "Deseja realmente remover este pedido?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {

                    if (isConfirm) {
                        window.location = urlRemoverItem;
                    }
                }
			);

		});

        // Limpar o conteúdo do carrinho
        this.botoes.limpar.on('click', function () {

            var limparCarrinho = $(this).data('url-limpar-carrinho');

            swal({
                    title: "Deseja realmente limpar o carrinho de pedidos?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {

                    if (isConfirm) {
                        window.location = limparCarrinho;
                    }
                }
            );
		});

        //Adiciona o item selecionado ao carrinho, botões azuis!
        $('.div-acompanhamentos').on('click', '.add-item-carrinho', function (e) {
        	e.preventDefault();

        	//Atualiza o conteúdo do carrinho.
            var url = self.formulario.data('url-atualizar');

            //Pega o id do cardápio pedido
            var id = $(this).data('id');

            //Pega o id do acompanhamento em questão
            var idAcompanhamento = $(this).data('id-acompanhamento');

            //Redireciona para a página de carrinhos
            self.campos.redirect.val('carrinho.exibir');

            //Seta o id do cardápio com a variável que foi pega anteriormente
            self.campos.idCardapio.val(id);

            //Seta o id do acompanhamento com a variável que foi pega anteriormente
            self.campos.idAcompanhamento.val(idAcompanhamento);

            //Seta a url atualizar no formulário
            self.formulario.attr('action', url);

            //Envia o formulário pra rota carrinho.atualizar
            self.formulario.submit();


		});

        //Vai pra página de entrega
        $('.btn-alterar-entrega').on('click', function (e) {
            e.preventDefault();

            var url = $(this).data('url');

            window.location = url;
        });

        //Atualiza o conteúdo do carrinho e continua a compra
        $('.btn-continue').on('click', function (e) {
            e.preventDefault();

            var url = self.formulario.data('url-atualizar');

            self.campos.redirect.val('site.cardapio');
            self.formulario.attr('action', url);
            self.formulario.submit();


        });

        //Finaliza a compra
        this.botoes.carrinho.on('click', function (e) {
            event.preventDefault()
            swal({
                    title: "Tudo certo? Deseja realmente finalizar seu pedido?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Não",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if(isConfirm){
                        self.formulario.submit();
                    }

                }

            );
        });

        //Adiciona o cupom à compra
        this.botoes.cupom.on('click', function (e) {
            //Atualiza o conteúdo do carrinho.
            var url = self.formulario.data('url-atualizar');

            //Redireciona para a página de carrinhos
            self.campos.redirect.val('carrinho.exibir');

            //Seta a url atualizar no formulário
            self.formulario.attr('action', url);

            //Envia o formulário pra rota carrinho.atualizar
            self.formulario.submit();
        });

        this.campos.cupom.on('keypress', function (e) {
            if(e.which == 13) {

                //Atualiza o conteúdo do carrinho.
                var url = self.formulario.data('url-atualizar');

                //Redireciona para a página de carrinhos
                self.campos.redirect.val('carrinho.exibir');

                //Seta a url atualizar no formulário
                self.formulario.attr('action', url);

                //Envia o formulário pra rota carrinho.atualizar
                self.formulario.submit();
            }
        });

	},

	iniciar: function () {
		this.iniciarCampos();
		this.acoes();

	}

}




