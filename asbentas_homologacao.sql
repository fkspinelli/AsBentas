-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 107.180.46.160    Database: asbentasnovo
-- ------------------------------------------------------
-- Server version	5.6.36-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acompanhamentos`
--

DROP TABLE IF EXISTS `acompanhamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acompanhamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoria_id` int(10) unsigned NOT NULL,
  `valor_sugerido` decimal(8,2) NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acompanhamentos_categoria_id_foreign` (`categoria_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acompanhamentos`
--

LOCK TABLES `acompanhamentos` WRITE;
/*!40000 ALTER TABLE `acompanhamentos` DISABLE KEYS */;
INSERT INTO `acompanhamentos` VALUES (15,'Detox: Couve, Laranja, Hortelã e Gengibre',9,5.00,'ativo','2017-09-26 21:47:22','2017-09-26 21:47:22',NULL),(4,'Chips de Beterraba',8,10.00,'ativo','2017-09-07 00:26:44','2017-09-15 22:14:10',NULL),(23,'Detox: Couve, Hortelã e Maracujá',9,5.00,'ativo','2017-10-28 01:18:17','2017-10-28 01:18:17',NULL),(6,'Suco de Couve',9,5.00,'ativo','2017-09-15 23:35:23','2017-09-15 23:35:23',NULL),(7,'Suco de Maracujá',9,5.00,'ativo','2017-09-15 23:35:45','2017-09-15 23:35:45',NULL),(8,'Suco de Hortelã',9,5.00,'ativo','2017-09-15 23:36:02','2017-09-15 23:36:02',NULL),(9,'Mate',9,3.00,'ativo','2017-09-15 23:36:25','2017-09-15 23:36:25',NULL),(10,'Suco de Abacaxi com Hortelã',9,5.00,'ativo','2017-09-15 23:36:56','2017-09-15 23:36:56',NULL),(11,'Suchá de Ibisco',9,7.00,'ativo','2017-09-15 23:37:27','2017-09-15 23:37:27',NULL),(12,'Suchá de Baunilha',9,7.00,'ativo','2017-09-15 23:37:55','2017-09-15 23:37:55',NULL),(13,'Suchá de Morango',9,7.00,'ativo','2017-09-15 23:38:16','2017-09-15 23:38:16',NULL),(14,'Suco de Laranja com Acerola',9,5.00,'ativo','2017-09-15 23:38:41','2017-09-15 23:38:41',NULL),(16,'Suco de Manga',9,5.00,'ativo','2017-09-26 21:50:29','2017-09-26 21:50:29',NULL),(17,'Suchá de Abacaxi com Chá Verde',9,5.00,'ativo','2017-09-26 21:52:33','2017-09-26 21:52:33',NULL),(18,'Suco de Laranja',9,5.00,'ativo','2017-09-26 21:54:47','2017-09-26 21:54:47',NULL),(19,'Detox: couve, laranja, hortelã e maçã',9,5.00,'ativo','2017-10-17 21:23:00','2017-10-17 21:23:00',NULL),(20,'Abacaxi com água de coco',9,5.00,'ativo','2017-10-17 21:23:29','2017-10-17 21:23:29',NULL),(21,'Suchá: maracujá com camomila',9,5.00,'ativo','2017-10-17 21:24:01','2017-10-17 21:24:01',NULL),(22,'Suco de Morango com Água de Coco',9,5.00,'ativo','2017-10-20 23:50:57','2017-10-20 23:50:57',NULL),(24,'Suchá: Mate Cítrico (laranja e limão)',9,5.00,'ativo','2017-10-28 01:28:27','2017-10-28 01:28:27',NULL);
/*!40000 ALTER TABLE `acompanhamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Administrador','admin@asbentas.com','$2y$10$Neh6sZBZSZYpAUsZbnks0.mJBl/ynhNIWNRuuol0MIIkfCFzcNZj.','6CYUfPHblERCN7YqQoMAEgMpiOXGBzuNAdONxTTzAYBP0aP7qaSuDrBSExsh','2017-09-06 20:29:16','2017-09-06 20:29:16'),(2,'Juliana Diniz','juliana@dizain.com.br','$2y$10$PDpodaWiQ6vxuT91S4pZkeH0MERn/N9lVauOPSa8C/S.QftPIVDhi','VdJXrMxY6uvi8KpNi9tSFSsu6joeqOPPCQWCiMLJxk9RnWSIB9ZQWGlrt36O','2017-09-07 05:34:52','2017-09-07 05:35:03'),(3,'Angelo','angelo@asbentas.com','$2y$10$CUD.y.BGw5qPUDXtrQjUb.3bbpTFwUdfU4aRGBIVH2qR1i0uT2ZpW','BWhIjNQidjgdSzwI47bjcEIh3R0UWLVuMJcNhdCdS3rvwANIGvJ50Od91OzT','2017-09-13 05:28:38','2017-09-13 05:28:38');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caracteristica_pratos`
--

DROP TABLE IF EXISTS `caracteristica_pratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caracteristica_pratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caracteristica_pratos`
--

LOCK TABLES `caracteristica_pratos` WRITE;
/*!40000 ALTER TABLE `caracteristica_pratos` DISABLE KEYS */;
INSERT INTO `caracteristica_pratos` VALUES (1,'GLUTEN FREE','ativo','2017-09-06 20:29:16','2017-09-06 20:29:16'),(2,'LAC FREE','ativo','2017-09-06 20:29:16','2017-09-06 20:29:16'),(3,'VEGGIE','ativo','2017-09-06 20:29:16','2017-09-06 20:29:16'),(4,'LOW CARB','ativo','2017-09-07 03:29:16','2017-09-07 03:29:16'),(5,'VEGANO','ativo','2017-09-07 03:29:16','2017-09-07 03:29:16');
/*!40000 ALTER TABLE `caracteristica_pratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardapio_bebidas`
--

DROP TABLE IF EXISTS `cardapio_bebidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardapio_bebidas` (
  `acompanhamento_id` int(10) unsigned NOT NULL,
  `cardapio_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acompanhamento_id`,`cardapio_id`),
  KEY `fk_cardapio_bebidas_cardapio` (`cardapio_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardapio_bebidas`
--

LOCK TABLES `cardapio_bebidas` WRITE;
/*!40000 ALTER TABLE `cardapio_bebidas` DISABLE KEYS */;
INSERT INTO `cardapio_bebidas` VALUES (2,11,'2017-09-18 19:40:38','2017-09-18 19:40:38'),(10,11,'2017-09-18 19:43:31','2017-09-18 19:43:31'),(6,10,'2017-09-20 06:44:18','2017-09-20 06:44:18'),(8,10,'2017-09-20 06:44:18','2017-09-20 06:44:18'),(15,12,'2017-09-26 21:49:27','2017-09-26 21:49:27'),(9,12,'2017-09-26 21:49:27','2017-09-26 21:49:27'),(16,13,'2017-09-26 21:51:17','2017-09-26 21:51:17'),(9,13,'2017-09-26 21:51:17','2017-09-26 21:51:17'),(17,14,'2017-09-26 21:52:53','2017-09-26 21:52:53'),(9,14,'2017-09-26 21:52:53','2017-09-26 21:52:53'),(7,15,'2017-09-26 21:53:36','2017-09-26 21:53:36'),(9,15,'2017-09-26 21:53:36','2017-09-26 21:53:36'),(18,16,'2017-09-26 21:55:02','2017-09-26 21:55:02'),(9,16,'2017-09-26 21:55:02','2017-09-26 21:55:02'),(1,17,'2017-10-05 00:36:17','2017-10-05 00:36:17'),(19,18,'2017-10-17 21:26:12','2017-10-17 21:26:12'),(2,18,'2017-10-17 21:26:12','2017-10-17 21:26:12'),(20,19,'2017-10-17 21:27:17','2017-10-17 21:27:17'),(2,19,'2017-10-17 21:27:17','2017-10-17 21:27:17'),(21,20,'2017-10-17 21:28:30','2017-10-17 21:28:30'),(2,20,'2017-10-17 21:28:30','2017-10-17 21:28:30'),(18,21,'2017-10-17 21:29:18','2017-10-17 21:29:18'),(2,21,'2017-10-17 21:29:18','2017-10-17 21:29:18'),(1,22,'2017-10-17 21:30:17','2017-10-17 21:30:17'),(2,22,'2017-10-17 21:30:17','2017-10-17 21:30:17'),(15,23,'2017-10-20 23:54:07','2017-10-20 23:54:07'),(2,23,'2017-10-20 23:54:07','2017-10-20 23:54:07'),(7,24,'2017-10-20 23:55:05','2017-10-20 23:55:05'),(2,24,'2017-10-20 23:55:05','2017-10-20 23:55:05'),(17,25,'2017-10-20 23:55:57','2017-10-20 23:55:57'),(2,25,'2017-10-20 23:55:57','2017-10-20 23:55:57'),(16,26,'2017-10-20 23:56:36','2017-10-20 23:56:36'),(2,26,'2017-10-20 23:56:36','2017-10-20 23:56:36'),(22,27,'2017-10-20 23:57:15','2017-10-20 23:57:15'),(2,27,'2017-10-20 23:57:15','2017-10-20 23:57:15'),(1,28,'2017-10-21 01:03:32','2017-10-21 01:03:32'),(2,28,'2017-10-21 01:03:32','2017-10-21 01:03:32'),(15,28,'2017-10-21 01:03:32','2017-10-21 01:03:32'),(23,29,'2017-10-28 01:30:24','2017-10-28 01:30:24'),(2,29,'2017-10-28 01:30:24','2017-10-28 01:30:24'),(2,30,'2017-10-28 01:43:00','2017-10-28 01:43:00'),(10,30,'2017-10-28 01:43:00','2017-10-28 01:43:00'),(2,31,'2017-10-30 19:07:38','2017-10-30 19:07:38'),(24,31,'2017-10-30 19:07:38','2017-10-30 19:07:38'),(2,2,'2017-10-31 21:28:40','2017-10-31 21:28:40'),(1,3,'2017-10-31 21:29:05','2017-10-31 21:29:05'),(1,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(2,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(15,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(1,5,'2017-10-31 21:57:38','2017-10-31 21:57:38'),(2,5,'2017-10-31 21:57:38','2017-10-31 21:57:38'),(2,14,'2017-11-01 00:37:28','2017-11-01 00:37:28'),(14,14,'2017-11-01 00:37:28','2017-11-01 00:37:28'),(1,6,'2017-10-31 23:29:14','2017-10-31 23:29:14'),(2,6,'2017-10-31 23:29:14','2017-10-31 23:29:14'),(14,15,'2017-11-01 00:41:03','2017-11-01 00:41:03'),(23,7,'2017-10-31 23:50:12','2017-10-31 23:50:12'),(2,7,'2017-10-31 23:50:12','2017-10-31 23:50:12'),(10,8,'2017-10-31 23:51:10','2017-10-31 23:51:10'),(2,8,'2017-10-31 23:51:10','2017-10-31 23:51:10'),(24,9,'2017-10-31 23:53:21','2017-10-31 23:53:21'),(15,26,'2017-11-10 17:10:01','2017-11-10 17:10:01'),(1,10,'2017-10-31 23:54:00','2017-10-31 23:54:00'),(2,10,'2017-10-31 23:54:00','2017-10-31 23:54:00'),(14,11,'2017-10-31 23:54:35','2017-10-31 23:54:35'),(14,12,'2017-10-31 23:55:27','2017-10-31 23:55:27'),(2,12,'2017-10-31 23:55:27','2017-10-31 23:55:27'),(15,13,'2017-11-01 00:28:57','2017-11-01 00:28:57'),(2,13,'2017-11-01 00:28:57','2017-11-01 00:28:57'),(2,15,'2017-11-01 00:41:03','2017-11-01 00:41:03'),(1,16,'2017-11-07 00:06:44','2017-11-07 00:06:44'),(2,16,'2017-11-07 00:06:44','2017-11-07 00:06:44'),(1,18,'2017-11-07 00:07:33','2017-11-07 00:07:33'),(1,19,'2017-11-07 22:43:31','2017-11-07 22:43:31'),(15,20,'2017-11-07 22:43:57','2017-11-07 22:43:57'),(23,20,'2017-11-07 22:43:57','2017-11-07 22:43:57'),(1,21,'2017-11-07 22:44:20','2017-11-07 22:44:20'),(6,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(7,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(8,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(1,23,'2017-11-09 18:14:09','2017-11-09 18:14:09'),(15,24,'2017-11-09 18:14:18','2017-11-09 18:14:18'),(6,25,'2017-11-09 18:14:26','2017-11-09 18:14:26'),(7,25,'2017-11-09 18:14:26','2017-11-09 18:14:26'),(23,27,'2017-11-10 17:10:23','2017-11-10 17:10:23'),(18,28,'2017-11-10 17:10:44','2017-11-10 17:10:44'),(15,29,'2017-11-10 17:11:03','2017-11-10 17:11:03'),(9,29,'2017-11-10 17:11:03','2017-11-10 17:11:03'),(8,30,'2017-11-10 17:11:20','2017-11-10 17:11:20'),(15,31,'2017-11-10 18:56:03','2017-11-10 18:56:03'),(23,31,'2017-11-10 18:56:03','2017-11-10 18:56:03');
/*!40000 ALTER TABLE `cardapio_bebidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardapio_prato`
--

DROP TABLE IF EXISTS `cardapio_prato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardapio_prato` (
  `prato_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cardapio_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prato_id`,`cardapio_id`),
  KEY `cardapio_prato_cardapio_id_foreign` (`cardapio_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardapio_prato`
--

LOCK TABLES `cardapio_prato` WRITE;
/*!40000 ALTER TABLE `cardapio_prato` DISABLE KEYS */;
INSERT INTO `cardapio_prato` VALUES (1,2,'2017-10-31 21:28:40','2017-10-31 21:28:40'),(1,3,'2017-10-31 21:29:05','2017-10-31 21:29:05'),(1,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(2,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(3,4,'2017-10-31 21:57:06','2017-10-31 21:57:06'),(4,5,'2017-10-31 21:57:38','2017-10-31 21:57:38'),(5,5,'2017-10-31 21:57:38','2017-10-31 21:57:38'),(6,5,'2017-10-31 21:57:38','2017-10-31 21:57:38'),(1,6,'2017-10-31 23:29:14','2017-10-31 23:29:14'),(2,6,'2017-10-31 23:29:14','2017-10-31 23:29:14'),(3,6,'2017-10-31 23:29:14','2017-10-31 23:29:14'),(60,7,'2017-10-31 23:50:12','2017-10-31 23:50:12'),(61,7,'2017-10-31 23:50:12','2017-10-31 23:50:12'),(26,7,'2017-10-31 23:50:12','2017-10-31 23:50:12'),(7,8,'2017-10-31 23:51:10','2017-10-31 23:51:10'),(62,8,'2017-10-31 23:51:10','2017-10-31 23:51:10'),(63,8,'2017-10-31 23:51:10','2017-10-31 23:51:10'),(64,9,'2017-10-31 23:53:21','2017-10-31 23:53:21'),(31,9,'2017-10-31 23:53:21','2017-10-31 23:53:21'),(38,9,'2017-10-31 23:53:21','2017-10-31 23:53:21'),(65,10,'2017-10-31 23:54:00','2017-10-31 23:54:00'),(28,10,'2017-10-31 23:54:00','2017-10-31 23:54:00'),(3,10,'2017-10-31 23:54:00','2017-10-31 23:54:00'),(30,11,'2017-10-31 23:54:35','2017-10-31 23:54:35'),(25,11,'2017-10-31 23:54:35','2017-10-31 23:54:35'),(23,11,'2017-10-31 23:54:35','2017-10-31 23:54:35'),(30,12,'2017-10-31 23:55:27','2017-10-31 23:55:27'),(25,12,'2017-10-31 23:55:27','2017-10-31 23:55:27'),(23,12,'2017-10-31 23:55:27','2017-10-31 23:55:27'),(2,13,'2017-11-01 00:28:57','2017-11-01 00:28:57'),(1,13,'2017-11-01 00:28:57','2017-11-01 00:28:57'),(5,13,'2017-11-01 00:28:57','2017-11-01 00:28:57'),(65,14,'2017-11-01 00:37:28','2017-11-01 00:37:28'),(28,14,'2017-11-01 00:37:28','2017-11-01 00:37:28'),(23,14,'2017-11-01 00:37:28','2017-11-01 00:37:28'),(30,15,'2017-11-01 00:41:03','2017-11-01 00:41:03'),(25,15,'2017-11-01 00:41:03','2017-11-01 00:41:03'),(23,15,'2017-11-01 00:41:03','2017-11-01 00:41:03'),(1,16,'2017-11-07 00:06:44','2017-11-07 00:06:44'),(2,16,'2017-11-07 00:06:44','2017-11-07 00:06:44'),(3,16,'2017-11-07 00:06:44','2017-11-07 00:06:44'),(1,17,'2017-11-07 00:07:28','2017-11-07 00:07:28'),(2,17,'2017-11-07 00:07:28','2017-11-07 00:07:28'),(3,17,'2017-11-07 00:07:28','2017-11-07 00:07:28'),(1,18,'2017-11-07 00:07:33','2017-11-07 00:07:33'),(2,18,'2017-11-07 00:07:33','2017-11-07 00:07:33'),(3,18,'2017-11-07 00:07:33','2017-11-07 00:07:33'),(65,19,'2017-11-07 22:43:31','2017-11-07 22:43:31'),(64,19,'2017-11-07 22:43:31','2017-11-07 22:43:31'),(63,19,'2017-11-07 22:43:31','2017-11-07 22:43:31'),(65,20,'2017-11-07 22:43:57','2017-11-07 22:43:57'),(64,20,'2017-11-07 22:43:57','2017-11-07 22:43:57'),(63,20,'2017-11-07 22:43:57','2017-11-07 22:43:57'),(63,21,'2017-11-07 22:44:20','2017-11-07 22:44:20'),(64,21,'2017-11-07 22:44:20','2017-11-07 22:44:20'),(65,21,'2017-11-07 22:44:20','2017-11-07 22:44:20'),(65,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(64,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(63,22,'2017-11-07 22:45:14','2017-11-07 22:45:14'),(65,23,'2017-11-09 18:14:09','2017-11-09 18:14:09'),(63,23,'2017-11-09 18:14:09','2017-11-09 18:14:09'),(61,23,'2017-11-09 18:14:09','2017-11-09 18:14:09'),(61,9,'2017-11-09 23:25:39','2017-11-09 23:25:39'),(65,26,'2017-11-10 17:10:01','2017-11-10 17:10:01'),(64,26,'2017-11-10 17:10:01','2017-11-10 17:10:01'),(63,26,'2017-11-10 17:10:01','2017-11-10 17:10:01'),(64,27,'2017-11-10 17:10:23','2017-11-10 17:10:23'),(61,27,'2017-11-10 17:10:23','2017-11-10 17:10:23'),(57,27,'2017-11-10 17:10:23','2017-11-10 17:10:23'),(65,28,'2017-11-10 17:10:44','2017-11-10 17:10:44'),(62,28,'2017-11-10 17:10:44','2017-11-10 17:10:44'),(65,29,'2017-11-10 17:11:03','2017-11-10 17:11:03'),(57,29,'2017-11-10 17:11:03','2017-11-10 17:11:03'),(56,29,'2017-11-10 17:11:03','2017-11-10 17:11:03'),(64,30,'2017-11-10 17:11:20','2017-11-10 17:11:20'),(63,30,'2017-11-10 17:11:20','2017-11-10 17:11:20'),(65,31,'2017-11-10 18:56:03','2017-11-10 18:56:03'),(64,31,'2017-11-10 18:56:03','2017-11-10 18:56:03'),(63,31,'2017-11-10 18:56:03','2017-11-10 18:56:03');
/*!40000 ALTER TABLE `cardapio_prato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardapios`
--

DROP TABLE IF EXISTS `cardapios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardapios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_cardapio` date NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardapios`
--

LOCK TABLES `cardapios` WRITE;
/*!40000 ALTER TABLE `cardapios` DISABLE KEYS */;
INSERT INTO `cardapios` VALUES (5,'2017-02-11','ativo','2017-10-31 21:57:38','2017-11-01 00:35:49'),(14,'2017-11-02','ativo','2017-11-01 00:37:28','2017-11-01 00:37:28'),(6,'2017-03-11','ativo','2017-10-31 23:29:14','2017-11-01 00:38:41'),(7,'2017-10-30','ativo','2017-10-31 23:50:12','2017-10-31 23:50:12'),(8,'2017-10-31','ativo','2017-10-31 23:51:10','2017-10-31 23:51:10'),(9,'2017-01-11','ativo','2017-10-31 23:53:21','2017-11-09 23:25:39'),(19,'2017-11-08','ativo','2017-11-07 22:43:31','2017-11-07 22:43:31'),(16,'2017-12-07','ativo','2017-11-07 00:06:44','2017-11-07 00:06:44'),(15,'2017-11-03','ativo','2017-11-01 00:41:03','2017-11-01 00:41:03'),(13,'2017-11-06','ativo','2017-11-01 00:28:57','2017-11-01 00:28:57'),(18,'2017-11-07','ativo','2017-11-07 00:07:33','2017-11-07 00:07:33'),(23,'2017-11-10','ativo','2017-11-09 18:14:09','2017-11-09 18:14:09'),(22,'2017-11-09','ativo','2017-11-07 22:45:14','2017-11-07 22:45:14'),(24,'2017-11-10','ativo','2017-11-09 18:14:18','2017-11-09 18:14:18'),(25,'2017-11-10','ativo','2017-11-09 18:14:26','2017-11-09 18:14:26'),(26,'2017-11-13','ativo','2017-11-10 17:10:01','2017-11-10 17:10:01'),(27,'2017-11-14','ativo','2017-11-10 17:10:23','2017-11-10 17:10:23'),(28,'2017-11-15','ativo','2017-11-10 17:10:44','2017-11-10 17:10:44'),(29,'2017-11-16','ativo','2017-11-10 17:11:03','2017-11-10 17:11:03'),(30,'2017-11-17','ativo','2017-11-10 17:11:20','2017-11-10 17:11:20');
/*!40000 ALTER TABLE `cardapios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_acompanhamentos`
--

DROP TABLE IF EXISTS `categoria_acompanhamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_acompanhamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_acompanhamentos`
--

LOCK TABLES `categoria_acompanhamentos` WRITE;
/*!40000 ALTER TABLE `categoria_acompanhamentos` DISABLE KEYS */;
INSERT INTO `categoria_acompanhamentos` VALUES (8,'Lanches','ativo','2017-09-15 22:09:41','2017-09-15 22:09:41'),(9,'Bebidas','ativo','2017-09-15 22:13:26','2017-09-15 22:13:26'),(7,'Sobremesas','ativo','2017-09-15 04:37:07','2017-09-15 22:09:30');
/*!40000 ALTER TABLE `categoria_acompanhamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuracaos`
--

DROP TABLE IF EXISTS `configuracaos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracaos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grupo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chave` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuracaos`
--

LOCK TABLES `configuracaos` WRITE;
/*!40000 ALTER TABLE `configuracaos` DISABLE KEYS */;
INSERT INTO `configuracaos` VALUES (1,'Entrega','qtd_hora_cancelamento','1','Qtd horas para cancelamento','2017-01-01 07:00:00','2017-11-03 08:24:17'),(2,'Entrega','horario_entrega','12:00 às 14hrs','Horário de Entrega','2017-01-01 07:00:00',NULL),(3,'Newsletter','email_contato','juliana@dizain.com.br','E-mail para receber Contato','2017-01-01 07:00:00',NULL),(4,'','hora_proxima_semana','7','Horário para exibir próxima semana ','2017-01-01 07:00:00','2017-11-10 17:09:11');
/*!40000 ALTER TABLE `configuracaos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta_ceps`
--

DROP TABLE IF EXISTS `consulta_ceps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consulta_ceps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta_ceps`
--

LOCK TABLES `consulta_ceps` WRITE;
/*!40000 ALTER TABLE `consulta_ceps` DISABLE KEYS */;
INSERT INTO `consulta_ceps` VALUES (1,'marcio@marciomoreira.net','22611030','2017-10-20 18:00:46','2017-10-20 18:00:46'),(2,'juliana@dizain.com.br','22790190','2017-10-20 20:15:31','2017-10-20 20:15:31'),(3,'juliediniz@gmail.com','22421010','2017-10-21 00:08:06','2017-10-21 00:08:06'),(4,'wagner@dizain.com.br','22430060','2017-10-21 00:12:08','2017-10-21 00:12:08'),(5,'wagner@dizain.com.br','22631250','2017-10-21 00:13:08','2017-10-21 00:13:08'),(6,'marcio@marciomoreira.net','27336620','2017-10-21 01:07:07','2017-10-21 01:07:07'),(7,'marcio@moreiee.net','27259222','2017-10-21 01:26:29','2017-10-21 01:26:29'),(8,'marcio@morieee.com','27336620','2017-10-21 01:26:55','2017-10-21 01:26:55'),(9,'marciomo@reira.com','22611030','2017-10-21 01:27:25','2017-10-21 01:27:25'),(10,'iurynovaes@id.uff.br','27288020','2017-10-24 00:01:34','2017-10-24 00:01:34'),(11,'juliana@dizain.com.br','22430060','2017-10-25 02:01:55','2017-10-25 02:01:55'),(12,'fdiniz@gmail.com','22430060','2017-10-25 02:21:53','2017-10-25 02:21:53'),(13,'rewanderley@gmail.com','20091020','2017-10-27 01:29:15','2017-10-27 01:29:15'),(14,'rewanderley@gmail.com','22290160','2017-10-27 01:30:56','2017-10-27 01:30:56'),(15,'marcio@moreira.net','11111111','2017-10-29 22:01:05','2017-10-29 22:01:05'),(16,'amazzocc@pobox.com','22460030','2017-10-30 19:30:07','2017-10-30 19:30:07'),(17,'marcio@marciomoreira.net','11111111','2017-10-31 22:19:05','2017-10-31 22:19:05'),(18,'marcio@moreira.com','36363636','2017-11-02 23:11:30','2017-11-02 23:11:30'),(19,'marcio@moreira.com','32323232','2017-11-02 23:12:17','2017-11-02 23:12:17'),(20,'fdiniz@gmail.com','20766340','2017-11-08 04:59:45','2017-11-08 04:59:45'),(21,'jlima@dizain.com.br','25085008','2017-11-10 02:24:20','2017-11-10 02:24:20');
/*!40000 ALTER TABLE `consulta_ceps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cupom_pedidos`
--

DROP TABLE IF EXISTS `cupom_pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cupom_pedidos` (
  `cupom_id` int(10) unsigned NOT NULL,
  `pedido_id` int(10) unsigned NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cupom_id`,`pedido_id`,`usuario_id`),
  KEY `cupom_pedidos_cupom_id_index` (`cupom_id`),
  KEY `cupom_pedidos_pedido_id_index` (`pedido_id`),
  KEY `cupom_pedidos_usuario_id_index` (`usuario_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cupom_pedidos`
--

LOCK TABLES `cupom_pedidos` WRITE;
/*!40000 ALTER TABLE `cupom_pedidos` DISABLE KEYS */;
INSERT INTO `cupom_pedidos` VALUES (2,17,19,'2017-11-07 22:49:13','2017-11-07 22:49:13'),(3,18,19,'2017-11-07 22:51:28','2017-11-07 22:51:28'),(1,19,19,'2017-11-07 22:53:15','2017-11-07 22:53:15'),(4,20,19,'2017-11-07 22:58:15','2017-11-07 22:58:15'),(8,21,19,'2017-11-07 23:12:16','2017-11-07 23:12:16'),(9,22,19,'2017-11-07 23:13:15','2017-11-07 23:13:15'),(10,23,19,'2017-11-07 23:46:52','2017-11-07 23:46:52');
/*!40000 ALTER TABLE `cupom_pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cupons`
--

DROP TABLE IF EXISTS `cupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` enum('%','$') COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor` double(8,2) NOT NULL,
  `data_expiracao` datetime DEFAULT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo_cupom` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cupons`
--

LOCK TABLES `cupons` WRITE;
/*!40000 ALTER TABLE `cupons` DISABLE KEYS */;
INSERT INTO `cupons` VALUES (11,'TEsteCupom','%',1000.00,'2017-11-11 16:43:18','ativo','2017-11-09 23:43:18','2017-11-09 23:43:18',0),(10,'Test','%',10.00,'2017-11-11 16:46:28','ativo','2017-11-07 23:46:28','2017-11-07 23:46:28',0),(12,'BENTAS20','$',2000.00,'2017-12-11 20:52:41','ativo','2017-11-10 03:47:55','2017-11-10 03:52:41',0);
/*!40000 ALTER TABLE `cupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desconto_progressivos`
--

DROP TABLE IF EXISTS `desconto_progressivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desconto_progressivos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dia` int(11) NOT NULL,
  `desconto` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desconto_progressivos`
--

LOCK TABLES `desconto_progressivos` WRITE;
/*!40000 ALTER TABLE `desconto_progressivos` DISABLE KEYS */;
INSERT INTO `desconto_progressivos` VALUES (1,1,0.00,'2017-11-09 09:13:29','2017-11-09 09:13:29'),(2,2,10.00,'2017-11-09 09:13:37','2017-11-09 09:13:37'),(3,3,15.00,'2017-11-09 09:13:47','2017-11-09 09:13:47'),(4,4,20.00,'2017-11-09 09:15:04','2017-11-09 09:15:04'),(5,5,20.00,'2017-11-09 09:15:14','2017-11-09 09:15:14');
/*!40000 ALTER TABLE `desconto_progressivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnpj` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contato` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_atuacao` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ativo','inativo','pre') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cep` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (1,'EMPRESA TESTE','123456789000','CONTATO','24-992590000','email@teste.com','TI','ativo','2017-09-06 20:29:16','2017-09-06 20:29:16',NULL),(2,'DIZAIN Presença Digital','01.482.242/0001-21','Juliana Diniz','(21) 3153-1780','juliana@dizain.com.br','Serviços de Internet','inativo','2017-09-07 05:00:49','2017-11-01 00:56:21',NULL),(3,'HSTERN','02.091.365/0001-02','Felipe Diniz','(21) 3153-1780','juliana@dizain.com.br','Mercado de Jóias','inativo','2017-09-16 02:48:08','2017-11-01 00:49:13','25085008'),(4,'Souza Cruz',NULL,'Juliana','21 3153-1780','juliana@dizain.com.br',NULL,'pre','2017-09-18 19:23:53','2017-09-18 19:23:53','25085008'),(5,'SOUZA CRUZ','33.009.911/0001-39','Renata Lima','(45) 45435-4354','juliana@dizain.com','Tabaco','ativo','2017-09-20 06:37:14','2017-09-20 06:37:14',NULL),(7,'BELEZA NATURAL','02.546.009/0001-28','Juliana Diniz','(12) 12121-2111','juliana@dizain.com.br','Beleza','ativo','2017-10-25 04:17:01','2017-10-25 04:17:01',NULL),(8,'ZC2',NULL,'eu','21 98112-6580','amazzocc@pobox.com',NULL,'ativo','2017-11-03 07:53:58','2017-11-03 07:55:38','22460030');
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enderecos`
--

DROP TABLE IF EXISTS `enderecos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enderecos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logradouro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complemento` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bairro` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cidade` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `empresa_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `usuario_id` int(10) unsigned DEFAULT NULL,
  `ddd` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefone` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `endereco_empresas_empresa_id_foreign` (`empresa_id`),
  KEY `fk_endereco_usuario` (`usuario_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enderecos`
--

LOCK TABLES `enderecos` WRITE;
/*!40000 ALTER TABLE `enderecos` DISABLE KEYS */;
INSERT INTO `enderecos` VALUES (1,'FILIAL LEBLON','22430-060','Afrânio de Melo Franco, 325',NULL,'Leblon','Rio de Janeiro','RJ','ativo',2,'2017-09-07 05:10:14','2017-09-07 05:10:32',NULL,NULL,NULL,NULL,NULL,NULL,0),(2,'IPANEMA','22421-010','Rua Garcia DAvila, 240',NULL,'Ipanema','Rio de Janeiro','RJ','ativo',3,'2017-09-16 03:02:56','2017-10-24 20:29:35','2017-10-24 20:29:35',NULL,NULL,NULL,NULL,NULL,0),(3,'BARRA DA TIJUCA','22640-102','Av. das Américas, 4666',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',3,'2017-09-16 03:10:30','2017-09-16 03:10:30',NULL,NULL,NULL,NULL,NULL,NULL,0),(4,'MINHA CASA','22790-190','Praça Miguel Osório','Bloco 2A','Recreio dos Bandeirantes','São Paulo','SP','ativo',NULL,'2017-10-17 21:39:53','2017-11-10 04:03:15',NULL,11,'21','3042-1429',NULL,'200',1),(5,'OUTRA RESIDÊNCIA','22420-041','Rua Prudente de Morais',NULL,'Ipanema','Rio de Janeiro','RJ','ativo',NULL,'2017-10-17 21:49:29','2017-10-25 03:08:18','2017-10-25 03:08:18',11,NULL,NULL,NULL,'200',0),(6,'Login-In','22631-250','Rua Dalcidio Jurandir',NULL,NULL,NULL,NULL,'ativo',6,'2017-10-18 02:00:24','2017-10-18 02:00:24',NULL,NULL,NULL,NULL,NULL,'255',0),(7,'Avenida Um','27336-620','Rua João Xavier Itaboraí',NULL,'Boa Vista II','Volta redonda','RJ','ativo',NULL,'2017-10-19 01:39:24','2017-10-21 01:07:33','2017-10-21 01:07:33',19,'24','2499-99999','teste@teste.com','785',0),(8,'Casa Márcio','22793-903','Avenida das Américas',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-10-21 01:08:30','2017-10-31 22:32:25',NULL,19,'24','9999-99999',NULL,'555',1),(9,'DIZAIN','22631-250','Rua Dalcidio Jurandir','239','Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-10-25 02:27:26','2017-11-10 01:45:51',NULL,11,'21','3153-1780',NULL,'255',0),(10,'RIO DESIGN BARRA','22793-081','Avenida das Américas',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-10-25 02:57:59','2017-11-10 01:46:04','2017-11-10 01:46:04',11,NULL,NULL,NULL,'7777',0),(11,'VILA DO MAR','22790-190','Praça Miguel Osório','202','Recreio dos Bandeirantes','Rio de Janeiro','RJ','ativo',NULL,'2017-10-25 03:25:03','2017-10-25 03:25:03',NULL,22,'21','3042-1429',NULL,'120',0),(12,'Casa','22460-030','Rua Pacheco Leão','13','Jardim Botânico','Rio de Janeiro','RJ','ativo',NULL,'2017-10-30 20:13:12','2017-10-30 20:50:03',NULL,14,'21','98112-6580',NULL,'674',0),(13,'Casa Márcio 2','22793-903','Avenida das Américas',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-10-31 22:32:06','2017-10-31 22:32:25','2017-10-31 22:32:25',19,'24','9999-99999',NULL,'555',1),(14,'Márcio house 2','22793-903','Avenida das Américas',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-10-31 22:32:59','2017-11-09 08:47:45',NULL,19,'24','99999-9999',NULL,'555',0),(15,'Rua Dalcidio Jurandir','22631-250','Rua Dalcidio Jurandir','Sala 239','Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-11-01 01:00:35','2017-11-01 01:00:35',NULL,29,'21','3153-1780',NULL,'255',0),(16,'Casa','22460-030','Rua Pacheco Leão','14','Jardim Botânico','Rio de Janeiro','RJ','ativo',NULL,'2017-11-01 04:16:25','2017-11-10 18:25:16',NULL,15,'21','98112-6580',NULL,'674',0),(17,'Mamãe','22460-030','Rua Pacheco Leão','1','Jardim Botanico','Rio de Janeiro','RJ','ativo',NULL,'2017-11-01 05:51:43','2017-11-10 18:24:35','2017-11-10 18:24:35',15,'21','2267-4857',NULL,'506',0),(18,'Test novo endereçp cadastrado on the fly','22460-030','Rua Pacheco Leão','221','Jardim Botânico','Rio de Janeiro','RJ','ativo',NULL,'2017-11-01 07:40:08','2017-11-10 18:24:30','2017-11-10 18:24:30',15,NULL,NULL,NULL,'18',0),(19,'Candelaria','22460-030','Rua da Candelaria 1',NULL,'Centro','Rio de Janeiro','RJ','ativo',5,'2017-11-03 07:52:36','2017-11-03 07:52:36',NULL,NULL,NULL,NULL,NULL,NULL,0),(20,'ZC2','22460-030','Rua Pacheco Leão',NULL,NULL,NULL,NULL,'ativo',8,'2017-11-03 07:53:58','2017-11-03 08:10:24','2017-11-03 08:10:24',30,NULL,NULL,NULL,'674',0),(21,'ZC2','22460-030','sdçgfasoçgfsaj',NULL,NULL,NULL,NULL,'ativo',8,'2017-11-03 08:15:03','2017-11-03 08:15:39','2017-11-03 08:15:39',30,NULL,NULL,NULL,'324',0),(22,'asdpfoj\'','22460-030','Rua Pacheco Leãosdf','12','jb','rj','rj','ativo',NULL,'2017-11-03 08:17:30','2017-11-03 08:23:23',NULL,30,'21','8888-8888',NULL,'12',0),(23,'ZC2 HDQ','22460-030','Rua Pacheco Leão 674 casa 13',NULL,'JB','RJ','RJ','ativo',8,'2017-11-03 08:20:41','2017-11-03 08:23:23',NULL,NULL,NULL,NULL,NULL,NULL,1),(24,'Casa Márcio 2','22793-903','Avenida das Américas',NULL,'Barra da Tijuca','Rio de Janeiro','RJ','ativo',NULL,'2017-11-09 23:06:48','2017-11-09 23:06:48',NULL,19,'24','3349-0362',NULL,'785',0),(25,'Praça Miguel Osório','22790-190','Praça Miguel Osório','Bloco 2A',NULL,'Rio de Janeiro','RJ','ativo',NULL,'2017-11-10 01:47:38','2017-11-10 01:47:43','2017-11-10 01:47:43',11,'21','3042-1429',NULL,'200',0),(26,'Mamãe','22460-030','Rua Pacheco Leão','13','JB','Rio de Janeiro','RJ','ativo',NULL,'2017-11-10 18:26:05','2017-11-10 18:26:05',NULL,15,'21','2267-4857',NULL,'674',0),(27,'Vó','22460-030','Rua Pacheco Leão','156','Jardim Botânico','Rio de Janeiro','RJ','ativo',NULL,'2017-11-10 18:27:29','2017-11-10 18:27:29',NULL,15,NULL,NULL,NULL,'128',0),(28,'test','22460-030','Rua Pacheco Leão',NULL,NULL,NULL,NULL,'ativo',NULL,'2017-11-10 18:29:00','2017-11-10 18:29:00',NULL,15,NULL,NULL,NULL,'15',0);
/*!40000 ALTER TABLE `enderecos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pergunta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resposta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (1,'Posso fazer pedidos para a semana inteira?','Sim, você pode colocar os pedidos para todos os dias. Nossas entregas não acontecem de uma única vez. Fazemos entregas diárias para garantir a comida fresca todos os dias',3,'ativo','2017-09-15 10:48:16','2017-09-15 21:45:19'),(2,'Até que horas podemos colocar os pedidos?','Os pedidos devem ser feitos até as 22h do dia anterior ao que deseja consumir o prato.',2,'ativo','2017-09-15 21:29:11','2017-09-15 21:29:11'),(3,'Como faço para cancelar um pedido?','Acesse a página de Meus Dados para poder fazer o cancelamento. Aceitamos cancelamentos até às XX horas do dia do pedido.',1,'ativo','2017-09-15 21:44:54','2017-09-15 21:46:36');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario_entregas`
--

DROP TABLE IF EXISTS `horario_entregas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horario_entregas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `horario_inicial` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `horario_final` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `observacao` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario_entregas`
--

LOCK TABLES `horario_entregas` WRITE;
/*!40000 ALTER TABLE `horario_entregas` DISABLE KEYS */;
INSERT INTO `horario_entregas` VALUES (1,'10:00','10:30',NULL,'2017-10-21 01:04:47','2017-10-25 02:47:53'),(2,'09:00','10:00',NULL,'2017-10-25 02:48:29','2017-10-25 02:48:29'),(3,'10:30','11:00',NULL,'2017-10-25 02:48:49','2017-10-25 02:48:49'),(4,'11:30','12:00',NULL,'2017-10-25 02:49:00','2017-10-25 02:49:00'),(5,'12:00','12:30',NULL,'2017-10-25 02:49:10','2017-10-25 02:49:10');
/*!40000 ALTER TABLE `horario_entregas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icone_pratos`
--

DROP TABLE IF EXISTS `icone_pratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icone_pratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `css` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icone_pratos`
--

LOCK TABLES `icone_pratos` WRITE;
/*!40000 ALTER TABLE `icone_pratos` DISABLE KEYS */;
INSERT INTO `icone_pratos` VALUES (1,'Icone Carne','icone-carne.png','carne','2017-09-06 20:29:16','2017-09-06 20:29:16'),(2,'Icone Veggie','icone-veggie.png','veggie','2017-09-06 20:29:16','2017-09-06 20:29:16'),(3,'Icone Peixe','icone-peixe.png','peixe','2017-09-06 20:29:16','2017-09-06 20:29:16'),(4,'Icone Frango','icone-frango.png','peixe','2017-09-06 20:29:16','2017-09-06 20:29:16');
/*!40000 ALTER TABLE `icone_pratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_pedido_cardapios`
--

DROP TABLE IF EXISTS `item_pedido_cardapios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_pedido_cardapios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pedido_cardapio_id` int(10) unsigned NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantidade` int(11) NOT NULL,
  `valor` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_pedido_cardapios_pedido_cardapio_id_foreign` (`pedido_cardapio_id`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_pedido_cardapios`
--

LOCK TABLES `item_pedido_cardapios` WRITE;
/*!40000 ALTER TABLE `item_pedido_cardapios` DISABLE KEYS */;
INSERT INTO `item_pedido_cardapios` VALUES (1,1,'Fajitas de carne',1,27.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(2,1,'Peixe com ghee',1,27.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(3,1,'Bobó de palmito orgânico',1,27.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(4,1,'Suco de Melancia',1,10.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(5,1,'Mate Artesanal',1,10.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(6,1,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(7,1,'Chips de Beterraba',1,10.00,'2017-10-31 21:58:48','2017-10-31 21:58:48'),(8,2,'Fajitas de carne',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(9,2,'Peixe com ghee',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(10,2,'Bobó de palmito orgânico',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(11,3,'Kibe Bento',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(12,3,'Frango ao Pesto',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(13,3,'Cogumelos Gratinados',1,27.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(14,2,'Suco de Melancia',1,10.00,'2017-10-31 22:34:41','2017-10-31 22:34:41'),(15,4,'Kibe Bento',1,27.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(16,4,'Frango ao Pesto',1,27.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(17,4,'Cogumelos Gratinados',1,27.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(18,4,'Chips de Beterraba',1,10.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(19,4,'Suco de Melancia',1,10.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(20,4,'Mate Artesanal',1,10.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(21,4,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(22,4,'Detox: Couve, Hortelã e Maracujá',1,5.00,'2017-10-31 22:45:29','2017-10-31 22:45:29'),(23,5,'Fajitas de carne',1,27.00,'2017-10-31 22:46:47','2017-10-31 22:46:47'),(24,5,'Peixe com ghee',1,27.00,'2017-10-31 22:46:47','2017-10-31 22:46:47'),(25,6,'Kibe Bento',1,27.00,'2017-10-31 22:46:47','2017-10-31 22:46:47'),(26,6,'Frango ao Pesto',1,27.00,'2017-10-31 22:46:47','2017-10-31 22:46:47'),(27,7,'Fajitas de carne',1,27.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(28,7,'Bobó de palmito orgânico',1,27.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(29,7,'Chips de Beterraba',1,10.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(30,7,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(31,7,'Mate Artesanal',1,10.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(32,7,'Suco de Melancia',1,10.00,'2017-10-31 22:54:03','2017-10-31 22:54:03'),(33,8,'Fajitas de carne',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(34,8,'Peixe com ghee',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(35,8,'Bobó de palmito orgânico',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(36,9,'Kibe Bento',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(37,9,'Frango ao Pesto',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(38,9,'Cogumelos Gratinados',1,27.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(39,8,'Suco de Melancia',5,50.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(40,8,'Mate Artesanal',5,50.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(41,8,'Suco de Melancia',1,10.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(42,8,'Mate Artesanal',1,10.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(43,8,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(44,8,'Chips de Beterraba',1,10.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(45,8,'Suco de Melancia',2,20.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(46,8,'Mate Artesanal',2,20.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(47,8,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-10-31 22:59:20','2017-10-31 22:59:20'),(48,10,'Fajitas de carne',10,270.00,'2017-10-31 23:25:36','2017-10-31 23:25:36'),(49,11,'Fajitas de carne',1,27.00,'2017-10-31 23:26:10','2017-10-31 23:26:10'),(50,11,'Peixe com ghee',1,27.00,'2017-10-31 23:26:10','2017-10-31 23:26:10'),(51,12,'Frango Italiano',1,27.00,'2017-11-01 00:16:53','2017-11-01 00:16:53'),(52,13,'Feijão Tropeiro Bento',1,27.00,'2017-11-01 00:43:11','2017-11-01 00:43:11'),(53,13,'Mate Artesanal',1,10.00,'2017-11-01 00:43:11','2017-11-01 00:43:11'),(54,14,'Frango ao Curry',1,27.00,'2017-11-01 00:44:20','2017-11-01 00:44:20'),(55,15,'Frango Italiano',1,27.00,'2017-11-01 01:01:06','2017-11-01 01:01:06'),(56,16,'Tomate de tofupiry orgânico',1,27.00,'2017-11-01 01:01:06','2017-11-01 01:01:06'),(57,17,'Rosbife de Mignon',1,27.00,'2017-11-01 01:01:06','2017-11-01 01:01:06'),(58,15,'Mate Artesanal',1,10.00,'2017-11-01 01:01:06','2017-11-01 01:01:06'),(59,16,'Suco de Laranja com Acerola',1,5.00,'2017-11-01 01:01:06','2017-11-01 01:01:06'),(60,18,'Feijão Tropeiro Bento',1,27.00,'2017-11-01 01:14:41','2017-11-01 01:14:41'),(61,18,'Chips de Beterraba',1,10.00,'2017-11-01 01:14:41','2017-11-01 01:14:41'),(62,18,'Mate Artesanal',1,10.00,'2017-11-01 01:14:41','2017-11-01 01:14:41'),(63,19,'Frango ao Curry',1,27.00,'2017-11-01 07:41:37','2017-11-01 07:41:37'),(64,19,'Chips de Beterraba',0,0.00,'2017-11-01 07:41:37','2017-11-01 07:41:37'),(65,19,'Mate Artesanal',0,0.00,'2017-11-01 07:41:37','2017-11-01 07:41:37'),(66,20,'Picadinho Carioca',1,27.00,'2017-11-01 08:05:16','2017-11-01 08:05:16'),(67,21,'Picadinho Carioca',1,27.00,'2017-11-07 22:47:51','2017-11-07 22:47:51'),(68,21,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:47:51','2017-11-07 22:47:51'),(69,22,'Picadinho Carioca',1,27.00,'2017-11-07 22:47:51','2017-11-07 22:47:51'),(70,22,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:47:51','2017-11-07 22:47:51'),(71,22,'Penne Caprese Gluten Free',1,27.00,'2017-11-07 22:47:51','2017-11-07 22:47:51'),(72,23,'Picadinho Carioca',1,27.00,'2017-11-07 22:49:13','2017-11-07 22:49:13'),(73,23,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:49:13','2017-11-07 22:49:13'),(74,24,'Picadinho Carioca',1,27.00,'2017-11-07 22:49:13','2017-11-07 22:49:13'),(75,24,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:49:13','2017-11-07 22:49:13'),(76,25,'Picadinho Carioca',1,27.00,'2017-11-07 22:51:28','2017-11-07 22:51:28'),(77,25,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:51:28','2017-11-07 22:51:28'),(78,26,'Picadinho Carioca',1,27.00,'2017-11-07 22:53:15','2017-11-07 22:53:15'),(79,26,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:53:15','2017-11-07 22:53:15'),(80,27,'Picadinho Carioca',1,27.00,'2017-11-07 22:58:15','2017-11-07 22:58:15'),(81,27,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 22:58:15','2017-11-07 22:58:15'),(82,28,'Picadinho Carioca',1,27.00,'2017-11-07 23:12:16','2017-11-07 23:12:16'),(83,28,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 23:12:16','2017-11-07 23:12:16'),(84,29,'Picadinho Carioca',1,27.00,'2017-11-07 23:13:15','2017-11-07 23:13:15'),(85,29,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 23:13:15','2017-11-07 23:13:15'),(86,30,'Picadinho Carioca',1,27.00,'2017-11-07 23:46:52','2017-11-07 23:46:52'),(87,30,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-07 23:46:52','2017-11-07 23:46:52'),(88,31,'Picadinho Carioca',1,27.00,'2017-11-08 00:30:05','2017-11-08 00:30:05'),(89,32,'Picadinho Carioca',1,27.00,'2017-11-08 00:30:57','2017-11-08 00:30:57'),(90,33,'Peixe com ghee',1,27.00,'2017-11-09 09:25:16','2017-11-09 09:25:16'),(91,34,'Fajitas de carne',1,27.00,'2017-11-09 09:25:16','2017-11-09 09:25:16'),(92,35,'Picadinho Carioca',1,27.00,'2017-11-09 09:25:16','2017-11-09 09:25:16'),(93,36,'Picadinho Carioca',1,27.00,'2017-11-09 09:25:16','2017-11-09 09:25:16'),(94,37,'Picadinho Carioca',1,27.00,'2017-11-09 18:15:33','2017-11-09 18:15:33'),(95,37,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 18:15:33','2017-11-09 18:15:33'),(96,37,'Frango a L´Orange',1,27.00,'2017-11-09 18:15:33','2017-11-09 18:15:33'),(97,38,'Peixe com ghee',1,27.00,'2017-11-09 23:38:39','2017-11-09 23:38:39'),(98,38,'Fajitas de carne',1,27.00,'2017-11-09 23:38:39','2017-11-09 23:38:39'),(99,38,'Frango ao Pesto',1,27.00,'2017-11-09 23:38:39','2017-11-09 23:38:39'),(100,39,'Fajitas de carne',1,27.00,'2017-11-09 23:39:04','2017-11-09 23:39:04'),(101,39,'Peixe com ghee',1,27.00,'2017-11-09 23:39:04','2017-11-09 23:39:04'),(102,39,'Bobó de palmito orgânico',1,27.00,'2017-11-09 23:39:04','2017-11-09 23:39:04'),(103,40,'Picadinho Carioca',1,27.00,'2017-11-09 23:39:46','2017-11-09 23:39:46'),(104,40,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-09 23:39:46','2017-11-09 23:39:46'),(105,40,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 23:39:46','2017-11-09 23:39:46'),(106,41,'Picadinho Carioca',1,27.00,'2017-11-09 23:40:07','2017-11-09 23:40:07'),(107,41,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 23:40:07','2017-11-09 23:40:07'),(108,41,'Frango a L´Orange',1,27.00,'2017-11-09 23:40:07','2017-11-09 23:40:07'),(109,42,'Picadinho Carioca',1,27.00,'2017-11-09 23:40:31','2017-11-09 23:40:31'),(110,42,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-09 23:40:31','2017-11-09 23:40:31'),(111,42,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 23:40:31','2017-11-09 23:40:31'),(112,43,'Picadinho Carioca',1,27.00,'2017-11-09 23:41:01','2017-11-09 23:41:01'),(113,43,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 23:41:01','2017-11-09 23:41:01'),(114,43,'Frango a L´Orange',1,27.00,'2017-11-09 23:41:01','2017-11-09 23:41:01'),(115,44,'Peixe com ghee',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(116,44,'Fajitas de carne',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(117,44,'Frango ao Pesto',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(118,45,'Fajitas de carne',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(119,45,'Peixe com ghee',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(120,45,'Bobó de palmito orgânico',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(121,46,'Picadinho Carioca',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(122,46,'Lasagna Integral a Bolognesa',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(123,46,'Penne Caprese Gluten Free',1,27.00,'2017-11-09 23:42:02','2017-11-09 23:42:02'),(124,47,'Picadinho Carioca',1,27.00,'2017-11-10 03:14:51','2017-11-10 03:14:51'),(125,48,'Frango a L´Orange',1,27.00,'2017-11-10 03:14:51','2017-11-10 03:14:51'),(126,47,'Suco de Couve',1,5.00,'2017-11-10 03:14:51','2017-11-10 03:14:51'),(127,47,'Suco de Maracujá',1,5.00,'2017-11-10 03:14:51','2017-11-10 03:14:51'),(128,48,'Detox: Couve, Laranja, Hortelã e Gengibre',1,5.00,'2017-11-10 03:14:51','2017-11-10 03:14:51'),(129,49,'Picadinho Carioca',1,27.00,'2017-11-10 23:53:30','2017-11-10 23:53:30');
/*!40000 ALTER TABLE `item_pedido_cardapios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `local_entregas`
--

DROP TABLE IF EXISTS `local_entregas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `local_entregas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cep_inicial` int(11) NOT NULL,
  `cep_final` int(11) NOT NULL,
  `bairro` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_entrega` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `local_entregas`
--

LOCK TABLES `local_entregas` WRITE;
/*!40000 ALTER TABLE `local_entregas` DISABLE KEYS */;
INSERT INTO `local_entregas` VALUES (1,22611030,22795901,'Barra da Tijuca',5.00,'2017-09-07 00:23:56','2017-09-07 00:23:56'),(2,22785000,22790877,'Recreio dos Bandeirantes',7.50,'2017-09-07 05:28:18','2017-09-07 05:28:28'),(3,22460000,22461999,'Jardim Botânico',2.00,'2017-10-30 19:32:28','2017-10-30 19:32:44');
/*!40000 ALTER TABLE `local_entregas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2017_08_07_144715_create_roles_table',1),(4,'2017_08_07_144940_create_role_user_table',1),(5,'2017_08_07_203239_create_admins_table',1),(6,'2017_08_08_180253_create_empresas_table',1),(7,'2017_08_08_181316_create_endereco_empresas_table',1),(8,'2017_08_15_213900_table_user_empresa',1),(9,'2017_08_16_213801_create_categoria_acompanhamentos_table',1),(10,'2017_08_16_214004_create_acompanhamentos_table',1),(11,'2017_08_16_215250_create_caracteristica_pratos_table',1),(12,'2017_08_16_215521_create_pratos_table',1),(13,'2017_08_16_225907_create_prato_caracteristica',1),(14,'2017_08_18_120735_create_icone_pratos_table',1),(15,'2017_08_18_123834_add_icone_pratos',1),(16,'2017_08_21_113827_add_tabela_preco',1),(17,'2017_08_21_140920_add_tabela_preco_acompanhamento',1),(18,'2017_08_21_145451_add_tabela_cardapio',1),(19,'2017_08_21_155537_add_cardapio_prato',1),(20,'2017_08_24_165206_add_primeiro_acesso_usuario',1),(21,'2017_08_24_202554_add_endereco_user_empresa',1),(22,'2017_08_25_122142_add_campos_usuario',1),(23,'2017_08_25_142039_add_campos_empresa',1),(24,'2017_08_25_180255_create_local_entregas_table',1),(25,'2017_08_28_123538_add_view_preco_prato',1),(26,'2017_08_28_123603_add_view_preco_acompanhamento',1),(27,'2017_08_30_032205_create_pedidos_table',2),(28,'2017_08_30_032535_create_pedido_cardapios_table',2),(29,'2017_08_30_032602_create_item_pedido_cardapios_table',2),(30,'2017_09_11_173914_add_campos_pedido',3),(31,'2017_09_12_034032_create_configuracaos_table',3),(32,'2017_09_12_113246_add_campos_pedido_cardapio',3),(33,'2017_09_13_133234_create_newsletters_table',4),(34,'2017_09_15_022023_create_f_a_qs_table',5),(35,'2017_09_17_224325_create_cardapio_bebidas',6),(36,'2017_09_17_234308_add_fk_cardapio_bebida',6),(37,'2017_10_13_123408_add_usuario_endereco',7),(38,'2017_10_13_123623_rename_table',7),(39,'2017_10_13_141135_add_email_telefone_contato_endereco',8),(40,'2017_10_14_043013_create_horario_entregas_table',8),(41,'2017_10_16_102949_remove_status_pedido_cardapio',9),(42,'2017_10_16_103033_add_status_pedido_cardapio',9),(43,'2017_10_16_111355_add_codigo_empresa',9),(44,'2017_10_16_180530_create_cupons_table',9),(45,'2017_10_18_180013_add_pin_usuario',10),(46,'2017_10_18_180321_remove_pin_users_empresa',10),(47,'2017_10_19_012501_create_consulta_ceps_table',11),(48,'2017_10_19_040530_add_frete_pedido',11),(49,'2017_10_19_100045_add_horario_pedido',11),(50,'2017_10_28_034804_change_status_pedido',12),(51,'2017_10_28_034839_change_status_pedido_cardapios',12),(52,'2017_10_30_115936_change_status_pedido_correcao',13),(53,'2017_10_30_120250_change_status_pedido_cardapios_correcao',13),(54,'2017_11_06_200141_create_cupom_pedidos_table',14),(55,'2017_11_06_202402_add_tipo_cupom',14),(56,'2017_11_06_202511_add_fk_cupom_pedidos',14),(57,'2017_11_07_102706_create_desconto_progressivos_table',15);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
INSERT INTO `newsletters` VALUES (1,'Juliana Diniz','juliediniz@gmail.com','2017-09-15 01:42:32','2017-09-15 01:42:32'),(2,'Raíssa','raissa@dizain.com.br','2017-10-27 01:58:22','2017-10-27 01:58:22');
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedido_cardapios`
--

DROP TABLE IF EXISTS `pedido_cardapios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedido_cardapios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cardapio_id` int(10) unsigned NOT NULL,
  `pedido_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `canceled_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('programado','entregue','cancelado','pago','congelado') COLLATE utf8mb4_unicode_ci DEFAULT 'programado',
  `valor_frete` double(8,2) NOT NULL DEFAULT '0.00',
  `desconto` double(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `pedido_cardapios_cardapio_id_foreign` (`cardapio_id`),
  KEY `pedido_cardapios_pedido_id_foreign` (`pedido_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedido_cardapios`
--

LOCK TABLES `pedido_cardapios` WRITE;
/*!40000 ALTER TABLE `pedido_cardapios` DISABLE KEYS */;
INSERT INTO `pedido_cardapios` VALUES (1,4,1,'2017-10-31 21:58:48','2017-10-31 21:59:22',NULL,NULL,'entregue',5.00,0.00),(2,4,2,'2017-10-31 22:34:41','2017-10-31 22:35:24',NULL,NULL,'cancelado',5.00,0.00),(3,5,2,'2017-10-31 22:34:41','2017-10-31 22:35:28',NULL,NULL,'cancelado',5.00,0.00),(4,5,3,'2017-10-31 22:45:29','2017-10-31 22:46:20','2017-10-31 15:46:20','id','cancelado',5.00,0.00),(5,4,4,'2017-10-31 22:46:47','2017-10-31 22:49:24',NULL,NULL,'cancelado',5.00,0.00),(6,5,4,'2017-10-31 22:46:47','2017-10-31 22:49:28',NULL,NULL,'cancelado',5.00,0.00),(7,4,5,'2017-10-31 22:54:03','2017-10-31 23:02:21','2017-10-31 16:02:21','id','cancelado',5.00,0.00),(8,4,6,'2017-10-31 22:59:20','2017-10-31 23:05:35','2017-10-31 16:05:35','id','cancelado',5.00,0.00),(9,5,6,'2017-10-31 22:59:20','2017-10-31 23:05:56','2017-10-31 16:05:56','id','cancelado',5.00,0.00),(10,4,7,'2017-10-31 23:25:36','2017-10-31 23:26:56',NULL,NULL,'pago',5.00,0.00),(11,4,8,'2017-10-31 23:26:10','2017-11-01 00:31:13',NULL,NULL,'pago',5.00,0.00),(12,9,9,'2017-11-01 00:16:53','2017-11-01 00:30:35',NULL,NULL,'entregue',5.00,0.00),(13,9,10,'2017-11-01 00:43:11','2017-11-01 00:43:29',NULL,NULL,'congelado',5.00,0.00),(14,15,11,'2017-11-01 00:44:20','2017-11-01 00:44:38',NULL,NULL,'pago',5.00,0.00),(15,9,12,'2017-11-01 01:01:06','2017-11-01 01:01:06',NULL,NULL,'programado',5.00,0.00),(16,14,12,'2017-11-01 01:01:06','2017-11-01 01:01:06',NULL,NULL,'programado',5.00,0.00),(17,15,12,'2017-11-01 01:01:06','2017-11-01 01:01:53','2017-10-31 18:01:53','id','cancelado',5.00,0.00),(18,9,13,'2017-11-01 01:14:41','2017-11-01 01:14:41',NULL,NULL,'programado',5.00,0.00),(19,15,14,'2017-11-01 07:41:37','2017-11-01 07:41:37',NULL,NULL,'programado',2.00,0.00),(20,14,15,'2017-11-01 08:05:16','2017-11-01 08:10:16','2017-11-01 01:10:16','id','cancelado',2.00,0.00),(21,19,16,'2017-11-07 22:47:51','2017-11-07 22:47:51',NULL,NULL,'programado',5.00,0.00),(22,22,16,'2017-11-07 22:47:51','2017-11-07 22:47:51',NULL,NULL,'programado',5.00,0.00),(23,19,17,'2017-11-07 22:49:13','2017-11-07 22:49:13',NULL,NULL,'programado',5.00,0.00),(24,22,17,'2017-11-07 22:49:13','2017-11-07 22:49:13',NULL,NULL,'programado',5.00,0.00),(25,19,18,'2017-11-07 22:51:28','2017-11-07 22:51:28',NULL,NULL,'programado',5.00,0.00),(26,19,19,'2017-11-07 22:53:15','2017-11-07 22:53:15',NULL,NULL,'programado',5.00,0.00),(27,19,20,'2017-11-07 22:58:15','2017-11-07 22:58:15',NULL,NULL,'programado',5.00,0.00),(28,19,21,'2017-11-07 23:12:16','2017-11-07 23:12:16',NULL,NULL,'programado',5.00,0.00),(29,19,22,'2017-11-07 23:13:15','2017-11-07 23:13:15',NULL,NULL,'programado',5.00,0.00),(30,19,23,'2017-11-07 23:46:52','2017-11-07 23:46:52',NULL,NULL,'programado',5.00,0.00),(31,19,24,'2017-11-08 00:30:05','2017-11-08 00:30:05',NULL,NULL,'programado',5.00,0.00),(32,19,25,'2017-11-08 00:30:57','2017-11-08 00:30:57',NULL,NULL,'programado',5.00,0.00),(33,13,26,'2017-11-09 09:25:16','2017-11-09 09:25:16',NULL,NULL,'programado',5.00,0.00),(34,18,26,'2017-11-09 09:25:16','2017-11-09 09:25:16',NULL,NULL,'programado',5.00,2.70),(35,19,26,'2017-11-09 09:25:16','2017-11-09 09:25:16',NULL,NULL,'programado',5.00,4.05),(36,22,26,'2017-11-09 09:25:16','2017-11-09 09:25:16',NULL,NULL,'programado',5.00,5.40),(37,23,27,'2017-11-09 18:15:33','2017-11-09 18:15:33',NULL,NULL,'programado',5.00,0.00),(38,13,28,'2017-11-09 23:38:39','2017-11-09 23:38:39',NULL,NULL,'programado',5.00,0.00),(39,18,29,'2017-11-09 23:39:04','2017-11-09 23:39:04',NULL,NULL,'programado',5.00,0.00),(40,19,30,'2017-11-09 23:39:46','2017-11-09 23:39:46',NULL,NULL,'programado',5.00,0.00),(41,23,31,'2017-11-09 23:40:07','2017-11-09 23:40:07',NULL,NULL,'programado',5.00,0.00),(42,22,32,'2017-11-09 23:40:31','2017-11-09 23:40:31',NULL,NULL,'programado',5.00,0.00),(43,23,33,'2017-11-09 23:41:01','2017-11-09 23:41:01',NULL,NULL,'programado',5.00,0.00),(44,13,34,'2017-11-09 23:42:02','2017-11-09 23:42:02',NULL,NULL,'programado',5.00,0.00),(45,18,34,'2017-11-09 23:42:02','2017-11-09 23:42:02',NULL,NULL,'programado',5.00,8.10),(46,19,34,'2017-11-09 23:42:02','2017-11-09 23:42:02',NULL,NULL,'programado',5.00,12.15),(47,22,35,'2017-11-10 03:14:51','2017-11-10 03:15:49','2017-11-09 20:15:49','id','cancelado',5.00,0.00),(48,23,35,'2017-11-10 03:14:51','2017-11-10 03:14:51',NULL,NULL,'programado',5.00,3.20),(49,26,36,'2017-11-10 23:53:30','2017-11-10 23:53:30',NULL,NULL,'programado',5.00,0.00);
/*!40000 ALTER TABLE `pedido_cardapios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pedidos`
--

DROP TABLE IF EXISTS `pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pedidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` enum('programado','entregue','cancelado','pago','congelado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco_entrega_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `complemento_endereco` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valor_total` decimal(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `data_entrega` datetime DEFAULT NULL,
  `horario_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pedidos_endereco_entrega_id_foreign` (`endereco_entrega_id`),
  KEY `pedidos_user_id_foreign` (`user_id`),
  KEY `fk_horarioentrega_pedido` (`horario_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pedidos`
--

LOCK TABLES `pedidos` WRITE;
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` VALUES (1,'entregue',8,19,NULL,121.00,'2017-10-31 21:58:48','2017-10-31 21:59:22',NULL,5),(2,'cancelado',8,19,NULL,182.00,'2017-10-31 22:34:41','2017-10-31 22:35:28',NULL,5),(3,'cancelado',14,19,'Casa 2',126.00,'2017-10-31 22:45:29','2017-10-31 22:46:20',NULL,4),(4,'cancelado',8,19,NULL,118.00,'2017-10-31 22:46:47','2017-10-31 22:49:28',NULL,5),(5,'cancelado',8,19,NULL,94.00,'2017-10-31 22:54:03','2017-10-31 23:02:21',NULL,5),(6,'cancelado',8,19,NULL,352.00,'2017-10-31 22:59:20','2017-10-31 23:05:56',NULL,5),(7,'pago',8,19,NULL,275.00,'2017-10-31 23:25:36','2017-10-31 23:26:56',NULL,5),(8,'pago',8,19,NULL,59.00,'2017-10-31 23:26:10','2017-11-01 00:31:13',NULL,2),(9,'entregue',3,11,NULL,32.00,'2017-11-01 00:16:53','2017-11-01 00:30:35',NULL,2),(10,'congelado',3,11,'Teste de Complemento',42.00,'2017-11-01 00:43:10','2017-11-01 00:43:29',NULL,2),(11,'pago',9,11,NULL,32.00,'2017-11-01 00:44:20','2017-11-01 00:44:38',NULL,2),(12,'programado',15,29,NULL,111.00,'2017-11-01 01:01:06','2017-11-01 01:01:06',NULL,2),(13,'programado',15,29,'Perto do Guanabara',52.00,'2017-11-01 01:14:41','2017-11-01 01:14:41',NULL,4),(14,'programado',16,15,'CPP PA18, ram 2757, perto da Predio Pessego',29.00,'2017-11-01 07:41:37','2017-11-01 07:41:37',NULL,1),(15,'cancelado',16,15,NULL,29.00,'2017-11-01 08:05:16','2017-11-01 08:10:16',NULL,2),(16,'programado',8,19,NULL,145.00,'2017-11-07 22:47:51','2017-11-07 22:47:51',NULL,5),(17,'programado',8,19,NULL,118.00,'2017-11-07 22:49:13','2017-11-07 22:49:13',NULL,2),(18,'programado',8,19,NULL,59.00,'2017-11-07 22:51:28','2017-11-07 22:51:28',NULL,2),(19,'programado',8,19,NULL,59.00,'2017-11-07 22:53:15','2017-11-07 22:53:15',NULL,2),(20,'programado',8,19,NULL,59.00,'2017-11-07 22:58:15','2017-11-07 22:58:15',NULL,2),(21,'programado',8,19,NULL,59.00,'2017-11-07 23:12:16','2017-11-07 23:12:16',NULL,2),(22,'programado',8,19,NULL,59.00,'2017-11-07 23:13:15','2017-11-07 23:13:15',NULL,2),(23,'programado',8,19,NULL,59.00,'2017-11-07 23:46:52','2017-11-07 23:46:52',NULL,2),(24,'programado',8,19,NULL,32.00,'2017-11-08 00:30:05','2017-11-08 00:30:05',NULL,2),(25,'programado',8,19,NULL,32.00,'2017-11-08 00:30:57','2017-11-08 00:30:57',NULL,2),(26,'programado',8,19,NULL,115.85,'2017-11-09 09:25:16','2017-11-09 09:25:16',NULL,2),(27,'programado',8,19,NULL,86.00,'2017-11-09 18:15:33','2017-11-09 18:15:33',NULL,2),(28,'programado',8,19,NULL,86.00,'2017-11-09 23:38:39','2017-11-09 23:38:39',NULL,2),(29,'programado',8,19,NULL,86.00,'2017-11-09 23:39:04','2017-11-09 23:39:04',NULL,2),(30,'programado',8,19,NULL,86.00,'2017-11-09 23:39:46','2017-11-09 23:39:46',NULL,2),(31,'programado',8,19,NULL,86.00,'2017-11-09 23:40:07','2017-11-09 23:40:07',NULL,2),(32,'programado',8,19,NULL,86.00,'2017-11-09 23:40:31','2017-11-09 23:40:31',NULL,2),(33,'programado',8,19,NULL,86.00,'2017-11-09 23:41:01','2017-11-09 23:41:01',NULL,2),(34,'programado',8,19,NULL,237.75,'2017-11-09 23:42:02','2017-11-09 23:42:02',NULL,2),(35,'programado',4,29,NULL,90.80,'2017-11-10 03:14:51','2017-11-10 03:14:51',NULL,2),(36,'programado',4,11,NULL,32.00,'2017-11-10 23:53:30','2017-11-10 23:53:30',NULL,2);
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prato_caracteristica`
--

DROP TABLE IF EXISTS `prato_caracteristica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prato_caracteristica` (
  `prato_id` int(10) unsigned NOT NULL DEFAULT '0',
  `caracteristica_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prato_id`,`caracteristica_id`),
  KEY `prato_caracteristica_caracteristica_id_foreign` (`caracteristica_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prato_caracteristica`
--

LOCK TABLES `prato_caracteristica` WRITE;
/*!40000 ALTER TABLE `prato_caracteristica` DISABLE KEYS */;
INSERT INTO `prato_caracteristica` VALUES (1,4,NULL,NULL),(2,1,NULL,NULL),(3,1,NULL,NULL),(3,2,NULL,NULL),(3,3,NULL,NULL),(1,5,NULL,NULL),(4,2,NULL,NULL),(5,1,NULL,NULL),(5,2,NULL,NULL),(6,1,NULL,NULL),(6,2,NULL,NULL),(6,5,NULL,NULL),(7,1,NULL,NULL),(7,2,NULL,NULL),(8,1,NULL,NULL),(9,2,NULL,NULL),(9,5,NULL,NULL),(10,1,NULL,NULL),(11,1,NULL,NULL),(11,2,NULL,NULL),(12,3,NULL,NULL),(13,1,NULL,NULL),(13,2,NULL,NULL),(14,2,NULL,NULL),(15,1,NULL,NULL),(15,2,NULL,NULL),(15,5,NULL,NULL),(16,1,NULL,NULL),(17,1,NULL,NULL),(18,1,NULL,NULL),(18,3,NULL,NULL),(19,1,NULL,NULL),(19,2,NULL,NULL),(20,1,NULL,NULL),(20,2,NULL,NULL),(21,1,NULL,NULL),(21,2,NULL,NULL),(21,5,NULL,NULL),(22,1,NULL,NULL),(23,1,NULL,NULL),(23,2,NULL,NULL),(23,5,NULL,NULL),(24,1,NULL,NULL),(24,2,NULL,NULL),(25,1,NULL,NULL),(25,2,NULL,NULL),(26,2,NULL,NULL),(26,5,NULL,NULL),(27,2,NULL,NULL),(28,1,NULL,NULL),(28,2,NULL,NULL),(29,3,NULL,NULL),(30,2,NULL,NULL),(31,1,NULL,NULL),(32,2,NULL,NULL),(32,5,NULL,NULL),(33,2,NULL,NULL),(33,5,NULL,NULL),(34,1,NULL,NULL),(34,2,NULL,NULL),(35,2,NULL,NULL),(36,1,NULL,NULL),(36,3,NULL,NULL),(36,4,NULL,NULL),(37,1,NULL,NULL),(38,1,NULL,NULL),(38,2,NULL,NULL),(38,5,NULL,NULL),(39,2,NULL,NULL),(40,1,NULL,NULL),(40,2,NULL,NULL),(41,1,NULL,NULL),(41,2,NULL,NULL),(41,4,NULL,NULL),(41,5,NULL,NULL),(42,1,NULL,NULL),(43,2,NULL,NULL),(44,1,NULL,NULL),(44,2,NULL,NULL),(45,1,NULL,NULL),(45,2,NULL,NULL),(46,2,NULL,NULL),(46,5,NULL,NULL),(47,1,NULL,NULL),(47,2,NULL,NULL),(47,5,NULL,NULL),(48,2,NULL,NULL),(48,4,NULL,NULL),(49,2,NULL,NULL),(50,1,NULL,NULL),(50,2,NULL,NULL),(50,5,NULL,NULL),(51,1,NULL,NULL),(51,2,NULL,NULL),(51,4,NULL,NULL),(52,1,NULL,NULL),(52,2,NULL,NULL),(52,5,NULL,NULL),(53,1,NULL,NULL),(54,1,NULL,NULL),(54,2,NULL,NULL),(54,4,NULL,NULL),(54,5,NULL,NULL),(55,1,NULL,NULL),(55,2,NULL,NULL),(56,2,NULL,NULL),(57,3,NULL,NULL),(58,1,NULL,NULL),(58,2,NULL,NULL),(59,1,NULL,NULL),(59,2,NULL,NULL),(59,5,NULL,NULL),(60,1,NULL,NULL),(60,2,NULL,NULL),(61,1,NULL,NULL),(61,2,NULL,NULL),(61,4,NULL,NULL),(62,2,NULL,NULL),(62,4,NULL,NULL),(63,1,NULL,NULL),(63,3,NULL,NULL),(65,1,NULL,NULL),(65,2,NULL,NULL);
/*!40000 ALTER TABLE `prato_caracteristica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pratos`
--

DROP TABLE IF EXISTS `pratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valor_sugerido` decimal(8,2) NOT NULL,
  `status` enum('ativo','inativo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ativo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `icone_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pratos_icone_id_foreign` (`icone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pratos`
--

LOCK TABLES `pratos` WRITE;
/*!40000 ALTER TABLE `pratos` DISABLE KEYS */;
INSERT INTO `pratos` VALUES (1,'Fajitas de carne','com molho chimichurri, aipim e arroz de 7 cereais com cenoura',27.00,'ativo','2017-09-07 00:27:49','2017-09-15 23:53:01',NULL,1),(2,'Peixe com ghee','(manteiga clarificada) de ervas, purê de couve-flor e legumes no vapor (ervilhas, abobrinha e vagem) *arroz 7 cereais',27.00,'ativo','2017-09-07 00:31:32','2017-09-07 00:31:32',NULL,3),(3,'Bobó de palmito orgânico','(palmito orgânico e leite de coco), farofa de milho e arroz integral',27.00,'ativo','2017-09-07 00:32:12','2017-09-07 00:32:12',NULL,2),(4,'Kibe Bento','com molho de hortelã, cebolas caramelizadas, tomate cereja assado e arroz de lentilha',27.00,'ativo','2017-09-15 23:23:49','2017-09-18 20:40:47',NULL,1),(5,'Frango ao Pesto','grelhado com legumes e alho confit, purê de couve-flor e arroz integral',27.00,'ativo','2017-09-15 23:24:33','2017-09-20 06:42:02',NULL,4),(6,'Cogumelos Gratinados','recheado com creme de tofu, purê de raízes e legumes salteados',27.00,'ativo','2017-09-15 23:25:13','2017-09-18 20:41:38',NULL,2),(7,'Hamburgão de Carne','com batatas rústicas, catchup caseiro e legumes',27.00,'ativo','2017-09-15 23:26:04','2017-09-18 20:41:51',NULL,1),(8,'Peixe Siciliano','com legumes no azeite de sálvia, creme de espinafre e arroz integral',27.00,'ativo','2017-09-15 23:26:44','2017-09-18 20:42:05',NULL,3),(9,'Risotto de Palmito','de pupunha orgânico com mix de legumes',27.00,'ativo','2017-09-15 23:27:16','2017-09-18 20:42:19',NULL,2),(10,'Polpetone Bento','com molho de tomates frescos e massa gluten free',27.00,'ativo','2017-09-15 23:27:49','2017-09-18 20:42:33',NULL,1),(11,'Frango Teriayki','com legumes orientais e arroz integral',27.00,'ativo','2017-09-15 23:28:17','2017-09-20 06:42:14',NULL,4),(12,'Gnochi de Baroa','mozzarela fresca, tomate assado e manjericão',27.00,'ativo','2017-09-15 23:29:28','2017-09-26 21:39:29',NULL,2),(13,'Filé Acebolado','com batata jovem e arroz integral de espinafre',27.00,'ativo','2017-09-15 23:30:03','2017-09-18 20:43:45',NULL,1),(14,'Dourado ao Curry','com farofa de coco e castanha, banana pimenta dedo de moça e arroz 7 cereais',27.00,'ativo','2017-09-15 23:30:39','2017-09-18 20:43:35',NULL,3),(15,'Tofu Mineiro','Tofu grelhado, couve refogada com crocantes de tofu defumado, tutu de feijão e arroz integral',27.00,'ativo','2017-09-15 23:32:54','2017-09-18 20:44:01',NULL,2),(16,'Strogonoff de Carne Bento','com chips de forno caseira e arroz integral',27.00,'ativo','2017-09-15 23:33:25','2017-09-18 20:44:17',NULL,1),(17,'Frango Cordon Bleu Bento','recheado com alho poró e ricota, creme de milho e arroz integral de brócolis',27.00,'ativo','2017-09-15 23:33:53','2017-09-20 06:42:25',NULL,4),(18,'Lasanha Veggie','de legumes (abobrinha, berinjela e palmito orgânico) com arroz integral',27.00,'ativo','2017-09-15 23:34:43','2017-09-18 20:45:08',NULL,2),(19,'Ghochi de batata doce orgânica','com ovo de codorna, farofa de banana e arroz integral com legumes',27.00,'ativo','2017-09-15 23:46:06','2017-09-26 21:40:12',NULL,2),(20,'Salmão Assado','com fitas de abobrinha, cenoura e palmito orgânico com molho de ervas e arroz integral de tomate',27.00,'ativo','2017-09-15 23:46:47','2017-09-15 23:46:47',NULL,3),(21,'Steak de cogumelo orgânico','com azeite de sálvia, legumes no vapor e arroz vermelho',27.00,'ativo','2017-09-15 23:47:43','2017-09-15 23:47:43',NULL,2),(22,'Strogonoff de frango Bento','com chips de forno e arroz integral',27.00,'ativo','2017-09-15 23:52:08','2017-09-20 06:42:41',NULL,4),(23,'Tomate de tofupiry orgânico','com tomate recheado com creme de tofu orgânico, quinoa e mix de legumes',27.00,'ativo','2017-09-15 23:52:45','2017-09-15 23:52:45',NULL,2),(24,'Churrasco Oriental','de carne e cogumelos, com molho teriyaki, legumes e arroz basmati com gergelim',27.00,'ativo','2017-09-26 21:30:12','2017-09-26 21:30:12',NULL,1),(25,'Frango ao Curry','com banana grelhada e arroz basmati',27.00,'ativo','2017-09-26 21:31:11','2017-09-26 21:31:11',NULL,4),(26,'Cogumelos à Provençal','em papilote com legumes salteados e arroz de 7 cereais',27.00,'ativo','2017-09-26 21:31:53','2017-09-26 21:31:53',NULL,2),(27,'Kafta Benta','assada com molho de hortelã, homus, cebola caramelizada e arroz de lentilha',27.00,'ativo','2017-09-26 21:32:39','2017-09-26 21:32:39',NULL,1),(28,'Peixe Cítrico','Com molho de limão e coentro, legumes salteados, tomate cereja assado e arroz integral de ervas',27.00,'ativo','2017-09-26 21:33:17','2017-09-26 21:33:17',NULL,3),(29,'Panqueca de Batata Doce','com recheio de ricota e alho poró e arroz 7 cereais com legumes',27.00,'ativo','2017-09-26 21:33:56','2017-09-26 21:33:56',NULL,2),(30,'Rosbife de Mignon','com crosta de ervas, mix de batatas e tomate cereja assado',27.00,'ativo','2017-09-26 21:34:31','2017-09-26 21:34:31',NULL,1),(31,'Frango Italiano','recheado com tomate seco e ricota, molho italiano, caponata e arroz integral',27.00,'ativo','2017-09-26 21:35:05','2017-09-26 21:35:05',NULL,4),(33,'Gnhochi de Abóbora','com molho de cogumelos e azeite de tomilho',27.00,'ativo','2017-09-26 21:41:00','2017-09-26 21:41:00',NULL,2),(34,'Almôndegas Italianas','com molho marinara e massa gluten free',27.00,'ativo','2017-09-26 21:41:38','2017-09-26 21:41:38',NULL,1),(35,'Salmão Asiático','com chutney de manga e coentro, e couscous marroquinho',27.00,'ativo','2017-09-26 21:42:12','2017-09-26 21:42:12',NULL,3),(36,'Omelete Caipira Popeye','de espinafre, recheado com ricota e alho porró, grão de bico com legumes e molho de rúcula',27.00,'ativo','2017-09-26 21:43:01','2017-09-26 21:43:01',NULL,2),(37,'Parmegiana Bento','jardineira de legumes e arroz integral',27.00,'ativo','2017-09-26 21:44:00','2017-09-26 21:44:00',NULL,1),(38,'Feijão Tropeiro Bento','feijão vermelho, banana grelhada, tofu, couve e arroz integral',27.00,'ativo','2017-09-26 21:45:16','2017-09-26 21:45:16',NULL,2),(39,'Almôndegas Orientais','com couscous marroquino e legumes',28.00,'ativo','2017-10-17 21:10:57','2017-10-17 21:10:57',NULL,1),(40,'Frango Chimichurri','com legumes no vapor e arroz integral de brócolis',28.00,'ativo','2017-10-17 21:12:03','2017-10-17 21:12:03',NULL,4),(41,'Caçarola de Shitake Orgânico','salteado no azeite de ervas, ratatoiulle e soja em grãos com legumes (ervilha e cenoura)',28.00,'ativo','2017-10-17 21:13:25','2017-10-17 21:13:25',NULL,2),(42,'Cheeseburger Artesanal','com batatas rústicas e picles caseiro',28.00,'ativo','2017-10-17 21:14:39','2017-10-17 21:14:39',NULL,1),(43,'Frango na Mostarda e Mel','com mix de batatas e arroz 7 cereais de legumes',28.00,'ativo','2017-10-17 21:17:34','2017-10-17 21:17:34',NULL,4),(44,'Mignon Alemão','ao molho de mostarda escura, chucrute bento e batatas assadas',28.00,'ativo','2017-10-17 21:19:01','2017-10-17 21:19:01',NULL,1),(45,'Moqueca de Peixe','com farofa de milho e arroz integral',28.00,'ativo','2017-10-17 21:19:46','2017-10-17 21:19:46',NULL,3),(46,'Churrasco de Tofu Orgânico','com vinagrete de tomilho, legumes no charbroiler e arroz 7 cereais',28.00,'ativo','2017-10-17 21:20:47','2017-10-17 21:20:47',NULL,2),(47,'Bobó de Cogumelos Orgânicos','com farofa de milho e arroz integral de brócolis',28.00,'ativo','2017-10-17 21:22:02','2017-10-17 21:22:02',NULL,2),(48,'Filé ao Molho de Madeira Bento','com purê de cenoura, cebola caramelizada e arroz 7 cereais',28.00,'ativo','2017-10-20 23:32:40','2017-10-20 23:32:40',NULL,1),(49,'Frango na Mostarda','em grãos, rostie de batata doce e arroz 7 cereais de tomate',28.00,'ativo','2017-10-20 23:34:40','2017-10-20 23:34:40',NULL,4),(50,'Medalhão de Palmito','orgânico (mini, 5 unidades) com azeite temperado, cenoura glaceada e arroz integral de espinafre',28.00,'ativo','2017-10-20 23:35:27','2017-10-20 23:35:27',NULL,2),(51,'Peixe ao Vinho','com legumes à provençal, tomate confit e arroz integral',28.00,'ativo','2017-10-20 23:37:01','2017-10-20 23:37:01',NULL,3),(52,'Baião de Dois Veggie','com tofu orgânico',28.00,'ativo','2017-10-20 23:37:54','2017-10-20 23:37:54',NULL,2),(53,'Frango Fit','recheado com ricota, rúcula e tomate seco, azeite de temperado, legumes e arroz integral',28.00,'ativo','2017-10-20 23:38:41','2017-10-20 23:38:41',NULL,4),(54,'Spaguetti ao Molho de Cogumelos Orgânicos','com palmito orgânico, cenoura e abobrinha',28.00,'ativo','2017-10-20 23:39:44','2017-10-20 23:39:44',NULL,2),(55,'Carne Louca','com batata doce braseada e arroz integral de espinafre',28.00,'ativo','2017-10-20 23:40:23','2017-10-20 23:40:23',NULL,1),(56,'Peixe Mediterrâneo','com crosta de macadâmia, ghee de limão siciliano, legumes e arroz 7 cereais',28.00,'ativo','2017-10-20 23:41:06','2017-10-20 23:41:06',NULL,3),(57,'Nhoque de Batata Doce Roxa','orgânica com ghee de sálvia e mozzarela de búfala',28.00,'ativo','2017-10-20 23:41:58','2017-10-20 23:41:58',NULL,2),(58,'Frango e Ervas','com legumes e arroz integral de tomate',28.00,'ativo','2017-10-20 23:42:58','2017-10-20 23:42:58',NULL,4),(59,'Churrasco de Cogumelos Orgânicos','ao molho shoyu, legumes e arroz integral',28.00,'ativo','2017-10-20 23:43:53','2017-10-20 23:43:53',NULL,2),(60,'Carne Picadinha','com purê de abóbora, couve-flor braseada e arroz integral',27.00,'ativo','2017-10-27 23:58:32','2017-10-27 23:58:32',NULL,1),(61,'Frango a L´Orange','com molho de laranja, legumes e arroz integral de ervas',27.00,'ativo','2017-10-27 23:59:40','2017-10-27 23:59:40',NULL,4),(62,'Pescado do Chef','com farofa de coco e castanha, banana pimenta dedo de moça e arroz 7 cereais',27.00,'ativo','2017-10-28 00:09:14','2017-10-28 00:09:14',NULL,3),(63,'Penne Caprese Gluten Free','com massa Gluten Free, mussarela de búfala, tomate cereja e pesto de manjericão',27.00,'ativo','2017-10-28 00:10:24','2017-10-28 00:10:24',NULL,2),(64,'Lasagna Integral a Bolognesa','com legumes',27.00,'ativo','2017-10-28 00:11:22','2017-10-28 00:11:22',NULL,1),(65,'Picadinho Carioca','com ovo de codorna, farofa de alho e arroz integral com legumes',27.00,'ativo','2017-10-28 01:14:57','2017-10-28 01:14:57',NULL,1);
/*!40000 ALTER TABLE `pratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `preco_acompanhamento`
--

DROP TABLE IF EXISTS `preco_acompanhamento`;
/*!50001 DROP VIEW IF EXISTS `preco_acompanhamento`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `preco_acompanhamento` AS SELECT 
 1 AS `id`,
 1 AS `nome`,
 1 AS `categoria_id`,
 1 AS `nome_categoria`,
 1 AS `valor`,
 1 AS `empresa_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `preco_prato`
--

DROP TABLE IF EXISTS `preco_prato`;
/*!50001 DROP VIEW IF EXISTS `preco_prato`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `preco_prato` AS SELECT 
 1 AS `id`,
 1 AS `data_cardapio`,
 1 AS `nome`,
 1 AS `descricao`,
 1 AS `icone`,
 1 AS `css`,
 1 AS `valor`,
 1 AS `prato_id`,
 1 AS `empresa_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,1,'2017-09-06 20:29:16','2017-09-06 20:29:16');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Cliente','Perfil de Cliente','2017-09-06 20:29:16','2017-09-06 20:29:16'),(2,'Administrador','Perfil de Administrador','2017-09-06 20:29:16','2017-09-06 20:29:16');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabela_preco_acompanhamento_empresa`
--

DROP TABLE IF EXISTS `tabela_preco_acompanhamento_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabela_preco_acompanhamento_empresa` (
  `acompanhamento_id` int(10) unsigned NOT NULL DEFAULT '0',
  `empresa_id` int(10) unsigned NOT NULL DEFAULT '0',
  `valor` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`acompanhamento_id`,`empresa_id`),
  KEY `tabela_preco_acompanhamento_empresa_empresa_id_foreign` (`empresa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabela_preco_acompanhamento_empresa`
--

LOCK TABLES `tabela_preco_acompanhamento_empresa` WRITE;
/*!40000 ALTER TABLE `tabela_preco_acompanhamento_empresa` DISABLE KEYS */;
INSERT INTO `tabela_preco_acompanhamento_empresa` VALUES (1,2,5.00,'2017-09-15 22:18:55','2017-09-15 22:18:55'),(1,1,25.00,'2017-09-15 22:04:27','2017-09-15 22:04:27'),(5,2,7.00,'2017-09-15 22:21:42','2017-09-15 22:21:42'),(1,3,5.00,'2017-09-18 19:31:47','2017-09-18 19:31:47'),(1,5,6.00,'2017-09-20 06:41:19','2017-09-20 06:41:19');
/*!40000 ALTER TABLE `tabela_preco_acompanhamento_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabela_preco_prato_empresa`
--

DROP TABLE IF EXISTS `tabela_preco_prato_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabela_preco_prato_empresa` (
  `prato_id` int(10) unsigned NOT NULL DEFAULT '0',
  `empresa_id` int(10) unsigned NOT NULL DEFAULT '0',
  `valor` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prato_id`,`empresa_id`),
  KEY `tabela_preco_prato_empresa_empresa_id_foreign` (`empresa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabela_preco_prato_empresa`
--

LOCK TABLES `tabela_preco_prato_empresa` WRITE;
/*!40000 ALTER TABLE `tabela_preco_prato_empresa` DISABLE KEYS */;
INSERT INTO `tabela_preco_prato_empresa` VALUES (1,2,20.00,'2017-09-15 22:22:30','2017-09-15 22:22:30'),(5,3,25.00,'2017-09-18 19:29:01','2017-09-18 19:29:01'),(1,5,28.00,'2017-09-20 06:40:40','2017-09-20 06:40:40');
/*!40000 ALTER TABLE `tabela_preco_prato_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_empresa`
--

DROP TABLE IF EXISTS `user_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_empresa` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `empresa_id` int(10) unsigned NOT NULL DEFAULT '0',
  `dt_inicio` datetime NOT NULL,
  `dt_termino` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `endereco_id` int(11) DEFAULT NULL,
  `codigo_empresa` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`,`empresa_id`),
  KEY `user_empresa_empresa_id_foreign` (`empresa_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_empresa`
--

LOCK TABLES `user_empresa` WRITE;
/*!40000 ALTER TABLE `user_empresa` DISABLE KEYS */;
INSERT INTO `user_empresa` VALUES (4,2,'2017-09-12 21:01:03',NULL,NULL,NULL,1,NULL),(6,2,'2017-09-14 21:02:26',NULL,NULL,NULL,1,NULL),(9,2,'2017-09-15 14:19:52',NULL,NULL,NULL,1,NULL),(10,3,'2017-09-15 20:08:49',NULL,NULL,NULL,2,NULL),(11,3,'2017-09-18 12:07:45','2017-10-31 17:49:13',NULL,NULL,3,'222222'),(12,4,'2017-09-18 12:23:53',NULL,NULL,NULL,NULL,NULL),(13,2,'2017-09-18 14:29:24','2017-10-31 17:56:21',NULL,NULL,1,NULL),(14,3,'2017-09-19 18:04:51','2017-10-31 17:49:13',NULL,NULL,3,NULL),(15,3,'2017-09-21 15:11:19','2017-10-31 17:49:13',NULL,NULL,2,NULL),(16,2,'2017-09-21 17:57:10','2017-10-31 17:56:21',NULL,NULL,1,NULL),(17,2,'2017-10-04 17:34:23','2017-10-31 17:56:21',NULL,NULL,1,NULL),(18,6,'2017-10-17 19:00:24',NULL,NULL,NULL,NULL,NULL),(23,2,'2017-10-24 20:49:01','2017-10-31 17:56:21',NULL,NULL,1,NULL),(29,2,'2017-10-31 17:56:05','2017-10-31 17:56:21',NULL,NULL,1,NULL),(30,8,'2017-11-03 00:53:58',NULL,NULL,NULL,20,'xxxxx');
/*!40000 ALTER TABLE `user_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `data_primeiro_acesso` datetime DEFAULT NULL,
  `telefone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receber_noticias` tinyint(1) NOT NULL DEFAULT '0',
  `pin` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Cliente','cliente@asbentas.com',NULL,NULL,'$2y$10$WNcPDXbCDkgDHTCGXM1bZ.vEiVqAnr2Tx.cpcyzkvNVUZzcu0hsKy',0,NULL,'0',NULL,'2017-09-06 20:29:16','2017-09-06 20:29:16',NULL,NULL,0,NULL),(2,'Sergio Schmid','sergio@dizain.com.br',NULL,NULL,'$2y$10$E/dY.hcNkiDQ3BQO8K6QauyOHJ9rMUjhjfE9ongpfV06e/Wa01JjK',0,'vGVgtj0gf72nlyr5U9XepfgeIQrs3m','0',NULL,'2017-09-06 22:26:04','2017-09-06 22:26:04',NULL,NULL,0,NULL),(12,'Felipe Diniz','juliediniz@gmail.com',NULL,NULL,'$2y$10$vA8TqQUxmVQgbJUKLN/SfObId0IuQxon8NL0LlGnF7R6H1DGq9Vuq',1,NULL,'0','WCiDkXSgtBKECIE0FEt5rhUKyllryfixkzqSWL2s1h5kdhkK4RaSGn1MXZ4H','2017-09-18 19:15:58','2017-09-18 19:20:38',NULL,NULL,0,NULL),(5,'Claudia Vasconcellos','cerofolio@gmail.com',NULL,NULL,'$2y$10$vCAShJyygYM8oYVCgpG4w.NIVOxX0.nwFqQmNoh7osQ6itY5/byBi',1,NULL,'0',NULL,'2017-09-13 08:16:35','2017-09-13 08:17:17',NULL,NULL,0,NULL),(11,'Antonio Vanni','juliana@dizain.com.br',NULL,NULL,'$2y$10$ZE9wJS4n4aaJ/0MxrFFDOuYH5zv3UAKDtDwh17E2NljLplcZQHIQq',1,NULL,'0','T9ASUTsULQIQvCkqvbXSDmJa5xl7e8CVo0LUPiCSWssqmCzC7TvRWp48nFEu','2017-09-18 19:03:26','2017-11-10 04:06:17','2017-10-17 14:39:53','',1,NULL),(13,'Wagner Guatimozim','wguati@gmail.com',NULL,NULL,'$2y$10$ar0wevTGKg4aWXt.IWikr.ieaW2UmDdiiUHwFTYLtDE2ANhgDcJuG',1,NULL,'0',NULL,'2017-09-18 21:20:08','2017-09-18 21:20:53',NULL,NULL,0,NULL),(19,'Marcio Moreira','marcio@marciomoreira.net',NULL,NULL,'$2y$10$ZGsxpDkvnkRAxYoPsUYXEer9AJtHzTt1hdmeIr.nHIxi1bHCnhekK',1,NULL,'0','DmUwGlvKBk0MmVTznIev1zwz79YeF9JFGuoa02IpiPAwaOyqWHAWf9Uaf6lO','2017-10-19 01:35:29','2017-11-09 23:05:47','2017-10-18 18:39:24','24 99999-9999',0,NULL),(14,'Angelo Mazzocchi','amazzocc@pobox.com','facebook','1515771005136150','$2y$10$XnrG2FFaUL0DdQT8dagxd.edPvxH4wndL1vAZrMwYLw8Zkj3n6laa',0,NULL,'0','yIe6zz0jAleffyItSOBVQHboqn1pgxs4ZsKqLG1DLmYCzOEz40pEf00nULQv','2017-09-19 18:52:08','2017-11-10 18:37:18','2017-10-30 13:13:12','',0,NULL),(15,'Angelo Mazzocchi','amazzocc2@gmail.com',NULL,NULL,'$2y$10$ry.yPZxNWAmR0QuybXtuj.5HsN/pJKbnHympyIONX2yBfRHtwfVly',1,NULL,'0','zkccCMxuwDz98lbEtO674XVp6qeWyZq1Pk8EL6O0Pxj2afTjmrSpvHBkSBh0','2017-09-21 22:04:51','2017-11-01 05:48:04','2017-10-31 21:16:25','21 98112-6580',0,NULL),(16,'Luah Leon','luah@dizain.com.br',NULL,NULL,'$2y$10$NebYKFZ8Wr3T0qxy9OWq/.3IJRadPlYFf5J66VaQb2.XotijP/C7O',1,NULL,'0',NULL,'2017-09-22 00:46:46','2017-09-22 00:47:15',NULL,NULL,0,NULL),(17,'Flávio','flavio@dizain.com.br',NULL,NULL,'$2y$10$nNVScshTvqXFMfoX7zayv.TLI4Xp5tGNhhqDrXmxKGHpUh4SBPMmS',1,NULL,'0','foI14VdGjMRnpIuTWNkZM0Ywm9dmJ0Wb982dixDRyAOM5qAxUWUz1XprozBv','2017-10-05 00:32:30','2017-10-05 00:32:43',NULL,NULL,0,NULL),(18,'Wagner DIZAIN','wagner@dizain.com.br',NULL,NULL,'$2y$10$DK.sSSXlcQs84nMs9VMf8OoOFH1mYYfBGpyv7K9q/Feer0oyNKabS',1,NULL,'0','zG6zqE8OoOIYWYMoUrEUgqy3P8kLEIEnoTK1Nj6Mkmc9iZ7gWzAz9xaXees8','2017-10-18 01:57:00','2017-10-18 02:00:24','2017-10-17 19:00:24',NULL,0,NULL),(20,'Flávio Spinelli','kenyspinelli@hotmail.com','facebook','1944092625842170',NULL,0,NULL,'0','6P53nHkprdD5zlctnv68CVz7od7SHL2zrqUMcRyiAj9zHJvkj7QXBSeePNtP','2017-10-20 18:50:45','2017-10-20 18:50:45',NULL,NULL,0,NULL),(21,'Iury Novaes','marcio@moreira.net',NULL,NULL,'$2y$10$hQQv.uLBF4TbjTTVV0hOo.lKZLDPHbXCrtWUdICEZzBROKz8UVe0u',0,'E7mdFDRi1bQ6R2x3mp6TsgoeMYALVX','0',NULL,'2017-10-24 00:04:36','2017-10-24 00:04:36',NULL,NULL,0,NULL),(23,'Joel Lima','jlima@dizain.com.br',NULL,NULL,'$2y$10$TrjRHxAkL6WhhAaTjAlb.ud3lpn62XmaMGzhyawqYS90NRKzLiLA6',1,NULL,'0','aA0K5pJlAPpIjP9fKnrDhmQa3QwM3tyRh29ClI1c1IgDZZAfSvtQmJodrKw4','2017-10-25 03:39:08','2017-10-25 03:49:01','2017-10-24 20:49:01',NULL,0,NULL),(22,'Felipe Diniz','fdiniz@gmail.com',NULL,NULL,'$2y$10$PEWXQ6EUjTUKd7/gYXed8uz7HPq77mHgxuzOvSwcXBwZRbpPQE7Qy',1,NULL,'0','ALBZhBG7MzYrXFrzWXMXnD925XmBQYZKfrqNdICyu9kN3E9XOTZ5HT4UUVF4','2017-10-25 02:23:45','2017-10-25 03:25:03','2017-10-24 20:25:03',NULL,0,NULL),(24,'Aline Namoratto','aline@dizain.com.br',NULL,NULL,'$2y$10$lM5Y.SV.wjPO/Ja8jmCpsupGscA3MjzOFYhiKvevItyuwhOjbKori',1,NULL,'0','f9p8isnQSIhiHrLkFckcW8IrYqECNgrzp4UUO4zqhVNC23Po8A7Eo0OoVceA','2017-10-27 00:29:40','2017-10-27 00:30:16',NULL,NULL,0,NULL),(25,'Aline Namoratto','aline.namoratto@hotmail.com','facebook','1725862974098731',NULL,0,NULL,'0','snZEaAWcM52TNMlfdYtcD8fXWYvcaA3MdM6P9dS3hXRWdM22O2BQpjtKeE3v','2017-10-27 00:30:38','2017-10-27 00:30:38',NULL,NULL,0,NULL),(29,'Raissa','raissa@dizain.com.br',NULL,NULL,'$2y$10$GVOLC6tZmRtCV.K5DznJgObVMJt0iqoo9zcSHWRq6/Zk78g/OntUy',1,NULL,'0','9mTy5lFqUj0scZBlSygVboHcKS6p7PmSm0qD9a2hUbAGLLmMYhHB0L5rtslu','2017-11-01 00:54:45','2017-11-01 00:56:05','2017-10-31 17:56:05',NULL,0,NULL),(27,'Felipe','fcarballo@envyron.com',NULL,NULL,'$2y$10$OHMABPfyduofVQRbgi7ikeA0o2gZMqbswKsCtdB.pcIowpVyWDcg6',1,NULL,'0',NULL,'2017-10-31 01:11:47','2017-10-31 01:12:04',NULL,NULL,0,NULL),(28,'Marcio Moreira','marcio@moreira.com.br',NULL,NULL,'$2y$10$r0HQGSHcoSQrOj4E8FI0hu2PBR/DbivhdGmQPVhcjdBTfLF5gkOFu',0,'AKsNv1cLzJsKWdOXlBjUtkiPBlHuH0','0',NULL,'2017-10-31 23:24:13','2017-10-31 23:24:13',NULL,NULL,0,NULL),(30,'Angelo Mazzocchi','angelo.mazzocchi@pobox.com',NULL,NULL,'$2y$10$rFX32BeTg4/wgb7PjebG6ObrnGq6L1GJflq7BB5vA/3eF/JyfmJ4W',1,NULL,'0','Td0aynJjFJQUXyo91iEdI5a61mIsgF8miz6jNdFR9WpK8vIBUeG9uVAeQIb4','2017-11-01 23:41:28','2017-11-03 08:08:50','2017-11-03 00:53:58','',0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `preco_acompanhamento`
--

/*!50001 DROP VIEW IF EXISTS `preco_acompanhamento`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`asbentasnovo`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `preco_acompanhamento` AS select `a`.`id` AS `id`,`a`.`nome` AS `nome`,`a`.`categoria_id` AS `categoria_id`,`c`.`nome` AS `nome_categoria`,ifnull(`pae`.`valor`,`a`.`valor_sugerido`) AS `valor`,`pae`.`empresa_id` AS `empresa_id` from ((`acompanhamentos` `a` join `categoria_acompanhamentos` `c` on((`c`.`id` = `a`.`categoria_id`))) left join `tabela_preco_acompanhamento_empresa` `pae` on((`a`.`id` = `pae`.`acompanhamento_id`))) where (`a`.`status` = 'ativo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `preco_prato`
--

/*!50001 DROP VIEW IF EXISTS `preco_prato`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`asbentasnovo`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `preco_prato` AS select `c`.`id` AS `id`,`c`.`data_cardapio` AS `data_cardapio`,`p`.`nome` AS `nome`,`p`.`descricao` AS `descricao`,`ip`.`icone` AS `icone`,`ip`.`css` AS `css`,ifnull(`ppe`.`valor`,`p`.`valor_sugerido`) AS `valor`,`p`.`id` AS `prato_id`,`ppe`.`empresa_id` AS `empresa_id` from ((((`cardapios` `c` join `cardapio_prato` `cp` on((`c`.`id` = `cp`.`cardapio_id`))) join `pratos` `p` on(((`p`.`id` = `cp`.`prato_id`) and (`p`.`status` = 'ativo')))) join `icone_pratos` `ip` on((`ip`.`id` = `p`.`icone_id`))) left join `tabela_preco_prato_empresa` `ppe` on((`ppe`.`prato_id` = `cp`.`prato_id`))) where (`c`.`status` = 'ativo') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-10 15:22:02
